﻿Imports MySql.Data.MySqlClient
Public Class clsConexion

#Region "Atributos"
    Private strLogin As String
    Private strPassword As String
    Private strPerfil As String
    Private strEstado As String
    Private strNomServidor As String
    Private strNomBD As String

    'permite establecer conexion con el alcance de datos, contiene la cadena
    Private MySqlConexion As New MySqlConnection
    'Permite la ejecucion de las consultas a la BD (I,M,E,C)
    Private MySqlComando As New MySqlCommand

#End Region

#Region "Constructor"
    Public Sub New()
        pUsuario = ""
        pPassword = ""
        pPerfil = ""
        pEstado = ""
        pNomServidor = ""
        pNomBD = ""

    End Sub

    Public Sub New(ByVal strLogin As String, ByVal strClave As String, ByVal strPerfil As String, ByVal strEstado As String, ByVal strNomServidor As String, ByVal strNomBD As String)

        pUsuario = strLogin
        pPassword = strClave
        pPerfil = strPerfil
        pEstado = strEstado
        pNomServidor = strNomServidor
        pNomBD = strNomBD
    End Sub

#End Region

#Region "Propiedades"
    Public Property pUsuario() As String
        Get
            Return strLogin
        End Get
        Set(value As String)
            Me.strLogin = value.Trim
        End Set
    End Property

    Public Property pPassword() As String
        Get
            Return strPassword
        End Get
        Set(value As String)
            Me.strPassword = value.Trim
        End Set
    End Property

    Public Property pPerfil() As String
        Get
            Return strPerfil
        End Get
        Set(value As String)
            Me.strPerfil = value.Trim
        End Set
    End Property

    Public Property pEstado() As String
        Get
            Return strEstado
        End Get
        Set(value As String)
            Me.strEstado = value.Trim
        End Set
    End Property

    Public Property pNomServidor() As String
        Get
            Return strNomServidor
        End Get
        Set(value As String)
            Me.strNomServidor = value.Trim
        End Set
    End Property

    Public Property pNomBD() As String
        Get
            Return strNomBD
        End Get
        Set(value As String)
            Me.strNomBD = value.Trim
        End Set
    End Property

    'Esta Propiedad realizara el get y set para la cadena de autenticacion con la BD

    Public Property pConexion() As MySql.Data.MySqlClient.MySqlConnection
        Get
            Return Me.MySqlConexion
        End Get
        Set(value As MySql.Data.MySqlClient.MySqlConnection)
            Me.MySqlConexion = value
            Me.MySqlConexion.Dispose()

        End Set
    End Property


#End Region

#Region "Metodos"
    'este metodo permite realizar las acciones de incluir, modifcar, eliminar a la BD
    Public Function mEjecutar(ByVal strSentencia As String) As Boolean
        If mAbrirConexion() Then
            Me.MySqlComando = New MySqlCommand(strSentencia, pConexion)
            Return MySqlComando.ExecuteNonQuery()
        Else
            Return False
            Exit Function

        End If
    End Function

    'Este metodo me permite abrir la base de datos con la conexion existente de la aplicacion
    Public Function mAbrirConexion() As Boolean
        Try
            pConexion = New MySqlConnection
            pConexion.ConnectionString = "server='" & pNomServidor & "'; database='" & pNomBD & "'; user id='" & pUsuario & "'; password='" & pPassword & "';"
            pConexion.Open()

            If pConexion.State = ConnectionState.Open Then
                Return True
            Else
                Return False

            End If
        Catch ex As Exception
            Return False

        End Try
    End Function

    'Este metodo permite realizar las extraccion de datos por medio de los select a la BD
    Public Function mConsultar(ByVal strSentencia As String) As MySql.Data.MySqlClient.MySqlDataReader
        If mAbrirConexion() Then
            MySqlComando = New MySqlCommand(strSentencia, pConexion)
            Return MySqlComando.ExecuteReader
        Else
            Return Nothing
            Exit Function

        End If
    End Function
#End Region
End Class
