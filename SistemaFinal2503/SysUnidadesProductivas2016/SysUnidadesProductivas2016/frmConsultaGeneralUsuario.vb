﻿'Representa una conexión abierta a una base de datos MySql
Imports MySql.Data.MySqlClient

'Esta clase nos permite realizar todos los métodos necesarios para mostrar en la ventana frmConsultaGeneralUsuario 
'se pueda visualizar la información de todos los usuarios que se encuentran registrados en la base de datos 
'y además nos permite seleccionar a un usuario específico 
Public Class frmConsultaGeneralUsuario

    'En esta región se crean las variables, instancias y referencias necesarias para el desarrollo ya sea de
    'métodos o acciones que se deben realizar para  el funcionamiento de la clase 
#Region "Atributos"
    'Creación de la instancia y referencia de la clase clsConexion
    Dim clConexion As AccesoDatos.clsConexion
    'Creación de la variable strLogin de tipo String
    Dim strLogin As String
    'Creación de la instancia y referencia de la clase clsLogicaUsuario
    Dim clUsuario As New LogicaNegocios.clsLogicaUsuario
    'Creación de la variable drUsuario de tipo MySqlDataReader que permite leer la infomación de un ususario en 
    ' la base de datos
    Dim drUsuario As MySql.Data.MySqlClient.MySqlDataReader
    'Creación de la variable intContador de tipo Integer
    Dim intContador As Integer
    'Creación de la variable frmUsuarios que este tipo frmUsuarios la cual
    'hace referencia a la ventana del mismo nombre
    Dim frmUsuarios As frmUsuarios
#End Region

    '*****************************************************************************************************************************

    'Esta  función permite la autocarga, posibilitando que las clases e interfaces sean cargadas automáticamente
    'aunque no se encuentre definidas
    Private Sub frmConsultaUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Se indica el color que contendrá el panel 1
        Panel1.BackColor = Color.FromArgb(60, 74, 96)

        'Se le indica a la variable que lea la información proveniente del método mConsultarGenUsuario 
        'el cual pertenece a la clsLogicaUsuario
        Me.drUsuario = clUsuario.mConsultarGenUsuario(clConexion)

        'Se inicializa la variable inContador en 0
        intContador = 0

        'Se crea un bucle, mientras se cumpla la condición de la lectura del drUsuario 
        While drUsuario.Read
            'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base
            lvConsultaUsu.Items.Add(drUsuario.Item("login"))
            'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base
            lvConsultaUsu.Items(intContador).SubItems.Add(drUsuario.Item("nombre"))
            'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base
            lvConsultaUsu.Items(intContador).SubItems.Add(drUsuario.Item("apellido1Usu"))
            'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base
            lvConsultaUsu.Items(intContador).SubItems.Add(drUsuario.Item("apellido2Usu"))

            'Se incrementa el contador de uno en uno
            intContador += 1
        End While
    End Sub

    'Se crea el constructor de la clase, al cual se le pasa por parámetros la clase clsConexion
    Public Sub New(pConexion As AccesoDatos.clsConexion)
        'Se inicializa la referencia anteriormente creada en la región atributos con la que ingresa por parámetros en el 
        'constructor
        clConexion = pConexion

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub


#Region "Metodos"


    'Esta propiedad nos permite establecer en el txtLogin y strLogin un control para que el contenido
    'no se pueda cambiar o no
    Public ReadOnly Property mRetornarLogin() As String
        'Descriptor de acceso  get que nos devolveráel login
        Get
            'Retorno del login del usuario
            Return Me.strLogin
        End Get 'Fin del Get
    End Property  ' Fin de la Propiedad 

    'Esta acción tiene lugar cuando se hace clic sobre un elemento específico del list view lvConsultaUsu
    Private Sub lvConsultaUsu_DoubleClick(sender As Object, e As EventArgs) Handles lvConsultaUsu.DoubleClick
        'Evento que cierra la ventana retornando a la ventana frmUsuarios
        Close()
    End Sub     'Fin de la acción

    'Este evento tiene lugar cuando se selecciona un indice específico del list view lvConsultaUsu y de esta 
    'manera mostrar información en otros controles
    Private Sub lvConsultaUsu_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvConsultaUsu.SelectedIndexChanged

        'Control para evitar errores en caso que no se encuentre el login o este vacío
        Try

            'Se crea un for para recorre los elementos del list view lvConsultaUsu
            For Me.intContador = 0 To lvConsultaUsu.Items.Count - 1
                'Se hace un condición para seleccionar el elemento que se encuenta en una posición en específica del list view 
                'lvConsultaUsu
                If lvConsultaUsu.Items(intContador).Selected Then
                    'Se le asigna a la variable strLogin el login que se seleccionó en el list view lvConsultaUsu
                    strLogin = lvConsultaUsu.Items(intContador).Text
                End If 'Fin del if 
            Next 'Fin del For

            'Examina en orden textual para determinar si controla la excepción
        Catch ex As Exception
        End Try 'Fin del Try
    End Sub 'Fin del evento lvConsultaUsu_SelectedIndexChanged

    'Esta acción tiene lugar cuando se hace click sobre el btnSalir en el frmConsultaUsu
    Private Sub btnRegresar_Click(sender As Object, e As EventArgs) Handles btnRegresar.Click
        'Evento que cierra la ventana retornando a la ventana frmUsuarios
        Close()
    End Sub 'Fin de la acción btnSali_Click

#End Region'Fin de la región 

End Class