﻿Public Class frmConfirmarProducto


#Region "Atributos"


    Dim clEntidadProducto As New EntidadNegocios.clsEntidadProductoServicio
    Dim clConexion As New AccesoDatos.clsConexion
    Dim clsLogicaProducto As New LogicaNegocios.clsLogicaProductoServicio
    Dim bolAgregar
    Dim frmProducto As frmProductosServicios

#End Region


    Public Sub New(producto As EntidadNegocios.clsEntidadProductoServicio, pConexion As AccesoDatos.clsConexion, frmProducto As frmProductosServicios)
        ' Esta llamada es exigida por el diseñador.
        Me.clEntidadProducto = producto
        Me.clConexion = pConexion
        Me.frmProducto = frmProducto
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
    End Sub


    Private Sub frmConfirmarProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        lblCodigo.Text = clEntidadProducto.pIdProductoSer
        lblNombre.Text = clEntidadProducto.pNombreProSer
        lblCosto.Text = clEntidadProducto.pCostoProSer
        lblPrecio.Text = clEntidadProducto.pPrecioProSer
        lblTipo.Text = clEntidadProducto.pTipoProSer

        txtDescripcion.Text = clEntidadProducto.pDescripcionProSer

    End Sub


#Region "Botones"
    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnRechazar_Click(sender As Object, e As EventArgs) Handles btnRechazar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        bolAgregar = clsLogicaProducto.mAgregarProducto(clEntidadProducto, clConexion)
        If bolAgregar = True Then
            MessageBox.Show("Producto agregado correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            frmProducto.mLimpiarCampos()
            frmProducto.Focus()
        Else
            MessageBox.Show("Problemas al agregar el producto", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            frmProducto.mLimpiarCampos()
        End If 'Fin del if acierto o error al agregar

        Me.Close()

    End Sub

#End Region

End Class