﻿Imports MySql.Data.MySqlClient
Imports EntidadNegocios
Imports LogicaNegocios

Public Class frmPermisosUsuarios

#Region "ATRIBUTOS"

    Dim clsConexion As AccesoDatos.clsConexion

    Dim clsEntidadVentana As New EntidadNegocios.clsEntidadVentana
    Dim clsEntidadUsuario As New EntidadNegocios.clsEntidadUsuario
    Dim clsEntidadPerUsModulo As New EntidadNegocios.clsEntidadPermisosUsersModulo
    Dim clsEntidadPerUsConsulta As New EntidadNegocios.clsEntidadPermisoUserConsulta

    Dim clsLogicaVentana As New LogicaNegocios.clsLogicaVentana
    Dim clsLogicaUsuario As New LogicaNegocios.clsLogicaUsuario
    Dim clsLogicaPerUsModulo As New LogicaNegocios.clsLogicaPermisosUsersModulo
    Dim clsLlogicaPerConsulta As New LogicaNegocios.clsLogicaPermisosUsersConsulta

    Dim drVentana, drVentana2, drUsusario, drUser, drPermisoModuloUser, drPermisoConsultaUser As MySql.Data.MySqlClient.MySqlDataReader

    Dim drWin As MySql.Data.MySqlClient.MySqlDataReader

    Dim intContador, intContador2, Filas, Filas2 As Integer
    Dim strLogin, strPermisoAgregar, strPermisoModificar, strPermisoConsultar, strPermisoGeneral, strVentana, strMiLogin As String

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Close()
    End Sub

    Dim bolAgregarPerModulo, bolAgregarPerConsulta, bolAgregar, bolConsulta, bolModificar, bolModificarPerModulo, bolModificarPerConsulta As Boolean



#End Region

#Region "LOAD & CONSTRUCTOR"
    Private Sub frmPermisosUsuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        dgvVentanaModulos.ForeColor = Color.Black
        dgvVentanasConsultas.ForeColor = Color.Black
        dgvVentanaModulos.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 11)
        dgvVentanasConsultas.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 11)
        'mLllenarGrillaVentanaModulos()
        'mLllenarGrillaVentanaConsultas()
        mLlenaComboBoxUsuario()


    End Sub
    Public Sub New(pConexion As AccesoDatos.clsConexion, pStrMilogin As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion
        strMiLogin = pStrMilogin

    End Sub

#End Region

#Region "HERRAMIENTAS"
    Private Sub cbxUsuarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxUsuarios.SelectedIndexChanged
        mLimpiarGrillas()
        strLogin = ""
        strLogin = cbxUsuarios.Text.Trim
        clsEntidadUsuario.pLogin = strLogin
        clsEntidadPerUsConsulta.pLoginUsuario = cbxUsuarios.Text.Trim
        clsEntidadPerUsModulo.pLoginUsuario = cbxUsuarios.Text.Trim
        drUser = clsLogicaUsuario.mConsultarUsuarioLogin(clsEntidadUsuario, clsConexion)
        If drUser.Read Then
            lblUsuario.Text = drUser.Item("nombre").ToString.ToUpper + " " + drUser.Item("apellido1Usu").ToString.ToUpper + " " + drUser.Item("apellido2Usu").ToString.ToUpper
        End If

        drPermisoConsultaUser = Me.clsLlogicaPerConsulta.mCosultarPermisoConsulta(clsEntidadPerUsConsulta, clsConexion)
        drPermisoModuloUser = Me.clsLogicaPerUsModulo.mCosultarPermisoModulo(clsEntidadPerUsModulo, clsConexion)


        If drPermisoModuloUser.Read Then
            mVerificarPermisosUsuarioAgregarModificar(strLogin)
        Else
            mLllenarGrillaVentanaModulos()
        End If

        If drPermisoConsultaUser.Read Then
            'mVerificarPermisosUsuarioConsulta(strLogin)
            ' mVerificarUsuariosPermisoConsulta(strLogin)
        Else
            mLllenarGrillaVentanaConsultas()
        End If



    End Sub


#End Region

#Region "BOTONES"
    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click

        Filas = dgvVentanaModulos.Rows.Count


        For Me.intContador = 0 To Filas - 1

            clsEntidadPerUsModulo.pIdVentana = dgvVentanaModulos.Rows(intContador).Cells("CODIGO").Value
            strVentana = dgvVentanaModulos.Rows(intContador).Cells("VENTANA").Value
            clsEntidadPerUsModulo.pLoginUsuario = strLogin
            If (Boolean.Parse(dgvVentanaModulos.Rows(intContador).Cells("AGREGAR").Value) = True) Then
                clsEntidadPerUsModulo.pPermisoAgregar = "SI"
                strPermisoAgregar = "SI"
            Else
                clsEntidadPerUsModulo.pPermisoAgregar = "NO"
                strPermisoAgregar = "NO"
            End If
            If (Boolean.Parse(dgvVentanaModulos.Rows(intContador).Cells("MODIFICAR").Value) = True) Then
                clsEntidadPerUsModulo.pPermisoModificar = "SI"
                strPermisoModificar = "SI"
            Else
                clsEntidadPerUsModulo.pPermisoModificar = "NO"
                strPermisoModificar = "NO"
            End If

            ''AGREGA LOS PERMISOS
            bolAgregarPerModulo = clsLogicaPerUsModulo.mAgregarPermisoModulo(clsEntidadPerUsModulo, clsConexion)
        Next

        Filas2 = dgvVentanasConsultas.Rows.Count

        For Me.intContador = 0 To Filas2 - 1
            clsEntidadPerUsConsulta.pLoginUsuario = strLogin
            clsEntidadPerUsConsulta.pIdVentana = dgvVentanasConsultas.Rows(intContador).Cells("CODIGOS").Value
            strVentana = dgvVentanasConsultas.Rows(intContador).Cells("VENTANAS").Value
            If (Boolean.Parse(dgvVentanasConsultas.Rows(intContador).Cells("CONSULTAR").Value) = True) Then
                clsEntidadPerUsConsulta.pPermisoConsultar = "SI"
                strPermisoConsultar = "SI"
            Else
                clsEntidadPerUsConsulta.pPermisoConsultar = "NO"
                strPermisoConsultar = "NO"
            End If

            ''AGREGA LOS PERMISOS
            bolAgregarPerConsulta = clsLlogicaPerConsulta.mAgregarPermisoConsulta(clsEntidadPerUsConsulta, clsConexion)

        Next

        If bolAgregarPerConsulta = True And bolAgregarPerModulo = True Then
            MessageBox.Show("Permisos Agregados al Usuario.", "INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Information)
            mLimpiarGrillas()
            cbxUsuarios.Items.Clear()
            mLlenaComboBoxUsuario()

        Else
            MessageBox.Show("Error al agregar los permisos al Usuario.", "INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            mLimpiarGrillas()
            cbxUsuarios.Items.Clear()
            mLlenaComboBoxUsuario()

        End If
    End Sub


    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        ''MODIFICAR PERMISOS DE LOS MODULOS

        Filas = dgvVentanaModulos.Rows.Count
        For Me.intContador = 0 To Filas - 1

            clsEntidadPerUsModulo.pIdVentana = dgvVentanaModulos.Rows(intContador).Cells("CODIGO").Value
            Me.clsEntidadPerUsModulo.pLoginUsuario = cbxUsuarios.Text.Trim
            strVentana = dgvVentanaModulos.Rows(intContador).Cells("VENTANA").Value
            clsEntidadPerUsModulo.pLoginUsuario = strLogin
            If (Boolean.Parse(dgvVentanaModulos.Rows(intContador).Cells("AGREGAR").Value) = True) Then
                clsEntidadPerUsModulo.pPermisoAgregar = "SI"
                strPermisoAgregar = "SI"
            Else
                clsEntidadPerUsModulo.pPermisoAgregar = "NO"
                strPermisoAgregar = "NO"
            End If
            If (Boolean.Parse(dgvVentanaModulos.Rows(intContador).Cells("MODIFICAR").Value) = True) Then
                clsEntidadPerUsModulo.pPermisoModificar = "SI"
                strPermisoModificar = "SI"
            Else
                clsEntidadPerUsModulo.pPermisoModificar = "NO"
                strPermisoModificar = "NO"
            End If


            bolModificarPerModulo = clsLogicaPerUsModulo.mModificarPermisos(clsEntidadPerUsModulo, clsConexion)



        Next

        ''MODIFICAR PERMISOS DE LAS CONSULTAS
        Filas2 = dgvVentanasConsultas.Rows.Count

        For Me.intContador = 0 To Filas2 - 1
            clsEntidadPerUsConsulta.pLoginUsuario = strLogin
            clsEntidadPerUsConsulta.pIdVentana = dgvVentanasConsultas.Rows(intContador).Cells("CODIGOS").Value
            clsEntidadPerUsConsulta.pLoginUsuario = cbxUsuarios.Text.Trim
            strVentana = dgvVentanasConsultas.Rows(intContador).Cells("VENTANAS").Value
            If (Boolean.Parse(dgvVentanasConsultas.Rows(intContador).Cells("CONSULTAR").Value) = True) Then
                clsEntidadPerUsConsulta.pPermisoConsultar = "SI"
                strPermisoConsultar = "SI"
            Else
                clsEntidadPerUsConsulta.pPermisoConsultar = "NO"
                strPermisoConsultar = "NO"
            End If

            ''AGREGA LOS PERMISOS
            bolModificarPerConsulta = clsLlogicaPerConsulta.mModificarPerimsos(clsEntidadPerUsConsulta, clsConexion)

        Next

        If bolModificarPerConsulta = True And bolModificarPerModulo = True Then
            MessageBox.Show("Permisos Modificados al Usuario.", "INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Information)
            mLimpiarGrillas()
            cbxUsuarios.Items.Clear()
            mLlenaComboBoxUsuario()
        Else
            MessageBox.Show("Error al modificar los permisos al Usuario.", "INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            mLimpiarGrillas()
            cbxUsuarios.Items.Clear()
            mLlenaComboBoxUsuario()

        End If
    End Sub

#End Region

#Region "METODOS"
    Private Sub mLllenarGrillaVentanaModulos()
        If strMiLogin = "aadmin" Then
            drVentana = clsLogicaVentana.mConsultarVentanas000_099(clsConexion)
        Else
            drVentana = clsLogicaVentana.mConsultarVentanas000_099_SinPermiso(clsConexion)
        End If

        While drVentana.Read
            With (dgvVentanaModulos)
                .Rows.Add(Trim(drVentana.Item("idVentana")), Trim(drVentana.Item("nombreVentana")), False, False)
            End With
        End While
    End Sub



    Private Sub mLllenarGrillaVentanaConsultas()
        drVentana = clsLogicaVentana.mConsultarVentanas100_199(clsConexion)
        While drVentana.Read
            With (dgvVentanasConsultas)
                .Rows.Add(Trim(drVentana.Item("idVentana")), Trim(drVentana.Item("nombreVentana")), False)
            End With
        End While
    End Sub

    Private Sub mLlenaComboBoxUsuario()
        drUsusario = clsLogicaUsuario.mConsultarGenUsuario(clsConexion)
        While drUsusario.Read
            cbxUsuarios.Items.Add(drUsusario.Item("login"))

        End While
    End Sub

    Private Sub mVerificarPermisosUsuarioConsulta(strLogin)


        Me.clsEntidadPerUsConsulta.pLoginUsuario = strLogin

        drPermisoConsultaUser = Me.clsLlogicaPerConsulta.mCosultarPermisoConsulta(clsEntidadPerUsConsulta, clsConexion)

        While drPermisoConsultaUser.Read

            bolConsulta = False
            If drPermisoConsultaUser.Item("permisoConsultar") = "SI" Then
                bolConsulta = True
            End If
            Me.clsEntidadVentana.pIDVentana = drPermisoConsultaUser.Item("idVentana")
            drVentana = clsLogicaVentana.mConsultarVentanas(clsEntidadVentana, clsConexion)
            Try
                If drVentana.Read Then
                    With (dgvVentanasConsultas)
                        .Rows.Add(Trim(drVentana.Item("idVentana")), Trim(drVentana.Item("nombreVentana")), bolConsulta)
                    End With
                End If

            Catch ex As Exception
                MessageBox.Show("El sistema ha detecto un error interno, se procedera a cerrar el sistema por completo.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End
            Finally
                If Not IsNothing(drVentana) Then
                    drVentana.Close()
                End If

            End Try


        End While


    End Sub



    Private Sub mVerificarPermisosUsuarioAgregarModificar(strLogin)
        Me.clsEntidadPerUsModulo.pLoginUsuario = strLogin
        drPermisoModuloUser = Me.clsLogicaPerUsModulo.mCosultarPermisoModulo(clsEntidadPerUsModulo, clsConexion)
        'PARA REALIZAR LAS CONSULTA DE LOS PERMISOS QUE EL USUARIO TIENEN
        While drPermisoModuloUser.Read

            bolAgregar = False
            bolModificar = False
            If drPermisoModuloUser.Item("permisoAgregar") = "SI" Then
                bolAgregar = True
            End If

            If drPermisoModuloUser.Item("permisoModificar") = "SI" Then
                bolModificar = True
            End If
            Me.clsEntidadVentana.pIDVentana = drPermisoModuloUser.Item("idVentana")
            drVentana = clsLogicaVentana.mConsultarVentanas(clsEntidadVentana, clsConexion)
            'se trabaja con un Try Catch, ya que en determinados momentos el sistema cierra la conexion con la base de datos
            Try
                If drVentana.Read Then
                    With (dgvVentanaModulos)
                        .Rows.Add(Trim(drVentana.Item("idVentana")), Trim(drVentana.Item("nombreVentana")), bolAgregar, bolModificar)
                    End With
                End If
            Catch ex As Exception
                MessageBox.Show("El sistema ha detecto un error interno, se procedera a cerrar el sistema por completo.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End


            Finally

                If Not IsNothing(drVentana) Then
                    drVentana.Close()
                End If

            End Try


        End While
        drPermisoModuloUser.Close()
        'drWin.Close()

    End Sub



    Private Sub mLimpiarGrillas()
        Me.Filas = Me.dgvVentanaModulos.Rows.Count
        For Me.Filas2 = 0 To Filas - 1
            dgvVentanaModulos.Rows.Clear()
            dgvVentanaModulos.DataSource = Nothing

        Next

        Me.Filas = Me.dgvVentanasConsultas.Rows.Count
        For Me.Filas2 = 0 To Filas - 1
            dgvVentanasConsultas.Rows.Clear()
            dgvVentanasConsultas.DataSource = Nothing

        Next



    End Sub
#End Region

End Class