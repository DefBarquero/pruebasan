﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios
Imports LogicaNegocios


Public Class frmClientes

    Private Sub frmClientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
        dvgCorreo.ForeColor = Color.Black
        mktCedula.Focus()
        mLlenarComboProvincia()
        txtNomCliente.Enabled = False
        txtNomEmpresa.Enabled = False
        txtOtrasSenas.Enabled = False
        txtPriApellido.Enabled = False
        txtSegApellido.Enabled = False
        dvgCorreo.Enabled = False
        dvgTelefono.Enabled = False
        rbnActivo.Enabled = False
        rbnEmpresa.Enabled = False
        rbnPasivo.Enabled = False
        rbnPersona.Enabled = False


    End Sub

#Region "Atributos"
    'CON RESPECTO A LOS CLIENTES
    Dim clsEntidadCli As New EntidadNegocios.clsEntidadCliente
    Dim clsEntidadCorreoCli As New EntidadNegocios.clsEntidadCorreoCliente
    Dim clsEntidadTelefonoCli As New EntidadNegocios.clsEntidadTelefonoCliente
    Dim clsEntidadProvincia As New EntidadNegocios.clsEntidadProvincia
    Dim clsEntidadCanton As New EntidadNegocios.clsEntidadCanton
    Dim clsEntidadDistrito As New EntidadNegocios.clsEntidadDistrito

    Dim clsLogicaCli As New LogicaNegocios.clsLogicaCliente
    Dim clsLogicaCorreoCli As New LogicaNegocios.clsLogicaClienteCorreo
    Dim clsLogicaTelefonoCli As New LogicaNegocios.clsLogicaClienteTelefono
    Dim clsLogicaProvincia As New LogicaNegocios.clsLogicaProvincia
    Dim clsLogicaCanton As New LogicaNegocios.clsLogicaCanton
    Dim clsLogicaDistrito As New LogicaNegocios.clsLogicaDistrito

    'CONECCIÓN A LA BASE DE DATOS
    Dim clsConexion As AccesoDatos.clsConexion

    'VARIABLES BOLEANAS
    Dim bolAgregarCli, bolAgregarCorreoCli, bolAgregarTelefonoCli, bolModificarCli, bolModificarCorreoCli, bolEliminarCorreoCli, bolModificarTelefonoCli, bolEliminarTelCli As Boolean

    'Para el diseño de los datagrid
    Dim tamaño, tamaño1 As Size

    'para el manejo de los datadrig
    Dim intCodigo, Filas, x, intContador, duplas As Integer

    Dim strIDCorreo As String

    Private Sub rbnActivo_CheckedChanged(sender As Object, e As EventArgs) Handles rbnActivo.CheckedChanged

    End Sub


    'VARIABLES MYSQLDATAREADER
    Dim drCliente, drCorreoCli, drCorreoCli2, drTelefonoCli, drProvincia, drCanton, drDistrito As MySql.Data.MySqlClient.MySqlDataReader

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub




    'VARIABLE PARA ACEPTAR SI DESEA AGREGAR INFORMACIÓN O NO
    Dim drResultado As DialogResult

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Close()

    End Sub

    Private Sub btnIr_Click(sender As Object, e As EventArgs) Handles btnIr.Click
        If mktCedula.Text <> "" Then
            clsEntidadCli.pCedulaCli = mktCedula.Text
            drCliente = clsLogicaCli.mBuscarCliente(clsEntidadCli, clsConexion)
            If drCliente.Read Then
                mInformacionCliente(mktCedula.Text)
                btnModificar.Enabled = True

            Else
                drResultado = MessageBox.Show("Cédula no se encuentra asociada a ningún Cliente. ¿Desea registrarla?", "INFORMACIÓN", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If drResultado = DialogResult.Yes Then
                    mktCedula.Enabled = False
                    btnAgregar.Enabled = True
                    txtNomCliente.Enabled = True
                    txtNomEmpresa.Enabled = True
                    txtOtrasSenas.Enabled = True
                    txtPriApellido.Enabled = True
                    txtSegApellido.Enabled = True
                    dvgCorreo.Enabled = True
                    dvgTelefono.Enabled = True
                    rbnActivo.Enabled = True
                    rbnEmpresa.Enabled = True
                    rbnPasivo.Enabled = True
                    rbnPersona.Enabled = True


                Else
                    mktCedula.Enabled = True
                    mktCedula.Text = ""
                    mktCedula.Mask = "000000000000000000"

                End If


            End If
        End If

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub rbnEmpresa_CheckedChanged(sender As Object, e As EventArgs) Handles rbnEmpresa.CheckedChanged
        txtNomEmpresa.Enabled = True
        txtNomEmpresa.BackColor = Color.White
        clsEntidadCli.pTipoCli = "EMPRESA"

    End Sub

    Private Sub rbnPersona_CheckedChanged(sender As Object, e As EventArgs) Handles rbnPersona.CheckedChanged
        txtNomEmpresa.Enabled = False
        txtNomEmpresa.BackColor = Color.Gray
        clsEntidadCli.pTipoCli = "PERSONA"
        txtNomEmpresa.Text = ""


    End Sub

    Private Sub cbxDistrito_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxDistrito.SelectedIndexChanged

    End Sub

    Private Sub cbxProvincia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxProvincia.SelectedIndexChanged
        cbxCanton.Items.Clear()
        cbxDistrito.Items.Clear()
        cbxCanton.Text = "Seleccionar"
        cbxDistrito.Text = "Seleccionar"


        clsEntidadProvincia.pNombreProvincia = cbxProvincia.Text.Trim
        drProvincia = clsLogicaProvincia.mConsultaProvinciaNombre(clsEntidadProvincia, clsConexion)

        If drProvincia.Read Then

            clsEntidadCanton.pIdProvincia = drProvincia.Item("idProvincia")
            drCanton = clsLogicaCanton.mConsultarCantonIdProvincia(clsEntidadCanton, clsConexion)

            While drCanton.Read
                cbxCanton.Items.Add(drCanton.Item("nombreCanton"))
            End While


        End If
    End Sub

    Private Sub cbxCanton_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxCanton.SelectedIndexChanged
        cbxDistrito.Items.Clear()
        cbxDistrito.Text = "Seleccionar"

        clsEntidadCanton.pNombreCanton = cbxCanton.Text.Trim
        drCanton = clsLogicaCanton.mConsultarCantonNombre(clsEntidadCanton, clsConexion)

        If drCanton.Read Then
            clsEntidadDistrito.pIdCanton = drCanton.Item("idCanton")
            drDistrito = clsLogicaDistrito.mConsultarDistritoIdCanton(clsEntidadDistrito, clsConexion)
            While drDistrito.Read()
                cbxDistrito.Items.Add(drDistrito.Item("nombreDistrito"))
            End While

        End If
    End Sub


#End Region

#Region "Constructor"
    'Entra una variable pConexion por parametros, para mantener la conexion abirta con la base de datos y así agregar la informacion
    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        'se le asigana el valor a clsConexion
        Me.clsConexion = pConexion
    End Sub
#End Region

#Region "Botones"
    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click

        If (rbnEmpresa.Checked = False And rbnPersona.Checked = False) Or (rbnActivo.Checked = False And rbnPasivo.Checked = False) Or cbxDistrito.Text = "" Or cbxCanton.Text = "" Or txtNomCliente.Text = "" Or txtOtrasSenas.Text = "" Or txtPriApellido.Text = "" Or cbxProvincia.Text = "" Or txtSegApellido.Text = "" Or mktCedula.Text = "" Then
            MessageBox.Show("Faltan campos que completar.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else

            'se extrae la información de los textbox y otras herramientas de la ventana
            Me.clsEntidadCli.pCedulaCli = Me.mktCedula.Text
            Me.clsEntidadCli.pNombreCli = Me.txtNomCliente.Text.ToUpper 'ToUpper para guardar todos los registros en Mayusculas
            Me.clsEntidadCli.pApellido1Cli = Me.txtPriApellido.Text.ToUpper
            Me.clsEntidadCli.pApellido2Cli = Me.txtSegApellido.Text.ToUpper
            Me.clsEntidadProvincia.pNombreProvincia = Me.cbxProvincia.Text.Trim
            Me.clsEntidadCanton.pNombreCanton = Me.cbxCanton.Text.Trim
            Me.clsEntidadDistrito.pNombreDistrito = Me.cbxDistrito.Text.Trim
            drProvincia = Me.clsLogicaProvincia.mConsultaProvinciaNombre(clsEntidadProvincia, clsConexion)
            If drProvincia.Read Then
                Me.clsEntidadCli.pProvinciaCli = drProvincia.Item("idProvincia")
                Me.clsEntidadCanton.pIdProvincia = drProvincia.Item("idProvincia")
            End If

            drCanton = Me.clsLogicaCanton.mConsultarCantonNombreIdProvincia(clsEntidadCanton, clsConexion)
            If drCanton.Read Then
                Me.clsEntidadCli.pCantonCli = drCanton.Item("idCanton")
                Me.clsEntidadDistrito.pIdCanton = drCanton.Item("idCanton")
            End If

            drDistrito = Me.clsLogicaDistrito.mConsultaDistritoNombreIdCanton(clsEntidadDistrito, clsConexion)
            If drDistrito.Read Then
                Me.clsEntidadCli.pDistritoCli = drDistrito.Item("idDistrito")
            End If

            Me.clsEntidadCli.pOtraSenasCli = Me.txtOtrasSenas.Text.ToUpper
            clsEntidadCli.pNombreEmpresa = txtNomEmpresa.Text.ToUpper


            'controlar si los clientes comprar con frecuencia son clientes activos
            'de lo contrario se cataloga como cliente pasivo
            If rbnActivo.Checked = True Then
                clsEntidadCli.pEstadoCli = "ACTIVO"
            ElseIf rbnPasivo.Checked = True Then
                clsEntidadCli.pEstadoCli = "PASIVO"
            End If

            Dim frmConfirmar As frmConfirmarCliente = New frmConfirmarCliente(clsEntidadCli, clsConexion, Me, clsEntidadTelefonoCli, clsEntidadCorreoCli, clsEntidadProvincia, clsEntidadCanton, clsEntidadDistrito)
            frmConfirmar.Show()

        End If 'fin if que verifica si los campos estan llenos.

    End Sub


    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim frmBuscarCliente As New frmClienteBuscar(clsConexion)
        With (frmBuscarCliente)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With

        If frmBuscarCliente.mRetornarCedulaCli <> "" Then

            btnAgregar.Enabled = False

            Me.mktCedula.Text = frmBuscarCliente.mRetornarCedulaCli

            Me.mInformacionCliente(frmBuscarCliente.mRetornarCedulaCli)
            mktCedula.Enabled = False
            txtNomCliente.Enabled = False
            txtPriApellido.Enabled = False
            txtSegApellido.Enabled = False
            dvgCorreo.Enabled = True
            dvgTelefono.Enabled = True
            rbnActivo.Enabled = True
            rbnEmpresa.Enabled = True
            rbnPasivo.Enabled = True
            rbnPersona.Enabled = True
            txtOtrasSenas.Enabled = True

        Else
            mLimpiarCampos()

        End If

    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        If (rbnEmpresa.Checked = False And rbnPersona.Checked = False) Or (rbnActivo.Checked = False And rbnPasivo.Checked = False) Or cbxDistrito.Text = "" Or cbxCanton.Text = "" Or txtNomCliente.Text = "" Or txtOtrasSenas.Text = "" Or txtPriApellido.Text = "" Or cbxProvincia.Text = "" Or txtSegApellido.Text = "" Or mktCedula.Text = "" Then
            MessageBox.Show("Faltan campos que completar.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else

            Me.clsEntidadCli.pCedulaCli = Me.mktCedula.Text

            Me.clsEntidadCli.pNombreEmpresa = Me.txtNomEmpresa.Text.ToUpper
            Me.clsEntidadCli.pOtraSenasCli = Me.txtOtrasSenas.Text.ToUpper


            Me.clsEntidadProvincia.pNombreProvincia = Me.cbxProvincia.Text.Trim
            Me.clsEntidadCanton.pNombreCanton = Me.cbxCanton.Text.Trim
            Me.clsEntidadDistrito.pNombreDistrito = Me.cbxDistrito.Text.Trim
            drProvincia = Me.clsLogicaProvincia.mConsultaProvinciaNombre(clsEntidadProvincia, clsConexion)
            If drProvincia.Read Then
                Me.clsEntidadCli.pProvinciaCli = drProvincia.Item("idProvincia")
                Me.clsEntidadCanton.pIdProvincia = drProvincia.Item("idProvincia")
            End If

            drCanton = Me.clsLogicaCanton.mConsultarCantonNombreIdProvincia(clsEntidadCanton, clsConexion)
            If drCanton.Read Then
                Me.clsEntidadCli.pCantonCli = drCanton.Item("idCanton")
                Me.clsEntidadDistrito.pIdCanton = drCanton.Item("idCanton")
            End If

            drDistrito = Me.clsLogicaDistrito.mConsultaDistritoNombreIdCanton(clsEntidadDistrito, clsConexion)
            If drDistrito.Read Then
                Me.clsEntidadCli.pDistritoCli = drDistrito.Item("idDistrito")
            End If

            bolModificarCli = clsLogicaCli.mModificarCliente(clsEntidadCli, clsConexion)
            If bolModificarCli = True Then

                Filas = Me.dvgTelefono.Rows.Count
                For Me.x = 0 To Filas - 2
                    If Me.dvgTelefono.Rows(x).Cells("NUMERO").Value = "" Then
                        clsEntidadTelefonoCli.pIDTelefonoCli = Me.dvgTelefono.Rows(x).Cells("IDTELEFONO").Value
                        bolEliminarTelCli = Me.clsLogicaTelefonoCli.mEliminarTelefonoCli(clsEntidadTelefonoCli, clsConexion)
                        If bolEliminarTelCli = True Then
                            'elimino
                        Else
                            'no elimino
                        End If


                    Else
                        'si el campo idtelefono es diferente de vacio
                        If Me.dvgTelefono.Rows(x).Cells("IDTELEFONO").Value <> "" Then
                            clsEntidadTelefonoCli.pTelefonoCli = Me.dvgTelefono.Rows(x).Cells("NUMERO").Value
                            clsEntidadTelefonoCli.pIDTelefonoCli = Me.dvgTelefono.Rows(x).Cells("IDTELEFONO").Value
                            clsEntidadTelefonoCli.pCedulaCli = Me.mktCedula.Text.Trim
                            bolModificarTelefonoCli = clsLogicaTelefonoCli.mModificarTelefonoCli(clsEntidadTelefonoCli, clsConexion)

                        Else
                            'si el campo idTelefono no esta vacio, entonces lo agrega
                            drTelefonoCli = Me.clsLogicaTelefonoCli.mGenerarIDTelefonoCli(clsConexion)
                            If drTelefonoCli.Read Then
                                Me.clsEntidadTelefonoCli.pIDTelefonoCli = drTelefonoCli.Item(0)
                                Me.clsEntidadTelefonoCli.pCedulaCli = mktCedula.Text
                                Me.clsEntidadTelefonoCli.pTelefonoCli = Me.dvgTelefono.Rows(x).Cells("NUMERO").Value
                                bolAgregarTelefonoCli = Me.clsLogicaTelefonoCli.mAgregarTelefonoCli(clsEntidadTelefonoCli, clsConexion)
                                If bolAgregarTelefonoCli = True Then
                                    'agrego
                                Else
                                    'no agregpo
                                End If
                            End If
                        End If
                    End If


                Next


                Filas = Me.dvgCorreo.Rows.Count

                For Me.x = 0 To Filas - 2

                    If dvgCorreo.Rows(x).Cells("EMAIL").Value = "" Then
                        clsEntidadCorreoCli.pIDCorreoCli = Me.dvgCorreo.Rows(x).Cells("IDEMAIL").Value
                        bolEliminarCorreoCli = Me.clsLogicaCorreoCli.mEliminarCorreoCli(clsEntidadCorreoCli, clsConexion)
                        If bolEliminarCorreoCli = True Then
                            'si se elimina el email
                        Else
                            'no se elimina el email
                        End If
                    Else
                        'si el campo es diferente de vacio, realiza la modificacion
                        If Me.dvgCorreo.Rows(x).Cells("IDEMAIL").Value <> "" Then
                            clsEntidadCorreoCli.pIDCorreoCli = Me.dvgCorreo.Rows(x).Cells("IDEMAIL").Value
                            clsEntidadCorreoCli.pCorreoCli = Me.dvgCorreo.Rows(x).Cells("EMAIL").Value
                            clsEntidadCorreoCli.pCedulaCli = Me.mktCedula.Text.Trim
                            bolModificarCorreoCli = clsLogicaCorreoCli.mModificarCorreoCli(clsEntidadCorreoCli, clsConexion)

                        Else
                            'si el campo de idEmail se encuenta vacio, entonces lo agrega
                            drCorreoCli = Me.clsLogicaCorreoCli.mGenerarIDCorreoCli(clsConexion)
                            If drCorreoCli.Read Then
                                clsEntidadCorreoCli.pIDCorreoCli = drCorreoCli.Item(0)
                                clsEntidadCorreoCli.pCorreoCli = Me.dvgCorreo.Rows(x).Cells("EMAIL").Value
                                clsEntidadCorreoCli.pCedulaCli = mktCedula.Text
                                bolAgregarCorreoCli = clsLogicaCorreoCli.mAgregarCorreoCli(clsEntidadCorreoCli, clsConexion)
                                If bolAgregarCorreoCli = True Then
                                    MsgBox("EXITO AGREGAR")
                                Else
                                    MsgBox("ERROR AGREGAR")
                                End If
                            End If

                        End If
                    End If


                Next
                MessageBox.Show("Modificación de Cliente correcta.", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information)
                mktCedula.Enabled = True
                txtNomCliente.Enabled = True
                txtPriApellido.Enabled = True
                txtSegApellido.Enabled = True

                mLimpiarCampos()
                mLlenarComboProvincia()
            Else
                MessageBox.Show("Error al modificar los datos del Cliente.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                mLimpiarCampos()
                mLlenarComboProvincia()

            End If
        End If
    End Sub

#End Region

#Region "Metodos"




    Public Sub mInformacionCliente(ByVal strCedula As String)
        mLimpiarCampos()
        clsEntidadCli.pCedulaCli = strCedula
        clsEntidadCorreoCli.pCedulaCli = strCedula
        clsEntidadTelefonoCli.pCedulaCli = strCedula

        drCliente = clsLogicaCli.mBuscarCliente(clsEntidadCli, clsConexion)
        drCorreoCli = clsLogicaCorreoCli.mConsultarCorreoCliCedula(clsEntidadCorreoCli, clsConexion)
        drTelefonoCli = clsLogicaTelefonoCli.mBuscarTelefonoCliCedula(clsEntidadTelefonoCli, clsConexion)

        'MOSTRAR LOS DATOS DEL CLIENTE
        If drCliente.Read Then
            'campos que no se pueden editar


            If drCliente.Item("tipoCli") = "PERSONA" Then
                rbnPersona.Checked = True
                rbnEmpresa.Checked = False
                txtNomEmpresa.Enabled = False
            Else
                rbnPersona.Checked = False
                rbnEmpresa.Checked = True
                txtNomEmpresa.Enabled = True
            End If
            Me.mktCedula.Text = drCliente.Item("cedulaCli")
            Me.txtNomEmpresa.Text = drCliente.Item("nomEmpresaCli")
            Me.txtNomCliente.Text = drCliente.Item("nombrecli")
            Me.txtPriApellido.Text = drCliente.Item("apellido1Cli")
            Me.txtSegApellido.Text = drCliente.Item("apellido2Cli")

            Me.clsEntidadProvincia.pIdProvincia = drCliente.Item("provinciaCli")
            Me.clsEntidadCanton.pIdCanton = drCliente.Item("cantonCli")
            Me.clsEntidadDistrito.pIdDistrito = drCliente.Item("distritoCli")

            drProvincia = Me.clsLogicaProvincia.mConsultaProvinciaID(clsEntidadProvincia, clsConexion)
            If drProvincia.Read Then
                cbxProvincia.Text = drProvincia.Item("nombreProvincia")
            End If

            drCanton = Me.clsLogicaCanton.mConsultarCantonID(clsEntidadCanton, clsConexion)
            If drCanton.Read Then
                cbxCanton.Text = drCanton.Item("nombreCanton")
            End If

            drDistrito = Me.clsLogicaDistrito.mConsultarDistritoID(clsEntidadDistrito, clsConexion)
            If drDistrito.Read Then
                cbxDistrito.Text = drDistrito.Item("nombreDistrito")
            End If


            Me.txtOtrasSenas.Text = drCliente.Item("otrasSenasCli")
            If drCliente.Item("estadoCli") = "PASIVO" Then
                rbnPasivo.Checked = True
                rbnActivo.Checked = False
            Else
                rbnActivo.Checked = True
                rbnPasivo.Checked = False
            End If


        End If

        '***********************************************************
        duplas = Me.dvgTelefono.Rows.GetRowCount(DataGridViewElementStates.Displayed)
        For Me.x = 0 To duplas - 2
            Me.dvgTelefono.Rows.Remove(dvgTelefono.CurrentRow)
            Me.dvgTelefono.Refresh()
        Next

        duplas = Me.dvgCorreo.Rows.GetRowCount(DataGridViewElementStates.Displayed)
        For Me.x = 0 To duplas - 2
            Me.dvgCorreo.Rows.Remove(dvgTelefono.CurrentRow)
            Me.dvgCorreo.Refresh()
        Next
        'MOSTRAR LOS CORREOS DE LOS CLIENTES
        intContador = 0
        While drCorreoCli.Read
            Try
                Me.dvgCorreo.Rows.Add()
                Me.dvgCorreo.Item("IDEMAIL", intContador).Value = Trim(drCorreoCli.Item("idCorreoCli"))

                Me.dvgCorreo.Item("EMAIL", intContador).Value = Trim(drCorreoCli.Item("correoCli"))
            Catch ex As Exception


            End Try
            intContador = intContador + 1
        End While

        'MOSTRAR LOS TELEFONOS DE LOS CLIENTES
        intContador = 0
        While drTelefonoCli.Read
            Try
                Me.dvgTelefono.Rows.Add()
                Me.dvgTelefono.Item("NUMERO", intContador).Value = Trim(drTelefonoCli.Item("telefonoCli"))
                Me.dvgTelefono.Item("IDTELEFONO", intContador).Value = Trim(drTelefonoCli.Item("idTelefonoCli"))
            Catch ex As Exception

            End Try
            intContador = intContador + 1

        End While




    End Sub 'fin metodo para cargar la información del cliente

    Public Sub mLimpiarCampos()

        cbxDistrito.Items.Clear()
        cbxCanton.Items.Clear()
        cbxProvincia.Items.Clear()
        mLlenarComboProvincia()
        txtNomCliente.Text = ""
        txtNomEmpresa.Text = ""
        txtOtrasSenas.Text = ""
        txtNomCliente.Enabled = True
        mktCedula.Enabled = True
        txtPriApellido.Text = ""
        txtPriApellido.Enabled = True
        txtSegApellido.Text = ""
        txtSegApellido.Enabled = True
        mktCedula.Text = ""
        mktCedula.Mask = "000000000000000000"
        rbnActivo.Checked = False
        rbnPersona.Checked = False
        rbnEmpresa.Checked = False
        rbnPasivo.Checked = False
        rbnActivo.Checked = False
        mktCedula.ReadOnly = False
        Filas = Me.dvgCorreo.Rows.Count
        For Me.x = 0 To Filas - 2
            dvgCorreo.Rows.Clear()  'para limpiarlo
            dvgCorreo.DataSource = Nothing 'para limpiar cualquier registro internamente y desenlazar el control de la BD
        Next
        Filas = Me.dvgTelefono.Rows.Count
        For Me.x = 0 To Filas - 2
            dvgTelefono.Rows.Clear()  'para limpiarlo
            dvgTelefono.DataSource = Nothing 'para limpiar cualquier registro internamente y desenlazar el control de la BD
        Next

        txtNomCliente.Enabled = False
        txtNomEmpresa.Enabled = False
        txtOtrasSenas.Enabled = False
        txtPriApellido.Enabled = False
        txtSegApellido.Enabled = False
        dvgCorreo.Enabled = False
        dvgTelefono.Enabled = False
        rbnActivo.Enabled = False
        rbnEmpresa.Enabled = False
        rbnPasivo.Enabled = False
        rbnPersona.Enabled = False

    End Sub

    Public Sub mLlenarComboProvincia()
        drProvincia = clsLogicaProvincia.mConsultaProvincia(clsConexion)
        While drProvincia.Read
            cbxProvincia.Items.Add(drProvincia.Item("nombreProvincia"))
        End While
    End Sub
#End Region


End Class