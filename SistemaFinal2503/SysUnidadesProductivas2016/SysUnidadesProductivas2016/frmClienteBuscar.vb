﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadCliente
Imports LogicaNegocios.clsLogicaCliente

Public Class frmClienteBuscar


#Region "Atributos"
    'para mantener la conexion a la base de datos
    Dim clsConexion As AccesoDatos.clsConexion

    'variables de cliente
    Dim clsEntidadCli As New EntidadNegocios.clsEntidadCliente
    Dim clsLogicaCli As New LogicaNegocios.clsLogicaCliente

    Dim drCliente As MySql.Data.MySqlClient.MySqlDataReader
    Dim intContador As Integer
    Dim strClienteCedula, strNombre As String


#End Region

#Region "Metodos"

    Public ReadOnly Property mRetornarCedulaCli() As String
        Get
            Return strClienteCedula

        End Get
    End Property

#End Region

#Region "Constructor"

    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion
    End Sub



    Private Sub frmClienteBuscar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)

        Me.drCliente = clsLogicaCli.mConsultaGeneralCli(clsConexion)
        intContador = 0
        While drCliente.Read
            lvClientes.Items.Add(drCliente.Item("cedulaCli"))
            lvClientes.Items(intContador).SubItems.Add(drCliente.Item("nombrecli"))
            lvClientes.Items(intContador).SubItems.Add(drCliente.Item("apellido1Cli"))
            lvClientes.Items(intContador).SubItems.Add(drCliente.Item("apellido2Cli"))

            intContador = intContador + 1

        End While

    End Sub

    'Metodo que permite consultar por nombre conforme se digita en el espacio del nombre
    Public Sub mConsultaNombreProducto()

        strNombre = ""
        intContador = 0
        strNombre = txtNombre.Text

        If strNombre = "" Then
            lvClientes.Items.Clear()
        Else
            lvClientes.Items.Clear()
            Try
                drCliente = clsLogicaCli.mConsultarNombreCliente(strNombre, clsConexion)

                While drCliente.Read
                    lvClientes.Items.Add(drCliente.Item("cedulaCli"))
                    lvClientes.Items(intContador).SubItems.Add(drCliente.Item("nombrecli"))
                    lvClientes.Items(intContador).SubItems.Add(drCliente.Item("apellido1Cli"))
                    lvClientes.Items(intContador).SubItems.Add(drCliente.Item("apellido2Cli"))

                    intContador += 1
                End While
            Catch ex As Exception
            End Try
        End If
    End Sub


    'Metodo que permite consultar por nombre conforme se digita en el espacio del nombre
    Public Sub mConsultaCedulaCliente()

        strClienteCedula = ""
        intContador = 0
        strClienteCedula = txtCedula.Text

        If strClienteCedula = "" Then
            lvClientes.Items.Clear()
        Else
            lvClientes.Items.Clear()
            Try
                drCliente = clsLogicaCli.mConsultarCedulaCliente(strClienteCedula, clsConexion)

                While drCliente.Read
                    lvClientes.Items.Add(drCliente.Item("cedulaCli"))
                    lvClientes.Items(intContador).SubItems.Add(drCliente.Item("nombrecli"))
                    lvClientes.Items(intContador).SubItems.Add(drCliente.Item("apellido1Cli"))
                    lvClientes.Items(intContador).SubItems.Add(drCliente.Item("apellido2Cli"))

                    intContador += 1
                End While
            Catch ex As Exception
            End Try
        End If
    End Sub

#End Region



#Region "Botones"
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Close()

    End Sub


    Private Sub txtNombre_TextChanged(sender As Object, e As EventArgs) Handles txtNombre.TextChanged
        mConsultaNombreProducto()
    End Sub

    Private Sub txtCedula_TextChanged(sender As Object, e As EventArgs) Handles txtCedula.TextChanged
        mConsultaCedulaCliente()
    End Sub

    Private Sub lvClientes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvClientes.SelectedIndexChanged
        Try
            For Me.intContador = 0 To lvClientes.Items.Count - 1
                If lvClientes.Items(intContador).Selected Then
                    strClienteCedula = lvClientes.Items(intContador).Text

                End If
            Next
        Catch ex As Exception

        End Try
    End Sub


#End Region



End Class