﻿Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadProveedor
Imports LogicaNegocios.clsLogicaProveedor

Public Class frmProveedorBuscar
#Region "Atributos"
    Dim clsConexion As AccesoDatos.clsConexion
    Dim clsEntidadProveedor As New EntidadNegocios.clsEntidadProveedor
    Dim clsLogicaProveedor As New LogicaNegocios.clsLogicaProveedor


    Dim drProveedor As MySql.Data.MySqlClient.MySqlDataReader
    Dim intContador As Integer
    Dim strProveedorCedula, strNombre As String


#End Region

#Region "Metodos"

    Public ReadOnly Property mRetornarCedulaPro() As String
        Get
            Return strProveedorCedula

        End Get
    End Property

    'Metodo que permite consultar por nombre conforme se digita en el espacio del nombre
    Public Sub mConsultaNombreProveedor()

        strNombre = ""
        intContador = 0
        strNombre = txtNombre.Text

        If strNombre = "" Then
            lvProveedores.Items.Clear()
        Else
            lvProveedores.Items.Clear()
            Try
                drProveedor = clsLogicaProveedor.mConsultarNombreProveedor(strNombre, clsConexion)

                While drProveedor.Read
                    lvProveedores.Items.Add(drProveedor.Item("cedulaPro"))
                    lvProveedores.Items(intContador).SubItems.Add(drProveedor.Item("nombrePro"))
                    lvProveedores.Items(intContador).SubItems.Add(drProveedor.Item("apellido1Pro"))
                    lvProveedores.Items(intContador).SubItems.Add(drProveedor.Item("apellido2Pro"))

                    intContador += 1
                End While
            Catch ex As Exception
            End Try
        End If
    End Sub

    Public Sub mConsultaCedulaProveedor()
        strProveedorCedula = ""
        intContador = 0
        strProveedorCedula = txtCedula.Text

        If strProveedorCedula = "" Then
            lvProveedores.Items.Clear()
        Else
            lvProveedores.Items.Clear()
            Try
                drProveedor = clsLogicaProveedor.mConsultarCedulaProveedor(strProveedorCedula, clsConexion)

                While drProveedor.Read
                    lvProveedores.Items.Add(drProveedor.Item("cedulaPro"))
                    lvProveedores.Items(intContador).SubItems.Add(drProveedor.Item("nombrePro"))
                    lvProveedores.Items(intContador).SubItems.Add(drProveedor.Item("apellido1Pro"))
                    lvProveedores.Items(intContador).SubItems.Add(drProveedor.Item("apellido2Pro"))


                    intContador += 1
                End While
            Catch ex As Exception
            End Try
        End If
    End Sub
#End Region

#Region "Constructor"
    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion
    End Sub
#End Region

    Private Sub frmProveedorBuscar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        drProveedor = clsLogicaProveedor.mConsultaGenProveedor(clsConexion)

        While drProveedor.Read
            lvProveedores.Items.Add(drProveedor.Item("cedulaPro"))
            lvProveedores.Items(intContador).SubItems.Add(drProveedor.Item("nombrePro"))
            lvProveedores.Items(intContador).SubItems.Add(drProveedor.Item("apellido1Pro"))
            lvProveedores.Items(intContador).SubItems.Add(drProveedor.Item("apellido2Pro"))

            intContador = intContador + 1
        End While

    End Sub




#Region "Botones"
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Close()

    End Sub

    Private Sub txtCedula_TextChanged(sender As Object, e As EventArgs) Handles txtCedula.TextChanged
        mConsultaCedulaProveedor()
    End Sub

    Private Sub txtNombre_TextChanged(sender As Object, e As EventArgs) Handles txtNombre.TextChanged
        mConsultaNombreProveedor()

    End Sub

    Private Sub lvProveedores_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvProveedores.SelectedIndexChanged
        Try
            For Me.intContador = 0 To lvProveedores.Items.Count - 1
                If lvProveedores.Items(intContador).Selected Then
                    strProveedorCedula = lvProveedores.Items(intContador).Text

                End If
            Next
        Catch ex As Exception

        End Try
    End Sub


#End Region


End Class