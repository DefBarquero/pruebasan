﻿'Se importan las bibliotecas y clases necesarias para el funcionamiento de esta 
Imports AccesoDatos.clsConexion
Imports EntidadNegocios
Imports LogicaNegocios
Imports MySql.Data.MySqlClient

'Se esta clase permite consultar todas las facturas que se encuentren registradas en la base de datos
Public Class frmConsultaGeneralFactura

    'Se esta region posee todas las variables necesarias para el funcionamiento de la ventana frmConsultaGeneralFactura
#Region "ATRIBUTOS"
    '******************************* Creacion de Variables
    Dim clsConexion As AccesoDatos.clsConexion
    Dim strCedula, strFecha As String
    Dim intContador, intTotal, intCantidad As Integer
    Dim deCosto, deTotal, deGanancia, deIva, deTotalSinIVa As Decimal
    Dim drCliente, drUsuario, drFacturaDet, drFacturaEnc, drProducto, drFacturaCostoEnc As MySql.Data.MySqlClient.MySqlDataReader
    '***********************************Instancias y refrencias 
    Dim clsEntidadFacturaEnc As New EntidadNegocios.clsEntidadFacturaEnc
    Dim clsEntidadFacturaDet As New EntidadNegocios.clsEntidadFacturaDet
    Dim clsEntidadCliente As New EntidadNegocios.clsEntidadCliente
    Dim clsEntidadProducto As New EntidadNegocios.clsEntidadProductoServicio

    Dim clsEntidadUsuario As New EntidadNegocios.clsEntidadUsuario
    Dim clsLogicaFacturaEnc As New LogicaNegocios.clsLogicaFacturaEnc
    Dim clsLogicaFacturaCostoEnc As New LogicaNegocios.clsLogicaFactutaCostoEnc
    Dim clsLogicaFacturaDet As New LogicaNegocios.clsLogicaFacturaDet
    Dim clsLogicaCliente As New LogicaNegocios.clsLogicaCliente
    Dim clsLogicaProducto As New LogicaNegocios.clsLogicaProductoServicio
    Dim clsLogicaUsuario As New LogicaNegocios.clsLogicaUsuario
#End Region

    'Accion al hacer click sobre el botón 
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Close() 'cierra la ventana
    End Sub

#Region "CONSTRUCTOR & LOAD"
    'Se crea el constructor de la clase, al cual se le pasa por parámetros la clase clsConexion
    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion

    End Sub



    'Esta  función permite la autocarga, posibilitando que las clases e interfaces sean cargadas automáticamente
    'aunque no se encuentre definidas
    Private Sub frmConsultaGeneralFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Color de la ventana
        Me.Panel2.BackColor = Color.FromArgb(60, 74, 96)
        'Posicion
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        'Extender pantalla
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
        'Se crea una variable 
        Dim contador = 0
        'Al dr se le asigna lo que devuelva el metodo
        drFacturaEnc = clsLogicaFacturaEnc.mConsultaGenFacturaEnc(clsConexion)
        '***************************Inicialización de variables***********************************
        Me.deCosto = 0.00                                                                       '*
        Me.intCantidad = 0                                                                      '*
        '*****************************************************************************************


        'Este ciclo permite la carga de información en el listView, lo cual nos permitira visualizar todas las facturas que se han realizado en el sistema
        While drFacturaEnc.Read

            lvConsulta.Items.Add(drFacturaEnc.Item("idFactura"))
            Me.clsEntidadFacturaDet.pIdFactura = drFacturaEnc.Item("idFactura")
            drFacturaDet = clsLogicaFacturaDet.mConsultaFacturaDet(clsEntidadFacturaDet, clsConexion)

            While drFacturaDet.Read
                intCantidad = drFacturaDet.Item("cantidad")



            End While

            clsEntidadCliente.pCedulaCli = drFacturaEnc.Item("cedulaCli")
            drCliente = clsLogicaCliente.mBuscarCliente(clsEntidadCliente, clsConexion)
            While drCliente.Read
                lvConsulta.Items(contador).SubItems.Add(drCliente.Item("nombreCli") + " " + drCliente.Item("apellido1Cli") + " " + drCliente.Item("apellido2Cli"))

            End While

            clsEntidadUsuario.pLogin = drFacturaEnc.Item("login")
            drUsuario = clsLogicaUsuario.mConsultarUsuarioLogin(clsEntidadUsuario, clsConexion)
            While drUsuario.Read
                lvConsulta.Items(contador).SubItems.Add(drUsuario.Item("nombre") + "  " + drUsuario.Item("apellido1Usu") + " " + drUsuario.Item("apellido2Usu"))


            End While

            lvConsulta.Items(contador).SubItems.Add(drFacturaEnc.Item("fechaFactura"))
            lvConsulta.Items(contador).SubItems.Add(drFacturaEnc.Item("talonario"))
            lvConsulta.Items(contador).SubItems.Add(drFacturaEnc.Item("tipoPago"))
            lvConsulta.Items(contador).SubItems.Add(drFacturaEnc.Item("total"))
            lvConsulta.Items(contador).SubItems.Add(drFacturaEnc.Item("iva"))
            deTotal += Convert.ToDecimal(drFacturaEnc.Item("total"))
            deIva += Convert.ToDecimal(drFacturaEnc.Item("iva"))
            contador += 1
        End While

        drFacturaCostoEnc = Me.clsLogicaFacturaCostoEnc.mConsultarFacturaCostoEnc(clsConexion)
        While drFacturaCostoEnc.Read()
            deCosto += Convert.ToDecimal(drFacturaCostoEnc.Item("total"))
        End While
        deGanancia = deTotal-deCosto-deIva
        lblTotal.Text = deTotal
        lblTotalCosto.Text = deCosto
        lblTotalGanancia.Text = deGanancia
        iva.Text=deIva

    End Sub
#End Region



End Class