﻿Public Class frmConsultaEspecificaAbonos

#Region "Atributos"

    'ACCESO DATOS
    Dim clsConexion As AccesoDatos.clsConexion

    'ENTIDAD
    Dim clsEntidadCliente As New EntidadNegocios.clsEntidadCliente
    Dim clsEntidadAbono As New EntidadNegocios.clsEntidadAbono


    'LOGICA
    Dim clsLogicaCliente As New LogicaNegocios.clsLogicaCliente
    Dim clsLogicaAbono As New LogicaNegocios.clsLogicaAbono

    'DATAREADER
    Dim drCliente, drAbono As MySql.Data.MySqlClient.MySqlDataReader

    'INTEGER
    Dim intContador1, intContador2, intContador3 As Integer

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        SetVisibleCore(False)
    End Sub

    Private Sub frmConsultaEspecificaAbono_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
    End Sub

#End Region

#Region "Herramientas"

    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion
    End Sub

    Private Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        lvClientes.Items.Clear()
        lvAbonos.Items.Clear()
        intContador1 = 0
        intContador2 = 0

        'MOSTAR VENTAN
        Dim frmBuscarCliente As New frmClienteBuscar(clsConexion)
        With (frmBuscarCliente)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With

        If frmBuscarCliente.mRetornarCedulaCli <> "" Then
            clsEntidadCliente.pCedulaCli = frmBuscarCliente.mRetornarCedulaCli


            drCliente = clsLogicaCliente.mBuscarCliente(clsEntidadCliente, clsConexion)
            If drCliente.Read Then
                lvClientes.Items.Add(drCliente.Item("cedulaCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("nombreCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("apellido1Cli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("apellido2Cli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("estadoCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("tipoCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("nomEmpresaCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("provinciaCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("cantonCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("distritoCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("otrasSenasCli"))
            End If 'fin del drCliente

            '***********************ABONOS****************************************

            drAbono = clsLogicaAbono.mConsultaEspecificaAbono(clsEntidadCliente, clsConexion)

            While drAbono.Read
                lvAbonos.Items.Add(drAbono.Item("idAbono"))
                lvAbonos.Items(intContador2).SubItems.Add(drAbono.Item("cedulaCli"))
                lvAbonos.Items(intContador2).SubItems.Add(drAbono.Item("fechaAbono"))
                lvAbonos.Items(intContador2).SubItems.Add(drAbono.Item("idFactura"))
                lvAbonos.Items(intContador2).SubItems.Add(drAbono.Item("montoCompra"))
                lvAbonos.Items(intContador2).SubItems.Add(drAbono.Item("montoAbonado"))
                lvAbonos.Items(intContador2).SubItems.Add(drAbono.Item("saldoActual"))

                intContador2 = intContador2 + 1

            End While

        End If ' fin frmBuscarCliente.mRetornarCedulaCli
    End Sub

#End Region

End Class