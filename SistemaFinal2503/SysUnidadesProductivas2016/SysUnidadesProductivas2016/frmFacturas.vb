﻿Imports AccesoDatos.clsConexion
Imports EntidadNegocios
Imports LogicaNegocios
Imports MySql.Data.MySqlClient

Public Class frmFacturas


#Region "ATRIBUTOS"
    Dim clsConexion As AccesoDatos.clsConexion
    Dim clsEntidadCliente As New EntidadNegocios.clsEntidadCliente
    Dim clsEntidadProducto As New EntidadNegocios.clsEntidadProductoServicio
    Dim clsEntidadFacturaDet As New EntidadNegocios.clsEntidadFacturaDet
    Dim clsEntidadFacturaEnc As New EntidadNegocios.clsEntidadFacturaEnc
    Dim clsEntidadFacturaCostoDet As New EntidadNegocios.clsEntidadFacturaCostoDet
    Dim clsEntidadFacturaCostoEnc As New EntidadNegocios.clsEntidadFacturaCostoEnc

    Dim clsLogicaCliente As New LogicaNegocios.clsLogicaCliente
    Dim clsLogicaProducto As New LogicaNegocios.clsLogicaProductoServicio
    Dim clsLogicaFacturaDet As New LogicaNegocios.clsLogicaFacturaDet
    Dim clsLogicaFacturaEnc As New LogicaNegocios.clsLogicaFacturaEnc
    Dim clsLogicaFacturaCostoDet As New LogicaNegocios.clsLogicaFacturaCostoDet
    Dim clsLogicaFacturaCostoEnc As New LogicaNegocios.clsLogicaFactutaCostoEnc



    Dim drCliente, drProducto As MySql.Data.MySqlClient.MySqlDataReader

    Dim strCedulaCliente, strCodigoProducto, strCodigoProducto2, strProductoCompra, strLogin, codex As String

    Dim intContador, intContadorListaProducto, intContadorListaCompra, intContadorListaCompra2, intIndex, Filas, intCampoCantidad, intSFilas As Integer
    Dim costoPro As Decimal
    Dim bolListaCompraEliminar, bolCampoCantidad, bolAgregraFacturaDet, bolAgregraFacturaEnc, bolEliminarFactDet, bolModificarCantidadPro, bolAgregraFacturaCostoDet, bolAgregraFacturaCostoEnc, bolEliminarFactCostoDet As Boolean


    Dim asd, iv, ivAcumulado As decimal

   

    Private Sub lvListaProductos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvListaProductos.SelectedIndexChanged

    End Sub

    Dim intSumatoria, deCosto, deCostoTotal, ivTotal, ivArticulo As Decimal

#End Region

#Region "CONSTRUCTOR & LOAD"
    Public Sub New(pConexion As AccesoDatos.clsConexion, pEntidadUsuario As EntidadNegocios.clsEntidadUsuario)
        strLogin = ""
        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        clsConexion = pConexion
        strLogin = pEntidadUsuario.pLogin


    End Sub



    Private Sub frmFacturas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        lblNFactura.Text = mGenerarIDFactura()
        mLlenarListaProducto()
        dvListadoCompra.ForeColor = Color.Black
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
        Me.dtpCalendario.CustomFormat = "dd/MM/yyyy"
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
    End Sub






#End Region

#Region "CONTROLES"
    Private Sub btnIr_Click(sender As Object, e As EventArgs) Handles btnIr.Click
        strCedulaCliente = ""
        Dim frmBuscarCliente As New frmClienteBuscar(clsConexion)
        With (frmBuscarCliente)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With

        If frmBuscarCliente.mRetornarCedulaCli <> "" Then
            strCedulaCliente = frmBuscarCliente.mRetornarCedulaCli
            clsEntidadCliente.pCedulaCli = strCedulaCliente
            drCliente = clsLogicaCliente.mBuscarCliente(clsEntidadCliente, clsConexion)

            If drCliente.Read Then
                lblCliente.Text = drCliente.Item("nombrecli") + " " + drCliente.Item("apellido1Cli") + " " + drCliente.Item("apellido2Cli")

            End If
        End If

    End Sub

    '****************BOTONES****************************************************************************************************

    Private Sub btnFacturar_Click(sender As Object, e As EventArgs) Handles btnFacturar.Click
        deCosto = 0
        deCostoTotal = 0
        If (rbContado.Checked = False And rbCredito.Checked = False) Or lblCliente.Text = "Nombre Cliente" Then
            MessageBox.Show("Indique el nombre del cliente y un modo de pago.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else

            If lblCliente.Text <> "Nombre Cliente" Then

                Filas = 0
                Filas = dvListadoCompra.Rows.Count

                For Me.intContador = 0 To Filas - 2
                    If (dvListadoCompra.Rows(intContador).Cells("CODIGO").Value) <> "" Then
                        clsEntidadFacturaDet.pIdFactura = lblNFactura.Text.Trim
                        clsEntidadFacturaDet.pIdProducto = dvListadoCompra.Rows(intContador).Cells("CODIGO").Value
                        clsEntidadFacturaDet.pCantidad = dvListadoCompra.Rows(intContador).Cells("CANTIDAD").Value
                        clsEntidadFacturaDet.pSubTotal = dvListadoCompra.Rows(intContador).Cells("TOTAL").Value
                        strCodigoProducto2 = dvListadoCompra.Rows(intContador).Cells("CODIGO").Value

                        clsEntidadFacturaCostoDet.pIdFactura = lblNFactura.Text.Trim
                        clsEntidadFacturaCostoDet.pIdProducto = dvListadoCompra.Rows(intContador).Cells("CODIGO").Value
                        clsEntidadFacturaCostoDet.pCantidad = dvListadoCompra.Rows(intContador).Cells("CANTIDAD").Value
                        clsEntidadFacturaDet.pIdProducto = dvListadoCompra.Rows(intContador).Cells("CODIGO").Value

                        deCosto = Me.costoProducto(clsEntidadFacturaDet.pIdProducto)

                        deCostoTotal += Convert.ToDecimal(clsEntidadFacturaDet.pCantidad) * deCosto



                        bolAgregraFacturaDet = clsLogicaFacturaDet.mAgregarFacturaDet(clsEntidadFacturaDet, clsConexion)
                        bolAgregraFacturaCostoDet = Me.clsLogicaFacturaCostoDet.mAgregarFacturaCostoDet(clsEntidadFacturaCostoDet, clsConexion)
                        If bolAgregraFacturaCostoDet = True Then
                            'MsgBox("AGREGO costo det")
                        Else
                            ' MsgBox("NO AGREGO costo det")
                        End If

                    End If


                Next

                clsEntidadFacturaEnc.pIdFactura = lblNFactura.Text
                clsEntidadFacturaEnc.pCedulaCli = strCedulaCliente
                clsEntidadFacturaEnc.pFecha = dtpCalendario.Value.Date
                clsEntidadFacturaEnc.pLogin = strLogin
                clsEntidadFacturaEnc.pTotal = lblTotal.Text
                clsEntidadFacturaEnc.pTalonario = txtTalonario.Text

                'impuesto de venta total de la factura_______
                clsEntidadFacturaEnc.pIva = ivaTotal.Text
                '____________________________________________

                If rbContado.Checked = True Then
                    clsEntidadFacturaEnc.pTipoPago = "Contado"
                ElseIf rbCredito.Checked = True Then
                    clsEntidadFacturaEnc.pTipoPago = "Credito"
                End If

                clsEntidadFacturaCostoEnc.pIdFactura = lblNFactura.Text
                clsEntidadFacturaCostoEnc.pCedulaCli = strCedulaCliente
                clsEntidadFacturaCostoEnc.pFecha = dtpCalendario.Value.Date
                clsEntidadFacturaCostoEnc.pLogin = strLogin
                clsEntidadFacturaCostoEnc.pTotal = deCostoTotal
                clsEntidadFacturaCostoEnc.pTalonario = txtTalonario.Text

                bolAgregraFacturaCostoEnc = Me.clsLogicaFacturaCostoEnc.mAgregarFacturaCostoEnc(clsEntidadFacturaCostoEnc, clsConexion)
                If bolAgregraFacturaCostoEnc = True Then
                    'MsgBox("AGREGO FAC COS ENC")
                Else
                    ' MsgBox("NO AGREGO FAC COS ENC")
                End If

                bolAgregraFacturaEnc = clsLogicaFacturaEnc.mAgregarFacturaEnc(clsEntidadFacturaEnc, clsConexion)
                If bolAgregraFacturaEnc = True Then
                    MessageBox.Show("Factura Registrada.", "INFORMACION", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Dim frmConfirmaFactura As frmConfirmarFactura = New frmConfirmarFactura(clsEntidadFacturaEnc, clsEntidadFacturaDet, clsConexion, Me, clsEntidadCliente)
                    frmConfirmaFactura.Show()
                    mLimpiar()

                Else
                    clsEntidadFacturaDet.pIdFactura = lblNFactura.Text
                    clsEntidadFacturaCostoDet.pIdFactura = lblNFactura.Text
                    bolEliminarFactCostoDet = clsLogicaFacturaCostoDet.mEliminarFacturaCostoDet(clsEntidadFacturaCostoDet, clsConexion)
                    If bolEliminarFactCostoDet = True Then
                        'MsgBox("ECD")
                    Else
                        'MsgBox("NOECD")
                    End If
                    bolEliminarFactDet = clsLogicaFacturaDet.mEliminarFacturaDet(clsEntidadFacturaDet, clsConexion)
                    If bolEliminarFactDet = True Then
                        MsgBox("Registro eliminado")
                        mLimpiar()

                    End If

                    'MsgBox("No registro")
                    mLimpiar()

                End If

            Else

                MessageBox.Show("Verifique que todos los datos sean los correctos.", "ATENCION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If


        End If


    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        MsgBox(dvListadoCompra.Rows.Count)
        mLimpiar()

    End Sub




    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        intContadorListaProducto = 0
        intContadorListaProducto = lvListaProductos.Items.Count

        If bolListaCompraEliminar = True Then
            'strProductoEliminar = dvListadoCompra.Rows(intIndex).Cells("CODIGO").Value
            lvListaProductos.Items.Add(dvListadoCompra.Rows(intIndex).Cells("CODIGO").Value)
            lvListaProductos.Items(intContadorListaProducto).SubItems.Add(dvListadoCompra.Rows(intIndex).Cells("DESCRIPCION").Value)

            Filas = dvListadoCompra.Rows.Count

            If Filas <> 2 Then
                dvListadoCompra.Rows.Remove(dvListadoCompra.CurrentRow)
                dvListadoCompra.Refresh()
                dvListadoCompra.DataSource = Nothing
                bolListaCompraEliminar = False
            Else
                btnFacturar.Enabled = False
                dvListadoCompra.Rows.Clear()
                dvListadoCompra.DataSource = Nothing
                dvListadoCompra.Refresh()
                bolListaCompraEliminar = False
            End If


        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Close()

    End Sub

    '****************FIN BOTONES****************************************************************************************************

    Private Sub lvListaProductos_DoubleClick(sender As Object, e As EventArgs) Handles lvListaProductos.DoubleClick
        btnFacturar.Enabled = True

        'bolListaCompra = False
        intContadorListaProducto = 0
        intContadorListaCompra = 0

        Filas = 0

        Try
            For Me.intContadorListaProducto = 0 To lvListaProductos.Items.Count - 1
                If lvListaProductos.Items(intContadorListaProducto).Selected Then
                    strCodigoProducto = lvListaProductos.Items(intContadorListaProducto).Text
                End If
            Next
        Catch ex As Exception

        End Try

        '****MOSTRAR LOS CAMPOS SELECCIONEDOS DEL LISTVIEWPEODUCTOS EN EL DATAGRIDVIEWCOMPRAS
        Try
            'Filas = Me.dvListadoCompra.Rows.GetRowCount(DataGridViewElementStates.Displayed)
            Filas = dvListadoCompra.Rows.Count
            Me.dvListadoCompra.Rows.Add()
            If strCodigoProducto <> "" Then

                drProducto = clsLogicaProducto.mConsultaProductoString(strCodigoProducto, clsConexion)
                If drProducto.Read Then
                    Me.dvListadoCompra.Item("CODIGO", Filas - 1).Value = drProducto.Item("idProducto_Servicio")
                    Me.dvListadoCompra.Item("DESCRIPCION", Filas - 1).Value = drProducto.Item("nombrePro_Ser")
                    Me.dvListadoCompra.Item("CANTIDAD", Filas - 1).Value = 1
                    Me.dvListadoCompra.Item("MONTO", Filas - 1).Value = drProducto.Item("precioPro_Ser")
                    Me.dvListadoCompra.Item("TOTAL", Filas - 1).Value = drProducto.Item("precioPro_Ser")
                    strCodigoProducto = ""
                End If
            End If


        Catch ex As Exception

        End Try

        Dim lista As ListViewItem = New ListViewItem()
        For Each lista In lvListaProductos.SelectedItems
            lista.Remove()

        Next
        Me.mSumaTotalProducto()
    End Sub


    '*************************************DATAGRIDVIEW********************************

    Private Sub dvListadoCompra_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dvListadoCompra.CellClick
    bolListaCompraEliminar = True

    intIndex = dvListadoCompra.CurrentRow.Index

    bolCampoCantidad = True
    intCampoCantidad = dvListadoCompra.CurrentRow.Index

    End Sub


    Private Sub dvListadoCompra_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dvListadoCompra.RowEnter
        strCodigoProducto2 = ""

        If bolCampoCantidad = True Then
            strCodigoProducto2 = dvListadoCompra.Rows(intCampoCantidad).Cells("CODIGO").Value

            dvListadoCompra.Item("TOTAL", intCampoCantidad).Value = (Convert.ToDecimal(dvListadoCompra.Item("CANTIDAD", intCampoCantidad).Value) * Convert.ToDecimal(dvListadoCompra.Item("MONTO", intCampoCantidad).Value))
            'PARA UN MEJOR CONTROL DE LAS GRILLAS
            'YA QUE EL AL MANIPULARLAS SE CAÍA EL SISTEMA
            bolCampoCantidad = False
            bolListaCompraEliminar = False


        End If

        If dvListadoCompra.Rows(intCampoCantidad).Cells("IVA").Value = True Then
             asd = Convert.ToDecimal(dvListadoCompra.Item("CANTIDAD", intCampoCantidad).Value) * Convert.ToDecimal(dvListadoCompra.Item("MONTO", intCampoCantidad).Value)
            iv = (asd*0.13)+asd
             dvListadoCompra.Item("TOTAL", intCampoCantidad).Value = iv
        End If

        Me.mSumaTotalProducto()
    End Sub


#End Region

#Region "METODOS"
    Public Function mGenerarIDFactura() As String
        Dim Numero As String
        Numero = Convert.ToString(Format(Now(), "yyyyMMddHHmmss"))
        mGenerarIDFactura = Numero
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        Me.dtpCalendario.CustomFormat = "dd/MM/yyyy HH:mm:ss"
        MsgBox(Me.dtpCalendario.Value)

    End Sub



    Public Sub mLlenarListaProducto()
        intContador = 0
        drProducto = clsLogicaProducto.mConsultaGenProducto(clsConexion)
        While drProducto.Read
            lvListaProductos.Items.Add(drProducto.Item("idProducto_Servicio"))
            lvListaProductos.Items(intContador).SubItems.Add(drProducto.Item("nombrePro_Ser"))

            intContador = intContador + 1
        End While
    End Sub

    Public Sub mSumaTotalProducto()
        intSumatoria = 0.00
        ivArticulo = 0.00
        intSFilas = Me.dvListadoCompra.Rows.Count

        For Me.intContador = 0 To intSFilas - 2
            intSumatoria += Convert.ToDecimal(dvListadoCompra.Rows(intContador).Cells("TOTAL").Value)
            ivArticulo+ =  Convert.ToDecimal(dvListadoCompra.Rows(intContador).Cells("TOTAL").Value)-(Convert.ToDecimal(dvListadoCompra.Item("CANTIDAD", intContador).Value) * Convert.ToDecimal(dvListadoCompra.Item("MONTO", intContador).Value))
        Next
        lblTotal.Text = intSumatoria
        ivaTotal.Text = ivArticulo
    End Sub

    Public Sub mLimpiar()
        Filas = Me.dvListadoCompra.Rows.GetRowCount(DataGridViewElementStates.Displayed)
        For Me.intContador = 0 To Filas - 2
            dvListadoCompra.Rows.Clear()  'para limpiarlo
            dvListadoCompra.DataSource = Nothing 'para limpiar cualquier registro internamente y desenlazar el control de la BD
        Next
        lvListaProductos.Items.Clear()

        mLlenarListaProducto()
        lblCliente.Text = "Nombre Cliente"
        lblNFactura.Text = mGenerarIDFactura()
        lblTotal.Text = ""
        ivaTotal.Text=""
        rbContado.Checked = False
        rbCredito.Checked = False
    End Sub

    Public Function costoProducto(codex) As Decimal
        drProducto = clsLogicaProducto.mConsultaProductoString(codex, clsConexion)
        Try
            If (drProducto.Read()) Then

                costoPro = Convert.ToDecimal(drProducto.Item("costoPro_Ser"))
            End If

        Catch ex As Exception
            MsgBox("Error costo")
        End Try


        Return costoPro
    End Function
#End Region






End Class