﻿'Representa una conexión abierta a una base de datos MySql 
Imports MySql.Data.MySqlClient

'Esta clase nos permite realizar todos los métodos necesarios para mostrar en la ventana frmConsultaEspecifica
'se pueda visualizar la información de un usuario que se encuentre registrado en la base de datos 
Public Class frmConsultaEspecificaUsuario

#Region "Atributos"

    'Creación de la instancia y referencia de la clase clsConexion
    Dim clConexion As AccesoDatos.clsConexion
    'Creación de la instancia y referencia de la clase clsLogicaUsuario
    Dim clUsuario As New LogicaNegocios.clsLogicaUsuario
    'Creación de la instancia y referencia de la clase clsEntidadUsuario
    Dim clEntidadUsuario As New EntidadNegocios.clsEntidadUsuario
    'Creación de la variable drUsuario de tipo MySqlDataReader que permite leer la infomación de un ususario en 
    ' la base de datos
    Dim drUsuario As MySql.Data.MySqlClient.MySqlDataReader
    'Creación de las variables strLogin y strNombre  de tipo String
    Dim strLogin, strNombre As String
    'Creación de la variable intContador de tipo Integer
    Dim intContador As Integer
    'Creación de la variable dataT de Tipo DataTable
    Dim dataT As DataTable
    'Creación de la variable frmUsuarios que este tipo frmUsuarios la cual
    'hace referencia a la ventana del mismo nombre
    Dim frmMenu As frmMDIMenuPrincipal
#End Region

    '************************************************************************************************************
    'Se crea el constructor de la clase, al cual se le pasa por parámetros la clase clsConexion
    Public Sub New(pConexion As AccesoDatos.clsConexion, pFrmMenu As frmMDIMenuPrincipal)

        'Se inicializa la referencia anteriormente creada en la región atributos con la que ingresa 
        ' por parámetros en el constructor
        clConexion = pConexion
        'Se inicializa la referencia anteriormente creada en la región atributos con la que ingresa 
        ' por parámetros en el constructor
        frmMenu = pFrmMenu

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    '************************************************************************************************************
    ' En esta región se crearán los métodos necesario para el funcionamiento de la clase y ventana
#Region "Metodos"

    'El método mConsultaNombre nos permite realizar la consulta dinámica de un usuario por medio del nombre, la parte dinámica 
    'consiste en la búsqueda de todos los nombres que tengan las mismas letras hasta llegar al usuario que deseamos consultar 
    Public Sub mConsultaNombre()

        'Se inicializa la strNombre igual a vacio
        strNombre = ""
        'Se inicializa la intContador igual a 0
        intContador = 0
        'Se asigna a la variable strNombre la información que contenga txtNombre
        strNombre = txtNombre.Text

        ' Se establece una condición para verificar que la variable strNombre no se encuentre vacía
        If strNombre = "" Then
            'Se borra la información que posee el list view 
            lvvConsultaUsu.Items.Clear()
        Else
            'Se borra la información que posee el list view 
            lvvConsultaUsu.Items.Clear()

            'Control para evitar errores en caso que no se encuentre el nombre 
            Try

                'Se le indica a la variable que lea la información proveniente del método mConsultarUsuarioNombre
                'el cual pertenece a la clsLogicaUsuario y además se le envía la variable strNombre y clConexión
                drUsuario = clUsuario.mConsultarUsuarioNombre(strNombre, clConexion)

                'Se crea un bucle, mientras se cumpla la condición de la lectura del drUsuario 
                While drUsuario.Read

                    'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base 
                    ' de datos en el campo login
                    lvvConsultaUsu.Items.Add(drUsuario.Item("login"))
                    'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base 
                    ' de datos en el campo nombre
                    lvvConsultaUsu.Items(intContador).SubItems.Add(drUsuario.Item("nombre"))
                    'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base 
                    ' de datos en el campo apellido1Usu
                    lvvConsultaUsu.Items(intContador).SubItems.Add(drUsuario.Item("apellido1Usu"))
                    'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base 
                    ' de datos en el campo apellido2Usu
                    lvvConsultaUsu.Items(intContador).SubItems.Add(drUsuario.Item("apellido2usu"))

                    'Se incrementa el contador de uno en uno
                    intContador += 1
                End While 'Fin del while
                'Examina en orden textual para determinar si controla la excepción
            Catch ex As Exception
            End Try 'Fin del Try
        End If 'Fin del if
    End Sub

    'Este evento se produce cuando el contenido del cuadro de texto cambia entre los envíos al servidor.
    Private Sub txtLogin_TextChanged(sender As Object, e As EventArgs) Handles txtLogin.TextChanged
        'Se llama al método mConsultaLogin
        mConsultaLogin()
    End Sub

    'Este evento se produce cuando el contenido del cuadro de texto cambia entre los envíos al servidor.
    Private Sub txtNombre_TextChanged(sender As Object, e As EventArgs) Handles txtNombre.TextChanged
        'Se llama al método mConsultaNombre
        mConsultaNombre()
    End Sub

    'Evento que tiene lugar cuando se hace clic sobre el botón btnRegresar
    Private Sub btnRegresar_Click_1(sender As Object, e As EventArgs) Handles btnRegresar.Click
        Close()
        'Se cierra la ventana
    End Sub

    'Evento que tiene lugar cuando se hace clic sobre el botón btnRegresar
    Private Sub btnRegresar_Click(sender As Object, e As EventArgs)
        'Control que nos permite cambiar el estado de la ventana a False 
        SetVisibleCore(False)
        'Muestra el formulario al que llamamos en este caso frmMenu
        frmMenu.Show()
    End Sub 'Fin del evento btnRegresar_Click

    Private Sub frmConsultaEspecificaUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
    End Sub


    'El método mConsultaLogin nos permite realizar la consulta dinámica de un usuario por medio del nombre, la parte dinámica 
    'consiste en la búsqueda de todos los logins que tengan las mismas letras hasta llegar al usuario que deseamos consultar 
    Public Sub mConsultaLogin()

        'Se inicializa la strLogin igual a vacio
        strLogin = ""
        'Se inicializa la intContador=0
        intContador = 0
        'Se asigna a la variable strNombre la información que contenga txtNombre
        strLogin = txtLogin.Text

        'Se establece una condición para verificar que la variable strLogin no se encuentre vacía
        If strLogin = "" Then
            'Se borra la información que posee el list view 
            lvvConsultaUsu.Items.Clear()
        Else
            'Se borra la información que posee el list view 
            lvvConsultaUsu.Items.Clear()

            'Control para evitar errores
            Try
                'Se le indica a la variable que lea la información proveniente del método mConsultarUsuarioLogin2
                'el cual pertenece a la clsLogicaUsuario y además se le envía la variable strLogin y clConexión
                drUsuario = clUsuario.mConsultarUsuarioLogin2(strLogin, clConexion)

                'Se crea un bucle, mientras se cumpla la condición de la lectura del drUsuario 
                While drUsuario.Read
                    'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base 
                    ' de datos en el campo login
                    lvvConsultaUsu.Items.Add(drUsuario.Item("login"))
                    'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base 
                    ' de datos en el campo nombre
                    lvvConsultaUsu.Items(intContador).SubItems.Add(drUsuario.Item("nombre"))
                    'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base 
                    ' de datos en el campo apellido1Usu
                    lvvConsultaUsu.Items(intContador).SubItems.Add(drUsuario.Item("apellido1Usu"))
                    'Al list view lvConsultaUsu se le agrega un nuevo item y además se le pasa la información que posee la base 
                    ' de datos en el campo apellido2Usu
                    lvvConsultaUsu.Items(intContador).SubItems.Add(drUsuario.Item("apellido2usu"))
                    'Se incrementa el contador de uno en uno
                    intContador += 1
                End While 'Fin del while
                'Examina en orden textual para determinar si controla la excepción
            Catch ex As Exception
            End Try 'Fin del Try
        End If 'Fin del If
    End Sub 'Fin del método mConsultarLogin


#End Region




End Class