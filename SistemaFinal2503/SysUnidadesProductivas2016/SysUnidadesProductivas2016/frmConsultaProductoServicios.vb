﻿Imports MySql.Data.MySqlClient

Public Class frmConsultaProductoServicios
#Region "Atributos"
    Dim clConexion As AccesoDatos.clsConexion
    Dim clProducto As New LogicaNegocios.clsLogicaProductoServicio
    Dim drProducto As MySql.Data.MySqlClient.MySqlDataReader
    Dim intContador As Integer
    Dim frmProductos As frmProductosServicios
    Dim strCodigo As String

#End Region

    Public Sub New(pConexion As AccesoDatos.clsConexion)

        clConexion = pConexion

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub frmConsultaProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
        Me.drProducto = clProducto.mConsultaGenProducto(clConexion)

        intContador = 0

        While drProducto.Read
            lvConsultaPro.Items.Add(drProducto.Item("idProducto_Servicio"))
            lvConsultaPro.Items(intContador).SubItems.Add(drProducto.Item("nombrePro_Ser"))
            lvConsultaPro.Items(intContador).SubItems.Add(drProducto.Item("descripcionPro_Ser"))
            lvConsultaPro.Items(intContador).SubItems.Add(drProducto.Item("costoPro_Ser"))
            lvConsultaPro.Items(intContador).SubItems.Add(drProducto.Item("precioPro_Ser"))
            lvConsultaPro.Items(intContador).SubItems.Add(drProducto.Item("tipoPro_Ser"))

            intContador += 1
        End While
    End Sub

#Region "Metodos"
    Public ReadOnly Property mRetornarCodigo() As String
        Get
            Return Me.strCodigo
        End Get
    End Property



#End Region
    Private Sub lvConsultaPro_DoubleClick(sender As Object, e As EventArgs) Handles lvConsultaPro.DoubleClick
        Close()
    End Sub

    Private Sub lvConsultaPro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvConsultaPro.SelectedIndexChanged
        Try
            For Me.intContador = 0 To lvConsultaPro.Items.Count - 1
                If lvConsultaPro.Items(intContador).Selected Then
                    strCodigo = lvConsultaPro.Items(intContador).Text
                End If
            Next

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnRegresar_Click(sender As Object, e As EventArgs) Handles btnRegresar.Click
        SetVisibleCore(False)
    End Sub
End Class