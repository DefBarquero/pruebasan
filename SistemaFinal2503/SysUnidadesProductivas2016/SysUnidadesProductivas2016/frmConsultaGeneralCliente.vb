﻿Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadCliente
Imports LogicaNegocios.clsLogicaCliente
Imports MySql.Data.MySqlClient


Public Class frmConsultaGeneralCliente

#Region "Atributos"
    'ACCESO A DATOS
    Dim clsConexion As AccesoDatos.clsConexion
    Dim clsEntidadProvincia As New EntidadNegocios.clsEntidadProvincia
    Dim clsEntidadCanton As New EntidadNegocios.clsEntidadCanton
    Dim clsEntidadDistrito As New EntidadNegocios.clsEntidadDistrito


    'ENTIDAD CLIENTE
    Dim clsEntidadCliente As New EntidadNegocios.clsEntidadCliente
    Dim clsLogicaProvincia As New LogicaNegocios.clsLogicaProvincia
    Dim clsLogicaCanton As New LogicaNegocios.clsLogicaCanton
    Dim clsLogicaDistrito As New LogicaNegocios.clsLogicaDistrito

    'LOGICA CLIENTE
    Dim clsLogicaCliente As New LogicaNegocios.clsLogicaCliente

    'DATAREADER
    Dim drCliente, drProvincia, drCanton, drDistrito As MySql.Data.MySqlClient.MySqlDataReader

    'INTEGER
    Dim intContador As Integer

#End Region

#Region "Herramientas"


    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion
    End Sub

    Private Sub frmConsultaGeneralCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size

        intContador = 0
        drCliente = clsLogicaCliente.mConsultaGeneralCli(clsConexion)
        While drCliente.Read
            lvClientes.Items.Add(drCliente.Item("cedulaCli"))
            lvClientes.Items(intContador).SubItems.Add(drCliente.Item("nombreCli"))
            lvClientes.Items(intContador).SubItems.Add(drCliente.Item("apellido1Cli"))
            lvClientes.Items(intContador).SubItems.Add(drCliente.Item("apellido2Cli"))
            lvClientes.Items(intContador).SubItems.Add(drCliente.Item("estadoCli"))
            lvClientes.Items(intContador).SubItems.Add(drCliente.Item("tipoCli"))
            lvClientes.Items(intContador).SubItems.Add(drCliente.Item("nomEmpresaCli"))
            clsEntidadProvincia.pIdProvincia = drCliente.Item("provinciaCli")
            drProvincia = clsLogicaProvincia.mConsultaProvinciaID(clsEntidadProvincia, clsConexion)
            If drProvincia.Read() Then
                lvClientes.Items(intContador).SubItems.Add(drProvincia.Item("nombreProvincia"))
            End If

            clsEntidadCanton.pIdCanton = drCliente.Item("cantonCli")
            drCanton = clsLogicaCanton.mConsultarCantonID(clsEntidadCanton, clsConexion)

            If drCanton.Read() Then
                lvClientes.Items(intContador).SubItems.Add(drCanton.Item("nombreCanton"))
            End If

            clsEntidadDistrito.pIdDistrito = drCliente.Item("distritoCli")
            drDistrito = clsLogicaDistrito.mConsultarDistritoID(clsEntidadDistrito, clsConexion)
            If drDistrito.Read() Then
                lvClientes.Items(intContador).SubItems.Add(drDistrito.Item("nombreDistrito"))
            End If

            lvClientes.Items(intContador).SubItems.Add(drCliente.Item("otrasSenasCli"))

            intContador = intContador + 1

        End While
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        SetVisibleCore(False)
    End Sub
#End Region

End Class