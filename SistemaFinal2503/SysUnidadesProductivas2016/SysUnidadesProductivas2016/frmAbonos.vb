﻿
Imports AccesoDatos.clsConexion
Imports EntidadNegocios
Imports LogicaNegocios
Imports MySql.Data.MySqlClient

Public Class frmAbonos

#Region "Atributos"
    Dim clsConexion As AccesoDatos.clsConexion
    Dim clsEntidadCliente As New EntidadNegocios.clsEntidadCliente
    Dim clsEntidadProducto As New EntidadNegocios.clsEntidadProductoServicio
    Dim clsEntidadFacturaDet As New EntidadNegocios.clsEntidadFacturaDet
    Dim clsEntidadFacturaEnc As New EntidadNegocios.clsEntidadFacturaEnc
    Dim clsEntidadFacturaCostoEnc As New EntidadNegocios.clsEntidadFacturaCostoEnc
    Dim clsEntidadFacturaCostoDet As New EntidadNegocios.clsEntidadFacturaCostoDet
    Dim clsEntidadAbono As New EntidadNegocios.clsEntidadAbono

    Dim clsLogicaCliente As New LogicaNegocios.clsLogicaCliente
    Dim clsLogicaProducto As New LogicaNegocios.clsLogicaProductoServicio
    Dim clsLogicaFacturaDet As New LogicaNegocios.clsLogicaFacturaDet
    Dim clsLogicaFacturaEnc As New LogicaNegocios.clsLogicaFacturaEnc
    Dim clsLogicaFacturaCostoEnc As New LogicaNegocios.clsLogicaFactutaCostoEnc
    Dim clsLogicaFacturaCostoDet As New LogicaNegocios.clsLogicaFacturaCostoDet
    Dim clsLogicaAbono As New LogicaNegocios.clsLogicaAbono

    Dim drCliente, drProducto, drFacturaEnc, drFacturaEnc2, drFacturaDet, drFacturaDet2, drAbono As MySql.Data.MySqlClient.MySqlDataReader
    Dim strCedulaCliente, strCodigoProducto, strFecha, codex, strNumFactura, debe As String
    Dim intContador, intContadorListaProducto, intContadorListaCompra, intContadorListaCompra2, intIndex, Filas, intCampoCantidad, intSFilas, intIdAbono As Integer

    Private Sub txtDeposito_TextChanged(sender As Object, e As EventArgs) 

    End Sub

    Dim bolAgregrarAbono As Boolean
    Dim montoFac, deposita, intTotal, montoActual, saldoActual, saldo, finPago As Decimal


#End Region

#Region "Constructor & Load"
    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

        clsConexion = pConexion


    End Sub

    'Metodo donde se carga la venta
    Private Sub frmAbonos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
    End Sub
#End Region

#Region "Botones"
    'Con este boton se busca el cliente que va a realizar el abono y carga los datos

    Private Sub btnIr_Click(sender As Object, e As EventArgs) Handles btnIr.Click
        Me.mLimpiar()


        strCedulaCliente = ""
        Dim frmBuscarCliente As New frmClienteBuscar(clsConexion)
        With (frmBuscarCliente)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With

        If frmBuscarCliente.mRetornarCedulaCli <> "" Then
            strCedulaCliente = frmBuscarCliente.mRetornarCedulaCli
            clsEntidadCliente.pCedulaCli = strCedulaCliente
            drCliente = clsLogicaCliente.mBuscarCliente(clsEntidadCliente, clsConexion)

            If drCliente.Read Then
                lblCliente.Text = drCliente.Item("nombrecli") + " " + drCliente.Item("apellido1Cli") + " " + drCliente.Item("apellido2Cli")
                intContador = 0
                drFacturaEnc = mBuscarFactura(drCliente)
                While drFacturaEnc.Read
                    strFecha = ""
                    clsEntidadFacturaDet.pIdFactura = drFacturaEnc.Item("idFactura")
                    strFecha = drFacturaEnc.Item("fechaFactura")
                    'intTotal += Convert.ToDecimal(drFacturaEnc.Item("total"))

                    drFacturaDet = clsLogicaFacturaDet.mConsultaFacturaDet(clsEntidadFacturaDet, clsConexion)
                    While drFacturaDet.Read
                        lvLista.Items.Add(drFacturaDet.Item("idFactura"))
                        lvLista.Items(intContador).SubItems.Add(strFecha)
                        clsEntidadProducto.pIdProductoSer = drFacturaDet.Item("idProducto")
                        drProducto = clsLogicaProducto.mConsultaProducto(clsEntidadProducto, clsConexion)
                        If drProducto.Read Then
                            lvLista.Items(intContador).SubItems.Add(drProducto.Item("nombrePro_Ser"))

                        End If

                        lvLista.Items(intContador).SubItems.Add(drFacturaDet.Item("cantidad"))
                        lvLista.Items(intContador).SubItems.Add(drFacturaDet.Item("subtotal"))

                        intContador = intContador + 1
                    End While ' Fin del while del detalle
                End While 'Fin del while de las facturas
            End If 'Fin del if si encontro clientes


        End If 'Fin del if buscarCliente
    End Sub

   ' Private Sub txtDeposito_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDeposito.KeyPress
        '::::::::::::::::::::::::::::::::::::::::::::::::::::::::
       ' clsEntidadAbono.pIdFactura = txtNumFactura.Text
       ' drAbono = clsLogicaAbono.mBuscarFactura(clsEntidadAbono, clsConexion)
       ' While drAbono.Read()
       ' saldoActual+ = drAbono.Item("saldoActual")
       ' End While
       ' If Asc(e.KeyChar) = 13 Then

        '    If saldoActual = 0.0 Then
        '      saldoActual = Convert.ToDecimal(txtMontoFac.Text - txtDeposito.Text)
        '        txtDeuda.Text = saldoActual
        '        txtDeposito.Text = ""
    

   '         Else
   '             If txtDeposito.Text <> "" Then
   '                 Dim deposito As Decimal
   '                 deposito = Convert.ToDecimal(txtDeposito.Text)
   '                 txtDeuda.Text = Me.saldoActual - deposito
   '                 txtDeposito.Text = ""
   '             End If 'Fin del if  txtDeposito vacio
   '         End If 'Fin del if txtDeuda vacio
   '     End If 'FIn de
   ' End Sub

    'Cuando se digita en el campo del deposito resta el valor de la factura
    Private Sub txtDeposito_Keyup(sender As Object, e As EventArgs) Handles txtDeposito.Keyup

        clsEntidadAbono.pIdFactura = txtNumFactura.Text
            drAbono = clsLogicaAbono.mBuscarFactura(clsEntidadAbono, clsConexion)
            if drAbono.Read()
                 saldoActual = Convert.ToDouble(drAbono.Item("saldoActual"))
                 txtDeuda.Text()= saldoActual
                Else 
                txtDeuda.Text()= txtMontoFac.Text()
                saldoActual = Convert.ToDouble(txtMontoFac.Text())
            End If

        If txtDeposito.Text() = ""
        Else
            montoActual = saldoActual - Convert.ToDouble(txtDeposito.Text())
            txtDeuda.Text()= montoActual
        End If
    End Sub

   '     clsEntidadAbono.pIdFactura = txtNumFactura.Text
    '    drAbono = clsLogicaAbono.mBuscarFactura(clsEntidadAbono, clsConexion)
     '   saldoActual = Convert.ToDecimal(drAbono.Item("saldoActual"))
      '  If saldoActual = 0.0 Then
       '     saldoActual = Convert.ToDecimal(txtMontoFac.Text - txtDeposito.Text)
        '    txtDeuda.Text = saldoActual
      '  Else
       '     If txtDeposito.Text <> "" Then
        '         txtDeposito.Text = "0"
         '       txtDeuda.Text = Me.saldoActual - Convert.ToDecimal(txtDeposito.Text)
          '  End If 'Fin del if  txtDeposito vacio
       ' End If 'Fin del if txtDeuda vacio
   ' End Sub

    'Boton para salir de la ventana de abonos
    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Close()
    End Sub

    'Boton que permite agregar un abono
    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        If txtDeposito.Text = "" Or lblCliente.Text = "" Or txtDeuda.Text = "" Then

            MessageBox.Show("Debe indicar cuanto depositará el cliente", "ATENCION", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Else

            clsEntidadAbono.pIdAbono = Me.mConsecutivoAbono
            clsEntidadAbono.pCedulaCli = strCedulaCliente
            clsEntidadAbono.pFechaAbono = dtpCalendario.Text
            clsEntidadAbono.pSaldoActual = txtDeuda.Text
            clsEntidadAbono.pSaldoAbonado=txtDeposito.Text

            montoFac = Convert.ToDecimal(txtMontoFac.Text)

            clsEntidadAbono.pMontoCompra = txtMontoFac.Text
            clsEntidadAbono.pIdFactura = txtNumFactura.Text

            finPago = Convert.ToDecimal(txtDeuda.Text)
            If finPago = 0.0 Then
                clsLogicaFacturaEnc.mActualizarEstado(clsEntidadAbono, clsConexion)
                MessageBox.Show("Ha saldado su deuda", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If 'Fin del pago de la factura

            bolAgregrarAbono = clsLogicaAbono.mIngresarAbono(clsEntidadAbono, clsConexion)
            If bolAgregrarAbono = True Then
                MessageBox.Show("Abono agregado correctamente", "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.mLimpiar()
            Else
                MessageBox.Show("No se hizo el abono", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If 'Fin del if si agrego el abono
        End If 'Fin del if si no indicó cuanto es el abono
    End Sub

    'Cuando se selecciona una factura, carga la informacion
    Private Sub lvLista_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvLista.SelectedIndexChanged
        txtDeuda.Text = ""
        txtDeposito.Text = ""
        saldo = 0.0

        Try
            For Me.intContador = 0 To lvLista.Items.Count - 1
                If lvLista.Items(intContador).Selected Then
                    strNumFactura = lvLista.Items(intContador).Text
                    clsEntidadFacturaEnc.pIdFactura = strNumFactura

                    drFacturaEnc = clsLogicaFacturaEnc.mConsultaNumFacturaEnc(clsEntidadFacturaEnc, clsConexion)
                    If drFacturaEnc.Read Then
                        txtNumFactura.Text = strNumFactura
                        txtMontoFac.Text = drFacturaEnc.Item("total")

                        'LLenado del campo de deuda para la resta y consulta obligatoria'
                        clsEntidadAbono.pIdFactura = txtNumFactura.Text
                        drAbono = clsLogicaAbono.mBuscarFactura(clsEntidadAbono, clsConexion)
                        if drAbono.Read()
                             saldoActual = Convert.ToDouble(drAbono.Item("saldoActual"))
                             txtDeuda.Text()= saldoActual
                            Else 
                            txtDeuda.Text()= txtMontoFac.Text()
                            saldoActual = Convert.ToDouble(txtMontoFac.Text())
                        End If
                        'FIn del metodo para extraer la info de deuda'

                        clsEntidadAbono.pIdFactura = drFacturaEnc.Item("idFactura")
                        drAbono = clsLogicaAbono.mBuscarFactura(clsEntidadAbono, clsConexion)
                        If drAbono.Read Then
                            debe = Me.mDeudaActual(clsEntidadAbono)
                            If debe = "" Then
                                Me.saldoActual = 0.0
                                txtDeuda.Text = 0.0
                            Else
                                Me.saldoActual = Convert.ToDecimal(debe)
                                txtDeuda.Text = debe
                            End If 'Fin del if saldo distinto de 0.0
                        Else
                            Me.saldoActual = 0.0
                        End If 'Fin del if sihay algun abono previo hecho
                    End If
                End If
            Next

        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "Metodos"
    'Metodo que permite buscar las facturas que pertenezcan a un cliente en especifico que sean a credito
    Private Function mBuscarFactura(drCliente As MySql.Data.MySqlClient.MySqlDataReader) As MySql.Data.MySqlClient.MySqlDataReader
        clsEntidadFacturaEnc.pCedulaCli = drCliente.Item("cedulaCli")
        drFacturaEnc = clsLogicaFacturaEnc.mConsultaFacturaEncCed(clsEntidadFacturaEnc.pCedulaCli, clsConexion)
        If drFacturaEnc.Read Then
            drFacturaEnc2 = clsLogicaFacturaEnc.mConsultaFacturaCredito(clsEntidadFacturaEnc.pCedulaCli, clsConexion)
        End If
        Return drFacturaEnc2
    End Function

    'Metodo donde se limpian los campos y la lista de las facturas a cobrar
    Private Sub mLimpiar()
        lvLista.Clear()
        Dim idFactura As New ColumnHeader
        With idFactura
            .Text = "N° FACTURA"
            .TextAlign = HorizontalAlignment.Left
            .Width = 150
        End With
        Dim fechaFactura As New ColumnHeader
        With fechaFactura
            .Text = "FECHA"
            .TextAlign = HorizontalAlignment.Center
            .Width = 150
        End With
        Dim producto As New ColumnHeader
        With producto
            .Text = "PRODUCTO"
            .TextAlign = HorizontalAlignment.Center
            .Width = 200
        End With
        Dim cantidad As New ColumnHeader
        With cantidad
            .Text = "CANTIDAD"
            .TextAlign = HorizontalAlignment.Center
            .Width = 150
        End With
        Dim subTotal As New ColumnHeader
        With subTotal
            .Text = "TOTAL"
            .TextAlign = HorizontalAlignment.Center
            .Width = 150
        End With
        Me.lvLista.Columns.Add(idFactura)
        Me.lvLista.Columns.Add(fechaFactura)
        Me.lvLista.Columns.Add(producto)
        Me.lvLista.Columns.Add(cantidad)
        Me.lvLista.Columns.Add(subTotal)

        intTotal = 0
        lblCliente.Text = "XXXXXX XXXXX XXXX"
        txtDeposito.Text = ""
        txtDeuda.Text = ""
        txtMontoFac.Text = ""
        txtNumFactura.Text = ""

    End Sub

    'Metodo que me indica cual va a ser el idAbono
    Private Function mConsecutivoAbono()
        intIdAbono = 0
        codex = ""
        drAbono = clsLogicaAbono.mUltimoAbono(clsConexion)
        If drAbono.Read Then
            codex = Convert.ToString(drAbono.Item("idAbono"))
            If codex = "" Then
                Return intIdAbono
            Else
                intIdAbono = Convert.ToInt64(drAbono.Item("idAbono"))
                intIdAbono = intIdAbono + 1
                Return intIdAbono
            End If 'Fin del if codex ""

        End If ' Fin del if 
        Return intIdAbono
    End Function

    'Metodo que me indica cual es el ultimo abono del cliente
    Public Function mDeudaActual(clsEntidadAbono As EntidadNegocios.clsEntidadAbono) As String
        debe = ""
        drAbono = clsLogicaAbono.mUltimoMonto(clsEntidadAbono, clsConexion)
        If drAbono.Read Then
            Return debe = drAbono.Item("saldoActual")
        End If
        Return debe
    End Function




#End Region


End Class