﻿Imports System.Windows.Forms
Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Public Class frmMDIMenuPrincipal

#Region "Atributos"
    Dim clsConexion As AccesoDatos.clsConexion
    Dim clsEntidadUsuario As EntidadNegocios.clsEntidadUsuario
    Dim clsEntidadPeModulo As New EntidadNegocios.clsEntidadPermisosUsersModulo

    Dim clsLogicaPeModula As New LogicaNegocios.clsLogicaPermisosUsersModulo

    Dim drPermisoModulo As MySql.Data.MySqlClient.MySqlDataReader

    Dim bolModificar, bolAgregar As Boolean

    Dim frmSistemaAcceso As frmSistemaAcceso

#End Region

#Region "Constructor"
    Public Sub New(pConexion As AccesoDatos.clsConexion, pEntidadUsuario As EntidadNegocios.clsEntidadUsuario, pFrmSistemaAcceso As frmSistemaAcceso)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion
        clsEntidadUsuario = pEntidadUsuario
        frmSistemaAcceso = pFrmSistemaAcceso
    End Sub
#End Region

    Private Sub ShowNewForm(ByVal sender As Object, ByVal e As EventArgs)
        ' Cree una nueva instancia del formulario secundario.
        Dim ChildForm As New System.Windows.Forms.Form
        ' Conviértalo en un elemento secundario de este formulario MDI antes de mostrarlo.
        ChildForm.MdiParent = Me

        m_ChildFormNumber += 1
        ChildForm.Text = "Ventana " & m_ChildFormNumber

        ChildForm.Show()
    End Sub

    Private Sub OpenFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim OpenFileDialog As New OpenFileDialog
        OpenFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        OpenFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*"
        If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = OpenFileDialog.FileName
            ' TODO: agregue código aquí para abrir el archivo.
        End If
    End Sub

    Private Sub SaveAsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim SaveFileDialog As New SaveFileDialog
        SaveFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        SaveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*"

        If (SaveFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = SaveFileDialog.FileName
            ' TODO: agregue código aquí para guardar el contenido actual del formulario en un archivo.
        End If
    End Sub


    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.Close()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Utilice My.Computer.Clipboard para insertar el texto o las imágenes seleccionadas en el Portapapeles
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Utilice My.Computer.Clipboard para insertar el texto o las imágenes seleccionadas en el Portapapeles
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Utilice My.Computer.Clipboard.GetText() o My.Computer.Clipboard.GetData para recuperar la información del Portapapeles.
    End Sub

    'Private Sub ToolBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.ToolStrip.Visible = Me.ToolBarToolStripMenuItem.Checked
    'End Sub

    'Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Me.StatusStrip.Visible = Me.StatusBarToolStripMenuItem.Checked
    'End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Cierre todos los formularios secundarios del principal.
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer

    Private Sub frmMDIMenuPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Me.PictureBox2.Parent = Me
        Me.Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size

    End Sub

    Private Sub ClientesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ClientesToolStripMenuItem.Click
        Me.mVerificarPermisos(clsEntidadUsuario.pLogin, "004")
        Dim frmClientes As New frmClientes(clsConexion)
        frmClientes.btnModificar.Enabled = bolModificar
        frmClientes.btnAgregar.Enabled = bolAgregar

        With (frmClientes)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With


    End Sub

    Private Sub ProveedoresToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProveedoresToolStripMenuItem.Click
        Dim frmProveedor As New frmProveedores(clsConexion)
        Me.mVerificarPermisos(clsEntidadUsuario.pLogin, "006")
        frmProveedor.btnAgregar.Enabled = bolAgregar
        frmProveedor.btnModificar.Enabled = bolModificar

        With (frmProveedor)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With

    End Sub









    Private Sub UsuariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuariosToolStripMenuItem.Click

        Dim frmUsuario As New frmUsuarios(clsConexion, Me)
        Me.mVerificarPermisos(clsEntidadUsuario.pLogin, "003")
        frmUsuario.btnAgregar.Enabled = bolAgregar
        frmUsuario.btnModificar.Enabled = bolModificar
        With (frmUsuario)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With
    End Sub




    Private Sub ProductosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProductosToolStripMenuItem.Click
        Dim frmProducto As New frmProductosServicios(clsConexion, Me)
        Me.mVerificarPermisos(clsEntidadUsuario.pLogin, "005")
        frmProducto.btnAgregar.Enabled = bolAgregar
        frmProducto.btnModificar.Enabled = bolModificar
        With (frmProducto)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With
    End Sub





    Private Sub UsuariosToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles UsuariosToolStripMenuItem1.Click

    End Sub

    Private Sub FacturaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FacturaToolStripMenuItem.Click
        Dim frmFactura As New frmFacturas(clsConexion, clsEntidadUsuario)
        Me.mVerificarPermisos(clsEntidadUsuario.pLogin, "007")
        frmFactura.btnFacturar.Enabled = bolAgregar
        With (frmFactura)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With
    End Sub

    Private Sub PermisosUsuarioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PermisosUsuarioToolStripMenuItem.Click
        Me.mVerificarPermisos(clsEntidadUsuario.pLogin, "008")
        Dim frmPermisoUser As New frmPermisosUsuarios(clsConexion, clsEntidadUsuario.pLogin)
        frmPermisoUser.btnAgregar.Enabled = bolAgregar
        frmPermisoUser.btnModificar.Enabled = bolModificar
        If bolAgregar = False And bolModificar = False Then
            frmPermisoUser.cbxUsuarios.Enabled = False
            frmPermisoUser.dgvVentanaModulos.Enabled = False
            frmPermisoUser.dgvVentanasConsultas.Enabled = False
        End If
        With (frmPermisoUser)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With
    End Sub

    Private Sub UsuarioCGeneralToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles UsuarioCGeneralToolStripMenuItem2.Click
        Dim frmConGeUsuarios As New frmConsultaGeneralUsuario(clsConexion)
        With (frmConGeUsuarios)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub

    Private Sub UsuarioCEspecificaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UsuarioCEspecificaToolStripMenuItem.Click
        Dim frmConEspUsuarios As New frmConsultaEspecificaUsuario(clsConexion, Me)
        With (frmConEspUsuarios)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With
    End Sub

    Private Sub ClienteCGeneralToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ClienteCGeneralToolStripMenuItem1.Click
        Dim frmConGeCliente As New frmConsultaGeneralCliente(clsConexion)
        With (frmConGeCliente)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub

    Private Sub ProductoCGeneralToolStripMenuItem3_Click(sender As Object, e As EventArgs) Handles ProductoCGeneralToolStripMenuItem3.Click
        Dim frmConsuGeneProducto As New frmConsultaProductoServicios(clsConexion)
        With (frmConsuGeneProducto)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With
    End Sub

    Private Sub ProductoCEspecificaToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ProductoCEspecificaToolStripMenuItem1.Click
        Dim frmConEspProducto As New frmConsultaEspecificaProductoServicios(clsConexion, Me)
        With (frmConEspProducto)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub

    Private Sub ProveedorCGeneralToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProveedorCGeneralToolStripMenuItem.Click
        Dim frmConGeProveedor As New frmConsultaGeneralProveedores(clsConexion)
        With (frmConGeProveedor)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub

    Private Sub ProveedorCEspecificaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProveedorCEspecificaToolStripMenuItem.Click
        Dim frmConEspProveedor As New frmConsultaEspecificaProveedores(clsConexion)
        With (frmConEspProveedor)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub

    Private Sub ClienteCEspecificaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ClienteCEspecificaToolStripMenuItem.Click
        Dim frmConEspCliente As New frmConsultaEspecificaCliente(clsConexion)
        With (frmConEspCliente)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub

    Private Sub FacturaCEspecificaToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles FacturaCEspecificaToolStripMenuItem2.Click
        Dim frmConsultaEspecificaFactura As New frmConsultaEspecificaFactura(clsConexion)
        With (frmConsultaEspecificaFactura)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub

    Private Sub FacturaCGeneralToolStripMenuItem4_Click(sender As Object, e As EventArgs) Handles FacturaCGeneralToolStripMenuItem4.Click
        Dim frmConsultaGeneralFactura As New frmConsultaGeneralFactura(clsConexion)
        With (frmConsultaGeneralFactura)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub

    Private Sub CerrarSesiónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarSesiónToolStripMenuItem.Click
        Close()

        frmSistemaAcceso.txtContrasena.Text = ""
        frmSistemaAcceso.txtContrasena.PasswordChar = "*"
        frmSistemaAcceso.txtLogin.Text = ""
    End Sub

    Private Sub AbonosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbonosToolStripMenuItem.Click
        Dim frmAbonos As New frmAbonos(clsConexion)
        With (frmAbonos)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub

    Private Sub EspecificoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EspecificoToolStripMenuItem.Click
         Dim frmConsultaEspecificaAbonos As New frmConsultaEspecificaAbonos(clsConexion)
        With (frmConsultaEspecificaAbonos)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub

    Private Sub GeneralToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GeneralToolStripMenuItem.Click
        Dim frmConsultaGeneralAbonos As New frmConsultaGeneralAbonos(clsConexion)
        With (frmConsultaGeneralAbonos)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With
    End Sub



#Region "Metodos"
    Public Sub mVerificarPermisos(ByVal pLogin As String, ByVal pIdVentana As String)
        bolAgregar = False
        bolModificar = False

        drPermisoModulo = clsLogicaPeModula.mCosultarPermisoModulo2(pLogin, pIdVentana, clsConexion)
        If drPermisoModulo.Read Then
            If drPermisoModulo.Item("permisoAgregar") = "SI" Then
                bolAgregar = True
            End If
            If drPermisoModulo.Item("permisoModificar") = "SI" Then
                bolModificar = True
            End If
        End If


    End Sub
#End Region
End Class
