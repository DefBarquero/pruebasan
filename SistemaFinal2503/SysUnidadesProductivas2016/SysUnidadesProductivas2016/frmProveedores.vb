﻿Public Class frmProveedores
    Private Sub frmProveedores_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
        'mFormatosGrid()
        dvgCorreo.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 11)
        dvgTelefono.DefaultCellStyle.Font = New Font("Microsoft Sans Serif", 11)
        dvgTelefono.ForeColor = Color.Black
        dvgCorreo.ForeColor = Color.Black
        mLlenarComboProvincia()

        txtSegApellido.Enabled = False
        txtPriApellido.Enabled = False
        txtOtrasSenas.Enabled = False
        txtNomEmpresa.Enabled = False
        txtNomCliente.Enabled = False
        txtDescripcion.Enabled = False
        dvgCorreo.Enabled = False
        dvgTelefono.Enabled = False
        rbnPersona.Enabled = False
        rbnEmpresa.Enabled = False

    End Sub

#Region "Atributos"
    'CONEXION CON LA BASE DE DATOS
    Dim clsConexion As AccesoDatos.clsConexion

    'EN RELACIÓN A LA INFORMACIÓN DE LOS PROVEEDORES
    'ENTIDADES
    Dim clsEntidadProveedor As New EntidadNegocios.clsEntidadProveedor
    Dim clsEntidadCorreoPro As New EntidadNegocios.clsEntidadCorreoProveedor
    Dim clsEntidadTelefonoPro As New EntidadNegocios.clsEntidadTelefonoProveedor
    Dim clsEntidadProvincia As New EntidadNegocios.clsEntidadProvincia
    Dim clsEntidadCanton As New EntidadNegocios.clsEntidadCanton
    Dim clsEntidadDistrito As New EntidadNegocios.clsEntidadDistrito

    'LOGICA
    Dim clsLogicaProveedor As New LogicaNegocios.clsLogicaProveedor
    Dim clsLogicaCorreoPro As New LogicaNegocios.clsLogicaProveedorCorreo
    Dim clsLogicaTelefonoPro As New LogicaNegocios.clsLogicaProveedorTelefono
    Dim clsLogicaProvincia As New LogicaNegocios.clsLogicaProvincia
    Dim clsLogicaCanton As New LogicaNegocios.clsLogicaCanton
    Dim clsLogicaDistrito As New LogicaNegocios.clsLogicaDistrito

    'DataReader
    Dim drProveedor, drCorreoPro, drTelefonoPro, drProvincia, drCanton, drDistrito As MySql.Data.MySqlClient.MySqlDataReader

    'Boolean 
    Dim bolAgregarPro, bolModificarPro, bolAgregarCorreoPro, bolModificarCorreoPro, bolEliminarCorreoPro, bolAgregarTelPro, bolModificarTelPro, bolEliminarTelPro As Boolean



    'Para aceptar si o no cuando desea agregar informacion
    Dim drResultado As DialogResult

    'Para el diseño de los datagrid
    Dim tamaño, tamaño1 As Size

    'para el manejo de los datadrig
    Dim intCodigo, Filas, x, intContador, duplas As Integer



    Dim strIDCorreo As String


#End Region

#Region "Constructores"
    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        clsConexion = pConexion
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
#End Region

#Region "Botones"
    Private Sub cbxCanton_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxCanton.SelectedIndexChanged
        cbxDistrito.Items.Clear()
        cbxDistrito.Text = "Seleccionar"

        clsEntidadCanton.pNombreCanton = cbxCanton.Text.Trim
        drCanton = clsLogicaCanton.mConsultarCantonNombre(clsEntidadCanton, clsConexion)

        If drCanton.Read Then
            clsEntidadDistrito.pIdCanton = drCanton.Item("idCanton")
            drDistrito = clsLogicaDistrito.mConsultarDistritoIdCanton(clsEntidadDistrito, clsConexion)
            While drDistrito.Read()
                cbxDistrito.Items.Add(drDistrito.Item("nombreDistrito"))
            End While

        End If
    End Sub

    Private Sub cbxProvincia_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxProvincia.SelectedIndexChanged
        cbxCanton.Items.Clear()
        cbxDistrito.Items.Clear()
        cbxCanton.Text = "Seleccionar"
        cbxDistrito.Text = "Seleccionar"


        clsEntidadProvincia.pNombreProvincia = cbxProvincia.Text.Trim
        drProvincia = clsLogicaProvincia.mConsultaProvinciaNombre(clsEntidadProvincia, clsConexion)

        If drProvincia.Read Then

            clsEntidadCanton.pIdProvincia = drProvincia.Item("idProvincia")
            drCanton = clsLogicaCanton.mConsultarCantonIdProvincia(clsEntidadCanton, clsConexion)

            While drCanton.Read
                cbxCanton.Items.Add(drCanton.Item("nombreCanton"))
            End While


        End If
    End Sub


    Private Sub rbnPersona_CheckedChanged(sender As Object, e As EventArgs) Handles rbnPersona.CheckedChanged
        clsEntidadProveedor.pTipoPro = "PERSONA"
        txtNomEmpresa.Enabled = False
        txtNomEmpresa.BackColor = Color.Gray

    End Sub


    Private Sub rbnEmpresa_CheckedChanged(sender As Object, e As EventArgs) Handles rbnEmpresa.CheckedChanged
        clsEntidadProveedor.pTipoPro = "EMPRESA"

        txtNomEmpresa.Enabled = True
        txtNomEmpresa.BackColor = Color.White

    End Sub

    Private Sub btnIr_Click(sender As Object, e As EventArgs) Handles btnIr.Click
        If mktCedula.Text <> "" Then
            clsEntidadProveedor.pCedulaPro = mktCedula.Text
            drProveedor = clsLogicaProveedor.mConsultaCedulaProveedor(clsEntidadProveedor, clsConexion)
            If drProveedor.Read Then
                mInformacionProveedor(mktCedula.Text.Trim)
                btnModificar.Enabled = True

            Else
                drResultado = MessageBox.Show("Cédula no se encuentra asociada a ningún Proveedor. ¿Desea registrarla?", "INFORMACIÓN", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                If drResultado = DialogResult.Yes Then
                    mktCedula.Enabled = False
                    btnAgregar.Enabled = True
                    txtSegApellido.Enabled = True
                    txtPriApellido.Enabled = True
                    txtOtrasSenas.Enabled = True
                    txtNomEmpresa.Enabled = True
                    txtNomCliente.Enabled = True
                    txtDescripcion.Enabled = True
                    dvgCorreo.Enabled = True
                    dvgTelefono.Enabled = True
                    rbnPersona.Enabled = True
                    rbnEmpresa.Enabled = True
                Else
                    mktCedula.Enabled = True
                    mktCedula.Text = ""
                    mktCedula.Mask = "000000000000000000"

                End If


            End If
        End If
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click

        If (rbnEmpresa.Checked = False And rbnPersona.Checked = False) Or cbxCanton.Text = "" Or txtDescripcion.Text = "" Or cbxDistrito.Text = "" Or txtNomCliente.Text = "" Or txtOtrasSenas.Text = "" Or txtPriApellido.Text = "" Or cbxProvincia.Text = "" Or txtSegApellido.Text = "" Or mktCedula.Text = "" Then
            MessageBox.Show("No pueden haber campos vacíos.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            clsEntidadProveedor.pCedulaPro = mktCedula.Text
            clsEntidadProveedor.pNombrePro = txtNomCliente.Text.ToUpper
            clsEntidadProveedor.pApellido1Pro = txtPriApellido.Text.ToUpper
            clsEntidadProveedor.pApellido2Pro = txtSegApellido.Text.ToUpper
            Me.clsEntidadProvincia.pNombreProvincia = Me.cbxProvincia.Text.Trim
            Me.clsEntidadCanton.pNombreCanton = Me.cbxCanton.Text.Trim
            Me.clsEntidadDistrito.pNombreDistrito = Me.cbxDistrito.Text.Trim
            drProvincia = Me.clsLogicaProvincia.mConsultaProvinciaNombre(clsEntidadProvincia, clsConexion)
            If drProvincia.Read Then
                Me.clsEntidadProveedor.pProvinciaPro = drProvincia.Item("idProvincia")
                Me.clsEntidadCanton.pIdProvincia = drProvincia.Item("idProvincia")
            End If

            drCanton = Me.clsLogicaCanton.mConsultarCantonNombreIdProvincia(clsEntidadCanton, clsConexion)
            If drCanton.Read Then
                Me.clsEntidadProveedor.pCantonPro = drCanton.Item("idCanton")
                Me.clsEntidadDistrito.pIdCanton = drCanton.Item("idCanton")
            End If

            drDistrito = Me.clsLogicaDistrito.mConsultaDistritoNombreIdCanton(clsEntidadDistrito, clsConexion)
            If drDistrito.Read Then
                Me.clsEntidadProveedor.pDistritoPro = drDistrito.Item("idDistrito")
            End If


            clsEntidadProveedor.pDescripcionPro = txtDescripcion.Text.ToUpper
            clsEntidadProveedor.pNomEmpresaPro = txtNomEmpresa.Text.ToUpper
            clsEntidadProveedor.pOtrasSenasPro = txtOtrasSenas.Text.ToUpper

            Dim frmConfirmarProveedor As frmConfirmarProveedor = New frmConfirmarProveedor(clsEntidadProveedor, clsConexion, Me, clsEntidadTelefonoPro, clsEntidadCorreoPro, clsEntidadProvincia, clsEntidadCanton, clsEntidadDistrito)
            frmConfirmarProveedor.Show()

            'bolAgregarPro = clsLogicaProveedor.mAgregarProveedor(clsEntidadProveedor, clsConexion)

            'If bolAgregarPro = True Then
            '    'AGREGAR LOS CORREOS DE LOS CLIENTES

            '    Filas = Me.dvgCorreo.Rows.Count

            '    For Me.x = 0 To Filas - 2 '' se le quitan 2 al conteo ya que el ultimo es un registro no usado, y el anterior esta en blanco
            '        drCorreoPro = Me.clsLogicaCorreoPro.mGenerarIDCorreoPro(clsConexion)
            '        If drCorreoPro.Read Then
            '            clsEntidadCorreoPro.pIdCorreoPro = drCorreoPro.Item(0)
            '        End If
            '        clsEntidadCorreoPro.pCorreoPro = Me.dvgCorreo.Rows(x).Cells("EMAIL").Value
            '        clsEntidadCorreoPro.pCedulaPro = Me.mktCedula.Text.Trim
            '        bolAgregarCorreoPro = clsLogicaCorreoPro.mAgregarCorreoPro(clsEntidadCorreoPro, clsConexion)
            '    Next


            '    'Filas = Me.dvgTelefono.Rows.GetRowCount(DataGridViewElementStates.Displayed)
            '    Filas = dvgTelefono.Rows.Count

            '    For Me.x = 0 To Filas - 2 ' se le quitan 2 al conteo ya que el ultimo es un registro no usado, y el anterior esta en blanco
            '        drTelefonoPro = Me.clsLogicaTelefonoPro.mGenerarIDTelefonoPro(clsConexion)
            '        If drTelefonoPro.Read Then
            '            Me.clsEntidadTelefonoPro.pIdTelefonoPro = drTelefonoPro.Item(0)
            '        End If
            '        clsEntidadTelefonoPro.pTelefonoPro = Me.dvgTelefono.Rows(x).Cells("Numero").Value
            '        clsEntidadTelefonoPro.pCedulaPro = Me.mktCedula.Text.Trim
            '        bolAgregarTelPro = Me.clsLogicaTelefonoPro.mAgregarTelefonoPro(clsEntidadTelefonoPro, clsConexion)
            '    Next

            '    MessageBox.Show("Proveedor agregado correctamente.", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '    mLimpiar()
            'Else
            '    MessageBox.Show("Error al agregar el Proveedor.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '    mLimpiar()

            'End If
        End If


    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        mLimpiar()
        Dim frmBuscarProveedor As New frmProveedorBuscar(clsConexion)
        With (frmBuscarProveedor)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With
        If frmBuscarProveedor.mRetornarCedulaPro() <> "" Then


            mktCedula.Focus()
            mktCedula.Text = frmBuscarProveedor.mRetornarCedulaPro()
            mInformacionProveedor(frmBuscarProveedor.mRetornarCedulaPro())
            mktCedula.Enabled = False
            txtNomCliente.Enabled = False
            txtPriApellido.Enabled = False
            txtSegApellido.Enabled = False
        Else
            mLimpiar()
        End If

    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click

        If (rbnEmpresa.Checked = False And rbnPersona.Checked = False) Or cbxCanton.Text = "" Or txtDescripcion.Text = "" Or cbxDistrito.Text = "" Or txtNomCliente.Text = "" Or txtOtrasSenas.Text = "" Or txtPriApellido.Text = "" Or cbxProvincia.Text = "" Or txtSegApellido.Text = "" Or mktCedula.Text = "" Then
            MessageBox.Show("No pueden haber campos vacíos.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            clsEntidadProveedor.pCedulaPro = mktCedula.Text.Trim

            'MODIFICAR

            Me.clsEntidadProvincia.pNombreProvincia = Me.cbxProvincia.Text.Trim
            Me.clsEntidadCanton.pNombreCanton = Me.cbxCanton.Text.Trim
            Me.clsEntidadDistrito.pNombreDistrito = Me.cbxDistrito.Text.Trim
            drProvincia = Me.clsLogicaProvincia.mConsultaProvinciaNombre(clsEntidadProvincia, clsConexion)
            If drProvincia.Read Then
                Me.clsEntidadProveedor.pProvinciaPro = drProvincia.Item("idProvincia")
                Me.clsEntidadCanton.pIdProvincia = drProvincia.Item("idProvincia")
            End If

            drCanton = Me.clsLogicaCanton.mConsultarCantonNombreIdProvincia(clsEntidadCanton, clsConexion)
            If drCanton.Read Then
                Me.clsEntidadProveedor.pCantonPro = drCanton.Item("idCanton")
                Me.clsEntidadDistrito.pIdCanton = drCanton.Item("idCanton")
            End If

            drDistrito = Me.clsLogicaDistrito.mConsultaDistritoNombreIdCanton(clsEntidadDistrito, clsConexion)
            If drDistrito.Read Then
                Me.clsEntidadProveedor.pDistritoPro = drDistrito.Item("idDistrito")
            End If

            clsEntidadProveedor.pDescripcionPro = txtDescripcion.Text.ToUpper
            clsEntidadProveedor.pNomEmpresaPro = txtNomEmpresa.Text.ToUpper
            clsEntidadProveedor.pOtrasSenasPro = txtOtrasSenas.Text.ToUpper

            bolModificarPro = clsLogicaProveedor.mModificarProveedor(clsEntidadProveedor, clsConexion)

            If bolModificarPro = True Then
                MessageBox.Show("Proveedor modificado correctamente.", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information)
                'Filas = Me.dvgTelefono.Rows.GetRowCount(DataGridViewElementStates.Displayed)
                Filas = Me.dvgTelefono.Rows.Count

                For Me.x = 0 To Filas - 2
                    If Me.dvgTelefono.Rows(x).Cells("Numero").Value = "" Then
                        clsEntidadTelefonoPro.pIdTelefonoPro = Me.dvgTelefono.Rows(x).Cells("IdTelefono").Value
                        bolEliminarTelPro = Me.clsLogicaTelefonoPro.mEliminarTelPro(clsEntidadTelefonoPro, clsConexion)
                        If bolEliminarTelPro = True Then
                            'MsgBox("ELIMINO TELEFONO")
                        Else
                            'MsgBox("NO ELIMINO TELEFONO")
                        End If

                    Else
                        'si el campo idtelefono es diferente de vacio
                        If Me.dvgTelefono.Rows(x).Cells("IdTelefono").Value <> "" Then
                            clsEntidadTelefonoPro.pTelefonoPro = Me.dvgTelefono.Rows(x).Cells("Numero").Value
                            clsEntidadTelefonoPro.pIdTelefonoPro = Me.dvgTelefono.Rows(x).Cells("IdTelefono").Value
                            clsEntidadTelefonoPro.pCedulaPro = Me.mktCedula.Text.Trim
                            bolModificarTelPro = clsLogicaTelefonoPro.mModificarTelefonoPro(clsEntidadTelefonoPro, clsConexion)
                        Else
                            drTelefonoPro = Me.clsLogicaTelefonoPro.mGenerarIDTelefonoPro(clsConexion)
                            If drTelefonoPro.Read Then
                                Me.clsEntidadTelefonoPro.pIdTelefonoPro = drTelefonoPro.Item(0)
                                clsEntidadTelefonoPro.pTelefonoPro = Me.dvgTelefono.Rows(x).Cells("Numero").Value
                                clsEntidadTelefonoPro.pCedulaPro = Me.mktCedula.Text.Trim
                                bolAgregarTelPro = Me.clsLogicaTelefonoPro.mAgregarTelefonoPro(clsEntidadTelefonoPro, clsConexion)
                                If bolAgregarTelPro = True Then
                                    '  MsgBox("agrego m tekefono")
                                Else
                                    '   MsgBox("no agrego m telefono")
                                End If
                            End If

                        End If
                    End If


                Next

                'Filas = Me.dvgCorreo.Rows.GetRowCount(DataGridViewElementStates.Displayed)
                Filas = Me.dvgCorreo.Rows.Count
                For Me.x = 0 To Filas - 2
                    If Me.dvgCorreo.Rows(x).Cells("EMAIL").Value = "" Then
                        clsEntidadCorreoPro.pIdCorreoPro = Me.dvgCorreo.Rows(x).Cells("IdEmail").Value
                        bolEliminarCorreoPro = Me.clsLogicaCorreoPro.mEliminarCorreoPro(clsEntidadCorreoPro, clsConexion)
                        If bolEliminarCorreoPro = True Then
                            ' MsgBox("elimino correo pro")
                        Else
                            'MsgBox("no elimino correo pro")
                        End If
                    Else
                        If Me.dvgCorreo.Rows(x).Cells("IdEmail").Value <> "" Then
                            clsEntidadCorreoPro.pIdCorreoPro = Me.dvgCorreo.Rows(x).Cells("IdEmail").Value
                            clsEntidadCorreoPro.pCorreoPro = Me.dvgCorreo.Rows(x).Cells("EMAIL").Value
                            clsEntidadCorreoPro.pCedulaPro = Me.mktCedula.Text.Trim

                            bolModificarCorreoPro = clsLogicaCorreoPro.mModificarCorreoPro(clsEntidadCorreoPro, clsConexion)
                            If bolModificarCorreoPro = True Then
                                '   MsgBox("correo modificado")
                            Else
                                '  MsgBox("correo no modificado")
                            End If
                        Else
                            drCorreoPro = Me.clsLogicaCorreoPro.mGenerarIDCorreoPro(clsConexion)
                            If drCorreoPro.Read Then
                                clsEntidadCorreoPro.pIdCorreoPro = drCorreoPro.Item(0)
                                clsEntidadCorreoPro.pCorreoPro = Me.dvgCorreo.Rows(x).Cells("EMAIL").Value
                                clsEntidadCorreoPro.pCedulaPro = Me.mktCedula.Text.Trim
                                bolAgregarCorreoPro = clsLogicaCorreoPro.mAgregarCorreoPro(clsEntidadCorreoPro, clsConexion)
                                If bolAgregarCorreoPro = True Then
                                    '     MsgBox("correo m agredado")
                                Else
                                    '    MsgBox("correo m no agregado")
                                End If
                            End If

                        End If
                    End If

                Next
                mLimpiar()
                mktCedula.Enabled = True
                txtNomCliente.Enabled = True
                txtPriApellido.Enabled = True
                txtSegApellido.Enabled = True

            Else
                MessageBox.Show("Error al modificar el Proveedor.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                mLimpiar()
                mktCedula.Enabled = True
                txtNomCliente.Enabled = True
                txtPriApellido.Enabled = True
                txtSegApellido.Enabled = True
            End If



        End If



    End Sub

    Private Sub btnConsultar_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        SetVisibleCore(False)

    End Sub


#End Region

#Region "Metodos"
    Public Sub mFormatosGrid()
        tamaño = New Size(423, 122)
        Me.dvgCorreo.Size = tamaño

        tamaño1 = New Size(423, 122)
        Me.dvgTelefono.Size = tamaño1
        'formato a las columnas, el encabezado
        Dim columnHeaderStyle As New DataGridViewCellStyle
        columnHeaderStyle.BackColor = Color.Beige
        columnHeaderStyle.Font = New Font("Microsoft Sans Serif", 9, FontStyle.Bold)


        Dim TxtIdEmaColumn As New DataGridViewTextBoxColumn
        TxtIdEmaColumn.Name = "idEmail"
        TxtIdEmaColumn.Width = 180
        Me.dvgCorreo.Columns.Add(TxtIdEmaColumn)
        Me.dvgCorreo.Columns("idEmail").Visible = False

        'para escribir el correo, un campo
        Dim TxtEmaColumn As New DataGridViewTextBoxColumn
        TxtEmaColumn.Name = "Correo Electronico"
        TxtEmaColumn.Width = 220
        Me.dvgCorreo.ColumnHeadersDefaultCellStyle.ApplyStyle(columnHeaderStyle)
        Me.dvgCorreo.Columns.Add(TxtEmaColumn)



        Dim TxtNumTelColumn As New DataGridViewTextBoxColumn
        TxtNumTelColumn.Name = "Numero"
        TxtNumTelColumn.Width = 220
        Me.dvgTelefono.Columns.Add(TxtNumTelColumn)

        Dim TxtIdTelefColumn As New DataGridViewTextBoxColumn
        TxtIdTelefColumn.Name = "idTelefono"
        TxtIdTelefColumn.Width = 100
        Me.dvgTelefono.Columns.Add(TxtIdTelefColumn)
        Me.dvgTelefono.Columns("idTelefono").Visible = False

        Me.dvgCorreo.ScrollBars = ScrollBars.Both

    End Sub

    Public Sub mLimpiar()
        cbxCanton.Items.Clear()
        cbxDistrito.Items.Clear()
        cbxProvincia.Items.Clear()
        mLlenarComboProvincia()

        'txtCanton.Text = ""
        txtDescripcion.Text = ""
        'txtDistrito.Text = ""
        txtNomCliente.Text = ""
        txtNomEmpresa.Text = ""
        txtOtrasSenas.Text = ""
        txtPriApellido.Text = ""
        'txtProvincia.Text = ""
        txtSegApellido.Text = ""
        mktCedula.Text = ""
        mktCedula.Mask = "000000000000000000"
        mktCedula.ReadOnly = False
        rbnEmpresa.Checked = False
        rbnPersona.Checked = False
        txtNomCliente.Enabled = True
        txtPriApellido.Enabled = True
        txtSegApellido.Enabled = True
        mktCedula.Enabled = True
        mktCedula.Focus()
        'Filas = Me.dvgCorreo.Rows.GetRowCount(DataGridViewElementStates.Displayed)
        Filas = dvgCorreo.Rows.Count
        For Me.x = 0 To Filas - 2
            dvgCorreo.Rows.Clear()  'para limpiarlo
            dvgCorreo.DataSource = Nothing 'para limpiar cualquier registro internamente y desenlazar el control de la BD
        Next
        'Filas = Me.dvgTelefono.Rows.GetRowCount(DataGridViewElementStates.Displayed)
        Filas = dvgTelefono.Rows.Count
        For Me.x = 0 To Filas - 2
            dvgTelefono.Rows.Clear()  'para limpiarlo
            dvgTelefono.DataSource = Nothing 'para limpiar cualquier registro internamente y desenlazar el control de la BD
        Next

        txtSegApellido.Enabled = False
        txtPriApellido.Enabled = False
        txtOtrasSenas.Enabled = False
        txtNomEmpresa.Enabled = False
        txtNomCliente.Enabled = False
        txtDescripcion.Enabled = False
        dvgCorreo.Enabled = False
        dvgTelefono.Enabled = False
        rbnPersona.Enabled = False
        rbnEmpresa.Enabled = False

    End Sub

    Private Sub mInformacionProveedor(ByVal strCedulaPro As String)
        mLimpiar()

        clsEntidadProveedor.pCedulaPro = strCedulaPro
        clsEntidadCorreoPro.pCedulaPro = strCedulaPro
        clsEntidadTelefonoPro.pCedulaPro = strCedulaPro
        mktCedula.Text = strCedulaPro
        drProveedor = clsLogicaProveedor.mConsultaCedulaProveedor(clsEntidadProveedor, clsConexion)
        drCorreoPro = clsLogicaCorreoPro.mConsultaCorreoProCedula(clsEntidadCorreoPro, clsConexion)
        drTelefonoPro = clsLogicaTelefonoPro.mConsultaTelefonoProCedula(clsEntidadTelefonoPro, clsConexion)


        If drProveedor.Read Then
            txtNomCliente.Text = drProveedor.Item("nombrePro")
            txtPriApellido.Text = drProveedor.Item("apellido1Pro")
            txtSegApellido.Text = drProveedor.Item("apellido2Pro")
            Me.clsEntidadProvincia.pIdProvincia = drProveedor.Item("provinciaPro")
            Me.clsEntidadCanton.pIdCanton = drProveedor.Item("cantonPro")
            Me.clsEntidadDistrito.pIdDistrito = drProveedor.Item("distritoPro")
            txtOtrasSenas.Text = drProveedor.Item("otrasSenasPro")
            txtDescripcion.Text = drProveedor.Item("descripcionPro")
            txtNomEmpresa.Text = drProveedor.Item("nomEmpresaPro")

            drProvincia = Me.clsLogicaProvincia.mConsultaProvinciaID(clsEntidadProvincia, clsConexion)
            If drProvincia.Read Then
                cbxProvincia.Text = drProvincia.Item("nombreProvincia")
            End If

            drCanton = Me.clsLogicaCanton.mConsultarCantonID(clsEntidadCanton, clsConexion)
            If drCanton.Read Then
                cbxCanton.Text = drCanton.Item("nombreCanton")
            End If

            drDistrito = Me.clsLogicaDistrito.mConsultarDistritoID(clsEntidadDistrito, clsConexion)
            If drDistrito.Read Then
                cbxDistrito.Text = drDistrito.Item("nombreDistrito")
            End If

            If drProveedor.Item("tipoPro") = "PERSONA" Then
                rbnPersona.Checked = True
            ElseIf drProveedor.Item("tipoPro") = "EMPRESA" Then
                rbnEmpresa.Checked = True

            End If
        End If

        '***********************************************************
        'duplas = Me.dvgTelefono.Rows.GetRowCount(DataGridViewElementStates.Displayed)
        duplas = Me.dvgTelefono.Rows.Count
        For Me.x = 0 To duplas - 2
            Me.dvgTelefono.Rows.Remove(dvgTelefono.CurrentRow)
            Me.dvgTelefono.Refresh()
        Next

        'duplas = Me.dvgCorreo.Rows.GetRowCount(DataGridViewElementStates.Displayed)
        duplas = Me.dvgCorreo.Rows.Count
        For Me.x = 0 To duplas - 2
            Me.dvgCorreo.Rows.Remove(dvgTelefono.CurrentRow)
            Me.dvgCorreo.Refresh()
        Next
        'MOSTRAR LOS CORREOS DE LOS CLIENTES
        intContador = 0
        While drCorreoPro.Read
            Try
                Me.dvgCorreo.Rows.Add()
                Me.dvgCorreo.Item("IdEmail", intContador).Value = Trim(drCorreoPro.Item("idCorreoPro"))

                Me.dvgCorreo.Item("Email", intContador).Value = Trim(drCorreoPro.Item("correoPro"))
            Catch ex As Exception


            End Try
            intContador = intContador + 1
        End While

        'MOSTRAR LOS TELEFONOS DE LOS CLIENTES
        intContador = 0
        While drTelefonoPro.Read
            Try
                Me.dvgTelefono.Rows.Add()
                Me.dvgTelefono.Item("Numero", intContador).Value = Trim(drTelefonoPro.Item("telefonoPro"))
                Me.dvgTelefono.Item("IdTelefono", intContador).Value = Trim(drTelefonoPro.Item("idTelefonoPro"))
            Catch ex As Exception

            End Try
            intContador = intContador + 1

        End While

    End Sub

    Public Sub mLlenarComboProvincia()
        drProvincia = clsLogicaProvincia.mConsultaProvincia(clsConexion)
        While drProvincia.Read
            cbxProvincia.Items.Add(drProvincia.Item("nombreProvincia"))
        End While
    End Sub

#End Region


End Class