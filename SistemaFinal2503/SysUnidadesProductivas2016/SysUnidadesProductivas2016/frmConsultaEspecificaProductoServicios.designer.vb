﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaEspecificaProductoServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvConsultaEspPro = New System.Windows.Forms.ListView()
        Me.lvCodigo = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvNombre = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvDescripcion = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvPrecio = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvCosto = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvTipo = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnRegresar = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout
        Me.SuspendLayout
        '
        'lvConsultaEspPro
        '
        Me.lvConsultaEspPro.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvConsultaEspPro.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lvConsultaEspPro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvConsultaEspPro.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvCodigo, Me.lvNombre, Me.lvDescripcion, Me.lvPrecio, Me.lvCosto, Me.lvTipo})
        Me.lvConsultaEspPro.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lvConsultaEspPro.FullRowSelect = true
        Me.lvConsultaEspPro.GridLines = true
        Me.lvConsultaEspPro.HoverSelection = true
        Me.lvConsultaEspPro.Location = New System.Drawing.Point(29, 213)
        Me.lvConsultaEspPro.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.lvConsultaEspPro.Name = "lvConsultaEspPro"
        Me.lvConsultaEspPro.Size = New System.Drawing.Size(761, 288)
        Me.lvConsultaEspPro.TabIndex = 0
        Me.lvConsultaEspPro.UseCompatibleStateImageBehavior = false
        Me.lvConsultaEspPro.View = System.Windows.Forms.View.Details
        '
        'lvCodigo
        '
        Me.lvCodigo.Text = "Codigo"
        Me.lvCodigo.Width = 120
        '
        'lvNombre
        '
        Me.lvNombre.Text = "Nombre"
        Me.lvNombre.Width = 120
        '
        'lvDescripcion
        '
        Me.lvDescripcion.Text = "Descripcion"
        Me.lvDescripcion.Width = 200
        '
        'lvPrecio
        '
        Me.lvPrecio.Text = "Precio"
        Me.lvPrecio.Width = 120
        '
        'lvCosto
        '
        Me.lvCosto.Text = "Costo"
        Me.lvCosto.Width = 105
        '
        'lvTipo
        '
        Me.lvTipo.Text = "Tipo"
        Me.lvTipo.Width = 94
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(154,Byte),Integer), CType(CType(164,Byte),Integer), CType(CType(175,Byte),Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(23, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(732, 72)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Consulta de Productos - Servicios"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label2.AutoSize = true
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(60,Byte),Integer), CType(CType(74,Byte),Integer), CType(CType(96,Byte),Integer))
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(68, 140)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(176, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Codigo del Producto:"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label3.AutoSize = true
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(60,Byte),Integer), CType(CType(74,Byte),Integer), CType(CType(96,Byte),Integer))
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(480, 140)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(181, 20)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Nombre del producto:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtCodigo.Location = New System.Drawing.Point(72, 164)
        Me.txtCodigo.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(200, 26)
        Me.txtCodigo.TabIndex = 4
        '
        'txtNombre
        '
        Me.txtNombre.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtNombre.Location = New System.Drawing.Point(484, 164)
        Me.txtNombre.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(200, 26)
        Me.txtNombre.TabIndex = 5
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtNombre)
        Me.Panel1.Controls.Add(Me.btnRegresar)
        Me.Panel1.Controls.Add(Me.txtCodigo)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.lvConsultaEspPro)
        Me.Panel1.Location = New System.Drawing.Point(15, 15)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(793, 574)
        Me.Panel1.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = true
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(189, 194)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(396, 15)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Ingresa el codigo del producto -servicio o el nombre que deseas buscar"
        '
        'btnRegresar
        '
        Me.btnRegresar.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.btnRegresar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnRegresar.BackgroundImage = Global.SysUnidadesProductivas2016.My.Resources.Resources.desenfoque7070
        Me.btnRegresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnRegresar.FlatAppearance.BorderSize = 0
        Me.btnRegresar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRegresar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnRegresar.ForeColor = System.Drawing.Color.Black
        Me.btnRegresar.Image = Global.SysUnidadesProductivas2016.My.Resources.Resources.regresar
        Me.btnRegresar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegresar.Location = New System.Drawing.Point(635, 521)
        Me.btnRegresar.Name = "btnRegresar"
        Me.btnRegresar.Size = New System.Drawing.Size(120, 35)
        Me.btnRegresar.TabIndex = 20
        Me.btnRegresar.Text = "Regresar"
        Me.btnRegresar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRegresar.UseVisualStyleBackColor = true
        '
        'frmConsultaEspecificaProductoServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7!, 15!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(26,Byte),Integer), CType(CType(42,Byte),Integer), CType(CType(68,Byte),Integer))
        Me.ClientSize = New System.Drawing.Size(820, 601)
        Me.ControlBox = false
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmConsultaEspecificaProductoServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(false)
        Me.Panel1.PerformLayout
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents lvConsultaEspPro As ListView
    Friend WithEvents lvCodigo As ColumnHeader
    Friend WithEvents lvNombre As ColumnHeader
    Friend WithEvents lvDescripcion As ColumnHeader
    Friend WithEvents lvPrecio As ColumnHeader
    Friend WithEvents lvCosto As ColumnHeader
    Friend WithEvents lvTipo As ColumnHeader
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtCodigo As TextBox
    Friend WithEvents txtNombre As TextBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnRegresar As Button
    Friend WithEvents Label4 As Label
End Class
