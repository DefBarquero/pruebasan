﻿Imports AccesoDatos.clsConexion
Imports EntidadNegocios
Imports LogicaNegocios
Imports MySql.Data.MySqlClient

Public Class frmConsultaEspecificaFactura

#Region "ATRIBUTOS"

    Dim clsConexion As AccesoDatos.clsConexion

    Dim clsEntidadFacturaEnc As New EntidadNegocios.clsEntidadFacturaEnc
    Dim clsEntidadFacturaDet As New EntidadNegocios.clsEntidadFacturaDet
    Dim clsEntidadFacturaCostoEnc As New EntidadNegocios.clsEntidadFacturaCostoEnc
    Dim clsEntidadFacturaCostoDet As New EntidadNegocios.clsEntidadFacturaCostoDet
    Dim clsEntidadCliente As New EntidadNegocios.clsEntidadCliente
    Dim clsEntidadProducto As New EntidadNegocios.clsEntidadProductoServicio

    Dim clsLogicaFacturaEnc As New LogicaNegocios.clsLogicaFacturaEnc
    Dim clsLogicaFacturaDet As New LogicaNegocios.clsLogicaFacturaDet
    Dim clsLogicaFacturaCostoEnc As New LogicaNegocios.clsLogicaFactutaCostoEnc
    Dim clsLogicaFacturaCostoDet As New LogicaNegocios.clsLogicaFacturaCostoDet


    Dim clsLogicaCliente As New LogicaNegocios.clsLogicaCliente
    Dim clsLogicaProducto As New LogicaNegocios.clsLogicaProductoServicio


    Dim drCliente, drFacturaDet, drFacturaEnc, drProducto, drFacturaCostoEnc As MySql.Data.MySqlClient.MySqlDataReader

    Dim strCedula, strFecha, strDate1, strDate2 As String

    Dim intContador As Integer
    Dim intCosto, intTotalCosto, intGanacias, intTotal,ivaTotal As Decimal

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        strDate1 = ""
        strDate2 = ""
        strDate1 = dtFecha1.Value.Date
        strDate2 = dtFecha2.Value.Date
        MsgBox(strDate1 + "   " + strDate2)
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Close()

    End Sub




#End Region

#Region "CONSTRUCTOR & LOAD"


    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion

    End Sub

    Private Sub frmFacturaConsultaEspecifica_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Panel1.BackColor = Color.FromArgb(100, 161, 171, 183)
        mLlenarComboCliente()
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
    End Sub

    Private Sub cbxUsuarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxClientes.SelectedIndexChanged
        intContador = 0
        intTotal = 0.00
        intTotalCosto = 0.00
        intGanacias = 0.00
        intCosto = 0.00
        ivaTotal=0.00
        lvLista.Items.Clear()
        lblTotalGanancia.Text = ""
        lblTotal.Text = ""
        lblTotalCosto.Text = ""
        lblTotal.Text = ""
        If cbxClientes.Text.Trim <> "" Then
            clsEntidadCliente.pCedulaCli = cbxClientes.Text.Trim
            drCliente = clsLogicaCliente.mBuscarCliente(clsEntidadCliente, clsConexion)
            If drCliente.Read Then
                lblCliente.Text = drCliente.Item("nombreCli") + " " + drCliente.Item("apellido1Cli") + " " + drCliente.Item("apellido2Cli")
                clsEntidadFacturaEnc.pCedulaCli = drCliente.Item("cedulaCli")
                drFacturaEnc = clsLogicaFacturaEnc.mConsultaFacturaEnc(clsEntidadFacturaEnc, clsConexion)
                While drFacturaEnc.Read
                    strFecha = ""
                    clsEntidadFacturaDet.pIdFactura = drFacturaEnc.Item("idFactura")
                    strFecha = drFacturaEnc.Item("fechaFactura")
                    intTotal += Convert.ToDecimal(drFacturaEnc.Item("total"))
                    ivaTotal+= Convert.ToDecimal(drFacturaEnc.Item("iva"))
                    drFacturaDet = clsLogicaFacturaDet.mConsultaFacturaDet(clsEntidadFacturaDet, clsConexion)
                    While drFacturaDet.Read
                        lvLista.Items.Add(drFacturaDet.Item("idFactura"))
                        lvLista.Items(intContador).SubItems.Add(strFecha)
                        clsEntidadProducto.pIdProductoSer = drFacturaDet.Item("idProducto")
                        drProducto = clsLogicaProducto.mConsultaProducto(clsEntidadProducto, clsConexion)
                        If drProducto.Read Then
                            lvLista.Items(intContador).SubItems.Add(drProducto.Item("nombrePro_Ser"))
                            'intCosto = Convert.ToDecimal(drProducto.Item("costo"))

                        End If

                        lvLista.Items(intContador).SubItems.Add(drFacturaDet.Item("cantidad"))
                        lvLista.Items(intContador).SubItems.Add(drFacturaDet.Item("subtotal"))
                        'intTotalCosto += intCosto * Convert.ToDecimal(drFacturaDet.Item("cantidad"))
                        intContador = intContador + 1

                    End While
                End While

                clsEntidadFacturaCostoEnc.pCedulaCli = cbxClientes.Text.Trim
                drFacturaCostoEnc = Me.clsLogicaFacturaCostoEnc.mConsultarFacturaCostoEncCedulaCli(clsEntidadFacturaCostoEnc, clsConexion)
                While drFacturaCostoEnc.Read
                    Me.intTotalCosto += Convert.ToDecimal(drFacturaCostoEnc.Item("total"))
                End While

                lblTotal.Text = intTotal
                lblTotalCosto.Text = intTotalCosto
                lblTotalGanancia.Text = intTotal - intTotalCosto - ivaTotal
                iva.Text=ivaTotal

            End If
        End If
    End Sub


#End Region

#Region "METODOS"
    Public Sub mLlenarComboCliente()
        drCliente = clsLogicaCliente.mConsultaGeneralCli(clsConexion)
        While drCliente.Read

            cbxClientes.Items.Add(drCliente.Item("cedulaCli"))
        End While

    End Sub



    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        lvLista.Items.Clear()
        lblTotalGanancia.Text = ""
        lblTotal.Text = ""
        lblTotalCosto.Text = ""

        intTotal = 0
        intContador = 0
        intTotalCosto = 0.0
         ivaTotal=0.00
        ' intGanacias = 0
        'intCosto = 0

        drFacturaEnc = clsLogicaFacturaEnc.mConsultaFacturaEncPorFechas(dtFecha1.Value.Date, dtFecha2.Value.Date, clsConexion)

        While drFacturaEnc.Read
            strFecha = ""
            clsEntidadFacturaDet.pIdFactura = drFacturaEnc.Item("idFactura")
            strFecha = drFacturaEnc.Item("fechaFactura")
            intTotal += Convert.ToDecimal(drFacturaEnc.Item("total"))
            ivaTotal+= Convert.ToDecimal(drFacturaEnc.Item("iva"))

            drFacturaDet = clsLogicaFacturaDet.mConsultaFacturaDet(clsEntidadFacturaDet, clsConexion)
            While drFacturaDet.Read
                lvLista.Items.Add(drFacturaDet.Item("idFactura"))
                lvLista.Items(intContador).SubItems.Add(strFecha)
                clsEntidadProducto.pIdProductoSer = drFacturaDet.Item("idProducto")
                drProducto = clsLogicaProducto.mConsultaProducto(clsEntidadProducto, clsConexion)
                If drProducto.Read Then
                    lvLista.Items(intContador).SubItems.Add(drProducto.Item("nombrePro_Ser"))
                    intCosto = Convert.ToDecimal(drProducto.Item("costoPro_Ser"))
                End If

                lvLista.Items(intContador).SubItems.Add(drFacturaDet.Item("cantidad"))
                lvLista.Items(intContador).SubItems.Add(drFacturaDet.Item("subtotal"))
                'intTotalCosto += intCosto * Convert.ToDecimal(drFacturaDet.Item("cantidad"))
                intContador = intContador + 1

            End While
        End While
        drFacturaCostoEnc = clsLogicaFacturaCostoEnc.mConsultaFacturaCostoEncPorFechas(dtFecha1.Value.Date, dtFecha2.Value.Date, clsConexion)
        While drFacturaCostoEnc.Read
            intTotalCosto += Convert.ToDecimal(drFacturaCostoEnc("total"))
        End While


        lblTotal.Text = intTotal
        lblTotalCosto.Text = intTotalCosto
        lblTotalGanancia.Text = intTotal - intTotalCosto

        lblTotal.Text = intTotal
        lblTotalCosto.Text = intTotalCosto
        lblTotalGanancia.Text = intTotal - intTotalCosto - ivaTotal
        iva.Text=ivaTotal
    End Sub


#End Region






End Class