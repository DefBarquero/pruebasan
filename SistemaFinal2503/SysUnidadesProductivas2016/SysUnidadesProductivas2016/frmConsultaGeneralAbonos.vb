﻿Public Class frmConsultaGeneralAbonos

#Region "Atributos"

    'ACCESO A DATOS
    Dim clsConexion As AccesoDatos.clsConexion

    'ENTIDAD
    Dim clsEntidadAbono As New EntidadNegocios.clsEntidadAbono

    'LOGICA
    Dim clsLogicaAbono As New LogicaNegocios.clsLogicaAbono

    'DATAREADER
    Dim drAbono As MySql.Data.MySqlClient.MySqlDataReader

    'INTEGER
    Dim intContador As Integer

#End Region

#Region "Herramientas"

    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        SetVisibleCore(False)
    End Sub

    Private Sub frmConsultaGeneralAbono_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size

        intContador = 0
        drAbono = clsLogicaAbono.mConsultaGeneralAbono(clsConexion)
        While drAbono.Read
            lvAbonos.Items.Add(drAbono.Item("idAbono"))
            lvAbonos.Items(intContador).SubItems.Add(drAbono.Item("cedulaCli"))
            lvAbonos.Items(intContador).SubItems.Add(drAbono.Item("fechaAbono"))
            lvAbonos.Items(intContador).SubItems.Add(drAbono.Item("idFactura"))
            lvAbonos.Items(intContador).SubItems.Add(drAbono.Item("montoCompra"))
            lvAbonos.Items(intContador).SubItems.Add(drAbono.Item("montoAbonado"))
            lvAbonos.Items(intContador).SubItems.Add(drAbono.Item("saldoActual"))

            intContador = intContador + 1

        End While
    End Sub


#End Region


End Class