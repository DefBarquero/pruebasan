﻿Imports MySql.Data.MySqlClient

Public Class frmConsultaEspecificaProductoServicios
#Region "Atributos"
    Dim clConexion As AccesoDatos.clsConexion
    Dim clsProducto As New LogicaNegocios.clsLogicaProductoServicio
    Dim clEntidadProducto As New EntidadNegocios.clsEntidadProductoServicio
    Dim drProducto As MySql.Data.MySqlClient.MySqlDataReader
    Dim strCodigo, strNombre As String
    Dim intContador As Integer
    Dim dataT As DataTable
    Dim frmMenu As frmMDIMenuPrincipal
#End Region
    Public Sub New(pConexion As AccesoDatos.clsConexion, pFrmMenu As frmMDIMenuPrincipal)

        clConexion = pConexion
        frmMenu = pFrmMenu

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

#Region "Metodos"

    'Metodo que permite consultar por nombre conforme se digita en el espacio del nombre
    Public Sub mConsultaNombreProducto()

        strNombre = ""
        intContador = 0
        strNombre = txtNombre.Text

        If strNombre = "" Then
            lvConsultaEspPro.Items.Clear()
        Else
            lvConsultaEspPro.Items.Clear()
            Try
                drProducto = clsProducto.mConsultarProductoNombre(strNombre, clConexion)

                While drProducto.Read
                    lvConsultaEspPro.Items.Add(drProducto.Item("idProducto_Servicio"))
                    lvConsultaEspPro.Items(intContador).SubItems.Add(drProducto.Item("nombrePro_Ser"))
                    lvConsultaEspPro.Items(intContador).SubItems.Add(drProducto.Item("descripcionPro_Ser"))
                    lvConsultaEspPro.Items(intContador).SubItems.Add(drProducto.Item("precioPro_Ser"))
                    lvConsultaEspPro.Items(intContador).SubItems.Add(drProducto.Item("costoPro_ser"))
                    lvConsultaEspPro.Items(intContador).SubItems.Add(drProducto.Item("tipoPro_Ser"))

                    intContador += 1
                End While
            Catch ex As Exception
            End Try
        End If
    End Sub

    'Metodo que permite consultar por codigo conforme se digita en el espacio del codigo
    Public Sub mConsultaCodigoProducto()

        strCodigo = ""
        intContador = 0
        strCodigo = txtCodigo.Text

        If strCodigo = "" Then
            lvConsultaEspPro.Items.Clear()
        Else
            lvConsultaEspPro.Items.Clear()
            Try
                drProducto = clsProducto.mConsultarCodigoProducto(strCodigo, clConexion)

                While drProducto.Read
                    lvConsultaEspPro.Items.Add(drProducto.Item("idProducto_Servicio"))
                    lvConsultaEspPro.Items(intContador).SubItems.Add(drProducto.Item("nombrePro_Ser"))
                    lvConsultaEspPro.Items(intContador).SubItems.Add(drProducto.Item("descripcionPro_Ser"))
                    lvConsultaEspPro.Items(intContador).SubItems.Add(drProducto.Item("costoPro_Ser"))
                    lvConsultaEspPro.Items(intContador).SubItems.Add(drProducto.Item("precioPro_Ser"))
                    lvConsultaEspPro.Items(intContador).SubItems.Add(drProducto.Item("tipoPro_Ser"))


                    intContador += 1
                End While
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub txtCodigo_TextChanged(sender As Object, e As EventArgs) Handles txtCodigo.TextChanged
        mConsultaCodigoProducto()
    End Sub



    Private Sub frmConsultaEspecificaProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
    End Sub

    Private Sub btnRegresar_Click(sender As Object, e As EventArgs) Handles btnRegresar.Click
        SetVisibleCore(False)
        frmMenu.Show()
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.Click

    End Sub

    Private Sub txtNombre_TextChanged(sender As Object, e As EventArgs) Handles txtNombre.TextChanged
        mConsultaNombreProducto()
    End Sub

#End Region
End Class