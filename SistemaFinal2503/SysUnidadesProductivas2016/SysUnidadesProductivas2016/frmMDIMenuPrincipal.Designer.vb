﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMDIMenuPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArchivosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CerrarSesiónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MódulosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbonosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuarioCGeneralToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuarioCEspecificaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClienteCGeneralToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClienteCEspecificaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CProdustosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductoCGeneralToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductoCEspecificaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedoresToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedorCGeneralToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedorCEspecificaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturaCGeneralToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FacturaCEspecificaToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeguridadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PermisosUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.AbonosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.GeneralToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EspecificoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout
        Me.Panel2.SuspendLayout
        Me.Panel1.SuspendLayout
        Me.Panel5.SuspendLayout
        Me.SuspendLayout
        '
        'ToolTip
        '
        Me.ToolTip.BackColor = System.Drawing.Color.Transparent
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.LightGray
        Me.MenuStrip1.BackgroundImage = Global.SysUnidadesProductivas2016.My.Resources.Resources.desenfoque5555
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivosToolStripMenuItem, Me.MódulosToolStripMenuItem, Me.ConsultasToolStripMenuItem, Me.SeguridadToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(984, 24)
        Me.MenuStrip1.TabIndex = 10
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArchivosToolStripMenuItem
        '
        Me.ArchivosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CerrarSesiónToolStripMenuItem})
        Me.ArchivosToolStripMenuItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ArchivosToolStripMenuItem.Name = "ArchivosToolStripMenuItem"
        Me.ArchivosToolStripMenuItem.Size = New System.Drawing.Size(80, 20)
        Me.ArchivosToolStripMenuItem.Text = "Archivos"
        '
        'CerrarSesiónToolStripMenuItem
        '
        Me.CerrarSesiónToolStripMenuItem.Name = "CerrarSesiónToolStripMenuItem"
        Me.CerrarSesiónToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.CerrarSesiónToolStripMenuItem.Text = "Cerrar Sesión"
        '
        'MódulosToolStripMenuItem
        '
        Me.MódulosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UsuariosToolStripMenuItem, Me.ClientesToolStripMenuItem, Me.ProductosToolStripMenuItem, Me.ProveedoresToolStripMenuItem, Me.FacturaToolStripMenuItem, Me.AbonosToolStripMenuItem})
        Me.MódulosToolStripMenuItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.MódulosToolStripMenuItem.Name = "MódulosToolStripMenuItem"
        Me.MódulosToolStripMenuItem.Size = New System.Drawing.Size(79, 20)
        Me.MódulosToolStripMenuItem.Text = "Módulos"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.ClientesToolStripMenuItem.Text = "Clientes"
        '
        'ProductosToolStripMenuItem
        '
        Me.ProductosToolStripMenuItem.Name = "ProductosToolStripMenuItem"
        Me.ProductosToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.ProductosToolStripMenuItem.Text = "Productos-Servicios"
        '
        'ProveedoresToolStripMenuItem
        '
        Me.ProveedoresToolStripMenuItem.Name = "ProveedoresToolStripMenuItem"
        Me.ProveedoresToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.ProveedoresToolStripMenuItem.Text = "Proveedores"
        '
        'FacturaToolStripMenuItem
        '
        Me.FacturaToolStripMenuItem.Name = "FacturaToolStripMenuItem"
        Me.FacturaToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.FacturaToolStripMenuItem.Text = "Factura"
        '
        'AbonosToolStripMenuItem
        '
        Me.AbonosToolStripMenuItem.Name = "AbonosToolStripMenuItem"
        Me.AbonosToolStripMenuItem.Size = New System.Drawing.Size(216, 22)
        Me.AbonosToolStripMenuItem.Text = "Abonos"
        '
        'ConsultasToolStripMenuItem
        '
        Me.ConsultasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UsuariosToolStripMenuItem1, Me.ClientesToolStripMenuItem1, Me.CProdustosToolStripMenuItem1, Me.ProveedoresToolStripMenuItem1, Me.FacturaToolStripMenuItem1, Me.AbonosToolStripMenuItem1})
        Me.ConsultasToolStripMenuItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.ConsultasToolStripMenuItem.Name = "ConsultasToolStripMenuItem"
        Me.ConsultasToolStripMenuItem.Size = New System.Drawing.Size(88, 20)
        Me.ConsultasToolStripMenuItem.Text = "Consultas"
        '
        'UsuariosToolStripMenuItem1
        '
        Me.UsuariosToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UsuarioCGeneralToolStripMenuItem2, Me.UsuarioCEspecificaToolStripMenuItem})
        Me.UsuariosToolStripMenuItem1.Name = "UsuariosToolStripMenuItem1"
        Me.UsuariosToolStripMenuItem1.Size = New System.Drawing.Size(216, 22)
        Me.UsuariosToolStripMenuItem1.Text = "Usuarios"
        '
        'UsuarioCGeneralToolStripMenuItem2
        '
        Me.UsuarioCGeneralToolStripMenuItem2.Name = "UsuarioCGeneralToolStripMenuItem2"
        Me.UsuarioCGeneralToolStripMenuItem2.Size = New System.Drawing.Size(149, 22)
        Me.UsuarioCGeneralToolStripMenuItem2.Text = "General"
        '
        'UsuarioCEspecificaToolStripMenuItem
        '
        Me.UsuarioCEspecificaToolStripMenuItem.Name = "UsuarioCEspecificaToolStripMenuItem"
        Me.UsuarioCEspecificaToolStripMenuItem.Size = New System.Drawing.Size(149, 22)
        Me.UsuarioCEspecificaToolStripMenuItem.Text = "Específica"
        '
        'ClientesToolStripMenuItem1
        '
        Me.ClientesToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClienteCGeneralToolStripMenuItem1, Me.ClienteCEspecificaToolStripMenuItem})
        Me.ClientesToolStripMenuItem1.Name = "ClientesToolStripMenuItem1"
        Me.ClientesToolStripMenuItem1.Size = New System.Drawing.Size(216, 22)
        Me.ClientesToolStripMenuItem1.Text = "Clientes"
        '
        'ClienteCGeneralToolStripMenuItem1
        '
        Me.ClienteCGeneralToolStripMenuItem1.Name = "ClienteCGeneralToolStripMenuItem1"
        Me.ClienteCGeneralToolStripMenuItem1.Size = New System.Drawing.Size(149, 22)
        Me.ClienteCGeneralToolStripMenuItem1.Text = "General"
        '
        'ClienteCEspecificaToolStripMenuItem
        '
        Me.ClienteCEspecificaToolStripMenuItem.Name = "ClienteCEspecificaToolStripMenuItem"
        Me.ClienteCEspecificaToolStripMenuItem.Size = New System.Drawing.Size(149, 22)
        Me.ClienteCEspecificaToolStripMenuItem.Text = "Específica"
        '
        'CProdustosToolStripMenuItem1
        '
        Me.CProdustosToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductoCGeneralToolStripMenuItem3, Me.ProductoCEspecificaToolStripMenuItem1})
        Me.CProdustosToolStripMenuItem1.Name = "CProdustosToolStripMenuItem1"
        Me.CProdustosToolStripMenuItem1.Size = New System.Drawing.Size(216, 22)
        Me.CProdustosToolStripMenuItem1.Text = "Productos-Servicios"
        '
        'ProductoCGeneralToolStripMenuItem3
        '
        Me.ProductoCGeneralToolStripMenuItem3.Name = "ProductoCGeneralToolStripMenuItem3"
        Me.ProductoCGeneralToolStripMenuItem3.Size = New System.Drawing.Size(149, 22)
        Me.ProductoCGeneralToolStripMenuItem3.Text = "General"
        '
        'ProductoCEspecificaToolStripMenuItem1
        '
        Me.ProductoCEspecificaToolStripMenuItem1.Name = "ProductoCEspecificaToolStripMenuItem1"
        Me.ProductoCEspecificaToolStripMenuItem1.Size = New System.Drawing.Size(149, 22)
        Me.ProductoCEspecificaToolStripMenuItem1.Text = "Específica"
        '
        'ProveedoresToolStripMenuItem1
        '
        Me.ProveedoresToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProveedorCGeneralToolStripMenuItem, Me.ProveedorCEspecificaToolStripMenuItem})
        Me.ProveedoresToolStripMenuItem1.Name = "ProveedoresToolStripMenuItem1"
        Me.ProveedoresToolStripMenuItem1.Size = New System.Drawing.Size(216, 22)
        Me.ProveedoresToolStripMenuItem1.Text = "Proveedores"
        '
        'ProveedorCGeneralToolStripMenuItem
        '
        Me.ProveedorCGeneralToolStripMenuItem.Name = "ProveedorCGeneralToolStripMenuItem"
        Me.ProveedorCGeneralToolStripMenuItem.Size = New System.Drawing.Size(149, 22)
        Me.ProveedorCGeneralToolStripMenuItem.Text = "General"
        '
        'ProveedorCEspecificaToolStripMenuItem
        '
        Me.ProveedorCEspecificaToolStripMenuItem.Name = "ProveedorCEspecificaToolStripMenuItem"
        Me.ProveedorCEspecificaToolStripMenuItem.Size = New System.Drawing.Size(149, 22)
        Me.ProveedorCEspecificaToolStripMenuItem.Text = "Específica"
        '
        'FacturaToolStripMenuItem1
        '
        Me.FacturaToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FacturaCGeneralToolStripMenuItem4, Me.FacturaCEspecificaToolStripMenuItem2})
        Me.FacturaToolStripMenuItem1.Name = "FacturaToolStripMenuItem1"
        Me.FacturaToolStripMenuItem1.Size = New System.Drawing.Size(216, 22)
        Me.FacturaToolStripMenuItem1.Text = "Factura"
        '
        'FacturaCGeneralToolStripMenuItem4
        '
        Me.FacturaCGeneralToolStripMenuItem4.Name = "FacturaCGeneralToolStripMenuItem4"
        Me.FacturaCGeneralToolStripMenuItem4.Size = New System.Drawing.Size(152, 22)
        Me.FacturaCGeneralToolStripMenuItem4.Text = "General"
        '
        'FacturaCEspecificaToolStripMenuItem2
        '
        Me.FacturaCEspecificaToolStripMenuItem2.Name = "FacturaCEspecificaToolStripMenuItem2"
        Me.FacturaCEspecificaToolStripMenuItem2.Size = New System.Drawing.Size(152, 22)
        Me.FacturaCEspecificaToolStripMenuItem2.Text = "Específica"
        '
        'SeguridadToolStripMenuItem
        '
        Me.SeguridadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PermisosUsuarioToolStripMenuItem})
        Me.SeguridadToolStripMenuItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.SeguridadToolStripMenuItem.Name = "SeguridadToolStripMenuItem"
        Me.SeguridadToolStripMenuItem.Size = New System.Drawing.Size(92, 20)
        Me.SeguridadToolStripMenuItem.Text = "Seguridad"
        '
        'PermisosUsuarioToolStripMenuItem
        '
        Me.PermisosUsuarioToolStripMenuItem.Name = "PermisosUsuarioToolStripMenuItem"
        Me.PermisosUsuarioToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.PermisosUsuarioToolStripMenuItem.Text = "Permisos Usuario"
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(990, 678)
        Me.Panel2.TabIndex = 16
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(79,Byte),Integer), CType(CType(93,Byte),Integer), CType(CType(113,Byte),Integer))
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Location = New System.Drawing.Point(22, 39)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(950, 610)
        Me.Panel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Label1.AutoSize = true
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(154,Byte),Integer), CType(CType(164,Byte),Integer), CType(CType(175,Byte),Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(101, 390)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(399, 33)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Software desarrollado para:"
        '
        'Panel3
        '
        Me.Panel3.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(154,Byte),Integer), CType(CType(164,Byte),Integer), CType(CType(175,Byte),Integer))
        Me.Panel3.BackgroundImage = Global.SysUnidadesProductivas2016.My.Resources.Resources.logo3
        Me.Panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel3.Location = New System.Drawing.Point(107, 69)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(743, 187)
        Me.Panel3.TabIndex = 0
        '
        'Panel5
        '
        Me.Panel5.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel5.BackColor = System.Drawing.Color.FromArgb(CType(CType(154,Byte),Integer), CType(CType(164,Byte),Integer), CType(CType(175,Byte),Integer))
        Me.Panel5.Controls.Add(Me.Panel4)
        Me.Panel5.Location = New System.Drawing.Point(75, 377)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(811, 211)
        Me.Panel5.TabIndex = 3
        '
        'Panel4
        '
        Me.Panel4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel4.BackgroundImage = Global.SysUnidadesProductivas2016.My.Resources.Resources.emprende
        Me.Panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel4.Location = New System.Drawing.Point(15, 63)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(779, 126)
        Me.Panel4.TabIndex = 2
        '
        'AbonosToolStripMenuItem1
        '
        Me.AbonosToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GeneralToolStripMenuItem, Me.EspecificoToolStripMenuItem})
        Me.AbonosToolStripMenuItem1.Name = "AbonosToolStripMenuItem1"
        Me.AbonosToolStripMenuItem1.Size = New System.Drawing.Size(216, 22)
        Me.AbonosToolStripMenuItem1.Text = "Abonos"
        '
        'GeneralToolStripMenuItem
        '
        Me.GeneralToolStripMenuItem.Name = "GeneralToolStripMenuItem"
        Me.GeneralToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.GeneralToolStripMenuItem.Text = "General"
        '
        'EspecificoToolStripMenuItem
        '
        Me.EspecificoToolStripMenuItem.Name = "EspecificoToolStripMenuItem"
        Me.EspecificoToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.EspecificoToolStripMenuItem.Text = "Especifico"
        '
        'frmMDIMenuPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(26,Byte),Integer), CType(CType(42,Byte),Integer), CType(CType(68,Byte),Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(984, 661)
        Me.ControlBox = false
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Panel2)
        Me.IsMdiContainer = true
        Me.Name = "frmMDIMenuPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.MenuStrip1.ResumeLayout(false)
        Me.MenuStrip1.PerformLayout
        Me.Panel2.ResumeLayout(false)
        Me.Panel1.ResumeLayout(false)
        Me.Panel1.PerformLayout
        Me.Panel5.ResumeLayout(false)
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents ToolTip As ToolTip
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ArchivosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MódulosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CerrarSesiónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProveedoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsultasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents CProdustosToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ProveedoresToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ProveedorCGeneralToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProveedorCEspecificaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ClienteCGeneralToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ClienteCEspecificaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UsuarioCGeneralToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents UsuarioCEspecificaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents ProductoCGeneralToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents ProductoCEspecificaToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents FacturaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FacturaToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents FacturaCGeneralToolStripMenuItem4 As ToolStripMenuItem
    Friend WithEvents FacturaCEspecificaToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents SeguridadToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PermisosUsuarioToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AbonosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AbonosToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents GeneralToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EspecificoToolStripMenuItem As ToolStripMenuItem
End Class
