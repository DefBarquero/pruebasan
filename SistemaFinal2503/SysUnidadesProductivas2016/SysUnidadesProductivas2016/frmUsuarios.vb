﻿'Se importa la biblioteca que nos permite encriptar y desencriptar 
Imports System.Security.Cryptography
'Se importa la biblioteca System.Text, la cual contiene clases que representan ASCII y la codificación de caracteres Unicode
Imports System.Text
Imports System.IO


'Esta clase nos permite realizar todos los métodos necesarios para mostrar en la ventana frmUsuariosse pueda visualizar, agregar
' modificar, la información de un usuario que se encuentre registrado en la base de datos
Public Class frmUsuarios

#Region "Atributos"
    Dim clConexion As AccesoDatos.clsConexion
    Dim clMenu As frmMDIMenuPrincipal
    Dim drUsuario As MySql.Data.MySqlClient.MySqlDataReader
    Dim clUsuario As New LogicaNegocios.clsLogicaUsuario
    Dim clEntidadUsario As New EntidadNegocios.clsEntidadUsuario
    Dim drResultado As DialogResult
    Dim bolAgregar, bolModificar As Boolean

    Dim strImagen, strImagenEncrip, strImagenDes As String
    Dim imagen As Image

#End Region

    '*********************************************************************************************************************
    'Se crea el constructor de la clase, al cual se le pasa por parámetros la clase clsConexion
    Public Sub New(pConexion As AccesoDatos.clsConexion, pMenu As frmMDIMenuPrincipal)
        'Se inicializa la referencia anteriormente creada en la región atributos con la que ingresa 
        ' por parámetros en el constructor
        clConexion = pConexion
        ' Se inicializa la referencia anteriormente creada en la región atributos con la que ingresa 
        ' por parámetros en el constructor
        clMenu = pMenu

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent()
    End Sub 'Fin del constructor 

    'Este evento se produce cuando el contenido del cuadro de texto cambia entre los envíos al servidor.
    Private Sub txtNomUsuario_TextChanged(sender As Object, e As EventArgs) Handles txtNomUsuario.TextChanged
        'Se le asigna al txtLogin, la información que extrae el método mExtraerPrimerLetra()
        txtLogin.Text = mExtraerPrimeraLetra()
    End Sub 'Fin del evento txtNomUsuario_TextChanged

    'Este evento se produce cuando el contenido del cuadro de texto cambia entre los envíos al servidor.
    Private Sub txtApellido1_TextChanged(sender As Object, e As EventArgs) Handles txtApellido1.TextChanged
        'Se le asigna al txtLogin, la información que extrae el método mExtraerPrimerLetra() y además se le 
        'concatena el apellido1Usu que se ha ingresado en el sistema
        txtLogin.Text = mExtraerPrimeraLetra() + txtApellido1.Text.ToLower
    End Sub 'Fin del evento txtApellido1_TextChanged

    'Este evento se produce cuando el contenido del cuadro de texto cambia entre los envíos al servidor.
    Private Sub txtApellido2_TextChanged(sender As Object, e As EventArgs) Handles txtApellido2.TextChanged
        'Se le asigna al txtLogin, la información que extrae el método mExtraerPrimerLetra() y además se le 
        'concatena el apellido1Usu que se ha ingresado en el sistema
        txtLogin.Text = mExtraerPrimeraLetra() + txtApellido1.Text.ToLower
        'Se llama al método mVerificarLogin
        mVerificarLogin()
    End Sub 'Fin del evento txtApellido2_TextChanged

    'Evento que ocurre cuando se hace clic sobre el botón btnAgregar
    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click

        'En la clsEntidad en el campo pLogin se le asigna el dato ingresado en el txtLogin
        clEntidadUsario.pLogin = Me.txtLogin.Text

        'Se le indica a la variable que lea la información proveniente del método mConsultarUsuarioLogin
        'el cual pertenece a la clsLogicaUsuario y además se le envía la variable clEntidadUsuario y clConexión
        drUsuario = clUsuario.mConsultarUsuarioLogin(clEntidadUsario, clConexion)

        'Se verifica que el usuario exista y se leído en el sistema
        If drUsuario.Read() Then
            '--------------En caso de que el usuario ya exista ------------------
            'Se informa de un error en caso de que existan que se desea ingresar ya exista en la base de datos
            MessageBox.Show("El usuario ya existe", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Se llama al método mLimpiarCampos
            mLimpiarCampos()
            'Se llama al método focus(), sirve para establecer un control en el foco de entrada del txtNomUsuario
            txtNomUsuario.Focus()
            'El estado del botón btnAgregar pasa hacer enabled
            btnAgregar.Enabled = True
        Else
            '-----------En caso de que el usuario no exista-----------------------
            'Verifica que los usuarios no ingresos datos vacíos
            If Me.txtNomUsuario.Text = "" Or txtApellido1.Text = "" Or txtApellido2.Text = "" Or Me.txtLogin.Text = "" Or Me.txtContrasena.Text = "" Or Me.txtConfirmarCont.Text = "" Then
                'En caso de que existan datos vacíos
                'Se muestra un mensaje que indique que al usuario que debe completar toda la información solicitada
                MessageBox.Show("Todos los campos de texto deben estar llenos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'Se llama al método mLimpiarCampos
                Me.mLimpiarCampos()
                'Se llama al método focus(), sirve para establecer un control en el foco de entrada del txtNomUsuario
                Me.txtNomUsuario.Focus()
            Else
                '---------------------En caso que los datos estén completos se procede a guardar la información-----------------------------
                'Se le asigna al clEntidadUsuario en el campo nombre la información que ha  sido ingresada en el txtNomUsu
                clEntidadUsario.pNombre = Me.txtNomUsuario.Text.Trim.ToLower
                'Se le asigna al clEntidadUsuario en el campo apellido1Usu la información que ha  sido ingresada en el txtApellido1
                clEntidadUsario.pApellido1Usu = Me.txtApellido1.Text.Trim.ToLower
                'Se le asigna al clEntidadUsuario en el campo apellido2Usu la información que ha  sido ingresada en el txtApellido2
                clEntidadUsario.pApellido2Usu = Me.txtApellido2.Text.Trim.ToLower
                'Se le asigna al clEntidadUsuario en el campo login la información que ha  sido ingresada en el txtLogin
                clEntidadUsario.pLogin = Me.txtLogin.Text.Trim.ToLower
                'Se le asigna al clEntidadUsuario en el campo contrasena la información que ha  sido ingresada en el txtConstrasena
                clEntidadUsario.pContrasena = Me.txtContrasena.Text
                'Se le asigna al clEntidadUsuario en el campo constrasena la información que ha  sido ingresada en el txtConfirmarCont
                clEntidadUsario.pConfirmarCont = Me.txtConfirmarCont.Text
                'Se le asigna al clEntidadUsuario en el campo imagenUsu la información que ha  sido encriptada correspondiente al strImagen
                If Me.strImagen = nothing Then  
                   clEntidadUsario.pImagenUsus = Me.mEncriptar(" ")
                Else
                    clEntidadUsario.pImagenUsus = Me.mEncriptar(Me.strImagen)
                End If 

                   
                'Se verifica que la constrasena sea la misma tanto en el txtContrasena como en el txtCOnfirmarCont
                If txtContrasena.Text = txtConfirmarCont.Text Then

                    '-----------------------------En caso que las contraseñas sean iguales--------------------------------------
                    'Se crea una variable strContasenna de tipo String  y se le iguala a la información que posee el txtConfirmarCont
                    Dim strContrasenna As String = Me.txtConfirmarCont.Text
                    'Se le asigna a la entidad usuario en el campo contrasena a la informacion encriptada que posee el strContrasenna
                    clEntidadUsario.pContrasena = mEncriptar(strContrasenna)

                    Dim frmConfirmar As frmUsuarioConfirmar = New frmUsuarioConfirmar(clEntidadUsario, clConexion, Me)
                    frmConfirmar.Show()

                    ''Se llama al metodo mAgregarUsuario se le pasa los parámetros necesarios y se le asigna a la variable bolAgregar
                    'bolAgregar = clUsuario.mAgregarUsuario(clEntidadUsario, clConexion)

                    ''Se verifica que la variable bolAgregar sea igual a True
                    'If bolAgregar = True Then
                    '    '--------------En caso de que la condición sea verdadera---------------------------------
                    '    'Se muestra un mensaje que la información ha sido guardada con éxito
                    '    MessageBox.Show("La información del usuario se agrego correctamente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    '    'Se llama al método mLimpiarCampos
                    '    mLimpiarCampos()
                    '    ' Se llama al método focus(), sirve para establecer un control en el foco de entrada del txtNomUsuario
                    '    Me.txtNomUsuario.Focus()
                    '    'Se limpia la imagen que contenga el picture box pcxImage
                    '    pcxImagen.Image = Nothing

                    'Else
                    '    '------------------En caso que la condición sea falsa-----------------------------------------
                    '    'Se muestra un mensaje indicando que existe un error al agregar el usuario
                    '    MessageBox.Show("Error al agregar la información del usuario", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    '    'Se llama al método mLimpiarCampos
                    '    mLimpiarCampos()
                    'End If 'Fin del If bol agregar
                Else
                    '--------------------En caso que las contraseñas no coincidan--------------------------------------------------------------------
                    'Se muestra un msj que indica que las contraseñas no son iguales
                    MsgBox("Contraseña no son iguales.", MsgBoxStyle.OkOnly, "Atención")
                    'Se limpia la información que contenga el txtContrasena
                    txtContrasena.Text = ""
                    'Se limpia la información que contenga el txtConfirmarCont
                    txtConfirmarCont.Text = ""
                    'Se llama al método focus(), sirve para establecer un control en el foco de entrada del txtContrasena
                    txtContrasena.Focus()
                End If 'Fin del If verificar contraseña
            End If  'Fin del If que verifica que no existan espacios en blanco
        End If ' Fin del If drUsuario
    End Sub 'Fin del evento btnAgregarUsuario

    'Evento que ocurre cuando se hace clic sobre el botón btnBuscar
    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        ' Se llama al método mLimpiarCampos
        mLimpiarCampos()
        'Se limpia la información que contenga el picturebox pcxImagen
        pcxImagen.Image = Nothing

        'Se crea una referencia e instancia de la frmConsultaUsuario y se le envia por parámetro la clConexion
        Dim frmConsultaUsu As New frmConsultaGeneralUsuario(clConexion)
        'Esta propiedad nos permite que la ventana se abra encima de otra ventana 
        With (frmConsultaUsu)
            'Establece la ventana frmConsulta como propietaria
            .Owner = Me
            'Establece la posición inicial de la ventana en tiempo de ejecución
            .StartPosition = FormStartPosition.CenterParent
            'Establece la posición inicial de la ventana en tiempo de ejecución
            .ShowDialog()
        End With 'Fin del With

        'Se verifica que el login sea diferente a vacío
        If frmConsultaUsu.mRetornarLogin <> "" Then
            'Se llama al método focus(), sirve para establecer un control en el foco de entrada del txLogin
            txtLogin.Focus()
            'Se le asigna a al txtLogin lo que retorna el método mRetornarLogin
            Me.txtLogin.Text = frmConsultaUsu.mRetornarLogin()
            'Se llama al método extraerDatosUsu y se le envía el login
            mExtrarDatosUsu(Me.txtLogin.Text)
        Else
            'Se llama al método limpiar campos
            mLimpiarCampos()
            'Se hace un focus sobre el txtNombre
            txtNomUsuario.Focus()
            'Se habilita txt
            txtNomUsuario.Enabled = True
            'Se habilita txt
            txtApellido1.Enabled = True
            txtApellido2.Enabled = True
            'Se habilita botón
            btnAgregar.Enabled = True
            'Se habilita botón
            btnModificar.Enabled = False

        End If 'Fin del if

    End Sub ' Fin del evento btnBuscar_Click

    'Evento que ocurre al hacer clic sobre el botón modificar
    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click

        ' A la entidad usuario en el campo contrasena se le asigna lo que posee el txtContrasena
        clEntidadUsario.pContrasena = Me.txtContrasena.Text
        ' A la entidad usuario en el pConfirmarCont se le asigna lo que posee el txtConfirmarCont
        clEntidadUsario.pConfirmarCont = Me.txtConfirmarCont.Text

        'Se realiza una condición para verificar que los datos no se encuentren vacíos
        If Me.txtContrasena.Text = "" Or txtConfirmarCont.Text = "" Then
            '******************************************En caso de  datos vacíos**********************************************
            'Se muestra un mensaje indicando el problema 
            MessageBox.Show("Los campos no pueden estar vacios", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Se limpian el campo pertenceciente al txtContrasena
            txtContrasena.Text = ""
            'Se limpian el campo pertenceciente al txtConfirmarCont
            txtConfirmarCont.Text = ""
            'Se llama al método focus(), sirve para establecer un control en el foco de entrada del txtContrasena 
            txtContrasena.Focus()
        Else
            '********************************************En caso de datos completos********************************************* 
            'Se verifica que los datos de las contraseñas sean iguales
            If txtContrasena.Text = txtConfirmarCont.Text Then
                ' Se crea una variable strContrasenna de tipo String y se le asigna lo que contenga el txtConfirmarCont 
                Dim strContrasenna As String = Me.txtConfirmarCont.Text
                'A la entidad usuario en el campo contrasena  se le asigna la variable strContrasenna encriptada 
                clEntidadUsario.pContrasena = mEncriptar(strContrasenna)

                'Asingnacion de la metodo mModificarUsuario a la variable bolModificar
                bolModificar = clUsuario.mModificarUsuario(clEntidadUsario, clConexion)

                'Se verifica que la variable bolModificar sea igual a True 
                If bolModificar = True Then
                    '************************************************En caso de que la condición sea afirmativa************************************
                    'Se envia un mensaje indicando la operación exitosa
                    MessageBox.Show("Su contraseña se ha modificado correctamente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'Se llama al metodo mLimpiarCampos
                    mLimpiarCampos()
                    'Se inahabilita el botón modificar
                    btnModificar.Enabled = False
                    'Se habilita el botón agregar
                    btnAgregar.Enabled = True
                    'Se habilita ek campo nombreUsuario
                    txtNomUsuario.Enabled = True
                    'Se habilita el campo apellido1
                    txtApellido1.Enabled = True
                    'Se habilida el campo apellido2
                    txtApellido2.Enabled = True
                    'Se inahabilita el campo del login
                    txtLogin.Enabled = False
                    'Se habilita el botón imagen
                    btnImagen.Enabled = True
                    'Se habilita el botón buscar
                    btnBuscar.Enabled = True
                    'Se hace en un focus en la entrada del txtNomUsuario
                    txtNomUsuario.Focus()
                Else
                    '***********************************************En caso de que la condición no se cumpla*******************************
                    'Se muestra un mensaje indicando el error
                    MessageBox.Show("Error al modificar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'Se llama al método mLimpiarCampos
                    mLimpiarCampos()
                End If 'Fin del bolModificar

            Else
                '***********************************************En caso de que la condición no se cumpla*******************************
                'Se muestra un mensaje indicando el error
                MessageBox.Show("Contraseña no son iguales", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                'Se limpia el campo  de Contraseña
                txtContrasena.Text = ""
                'Se limpia el campo de confirmarCont
                txtConfirmarCont.Text = ""
                'Se hace un enfásis de entrada en el campo de contraseña
                txtContrasena.Focus()
            End If 'Fin  de if Contrasenas
        End If 'Fin del if Campos vacíos

    End Sub 'Fin de la función

    '***************************************************************************************************************************
    'Esta región contiene los métodos necesarios para el funcionamiento de la clase
#Region "Metodos"
    'Método que permite la extracción de  la primer letra del nombre, que se utilizara para el Login
    Private Function mExtraerPrimeraLetra() As String
        'Se crea la variable str de tipo String 
        Dim str As String
        'Se hace una asignación a la varible str
        str = Mid(txtNomUsuario.Text, 1, 1)
        'Se retorna la primera letra para el login
        Return str.ToLower
    End Function

    'Permite asignar un login de acuerdo a la informacion que ingresa el usuario y lo que se encuentre en la base de datos
    Private Sub mVerificarLogin()
        'Se concatena en el campo txtLogin el método mExtraerPrimeraLetra + txtApellido1
        txtLogin.Text = mExtraerPrimeraLetra() + txtApellido1.Text.ToLower
        ' Se le asigna al entidad usuario en el campo login lo que contenga el txtLogin
        clEntidadUsario.pLogin = Me.txtLogin.Text
        'Se le indica a la variable que lea los que posee el método mConsultarUsuarioLogin y se envían los párametros necesarios
        drUsuario = clUsuario.mConsultarUsuarioLogin(clEntidadUsario, clConexion)
        'Se verifica que el drUsuario lea el login
        If drUsuario.Read Then
            '***********************************En caso que el login exista*********************************************************************
            'En el campo txtLogin se concatena la el método mExtraerPrimeraLetra más el primer y segundo apellido ingresado
            txtLogin.Text = mExtraerPrimeraLetra() + txtApellido1.Text.ToLower + txtApellido2.Text.ToLower
        Else
            '*****************************************En caso que el login no exista ********************************************************
            'En el campo txtLogin se concatena el método mExtraerPrimeraLetra mas el primer apellido
            txtLogin.Text = mExtraerPrimeraLetra() + txtApellido1.Text.ToLower
        End If 'Fin del If drUsuario
    End Sub 'Fin del método mVerificarLogin

    'Método que permite limpiar los campos de textos
    Public Sub mLimpiarCampos()
        'Elimina la información que posee el txtNomUsuario
        Me.txtNomUsuario.Text = ""
        'Elimina la información que posee el txtApellido1
        Me.txtApellido1.Text = ""
        'Elimina la información que posee el txtApellido2
        Me.txtApellido2.Text = ""
        'Elimina la información que posee el txtLogin
        Me.txtLogin.Text = ""
        'Elimina la información que posee el txtContrasena
        Me.txtContrasena.Text = ""
        'Elimina la información que posee el txtConfirmarCont
        Me.txtConfirmarCont.Text = ""
        'Elimina la información que posee el pcxImagen
        Me.pcxImagen.Image = Nothing
        'Se define el pcImagen2 como no visible 
        picImagen2.Visible = False
    End Sub 'Fin del método mLimpiarCampos

    'Método que permite Encriptar la contraseña
    Public Function mEncriptar(ByVal Clave As String) As String
        'Creación de la variable IV de tipo Byte
        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("qualityi") 'La clave debe ser de 8 caracteres
        'Creación de la variable EncrpytionKey de tipo Byte 
        Dim EncryptionKey() As Byte = Convert.FromBase64String("rpaSPvIvVLlrcmtzPU9/c67Gkj7yL1S5") 'No se puede alterar la cantidad de caracteres pero si la clave
        ' Se crea la variable buffer de tipo Byte
        Dim buffer() As Byte = Encoding.UTF8.GetBytes(Clave)
        'se crea una variable des de tipo TripleDESCryptoServiceProvider
        Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        'Se igualan las variables antes creadas 
        des.Key = EncryptionKey
        'Se igualan las variables antes creadas 
        des.IV = IV

        'Se retorna la contraseña encriptada
        Return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

    End Function 'FIn del método mEncriptar

    'Metodo para extrar los datos de la base de datos
    Public Sub mExtrarDatosUsu(ByVal strLogin As String)
        'Se indica a la entidad usuario que tome lo que tiene el strLogin
        clEntidadUsario.pLogin = strLogin
        'Se asiigna a drUsuario lo que devuelva el método MConsultarUsuarioLogin y se envían los parámetros establecidos 
        drUsuario = clUsuario.mConsultarUsuarioLogin(clEntidadUsario, clConexion)
        'Se define el pcImagen2 como no visible 
        picImagen2.Visible = False

        'Se crea una variable de tipo String 
        Dim desContrasena As String
        'Se verifica que se lea la drUsuario
        If drUsuario.Read Then
            ' Se extrae lo que la base tiene en el campo
            txtNomUsuario.Text = drUsuario.Item("nombre")
            ' Se extrae lo que la base tiene en el campo
            txtApellido1.Text = drUsuario.Item("apellido1Usu")
            ' Se extrae lo que la base tiene en el campo
            txtApellido2.Text = drUsuario.Item("apellido2Usu")
            ' Se extrae lo que la base tiene en el campo
            txtLogin.Text = drUsuario.Item("login")
            ' Se extrae lo que la base tiene en el campo
            desContrasena = clUsuario.mDesencriptar(drUsuario.Item("contrasena"))
            'se desencripta la contraseña
            txtContrasena.Text = desContrasena
            'se desencripta la contraseña
            txtConfirmarCont.Text = desContrasena

            'Verifica que la ruta de la imagen exista
            If (File.Exists(clUsuario.mDesencriptar(drUsuario.Item("imagenUsu")))) Then
                'Se carga la imagen con la ruta desencriptada
                pcxImagen.Load(clUsuario.mDesencriptar(drUsuario.Item("imagenUsu")))

            Else
                'Se determina el picturebox como visible 
                Me.picImagen2.Visible = True

            End If 'Fin del if

            'Se inahabilita el texto
            txtNomUsuario.Enabled = False
            'Se inahabilita el texto
            txtApellido1.Enabled = False
            'Se inahabilita el texto
            txtApellido2.Enabled = False
            'Se inahabilita el texto
            txtLogin.Enabled = False
            'Se inahabilita el botón
            btnAgregar.Enabled = False
            'Se inahabilita el botón
            btnImagen.Enabled = False
            'Se habilita el botón
            btnModificar.Enabled = True
        End If

    End Sub 'Fin del método

    'Esta función permite la autocarga de elementos d ela ventana
    Private Sub frmUsuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Se indica el color que obtendrá el panel
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        'Se indica la posicion 
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        'Se indica el tamaño de la pantalla  para que se adapte al tamaño de la pantalla
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
    End Sub 'Fin de la función

    'Evento que sucede al hacer clic sobre el boton imagen
    Private Sub btnImagen_Click(sender As Object, e As EventArgs) Handles btnImagen.Click
        'Se determina e picturebox como no visible 
        picImagen2.Visible = False
        'Se llama al método 
        mCargarImagen()
    End Sub 'Fin del evento

    'Evento que sucede cuando se hace click sobre el botón regresar
    Private Sub btnRegresar_Click(sender As Object, e As EventArgs) Handles btnRegresar.Click
        'Se pone inahabilita la visibilidad de la ventana
        SetVisibleCore(False)
        'Se habilita la vista a la ventana menu
        clMenu.Show()
    End Sub 'Fin del evento

    'Metodo para cargar la imagen 
    Private Sub mCargarImagen()
        ' Se inicializa la variable vacía
        strImagen = ""

        'Evita errores
        Try
            'Permite abrir la imagen la imagen
            Me.OpenFileDialog1.ShowDialog()

            'Se verifica que la ruta no se encuentre vacía
            If Me.OpenFileDialog1.FileName <> "" Then
                'Se asigna la ruta de la imagen
                strImagen = Me.OpenFileDialog1.FileName.ToString
                ' Se crea una variable de tipo integer
                Dim largo As Integer = strImagen.Length
                ' Se crea una variable de tipo integer
                Dim imagen As String
                imagen = CStr(Microsoft.VisualBasic.Mid(RTrim(strImagen), largo - 2, largo))
                If imagen <> "gif" And imagen <> "bmp" And imagen <> "jpg" And imagen <> "jpeg" And imagen <> "GIF" And imagen <> "BMP" And imagen <> "JPG" And imagen <> "JPEG" And imagen <> "log1" Then
                    imagen = CStr(Microsoft.VisualBasic.Mid(RTrim(strImagen), largo - 3, largo))
                    'Verifica el formato de la imagen
                    If imagen <> "jpg" And imagen <> "JPEG" And imagen <> "log1" Then
                        MsgBox("Formato no válido") : Exit Sub
                        'Verfica que imagen se diferente a log1
                        If imagen <> "log1" Then
                            Exit Sub    'Indica que se termine el método 
                            pcxImagen.Load(strImagen)
                        End If  'Fin del If <>log1
                    End If  'Fin del If Formato no válido
                End If  'Fin del if Cstr
            End If  'Fin del If  vacío
        Catch ex As Exception 'Lee exepciones a errores
        End Try     'Fin del Try

        'Verifica que el nombre de la imagen sea diferente a openfileDIalog1
        If strImagen <> "OpenFileDialog1" Then
            'Se autocaega la imagen  y se envía la ruta por párametro
            pcxImagen.Load(strImagen)
        End If ' Fin del if
    End Sub 'Fin del método mCargarImagen

#End Region'Fin de la region

End Class 'Fin de la clase