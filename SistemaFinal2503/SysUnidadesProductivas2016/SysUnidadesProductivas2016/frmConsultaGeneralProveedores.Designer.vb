﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmConsultaGeneralProveedores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.lvProveedor = New System.Windows.Forms.ListView()
        Me.Cedula = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Nombre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.PriApellido = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SegApellido = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Tipo = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.NombreEmpresa = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Provincia = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Canton = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Distrito = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.OtrasSenas = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Descripcion = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.lvProveedor)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(15, 15)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(979, 617)
        Me.Panel1.TabIndex = 0
        '
        'Button2
        '
        Me.Button2.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Button2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button2.BackgroundImage = Global.SysUnidadesProductivas2016.My.Resources.Resources.desenfoque7070
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Image = Global.SysUnidadesProductivas2016.My.Resources.Resources.regresar
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(815, 556)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 35)
        Me.Button2.TabIndex = 18
        Me.Button2.Text = "Regresar"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = True
        '
        'lvProveedor
        '
        Me.lvProveedor.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lvProveedor.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Cedula, Me.Nombre, Me.PriApellido, Me.SegApellido, Me.Tipo, Me.NombreEmpresa, Me.Provincia, Me.Canton, Me.Distrito, Me.OtrasSenas, Me.Descripcion})
        Me.lvProveedor.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvProveedor.FullRowSelect = True
        Me.lvProveedor.GridLines = True
        Me.lvProveedor.Location = New System.Drawing.Point(18, 137)
        Me.lvProveedor.Name = "lvProveedor"
        Me.lvProveedor.Size = New System.Drawing.Size(944, 375)
        Me.lvProveedor.TabIndex = 1
        Me.lvProveedor.UseCompatibleStateImageBehavior = False
        Me.lvProveedor.View = System.Windows.Forms.View.Details
        '
        'Cedula
        '
        Me.Cedula.Text = "Cédula"
        Me.Cedula.Width = 120
        '
        'Nombre
        '
        Me.Nombre.Text = "Nombre"
        Me.Nombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Nombre.Width = 120
        '
        'PriApellido
        '
        Me.PriApellido.Text = "Pri. Apellido"
        Me.PriApellido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.PriApellido.Width = 120
        '
        'SegApellido
        '
        Me.SegApellido.Text = "Seg. Apellido"
        Me.SegApellido.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.SegApellido.Width = 120
        '
        'Tipo
        '
        Me.Tipo.Text = "Tipo"
        Me.Tipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Tipo.Width = 120
        '
        'NombreEmpresa
        '
        Me.NombreEmpresa.Text = "Empresa"
        Me.NombreEmpresa.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.NombreEmpresa.Width = 120
        '
        'Provincia
        '
        Me.Provincia.Text = "Provincia"
        Me.Provincia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Provincia.Width = 120
        '
        'Canton
        '
        Me.Canton.Text = "Cantón"
        Me.Canton.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Canton.Width = 120
        '
        'Distrito
        '
        Me.Distrito.Text = "Distrito"
        Me.Distrito.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Distrito.Width = 120
        '
        'OtrasSenas
        '
        Me.OtrasSenas.Text = "Otras Señas"
        Me.OtrasSenas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.OtrasSenas.Width = 284
        '
        'Descripcion
        '
        Me.Descripcion.Text = "Descripción"
        Me.Descripcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.Descripcion.Width = 200
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(154, Byte), Integer), CType(CType(164, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(25, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(910, 72)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Listado de Proveedores"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmConsultaGeneralProveedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(26, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1006, 649)
        Me.ControlBox = False
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmConsultaGeneralProveedores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents lvProveedor As ListView
    Friend WithEvents Cedula As ColumnHeader
    Friend WithEvents Nombre As ColumnHeader
    Friend WithEvents PriApellido As ColumnHeader
    Friend WithEvents SegApellido As ColumnHeader
    Friend WithEvents Tipo As ColumnHeader
    Friend WithEvents NombreEmpresa As ColumnHeader
    Friend WithEvents Provincia As ColumnHeader
    Friend WithEvents Canton As ColumnHeader
    Friend WithEvents Distrito As ColumnHeader
    Friend WithEvents OtrasSenas As ColumnHeader
    Friend WithEvents Label1 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Descripcion As ColumnHeader
End Class
