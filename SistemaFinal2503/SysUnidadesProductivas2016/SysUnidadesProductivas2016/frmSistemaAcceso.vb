﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadUsuario

Public Class frmSistemaAcceso

#Region "Atributos"
    'ABRIR CONEXION CON LA BASE DE DATOS
    Dim clsConexion As New AccesoDatos.clsConexion
    Dim bolAbrirConexion, bolConfirmarUsuario As Boolean
    'EN RELACION A LOS USUARIOS
    Dim clsEntidadUsuario As New EntidadNegocios.clsEntidadUsuario
    Dim clsEntidadPermisoUsModulo As New EntidadNegocios.clsEntidadPermisosUsersModulo
    Dim clsEntidadPermisoUsConsulta As New EntidadNegocios.clsEntidadPermisoUserConsulta
    Dim clsLogicaUsuario As New LogicaNegocios.clsLogicaUsuario
    Dim clsLogicaPermisoUsModulo As New LogicaNegocios.clsLogicaPermisosUsersModulo
    Dim clsLogicaPermisoUsConsulta As New LogicaNegocios.clsLogicaPermisosUsersConsulta



    Dim strContrasenaDes As String

    Dim drUsuario, drPermisoUsModulos, drPermisoUsConsulta As MySql.Data.MySqlClient.MySqlDataReader

    Dim bolMUsuario, bolMCliente, bolMProducto, bolMProveedor, bolMFactura As Boolean
    Dim bolCUsuario, bolCUsuarioEsp, bolCUsuarioGen, bolCCliente, bolCClienteEsp, bolCClienteGen, bolCProducto, bolCProductoEsp, bolCProductoGen, bolCProveedor, bolCProveedorEsp, bolCProveedorGen, bolCFactura, bolCFacturaEsp, bolCFacturaGen, bolPermiso As Boolean


#End Region
#Region "LOAD"
    Private Sub frmSistemaAcceso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        clsConexion.pUsuario = "root"
        clsConexion.pNomBD = "dbProyectoSan"
        clsConexion.pNomServidor = "localhost"
        clsConexion.pPassword = ""



    End Sub
#End Region
#Region "Botones"
    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        mVariablesFalse()

        clsEntidadUsuario.pLogin = txtLogin.Text.ToLower

       drUsuario = clsLogicaUsuario.mConsultarUsuarioLogin(clsEntidadUsuario, clsConexion)
        If drUsuario.Read Then
            strContrasenaDes = clsLogicaUsuario.mDesencriptar(drUsuario.Item("contrasena"))

            If strContrasenaDes = txtContrasena.Text Then
                clsEntidadUsuario.pLogin = txtLogin.Text.Trim
                clsEntidadPermisoUsModulo.pLoginUsuario = txtLogin.Text.Trim
                '+++SEGURIDAD
                drPermisoUsModulos = clsLogicaPermisoUsModulo.mCosultarPermisoModulo(clsEntidadPermisoUsModulo, clsConexion)

                While drPermisoUsModulos.Read
                    If drPermisoUsModulos.Item("idVentana") = "003" Then
                        bolMUsuario = True
                    End If

                    If drPermisoUsModulos.Item("idVentana") = "004" Then
                        bolMCliente = True
                    End If

                    If drPermisoUsModulos.Item("idVentana") = "005" Then
                        bolMProducto = True
                    End If

                    If drPermisoUsModulos.Item("idVentana") = "006" Then
                        bolMProveedor = True
                    End If

                    If drPermisoUsModulos.Item("idVentana") = "007" Then
                        bolMFactura = True
                    End If

                    If drPermisoUsModulos.Item("idVentana") = "008" Then
                        bolPermiso = True
                    End If


                End While
                clsEntidadPermisoUsConsulta.pLoginUsuario = txtLogin.Text.Trim

                drPermisoUsConsulta = clsLogicaPermisoUsConsulta.mCosultarPermisoConsulta(clsEntidadPermisoUsConsulta, clsConexion)
                While drPermisoUsConsulta.Read
                    If drPermisoUsConsulta.Item("idVentana") = "100" And drPermisoUsConsulta.Item("permisoConsultar") = "SI" Then
                        bolCUsuarioEsp = True
                    End If

                    If drPermisoUsConsulta.Item("idVentana") = "101" And drPermisoUsConsulta.Item("permisoConsultar") = "SI" Then
                        bolCUsuarioGen = True
                    End If

                    If drPermisoUsConsulta.Item("idVentana") = "102" And drPermisoUsConsulta.Item("permisoConsultar") = "SI" Then
                        bolCClienteEsp = True
                    End If

                    If drPermisoUsConsulta.Item("idVentana") = "103" And drPermisoUsConsulta.Item("permisoConsultar") = "SI" Then
                        bolCClienteGen = True
                    End If

                    If drPermisoUsConsulta.Item("idVentana") = "104" And drPermisoUsConsulta.Item("permisoConsultar") = "SI" Then
                        bolCProductoEsp = True
                    End If

                    If drPermisoUsConsulta.Item("idVentana") = "105" And drPermisoUsConsulta.Item("permisoConsultar") = "SI" Then
                        bolCProductoGen = True
                    End If

                    If drPermisoUsConsulta.Item("idVentana") = "106" And drPermisoUsConsulta.Item("permisoConsultar") = "SI" Then
                        bolCProveedorEsp = True
                    End If

                    If drPermisoUsConsulta.Item("idVentana") = "107" And drPermisoUsConsulta.Item("permisoConsultar") = "SI" Then
                        bolCProveedorGen = True
                    End If
                    If drPermisoUsConsulta.Item("idVentana") = "108" And drPermisoUsConsulta.Item("permisoConsultar") = "SI" Then
                        bolCFacturaEsp = True
                    End If
                    If drPermisoUsConsulta.Item("idVentana") = "109" And drPermisoUsConsulta.Item("permisoConsultar") = "SI" Then
                        bolCFacturaGen = True
                    End If

                End While
                Dim frm As New frmMDIMenuPrincipal(clsConexion, clsEntidadUsuario, Me)
                frm.UsuariosToolStripMenuItem.Enabled = bolMUsuario
                frm.ClientesToolStripMenuItem.Enabled = bolMCliente
                frm.ProductosToolStripMenuItem.Enabled = bolMProducto
                frm.ProveedoresToolStripMenuItem.Enabled = bolMProveedor
                frm.FacturaToolStripMenuItem.Enabled = bolMFactura
                frm.UsuarioCGeneralToolStripMenuItem2.Enabled = bolCUsuarioGen
                frm.UsuarioCEspecificaToolStripMenuItem.Enabled = bolCUsuarioEsp
                frm.ClienteCGeneralToolStripMenuItem1.Enabled = bolCClienteGen
                frm.ClienteCEspecificaToolStripMenuItem.Enabled = bolCClienteEsp
                frm.ProductoCGeneralToolStripMenuItem3.Enabled = bolCProductoGen
                frm.ProductoCEspecificaToolStripMenuItem1.Enabled = bolCProductoEsp
                frm.ProveedorCGeneralToolStripMenuItem.Enabled = bolCProveedorGen
                frm.ProveedorCEspecificaToolStripMenuItem.Enabled = bolCProveedorEsp
                frm.FacturaCGeneralToolStripMenuItem4.Enabled = bolCFacturaGen
                frm.FacturaCEspecificaToolStripMenuItem2.Enabled = bolCFacturaEsp
                frm.PermisosUsuarioToolStripMenuItem.Enabled = bolPermiso


                With (frm)
                    .Owner = Me
                    .StartPosition = FormStartPosition.CenterParent
                    .ShowDialog()
                    .Dispose()




                End With


            Else
                MessageBox.Show("Usuario/Contraseña incorrectos, verifique su información.", "ATENCION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                txtContrasena.Text = ""
                txtContrasena.PasswordChar = "*"
                txtLogin.Text = ""

            End If
        Else
            MessageBox.Show("Usuario/Contraseña incorrectos, verifique su información.", "ATENCION", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtContrasena.Text = ""
            txtContrasena.PasswordChar = "*"
            txtLogin.Text = ""
        End If

        'bolAbrirConexion = clsConexion.mAbrirConexion()
        'If bolAbrirConexion = True Then

        'Else
        '    MessageBox.Show("error", "error")
        'End If


    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        End

    End Sub
#End Region

#Region "Constructor"

#End Region

#Region "Metodos"
    Public Sub mVariablesFalse()
        bolMUsuario = False
        bolMCliente = False
        bolMProducto = False
        bolMProveedor = False
        bolMFactura = False
        bolCUsuario = False
        bolCUsuarioEsp = False
        bolCUsuarioGen = False
        bolCCliente = False
        bolCClienteEsp = False
        bolCClienteGen = False
        bolCProducto = False
        bolCProductoEsp = False
        bolCProductoGen = False
        bolCProveedor = False
        bolCProveedorEsp = False
        bolCProveedorGen = False
        bolCFactura = False
        bolCFacturaEsp = False
        bolCFacturaGen = False
        bolPermiso = False
    End Sub
#End Region



End Class