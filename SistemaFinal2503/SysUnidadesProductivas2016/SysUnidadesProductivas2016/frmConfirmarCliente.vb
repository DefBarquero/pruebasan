﻿Public Class frmConfirmarCliente


#Region "Atributos"


    Dim frmClientes As frmClientes

    Dim EntidadProvincia As EntidadNegocios.clsEntidadProvincia
    Dim EntidadCanton As EntidadNegocios.clsEntidadCanton
    Dim EntidadDistrito As EntidadNegocios.clsEntidadDistrito


    Dim clsEntidadCli As New EntidadNegocios.clsEntidadCliente
    Dim clsEntidadCorreoCli As New EntidadNegocios.clsEntidadCorreoCliente
    Dim clsEntidadTelefonoCli As New EntidadNegocios.clsEntidadTelefonoCliente


    Dim clsLogicaCli As New LogicaNegocios.clsLogicaCliente
    Dim clsLogicaCorreoCli As New LogicaNegocios.clsLogicaClienteCorreo
    Dim clsLogicaTelefonoCli As New LogicaNegocios.clsLogicaClienteTelefono


    Dim clsConexion As AccesoDatos.clsConexion
    Dim bolAgregarCli, bolAgregarCorreoCli, bolAgregarTelefonoCli


    'para el manejo de los datadrig
    Dim intCodigo, Filas, x, intContador, duplas As Integer

    Dim drCliente, drCorreoCli, drCorreoCli2, drTelefonoCli, drProvincia, drCanton, drDistrito As MySql.Data.MySqlClient.MySqlDataReader



#End Region


    Public Sub New(clientes As EntidadNegocios.clsEntidadCliente, pConexion As AccesoDatos.clsConexion, frmCliente As frmClientes, telefono As EntidadNegocios.clsEntidadTelefonoCliente,
                   correo As EntidadNegocios.clsEntidadCorreoCliente, provincia As EntidadNegocios.clsEntidadProvincia,
                   canton As EntidadNegocios.clsEntidadCanton, distrito As EntidadNegocios.clsEntidadDistrito)

        ' Esta llamada es exigida por el diseñador.

        Me.clsConexion = pConexion
        Me.frmClientes = frmCliente
        Me.clsEntidadCli = clientes
        Me.EntidadProvincia = provincia
        Me.EntidadCanton = canton
        Me.EntidadDistrito = distrito


        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub frmConfirmarCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Datos personales
        lblCedula.Text = clsEntidadCli.pCedulaCli
        lblNombre.Text = clsEntidadCli.pNombreCli
        lblPrimerA.Text = clsEntidadCli.pApellido1Cli
        lblSegundoA.Text = clsEntidadCli.pApellido2Cli
        lblTelefono.Text = frmClientes.dvgTelefono.Rows(x).Cells("NUMERO").Value
        lblCorreo.Text = frmClientes.dvgCorreo.Rows(x).Cells("EMAIL").Value

        'Datos de Ubicacion

        lblProvincia.Text = EntidadProvincia.pNombreProvincia
        lblCanton.Text = EntidadCanton.pNombreCanton
        lblDistrito.Text = EntidadDistrito.pNombreDistrito
        txtOtrasSenas.Text = clsEntidadCli.pOtraSenasCli

    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub

    Private Sub btnRechazar_Click(sender As Object, e As EventArgs) Handles btnRechazar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        bolAgregarCli = clsLogicaCli.mAgregarCliente(clsEntidadCli, clsConexion)


        If bolAgregarCli = True Then

            'AGREGAR LOS CORREOS DE LOS CLIENTES

            Filas = frmClientes.dvgCorreo.Rows.Count
            For Me.x = 0 To Filas - 2 '' se le quitan 2 al conteo ya que el ultimo es un registro no usado, y el anterior esta en blanco

                drCorreoCli = Me.clsLogicaCorreoCli.mGenerarIDCorreoCli(clsConexion)
                If drCorreoCli.Read Then
                    clsEntidadCorreoCli.pIDCorreoCli = drCorreoCli.Item(0)
                End If

                clsEntidadCorreoCli.pCorreoCli = frmClientes.dvgCorreo.Rows(x).Cells("EMAIL").Value
                clsEntidadCorreoCli.pCedulaCli = frmClientes.mktCedula.Text.Trim

                bolAgregarCorreoCli = clsLogicaCorreoCli.mAgregarCorreoCli(clsEntidadCorreoCli, clsConexion)

            Next


            Filas = frmClientes.dvgTelefono.Rows.Count
            For Me.x = 0 To Filas - 2 ' se le quitan 2 al conteo ya que el ultimo es un registro no usado, y el anterior esta en blanco

                drTelefonoCli = Me.clsLogicaTelefonoCli.mGenerarIDTelefonoCli(clsConexion)
                If drTelefonoCli.Read Then
                    Me.clsEntidadTelefonoCli.pIDTelefonoCli = drTelefonoCli.Item(0)
                End If

                clsEntidadTelefonoCli.pTelefonoCli = frmClientes.dvgTelefono.Rows(x).Cells("NUMERO").Value

                clsEntidadTelefonoCli.pCedulaCli = frmClientes.mktCedula.Text.Trim

                bolAgregarTelefonoCli = Me.clsLogicaTelefonoCli.mAgregarTelefonoCli(clsEntidadTelefonoCli, clsConexion)

            Next

            MessageBox.Show("Cliente agregado correctamente.", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information)
            frmClientes.mLimpiarCampos()
            frmClientes.mktCedula.Enabled = True
            frmClientes.mLlenarComboProvincia()

        Else
            MessageBox.Show("Error al agregar el Cliente", "ADVERTENCIA", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            frmClientes.mLimpiarCampos()
            frmClientes.mktCedula.Enabled = True
            frmClientes.mLlenarComboProvincia()

        End If 'fin if para agregar la información.

        Me.Close()

    End Sub
End Class