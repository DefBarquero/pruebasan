﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios
Imports LogicaNegocios

Public Class frmConsultaEspecificaProveedores

#Region "Atibutos"
    Dim clsConexion As AccesoDatos.clsConexion

    Dim clsEntidadProveedor As New EntidadNegocios.clsEntidadProveedor
    Dim clsEntidadCorreoPro As New EntidadNegocios.clsEntidadCorreoProveedor
    Dim clsEntidadTelefonoPro As New EntidadNegocios.clsEntidadTelefonoProveedor

    Dim clsLogicaProveedor As New LogicaNegocios.clsLogicaProveedor
    Dim clsLogicaCorreoPro As New LogicaNegocios.clsLogicaProveedorCorreo
    Dim clsLogicaTelefonoPro As New LogicaNegocios.clsLogicaProveedorTelefono

    Dim drProveedor, drCorreoPro, drTelefonoPro As MySql.Data.MySqlClient.MySqlDataReader

    Dim intContador1, intContador2 As Integer



    Dim strNombre, strCedula As String

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        SetVisibleCore(False)
    End Sub


#End Region

#Region "Herramientas"

    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion


    End Sub

    Private Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        lvCorreo.Items.Clear()
        lvTelefono.Items.Clear()
        intContador1 = 0
        intContador2 = 0

        Dim frmBuscarProveedor As New frmProveedorBuscar(clsConexion)
        With (frmBuscarProveedor)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With

        If frmBuscarProveedor.mRetornarCedulaPro <> "" Then

            clsEntidadCorreoPro.pCedulaPro = frmBuscarProveedor.mRetornarCedulaPro
            clsEntidadProveedor.pCedulaPro = frmBuscarProveedor.mRetornarCedulaPro
            clsEntidadTelefonoPro.pCedulaPro = frmBuscarProveedor.mRetornarCedulaPro

            drProveedor = clsLogicaProveedor.mConsultaCedulaProveedor(clsEntidadProveedor, clsConexion)

            If drProveedor.Read Then
                lvProveedor.Items.Add(drProveedor.Item("cedulaPro"))
                lvProveedor.Items(0).SubItems.Add(drProveedor.Item("nombrePro"))
                lvProveedor.Items(0).SubItems.Add(drProveedor.Item("apellido1Pro"))
                lvProveedor.Items(0).SubItems.Add(drProveedor.Item("apellido2Pro"))
                lvProveedor.Items(0).SubItems.Add(drProveedor.Item("tipoPro"))
                lvProveedor.Items(0).SubItems.Add(drProveedor.Item("nomEmpresaPro"))
                lvProveedor.Items(0).SubItems.Add(drProveedor.Item("provinciaPro"))
                lvProveedor.Items(0).SubItems.Add(drProveedor.Item("cantonPro"))
                lvProveedor.Items(0).SubItems.Add(drProveedor.Item("distritoPro"))
                lvProveedor.Items(0).SubItems.Add(drProveedor.Item("otrasSenasPro"))
                lvProveedor.Items(0).SubItems.Add(drProveedor.Item("descripcionPro"))
            End If

            drTelefonoPro = clsLogicaTelefonoPro.mConsultaTelefonoProCedula(clsEntidadTelefonoPro, clsConexion)
            While drTelefonoPro.Read
                drProveedor = clsLogicaProveedor.mConsultaCedulaProveedor(clsEntidadProveedor, clsConexion)
                If drProveedor.Read Then
                    lvTelefono.Items.Add(drProveedor.Item("cedulaPro"))

                    lvTelefono.Items(intContador2).SubItems.Add(drTelefonoPro.Item("telefonoPro"))

                End If
                intContador2 = intContador2 + 1

            End While


            drCorreoPro = clsLogicaCorreoPro.mConsultaCorreoProCedula(clsEntidadCorreoPro, clsConexion)
            While drCorreoPro.Read
                drProveedor = clsLogicaProveedor.mConsultaCedulaProveedor(clsEntidadProveedor, clsConexion)
                If drProveedor.Read Then
                    lvCorreo.Items.Add(drProveedor.Item("cedulaPro"))

                    lvCorreo.Items(intContador1).SubItems.Add(drCorreoPro.Item("correoPro"))

                End If
                intContador1 = intContador1 + 1

            End While



        End If
    End Sub



    Private Sub frmConsultaEspecificaProveedores_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
    End Sub

#End Region

#Region "Metodos"

#End Region


End Class