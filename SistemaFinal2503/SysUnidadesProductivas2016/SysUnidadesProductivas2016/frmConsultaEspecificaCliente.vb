﻿Imports MySql.Data.MySqlClient
Imports EntidadNegocios
Imports LogicaNegocios
Imports AccesoDatos.clsConexion
Public Class frmConsultaEspecificaCliente

#Region "Atributos"

    'ACCESO DATOS
    Dim clsConexion As AccesoDatos.clsConexion

    'ENTIDAD
    Dim clsEntidadCliente As New EntidadNegocios.clsEntidadCliente
    Dim clsEntidadCorreoCli As New EntidadNegocios.clsEntidadCorreoCliente
    Dim clsEntidadTelefonoCli As New EntidadNegocios.clsEntidadTelefonoCliente

    'LOGICA
    Dim clsLogicaCliente As New LogicaNegocios.clsLogicaCliente
    Dim clsLogicaCorreoCli As New LogicaNegocios.clsLogicaClienteCorreo
    Dim clsLogicaTelefonoCli As New LogicaNegocios.clsLogicaClienteTelefono

    'DATAREADER
    Dim drCliente, drCorreoCli, drTelefonoCli As MySql.Data.MySqlClient.MySqlDataReader

    'INTEGER
    Dim intContador1, intContador2, intContador3 As Integer

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        SetVisibleCore(False)
    End Sub

    Private Sub frmConsultaEspecificaCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
    End Sub

#End Region

#Region "Herramientas"
    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        clsConexion = pConexion
    End Sub

    Private Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        lvClientes.Items.Clear()
        lvCorreos.Items.Clear()
        lvTelefonos.Items.Clear()
        intContador1 = 0
        intContador2 = 0

        'MOSTAR VENTAN
        Dim frmBuscarCliente As New frmClienteBuscar(clsConexion)
        With (frmBuscarCliente)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()

        End With

        If frmBuscarCliente.mRetornarCedulaCli <> "" Then
            clsEntidadCliente.pCedulaCli = frmBuscarCliente.mRetornarCedulaCli
            clsEntidadCorreoCli.pCedulaCli = frmBuscarCliente.mRetornarCedulaCli
            clsEntidadTelefonoCli.pCedulaCli = frmBuscarCliente.mRetornarCedulaCli

            drCliente = clsLogicaCliente.mBuscarCliente(clsEntidadCliente, clsConexion)
            If drCliente.Read Then
                lvClientes.Items.Add(drCliente.Item("cedulaCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("nombreCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("apellido1Cli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("apellido2Cli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("estadoCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("tipoCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("nomEmpresaCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("provinciaCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("cantonCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("distritoCli"))
                lvClientes.Items(0).SubItems.Add(drCliente.Item("otrasSenasCli"))
            End If 'fin del drCliente

            '***********************TELEFONOS***************************************
            drTelefonoCli = clsLogicaTelefonoCli.mBuscarTelefonoCliCedula(clsEntidadTelefonoCli, clsConexion)
            While drTelefonoCli.Read
                lvTelefonos.Items.Add(drTelefonoCli.Item("cedulaCli"))
                lvTelefonos.Items(intContador1).SubItems.Add(drTelefonoCli.Item("telefonoCli"))

                intContador1 = intContador1 + 1
            End While


            '***********************CORREOS****************************************

            drCorreoCli = clsLogicaCorreoCli.mConsultarCorreoCliCedula(clsEntidadCorreoCli, clsConexion)

            While drCorreoCli.Read
                lvCorreos.Items.Add(drCorreoCli.Item("cedulaCli"))
                lvCorreos.Items(intContador2).SubItems.Add(drCorreoCli.Item("correoCli"))

                intContador2 = intContador2 + 1

            End While

        End If ' fin frmBuscarCliente.mRetornarCedulaCli
    End Sub
#End Region





End Class