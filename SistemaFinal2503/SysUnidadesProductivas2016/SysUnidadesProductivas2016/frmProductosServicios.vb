﻿Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Security.Cryptography
Imports System.Text

Imports System.Object
Imports System.MarshalByRefObject
Imports System.IO.Stream
Imports System.IO.MemoryStream
Imports System.IO

Public Class frmProductosServicios

#Region "Atributos"
    Dim clConexion As AccesoDatos.clsConexion
    Dim clMenu As frmMDIMenuPrincipal
    Dim clEntidadProducto As New EntidadNegocios.clsEntidadProductoServicio
    Dim clsLogicaProducto As New LogicaNegocios.clsLogicaProductoServicio
    Dim drProducto As MySql.Data.MySqlClient.MySqlDataReader
    Dim drResultado As DialogResult
    Dim bolAgregar, bolModificar, bolEliminar As Boolean
    Dim strImagen As String

#End Region

#Region "Metodos"
    Public Sub New(pConexion As AccesoDatos.clsConexion, pMenu As frmMDIMenuPrincipal)
        clConexion = pConexion
        clMenu = pMenu

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub pbImagen_Click(sender As Object, e As EventArgs) Handles pbImagen.Click



    End Sub


    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        clEntidadProducto.pIdProductoSer = Me.txtCodigo.Text
        drProducto = clsLogicaProducto.mConsultaProducto(clEntidadProducto, clConexion)
        If drProducto.Read() Then
            MessageBox.Show("Este producto ya fue agregado previamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtCodigo.Focus()
            Me.mLimpiarCampos()
        Else
            If Me.txtCodigo.Text = "" Or txtDescripcion.Text = "" Or Me.txtNombre.Text = "" Or Me.mskCosto.Text = "" Or Me.mskPrecio.Text = "" Or (Me.rbnProducto.Text = "" Or Me.rbnServicio.Text = "") Then
                MessageBox.Show("Faltan campos por llenar", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Me.mLimpiarCampos()
                Me.txtCodigo.Focus()
            Else
                clEntidadProducto.pNombreProSer = txtNombre.Text.Trim
                clEntidadProducto.pCostoProSer = mskCosto.Text.Trim

                clEntidadProducto.pPrecioProSer = mskPrecio.Text.Trim
                clEntidadProducto.pDescripcionProSer = txtDescripcion.Text.Trim
                If rbnServicio.Checked = True Then
                    clEntidadProducto.pTipoProSer = "SERVICIO"
                Else
                    clEntidadProducto.pTipoProSer = "PRODUCTO"
                End If 'fin del rbn tipoPro
                If strImagen = Nothing Then
                    clEntidadProducto.pImagenProSer = mEncriptar(" ")
                Else
                    clEntidadProducto.pImagenProSer = mEncriptar(strImagen)
                End If

                Dim frmConfirmarProducto As frmConfirmarProducto = New frmConfirmarProducto(clEntidadProducto, clConexion, Me)
                frmConfirmarProducto.Show()


                '    bolAgregar = clsLogicaProducto.mAgregarProducto(clEntidadProducto, clConexion)
                '    If bolAgregar = True Then
                '        MessageBox.Show("Producto agregado correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '        Me.mLimpiarCampos()
                '        Me.txtCodigo.Focus()
                '    Else
                '        MessageBox.Show("Problemas al agregar el producto", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '        Me.mLimpiarCampos()
                '    End If 'Fin del if acierto o error al agregar
            End If 'Fin del if codigoProducto
        End If 'Fin del if si hay campos vacios
    End Sub 'fin del metodo agregar


    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        clEntidadProducto.pIdProductoSer = Me.txtCodigo.Text

        clEntidadProducto.pCostoProSer = mskCosto.Text.Trim
        clEntidadProducto.pPrecioProSer = mskPrecio.Text.Trim
        clEntidadProducto.pDescripcionProSer = txtDescripcion.Text.Trim
        clEntidadProducto.pImagenProSer = mEncriptar(strImagen)
        If rbnServicio.Checked = True Then
            clEntidadProducto.pTipoProSer = "SERVICIO"
        Else
            clEntidadProducto.pTipoProSer = "PRODUCTO"
        End If
            bolModificar = clsLogicaProducto.mModificarProducto(clEntidadProducto, clConexion)

        If bolModificar = True Then
            MessageBox.Show("Producto modificado correctamente", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.mLimpiarCampos()

        Else

            MessageBox.Show("Error al modificar el producto", "", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If 'Fin del if de modificar
    End Sub 'fin del metodo modificar

    Private Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        Dim frmConsultaPro As New frmConsultaProductoServicios(clConexion)
        With (frmConsultaPro)
            .Owner = Me
            .StartPosition = FormStartPosition.CenterParent
            .ShowDialog()
        End With

        If frmConsultaPro.mRetornarCodigo <> "" Then

            txtCodigo.Focus()
            Me.txtCodigo.Text = frmConsultaPro.mRetornarCodigo()
            mExtrarDatosProducto(Me.txtCodigo.Text)
        End If
    End Sub 'Fin del boton consultar


    'Limpia los campos de texto
    Public Sub mLimpiarCampos()
        Me.txtCodigo.Text = ""
        Me.txtNombre.Text = ""
        Me.txtDescripcion.Text = ""

        Me.mskCosto.Text = ""
        Me.mskPrecio.Text = ""
        rbnProducto.Checked = False
        rbnServicio.Checked = False
        pbImagen.Image = Nothing

        btnAgregar.Enabled = True
        btnConsultar.Enabled = True
        btnModificar.Enabled = False
        btnSalir.Enabled = True
        txtCodigo.Enabled = True
        txtNombre.Enabled = True
        txtDescripcion.Enabled = True
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs)
        Close()
    End Sub

    Private Sub frmProductos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.BackColor = Color.FromArgb(60, 74, 96)
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
    End Sub

    Public Function mEncriptar(ByVal Clave As String) As String

        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("qualityi") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("rpaSPvIvVLlrcmtzPU9/c67Gkj7yL1S5") 'No se puede alterar la cantidad de caracteres pero si la clave
        Dim buffer() As Byte = Encoding.UTF8.GetBytes(Clave)
        Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        des.Key = EncryptionKey
        des.IV = IV
        Return Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length()))
    End Function

    Private Sub btnSalir_Click_1(sender As Object, e As EventArgs) Handles btnSalir.Click
        SetVisibleCore(False)
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        picImage2.Visible = False

        mCargarImagen()
    End Sub

    Private Sub Label7_Click(sender As Object, e As EventArgs) Handles Label7.Click

    End Sub

    Private Sub mCargarImagen()
        strImagen = ""
        Try
            Me.OpenFileDialog1.ShowDialog()

            If Me.OpenFileDialog1.FileName <> "" Then
                strImagen = Me.OpenFileDialog1.FileName
                Dim largo As Integer = strImagen.Length
                Dim imagen As String
                imagen = CStr(Microsoft.VisualBasic.Mid(RTrim(strImagen), largo - 2, largo))
                If imagen <> "gif" And imagen <> "bmp" And imagen <> "jpg" And imagen <> "jpeg" And imagen <> "GIF" And imagen <> "BMP" And imagen <> "JPG" And imagen <> "JPEG" And imagen <> "log1" Then
                    imagen = CStr(Microsoft.VisualBasic.Mid(RTrim(strImagen), largo - 3, largo))
                    If imagen <> "jpg" And imagen <> "JPEG" And imagen <> "log1" Then
                        MsgBox("Formato No valido") : Exit Sub
                        If imagen <> "log1" Then Exit Sub
                        pbImagen.Load(strImagen)
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
        If strImagen <> "OpenFileDialog1" Then
            pbImagen.Load(strImagen) 'pbImage es el nombre del picturebox
        End If


    End Sub

    'Metodo que sirve para extraer los datos del producto 
    Public Sub mExtrarDatosProducto(ByVal strCodigo As String)

        clEntidadProducto.pIdProductoSer = Me.txtCodigo.Text
        drProducto = clsLogicaProducto.mConsultaProducto(clEntidadProducto, clConexion)
        If drProducto.Read() Then
            btnAgregar.Enabled = False
            btnModificar.Enabled = True
            txtCodigo.Enabled = False
            txtNombre.Enabled = False
            txtDescripcion.Enabled = False

            txtDescripcion.Text = drProducto.Item("descripcionPro_Ser")
            txtNombre.Text = drProducto.Item("nombrePro_Ser")
            If drProducto.Item("tipoPro_Ser") = "Servicio" Then
                rbnServicio.Checked = True
                rbnProducto.Checked = False
            Else
                rbnServicio.Checked = False
                rbnProducto.Checked = True
            End If

            mskCosto.Text = drProducto.Item("costoPro_Ser")
            mskPrecio.Text = drProducto.Item("precioPro_Ser")

            If (File.Exists(clsLogicaProducto.mDesencriptar(drProducto.Item("imagenPro_Ser")))) Then
                pbImagen.Load(clsLogicaProducto.mDesencriptar(drProducto.Item("imagenPro_Ser")))
            Else
                picImage2.Enabled = True


            End If

        Else
                MessageBox.Show("No existe ese producto", "Atencion", MessageBoxButtons.OK, MessageBoxIcon.Information)
            mLimpiarCampos()
        End If 'Fin del if existe el codigo
    End Sub
#End Region

End Class