﻿Imports MySql.Data.MySqlClient
Imports EntidadNegocios.clsEntidadProveedor
Imports LogicaNegocios.clsLogicaProveedor

Public Class frmConsultaGeneralProveedores

#Region "Atributos"

    Dim clsConexion As AccesoDatos.clsConexion

    Dim clsEntidadProveedor As New EntidadNegocios.clsEntidadProveedor
    Dim clsLogicaProveedor As New LogicaNegocios.clsLogicaProveedor

    Dim drProveedor As MySql.Data.MySqlClient.MySqlDataReader

    Dim intContador As Integer


#End Region

#Region "Costructor"
    Public Sub New(pConexion As AccesoDatos.clsConexion)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        clsConexion = pConexion
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
#End Region
    Private Sub frmConsultaGeneralProveedores_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = Screen.PrimaryScreen.WorkingArea.Location
        Me.Size = Screen.PrimaryScreen.WorkingArea.Size
        Panel1.BackColor = Color.FromArgb(60, 74, 96)

        drProveedor = clsLogicaProveedor.mConsultaGenProveedor(clsConexion)

        While drProveedor.Read
            lvProveedor.Items.Add(drProveedor.Item("cedulaPro"))
            lvProveedor.Items(intContador).SubItems.Add(drProveedor.Item("nombrePro"))
            lvProveedor.Items(intContador).SubItems.Add(drProveedor.Item("apellido1Pro"))
            lvProveedor.Items(intContador).SubItems.Add(drProveedor.Item("apellido2Pro"))
            lvProveedor.Items(intContador).SubItems.Add(drProveedor.Item("tipoPro"))
            lvProveedor.Items(intContador).SubItems.Add(drProveedor.Item("nomEmpresaPro"))
            lvProveedor.Items(intContador).SubItems.Add(drProveedor.Item("provinciaPro"))
            lvProveedor.Items(intContador).SubItems.Add(drProveedor.Item("cantonPro"))
            lvProveedor.Items(intContador).SubItems.Add(drProveedor.Item("distritoPro"))
            lvProveedor.Items(intContador).SubItems.Add(drProveedor.Item("otrasSenasPro"))
            lvProveedor.Items(intContador).SubItems.Add(drProveedor.Item("descripcionPro"))


            intContador = intContador + 1
        End While
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        SetVisibleCore(False)
    End Sub
End Class