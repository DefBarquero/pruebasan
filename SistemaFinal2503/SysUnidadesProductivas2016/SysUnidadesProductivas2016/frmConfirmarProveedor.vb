﻿Public Class frmConfirmarProveedor

#Region "Atributos"

    'CONEXION CON LA BASE DE DATOS
    Dim clsConexion As AccesoDatos.clsConexion

    Dim clsEntidadProveedor As New EntidadNegocios.clsEntidadProveedor
    Dim clsEntidadCorreoPro As New EntidadNegocios.clsEntidadCorreoProveedor
    Dim clsEntidadTelefonoPro As New EntidadNegocios.clsEntidadTelefonoProveedor
    Dim clsEntidadProvincia As New EntidadNegocios.clsEntidadProvincia
    Dim clsEntidadCanton As New EntidadNegocios.clsEntidadCanton
    Dim clsEntidadDistrito As New EntidadNegocios.clsEntidadDistrito

    'Logicas de Negocios
    Dim clsLogicaProveedor As New LogicaNegocios.clsLogicaProveedor
    Dim clsLogicaCorreoPro As New LogicaNegocios.clsLogicaProveedorCorreo
    Dim clsLogicaTelefonoPro As New LogicaNegocios.clsLogicaProveedorTelefono
    Dim clsLogicaProvincia As New LogicaNegocios.clsLogicaProvincia
    Dim clsLogicaCanton As New LogicaNegocios.clsLogicaCanton
    Dim clsLogicaDistrito As New LogicaNegocios.clsLogicaDistrito

    'DataReader
    Dim drProveedor, drCorreoPro, drTelefonoPro, drProvincia, drCanton, drDistrito As MySql.Data.MySqlClient.MySqlDataReader

    'Boolean 
    Dim bolAgregarPro, bolAgregarCorreoPro, bolAgregarTelPro

    'para el manejo de los datadrig
    Dim intCodigo, Filas, x, intContador, duplas As Integer

    Dim frmProveedor As frmProveedores

#End Region


    Public Sub New(proveedor As EntidadNegocios.clsEntidadProveedor, pConexion As AccesoDatos.clsConexion, frmProveedor As frmProveedores, telefono As EntidadNegocios.clsEntidadTelefonoProveedor,
                   correo As EntidadNegocios.clsEntidadCorreoProveedor, provincia As EntidadNegocios.clsEntidadProvincia,
                   canton As EntidadNegocios.clsEntidadCanton, distrito As EntidadNegocios.clsEntidadDistrito)

        ' Esta llamada es exigida por el diseñador.

        Me.clsConexion = pConexion

        Me.clsEntidadProveedor = proveedor
        Me.clsEntidadCorreoPro = correo
        Me.clsEntidadTelefonoPro = telefono
        Me.clsEntidadProvincia = provincia
        Me.clsEntidadCanton = canton
        Me.clsEntidadDistrito = distrito
        Me.frmProveedor = frmProveedor

        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub frmConfirmarProveedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        lblCedula.Text = clsEntidadProveedor.pCedulaPro
        lblNombre.Text = clsEntidadProveedor.pNombrePro
        lblPrimerA.Text = clsEntidadProveedor.pApellido1Pro
        lblSegundoA.Text = clsEntidadProveedor.pApellido2Pro
        lblCorreo.Text = frmProveedor.dvgCorreo.Rows(x).Cells("EMAIL").Value
        lblTelefono.Text = frmProveedor.dvgTelefono.Rows(x).Cells("Numero").Value

        'Direccion

        lblProvincia.Text = clsEntidadProvincia.pNombreProvincia
        lblCanton.Text = clsEntidadCanton.pNombreCanton
        lblDistrito.Text = clsEntidadDistrito.pNombreDistrito
        txtOtrasSenas.Text = clsEntidadProveedor.pOtrasSenasPro

        'Otros Datos

        lblTipo.Text = clsEntidadProveedor.pTipoPro
        txtDescripcion.Text = clsEntidadProveedor.pDescripcionPro
        lblNombreE.Text = clsEntidadProveedor.pNomEmpresaPro

    End Sub

#Region "Botones"

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub


    Private Sub btnRechazar_Click(sender As Object, e As EventArgs) Handles btnRechazar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click


        bolAgregarPro = clsLogicaProveedor.mAgregarProveedor(clsEntidadProveedor, clsConexion)

        If bolAgregarPro = True Then
            '    'AGREGAR LOS CORREOS DE LOS CLIENTES

            Filas = frmProveedor.dvgCorreo.Rows.Count

            For Me.x = 0 To Filas - 2 '' se le quitan 2 al conteo ya que el ultimo es un registro no usado, y el anterior esta en blanco
                drCorreoPro = Me.clsLogicaCorreoPro.mGenerarIDCorreoPro(clsConexion)

                If drCorreoPro.Read Then
                    clsEntidadCorreoPro.pIdCorreoPro = drCorreoPro.Item(0)
                End If
                clsEntidadCorreoPro.pCorreoPro = frmProveedor.dvgCorreo.Rows(x).Cells("EMAIL").Value
                clsEntidadCorreoPro.pCedulaPro = frmProveedor.mktCedula.Text.Trim
                bolAgregarCorreoPro = clsLogicaCorreoPro.mAgregarCorreoPro(clsEntidadCorreoPro, clsConexion)
            Next


            Filas = frmProveedor.dvgTelefono.Rows.GetRowCount(DataGridViewElementStates.Displayed)
            Filas = frmProveedor.dvgTelefono.Rows.Count

            For Me.x = 0 To Filas - 2 ' se le quitan 2 al conteo ya que el ultimo es un registro no usado, y el anterior esta en blanco
                drTelefonoPro = Me.clsLogicaTelefonoPro.mGenerarIDTelefonoPro(clsConexion)

                If drTelefonoPro.Read Then
                    Me.clsEntidadTelefonoPro.pIdTelefonoPro = drTelefonoPro.Item(0)
                End If

                clsEntidadTelefonoPro.pTelefonoPro = frmProveedor.dvgTelefono.Rows(x).Cells("Numero").Value
                clsEntidadTelefonoPro.pCedulaPro = frmProveedor.mktCedula.Text.Trim
                bolAgregarTelPro = Me.clsLogicaTelefonoPro.mAgregarTelefonoPro(clsEntidadTelefonoPro, clsConexion)
            Next

            MessageBox.Show("Proveedor agregado correctamente.", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information)
            frmProveedor.mLimpiar()
        Else
            MessageBox.Show("Error al agregar el Proveedor.", "ATENCIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            frmProveedor.mLimpiar()

        End If

    End Sub

#End Region


End Class