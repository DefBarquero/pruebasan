﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaProductoServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvConsultaPro = New System.Windows.Forms.ListView()
        Me.lvCodigo = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvNombre = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvDescripcion = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvCosto = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvPrecio = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvTipo = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnRegresar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout
        Me.SuspendLayout
        '
        'lvConsultaPro
        '
        Me.lvConsultaPro.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lvConsultaPro.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lvConsultaPro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lvConsultaPro.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.lvCodigo, Me.lvNombre, Me.lvDescripcion, Me.lvCosto, Me.lvPrecio, Me.lvTipo})
        Me.lvConsultaPro.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lvConsultaPro.FullRowSelect = true
        Me.lvConsultaPro.GridLines = true
        Me.lvConsultaPro.HoverSelection = true
        Me.lvConsultaPro.Location = New System.Drawing.Point(21, 107)
        Me.lvConsultaPro.Name = "lvConsultaPro"
        Me.lvConsultaPro.Size = New System.Drawing.Size(811, 325)
        Me.lvConsultaPro.TabIndex = 0
        Me.lvConsultaPro.UseCompatibleStateImageBehavior = false
        Me.lvConsultaPro.View = System.Windows.Forms.View.Details
        '
        'lvCodigo
        '
        Me.lvCodigo.Text = "Codigo"
        Me.lvCodigo.Width = 90
        '
        'lvNombre
        '
        Me.lvNombre.Text = "Nombre"
        Me.lvNombre.Width = 150
        '
        'lvDescripcion
        '
        Me.lvDescripcion.Text = "Descripcion"
        Me.lvDescripcion.Width = 210
        '
        'lvCosto
        '
        Me.lvCosto.Text = "Costo"
        Me.lvCosto.Width = 115
        '
        'lvPrecio
        '
        Me.lvPrecio.Text = "Precio"
        Me.lvPrecio.Width = 103
        '
        'lvTipo
        '
        Me.lvTipo.Text = "Tipo"
        Me.lvTipo.Width = 142
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.btnRegresar)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.lvConsultaPro)
        Me.Panel1.Location = New System.Drawing.Point(15, 15)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(859, 519)
        Me.Panel1.TabIndex = 2
        '
        'btnRegresar
        '
        Me.btnRegresar.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.btnRegresar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnRegresar.BackgroundImage = Global.SysUnidadesProductivas2016.My.Resources.Resources.desenfoque7070
        Me.btnRegresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnRegresar.FlatAppearance.BorderSize = 0
        Me.btnRegresar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRegresar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnRegresar.ForeColor = System.Drawing.Color.Black
        Me.btnRegresar.Image = Global.SysUnidadesProductivas2016.My.Resources.Resources.regresar
        Me.btnRegresar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegresar.Location = New System.Drawing.Point(712, 463)
        Me.btnRegresar.Name = "btnRegresar"
        Me.btnRegresar.Size = New System.Drawing.Size(120, 35)
        Me.btnRegresar.TabIndex = 8
        Me.btnRegresar.Text = "Regresar"
        Me.btnRegresar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRegresar.UseVisualStyleBackColor = true
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(154,Byte),Integer), CType(CType(164,Byte),Integer), CType(CType(175,Byte),Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(21, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(811, 72)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Listado de Productos - Servicios"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmConsultaProductoServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(26,Byte),Integer), CType(CType(42,Byte),Integer), CType(CType(68,Byte),Integer))
        Me.ClientSize = New System.Drawing.Size(886, 546)
        Me.ControlBox = false
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmConsultaProductoServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents lvConsultaPro As ListView
    Friend WithEvents lvCodigo As ColumnHeader
    Friend WithEvents lvNombre As ColumnHeader
    Friend WithEvents lvDescripcion As ColumnHeader
    Friend WithEvents lvCosto As ColumnHeader
    Friend WithEvents lvPrecio As ColumnHeader
    Friend WithEvents lvTipo As ColumnHeader
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents btnRegresar As Button
End Class
