﻿Public Class frmConfirmarFactura


#Region "Atributos"

    Dim clsConexion As AccesoDatos.clsConexion
    Dim clsEntidadCliente As New EntidadNegocios.clsEntidadCliente
    Dim clsEntidadProducto As New EntidadNegocios.clsEntidadProductoServicio
    Dim clsEntidadFacturaDet As New EntidadNegocios.clsEntidadFacturaDet
    Dim clsEntidadFacturaEnc As New EntidadNegocios.clsEntidadFacturaEnc
    Dim clsEntidadFacturaCostoDet As New EntidadNegocios.clsEntidadFacturaCostoDet
    Dim clsEntidadFacturaCostoEnc As New EntidadNegocios.clsEntidadFacturaCostoEnc

    Dim clsLogicaCliente As New LogicaNegocios.clsLogicaCliente
    Dim clsLogicaProducto As New LogicaNegocios.clsLogicaProductoServicio
    Dim pEntidadUsuario As EntidadNegocios.clsEntidadUsuario

    Dim frmFacturas As frmFacturas

    Dim drCliente, drProducto

    Dim Filas, intContador

#End Region


    Public Sub New(facturaEnc As EntidadNegocios.clsEntidadFacturaEnc, facturaDet As EntidadNegocios.clsEntidadFacturaDet, pConexion As AccesoDatos.clsConexion, frmFactura As frmFacturas, cliente As EntidadNegocios.clsEntidadCliente)

        ' Esta llamada es exigida por el diseñador.

        Me.clsConexion = pConexion
        Me.clsEntidadFacturaDet = facturaDet
        Me.clsEntidadFacturaEnc = facturaEnc
        Me.clsEntidadCliente = cliente

        Me.frmFacturas = frmFacturas
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub



    Private Sub frmConfirmaFactura_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        lblFecha.Text = clsEntidadFacturaEnc.pFecha
        lblIVA.Text = clsEntidadFacturaEnc.pIva
        lblTipoP.Text = clsEntidadFacturaEnc.pTipoPago
        lblNumeroF.Text = clsEntidadFacturaEnc.pIdFactura
        lblTotal.Text = clsEntidadFacturaEnc.pTotal
        'lblSubTotal.Text = clsEntidadFacturaDet.pSubTotal
        lblTalonario.Text = clsEntidadFacturaEnc.pTalonario

        drCliente = clsLogicaCliente.mBuscarCliente(clsEntidadCliente, clsConexion)

        If drCliente.Read Then
            lblNombreC.Text = drCliente.Item("nombrecli") + " " + drCliente.Item("apellido1Cli") + " " + drCliente.Item("apellido2Cli")

        End If

        Filas = 0


        Filas = dgvProductos.Rows.Count
            dgvProductos.Rows.Add()
            'If (frmFacturas.dvListadoCompra.Rows(intContador).Cells("CODIGO").Value) <> "" Then

            Filas = dgvProductos.Rows.Count
        'dvgListaP = frmFacturas.dvListadoCompra.Rows(intContador).Cells("CANTIDAD").Value
        'dvgListaP = frmFacturas.dvListadoCompra.Rows(intContador).Cells("TOTAL").Value
        'End If

        'drProducto = clsLogicaProducto.mConsultaProductoString(strCodigoProducto, clsConexion)
        If drProducto.Read Then
            'Me.dgvProductos.Item(Me.drProducto.Item("CODIGO", Filas - 1).Value = drProducto.Item("idProducto_Servicio"))
            Me.dgvProductos.Item("DESCRIPCION", Filas - 1).Value = drProducto.Item("nombrePro_Ser")
                Me.dgvProductos.Item("CANTIDAD", Filas - 1).Value = 1
                Me.dgvProductos.Item("MONTO", Filas - 1).Value = drProducto.Item("precioPro_Ser")
                Me.dgvProductos.Item("TOTAL", Filas - 1).Value = drProducto.Item("precioPro_Ser")
                'strCodigoProducto = ""
            End If

    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
End Class