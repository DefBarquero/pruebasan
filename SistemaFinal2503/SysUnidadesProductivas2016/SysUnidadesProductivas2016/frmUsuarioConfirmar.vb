﻿Public Class frmUsuarioConfirmar

#Region "Atributos"

    Dim clEntidadUsario As EntidadNegocios.clsEntidadUsuario
    Dim clConexion As AccesoDatos.clsConexion
    Dim bolAgregar, bolModificar As Boolean
    Dim clUsuario As New LogicaNegocios.clsLogicaUsuario
    Dim frmUsuario As frmUsuarios



#End Region

    Public Sub New(usuarios As EntidadNegocios.clsEntidadUsuario, pConexion As AccesoDatos.clsConexion, frmUsuarioC As frmUsuarios)

        ' Esta llamada es exigida por el diseñador.
        Me.clEntidadUsario = usuarios
        Me.clConexion = pConexion
        Me.frmUsuario = frmUsuarioC
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub


    Private Sub frmUsuarioConfirmar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.lblNombre.Text = clEntidadUsario.pNombre
        Me.lblPrimerA.Text = clEntidadUsario.pApellido1Usu
        Me.lblSegundoA.Text = clEntidadUsario.pApellido2Usu
        Me.lblLogin.Text = clEntidadUsario.pLogin
        Me.lblContrasena.Text = clEntidadUsario.pContrasena.ToString()


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnRechazar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        'Metodo para agregar el usuario a la base de datos
        bolAgregar = clUsuario.mAgregarUsuario(clEntidadUsario, clConexion)

        'Se verifica que la variable bolAgregar sea igual a True
        If bolAgregar = True Then
            '--------------En caso de que la condición sea verdadera---------------------------------
            'Se muestra un mensaje que la información ha sido guardada con éxito
            MessageBox.Show("La información del usuario se agrego correctamente", "Atención", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Se llama al método mLimpiarCampos
            frmUsuario.mLimpiarCampos()
            ' Se llama al método focus(), sirve para establecer un control en el foco de entrada del txtNomUsuario
            frmUsuario.txtNomUsuario.Focus()
            'Se limpia la imagen que contenga el picture box pcxImage
            frmUsuario.pcxImagen.Image = Nothing

        Else
            '------------------En caso que la condición sea falsa-----------------------------------------
            'Se muestra un mensaje indicando que existe un error al agregar el usuario
            MessageBox.Show("Error al agregar la información del usuario", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            'Se llama al método mLimpiarCampos
            frmUsuario.mLimpiarCampos()
        End If 'Fin del If bol agregar

        Me.Close()

    End Sub

#Region "Metodos"



#End Region

End Class