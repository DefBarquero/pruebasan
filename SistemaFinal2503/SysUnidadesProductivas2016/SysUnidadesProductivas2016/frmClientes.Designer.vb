﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnIr = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbxDistrito = New System.Windows.Forms.ComboBox()
        Me.cbxCanton = New System.Windows.Forms.ComboBox()
        Me.cbxProvincia = New System.Windows.Forms.ComboBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.rbnActivo = New System.Windows.Forms.RadioButton()
        Me.rbnPasivo = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbnPersona = New System.Windows.Forms.RadioButton()
        Me.rbnEmpresa = New System.Windows.Forms.RadioButton()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.dvgCorreo = New System.Windows.Forms.DataGridView()
        Me.IDEMAIL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EMAIL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dvgTelefono = New System.Windows.Forms.DataGridView()
        Me.idTelefono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NUMERO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtOtrasSenas = New System.Windows.Forms.TextBox()
        Me.lblOtrasSenas = New System.Windows.Forms.Label()
        Me.lblProvincia = New System.Windows.Forms.Label()
        Me.lblCanton = New System.Windows.Forms.Label()
        Me.lblDistrito = New System.Windows.Forms.Label()
        Me.lblDireccion = New System.Windows.Forms.Label()
        Me.lblSegApellido = New System.Windows.Forms.Label()
        Me.txtSegApellido = New System.Windows.Forms.TextBox()
        Me.txtPriApellido = New System.Windows.Forms.TextBox()
        Me.lblPriApellido = New System.Windows.Forms.Label()
        Me.txtNomCliente = New System.Windows.Forms.TextBox()
        Me.mktCedula = New System.Windows.Forms.MaskedTextBox()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.LblCedula = New System.Windows.Forms.Label()
        Me.lblNomEmpresa = New System.Windows.Forms.Label()
        Me.txtNomEmpresa = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout
        Me.GroupBox1.SuspendLayout
        Me.GroupBox5.SuspendLayout
        Me.GroupBox2.SuspendLayout
        Me.GroupBox4.SuspendLayout
        CType(Me.dvgCorreo,System.ComponentModel.ISupportInitialize).BeginInit
        Me.GroupBox3.SuspendLayout
        CType(Me.dvgTelefono,System.ComponentModel.ISupportInitialize).BeginInit
        Me.Panel2.SuspendLayout
        Me.SuspendLayout
        '
        'btnIr
        '
        Me.btnIr.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.btnIr.BackgroundImage = Global.SysUnidadesProductivas2016.My.Resources.Resources.desenfoque7070
        Me.btnIr.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnIr.FlatAppearance.BorderSize = 0
        Me.btnIr.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnIr.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnIr.ForeColor = System.Drawing.Color.White
        Me.btnIr.Image = Global.SysUnidadesProductivas2016.My.Resources.Resources.search
        Me.btnIr.Location = New System.Drawing.Point(226, 58)
        Me.btnIr.Name = "btnIr"
        Me.btnIr.Size = New System.Drawing.Size(52, 35)
        Me.btnIr.TabIndex = 21
        Me.btnIr.UseVisualStyleBackColor = true
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.btnBuscar)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.btnModificar)
        Me.Panel1.Controls.Add(Me.btnAgregar)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Location = New System.Drawing.Point(32, 22)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(975, 722)
        Me.Panel1.TabIndex = 1
        '
        'btnBuscar
        '
        Me.btnBuscar.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.btnBuscar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnBuscar.BackColor = System.Drawing.Color.FromArgb(CType(CType(95,Byte),Integer), CType(CType(106,Byte),Integer), CType(CType(124,Byte),Integer))
        Me.btnBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnBuscar.FlatAppearance.BorderSize = 0
        Me.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnBuscar.ForeColor = System.Drawing.Color.Black
        Me.btnBuscar.Image = Global.SysUnidadesProductivas2016.My.Resources.Resources.checklist
        Me.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBuscar.Location = New System.Drawing.Point(264, 639)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(120, 35)
        Me.btnBuscar.TabIndex = 0
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnBuscar.UseVisualStyleBackColor = false
        '
        'Button2
        '
        Me.Button2.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.Button2.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(95,Byte),Integer), CType(CType(106,Byte),Integer), CType(CType(124,Byte),Integer))
        Me.Button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button2.FlatAppearance.BorderSize = 0
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Image = Global.SysUnidadesProductivas2016.My.Resources.Resources.regresar1
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(642, 639)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 35)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Regresar"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = false
        '
        'btnModificar
        '
        Me.btnModificar.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.btnModificar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnModificar.BackColor = System.Drawing.Color.FromArgb(CType(CType(95,Byte),Integer), CType(CType(106,Byte),Integer), CType(CType(124,Byte),Integer))
        Me.btnModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnModificar.Enabled = false
        Me.btnModificar.FlatAppearance.BorderSize = 0
        Me.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnModificar.ForeColor = System.Drawing.Color.Black
        Me.btnModificar.Image = Global.SysUnidadesProductivas2016.My.Resources.Resources.modificar
        Me.btnModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnModificar.Location = New System.Drawing.Point(516, 639)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(120, 35)
        Me.btnModificar.TabIndex = 2
        Me.btnModificar.Text = "Modificar"
        Me.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnModificar.UseVisualStyleBackColor = false
        '
        'btnAgregar
        '
        Me.btnAgregar.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.btnAgregar.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAgregar.BackColor = System.Drawing.Color.FromArgb(CType(CType(95,Byte),Integer), CType(CType(106,Byte),Integer), CType(CType(124,Byte),Integer))
        Me.btnAgregar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAgregar.Enabled = false
        Me.btnAgregar.FlatAppearance.BorderSize = 0
        Me.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAgregar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.btnAgregar.ForeColor = System.Drawing.Color.Black
        Me.btnAgregar.Image = Global.SysUnidadesProductivas2016.My.Resources.Resources.agregar
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(390, 639)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(120, 35)
        Me.btnAgregar.TabIndex = 1
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAgregar.UseVisualStyleBackColor = false
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.GroupBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60,Byte),Integer), CType(CType(74,Byte),Integer), CType(CType(96,Byte),Integer))
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cbxDistrito)
        Me.GroupBox1.Controls.Add(Me.cbxCanton)
        Me.GroupBox1.Controls.Add(Me.cbxProvincia)
        Me.GroupBox1.Controls.Add(Me.btnIr)
        Me.GroupBox1.Controls.Add(Me.GroupBox5)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.GroupBox4)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Controls.Add(Me.txtOtrasSenas)
        Me.GroupBox1.Controls.Add(Me.lblOtrasSenas)
        Me.GroupBox1.Controls.Add(Me.lblProvincia)
        Me.GroupBox1.Controls.Add(Me.lblCanton)
        Me.GroupBox1.Controls.Add(Me.lblDistrito)
        Me.GroupBox1.Controls.Add(Me.lblDireccion)
        Me.GroupBox1.Controls.Add(Me.lblSegApellido)
        Me.GroupBox1.Controls.Add(Me.txtSegApellido)
        Me.GroupBox1.Controls.Add(Me.txtPriApellido)
        Me.GroupBox1.Controls.Add(Me.lblPriApellido)
        Me.GroupBox1.Controls.Add(Me.txtNomCliente)
        Me.GroupBox1.Controls.Add(Me.mktCedula)
        Me.GroupBox1.Controls.Add(Me.lblNombre)
        Me.GroupBox1.Controls.Add(Me.LblCedula)
        Me.GroupBox1.Controls.Add(Me.lblNomEmpresa)
        Me.GroupBox1.Controls.Add(Me.txtNomEmpresa)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(28, 131)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(925, 480)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label3.Location = New System.Drawing.Point(20, 457)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(174, 13)
        Me.Label3.TabIndex = 40
        Me.Label3.Text = "Recuerda no dejar campos vacíos."
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label2.Location = New System.Drawing.Point(23, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(273, 13)
        Me.Label2.TabIndex = 39
        Me.Label2.Text = "Haz click sobre la lupa para verificar si existe esa cedula"
        '
        'cbxDistrito
        '
        Me.cbxDistrito.DropDownHeight = 90
        Me.cbxDistrito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxDistrito.FormattingEnabled = true
        Me.cbxDistrito.IntegralHeight = false
        Me.cbxDistrito.Location = New System.Drawing.Point(300, 272)
        Me.cbxDistrito.Name = "cbxDistrito"
        Me.cbxDistrito.Size = New System.Drawing.Size(200, 28)
        Me.cbxDistrito.TabIndex = 38
        '
        'cbxCanton
        '
        Me.cbxCanton.DropDownHeight = 90
        Me.cbxCanton.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxCanton.FormattingEnabled = true
        Me.cbxCanton.IntegralHeight = false
        Me.cbxCanton.Location = New System.Drawing.Point(300, 202)
        Me.cbxCanton.Name = "cbxCanton"
        Me.cbxCanton.Size = New System.Drawing.Size(200, 28)
        Me.cbxCanton.TabIndex = 37
        '
        'cbxProvincia
        '
        Me.cbxProvincia.DropDownHeight = 90
        Me.cbxProvincia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxProvincia.FormattingEnabled = true
        Me.cbxProvincia.IntegralHeight = false
        Me.cbxProvincia.ItemHeight = 20
        Me.cbxProvincia.Location = New System.Drawing.Point(300, 132)
        Me.cbxProvincia.Name = "cbxProvincia"
        Me.cbxProvincia.Size = New System.Drawing.Size(200, 28)
        Me.cbxProvincia.TabIndex = 36
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rbnActivo)
        Me.GroupBox5.Controls.Add(Me.rbnPasivo)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.White
        Me.GroupBox5.Location = New System.Drawing.Point(580, 39)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(246, 60)
        Me.GroupBox5.TabIndex = 20
        Me.GroupBox5.TabStop = false
        Me.GroupBox5.Text = "Estado de Cliente:"
        '
        'rbnActivo
        '
        Me.rbnActivo.AutoSize = true
        Me.rbnActivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.rbnActivo.Location = New System.Drawing.Point(6, 25)
        Me.rbnActivo.Name = "rbnActivo"
        Me.rbnActivo.Size = New System.Drawing.Size(76, 24)
        Me.rbnActivo.TabIndex = 0
        Me.rbnActivo.TabStop = true
        Me.rbnActivo.Text = "Activo"
        Me.rbnActivo.UseVisualStyleBackColor = true
        '
        'rbnPasivo
        '
        Me.rbnPasivo.AutoSize = true
        Me.rbnPasivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.rbnPasivo.Location = New System.Drawing.Point(88, 24)
        Me.rbnPasivo.Name = "rbnPasivo"
        Me.rbnPasivo.Size = New System.Drawing.Size(79, 24)
        Me.rbnPasivo.TabIndex = 1
        Me.rbnPasivo.TabStop = true
        Me.rbnPasivo.Text = "Pasivo"
        Me.rbnPasivo.UseVisualStyleBackColor = true
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbnPersona)
        Me.GroupBox2.Controls.Add(Me.rbnEmpresa)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(20, 319)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(236, 60)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = false
        Me.GroupBox2.Text = "Tipo de Persona:"
        '
        'rbnPersona
        '
        Me.rbnPersona.AutoSize = true
        Me.rbnPersona.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.rbnPersona.Location = New System.Drawing.Point(6, 25)
        Me.rbnPersona.Name = "rbnPersona"
        Me.rbnPersona.Size = New System.Drawing.Size(74, 24)
        Me.rbnPersona.TabIndex = 1
        Me.rbnPersona.TabStop = true
        Me.rbnPersona.Text = "Fisica"
        Me.rbnPersona.UseVisualStyleBackColor = true
        '
        'rbnEmpresa
        '
        Me.rbnEmpresa.AutoSize = true
        Me.rbnEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.rbnEmpresa.Location = New System.Drawing.Point(105, 25)
        Me.rbnEmpresa.Name = "rbnEmpresa"
        Me.rbnEmpresa.Size = New System.Drawing.Size(89, 24)
        Me.rbnEmpresa.TabIndex = 0
        Me.rbnEmpresa.TabStop = true
        Me.rbnEmpresa.Text = "Juridica"
        Me.rbnEmpresa.UseVisualStyleBackColor = true
        '
        'Label15
        '
        Me.Label15.AutoSize = true
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label15.Location = New System.Drawing.Point(585, 115)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(88, 20)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Teléfonos"
        '
        'Label14
        '
        Me.Label14.AutoSize = true
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label14.Location = New System.Drawing.Point(585, 325)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(72, 20)
        Me.Label14.TabIndex = 22
        Me.Label14.Text = "Correos"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dvgCorreo)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox4.ForeColor = System.Drawing.Color.Black
        Me.GroupBox4.Location = New System.Drawing.Point(580, 326)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(324, 139)
        Me.GroupBox4.TabIndex = 35
        Me.GroupBox4.TabStop = false
        Me.GroupBox4.Text = "             "
        '
        'dvgCorreo
        '
        Me.dvgCorreo.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.dvgCorreo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dvgCorreo.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDEMAIL, Me.EMAIL})
        Me.dvgCorreo.Location = New System.Drawing.Point(9, 30)
        Me.dvgCorreo.Name = "dvgCorreo"
        Me.dvgCorreo.Size = New System.Drawing.Size(296, 103)
        Me.dvgCorreo.TabIndex = 0
        '
        'IDEMAIL
        '
        Me.IDEMAIL.HeaderText = "IDEMAIL"
        Me.IDEMAIL.Name = "IDEMAIL"
        Me.IDEMAIL.ReadOnly = true
        Me.IDEMAIL.Visible = false
        '
        'EMAIL
        '
        Me.EMAIL.HeaderText = "CORREO ELECTRONICO"
        Me.EMAIL.Name = "EMAIL"
        Me.EMAIL.Width = 250
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dvgTelefono)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.Black
        Me.GroupBox3.Location = New System.Drawing.Point(580, 116)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(324, 151)
        Me.GroupBox3.TabIndex = 34
        Me.GroupBox3.TabStop = false
        Me.GroupBox3.Text = "          "
        '
        'dvgTelefono
        '
        Me.dvgTelefono.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dvgTelefono.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.idTelefono, Me.NUMERO})
        Me.dvgTelefono.GridColor = System.Drawing.Color.Black
        Me.dvgTelefono.Location = New System.Drawing.Point(6, 25)
        Me.dvgTelefono.Name = "dvgTelefono"
        Me.dvgTelefono.Size = New System.Drawing.Size(299, 111)
        Me.dvgTelefono.TabIndex = 0
        '
        'idTelefono
        '
        Me.idTelefono.HeaderText = "IDTELEFONO"
        Me.idTelefono.Name = "idTelefono"
        Me.idTelefono.Visible = false
        Me.idTelefono.Width = 250
        '
        'NUMERO
        '
        Me.NUMERO.HeaderText = "NUMERO"
        Me.NUMERO.Name = "NUMERO"
        Me.NUMERO.Width = 250
        '
        'txtOtrasSenas
        '
        Me.txtOtrasSenas.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtOtrasSenas.Location = New System.Drawing.Point(300, 342)
        Me.txtOtrasSenas.Multiline = true
        Me.txtOtrasSenas.Name = "txtOtrasSenas"
        Me.txtOtrasSenas.Size = New System.Drawing.Size(230, 97)
        Me.txtOtrasSenas.TabIndex = 19
        '
        'lblOtrasSenas
        '
        Me.lblOtrasSenas.AutoSize = true
        Me.lblOtrasSenas.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblOtrasSenas.ForeColor = System.Drawing.Color.White
        Me.lblOtrasSenas.Location = New System.Drawing.Point(300, 319)
        Me.lblOtrasSenas.Name = "lblOtrasSenas"
        Me.lblOtrasSenas.Size = New System.Drawing.Size(114, 20)
        Me.lblOtrasSenas.TabIndex = 18
        Me.lblOtrasSenas.Text = "Otras Señas:"
        '
        'lblProvincia
        '
        Me.lblProvincia.AutoSize = true
        Me.lblProvincia.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblProvincia.ForeColor = System.Drawing.Color.White
        Me.lblProvincia.Location = New System.Drawing.Point(300, 109)
        Me.lblProvincia.Name = "lblProvincia"
        Me.lblProvincia.Size = New System.Drawing.Size(86, 20)
        Me.lblProvincia.TabIndex = 12
        Me.lblProvincia.Text = "Provincia:"
        '
        'lblCanton
        '
        Me.lblCanton.AutoSize = true
        Me.lblCanton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblCanton.ForeColor = System.Drawing.Color.White
        Me.lblCanton.Location = New System.Drawing.Point(300, 179)
        Me.lblCanton.Name = "lblCanton"
        Me.lblCanton.Size = New System.Drawing.Size(72, 20)
        Me.lblCanton.TabIndex = 14
        Me.lblCanton.Text = "Cantón:"
        '
        'lblDistrito
        '
        Me.lblDistrito.AutoSize = true
        Me.lblDistrito.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblDistrito.ForeColor = System.Drawing.Color.White
        Me.lblDistrito.Location = New System.Drawing.Point(300, 249)
        Me.lblDistrito.Name = "lblDistrito"
        Me.lblDistrito.Size = New System.Drawing.Size(72, 20)
        Me.lblDistrito.TabIndex = 16
        Me.lblDistrito.Text = "Distrito:"
        '
        'lblDireccion
        '
        Me.lblDireccion.AutoSize = true
        Me.lblDireccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblDireccion.ForeColor = System.Drawing.Color.White
        Me.lblDireccion.Location = New System.Drawing.Point(300, 29)
        Me.lblDireccion.Name = "lblDireccion"
        Me.lblDireccion.Size = New System.Drawing.Size(89, 20)
        Me.lblDireccion.TabIndex = 11
        Me.lblDireccion.Text = "Dirección:"
        '
        'lblSegApellido
        '
        Me.lblSegApellido.AutoSize = true
        Me.lblSegApellido.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblSegApellido.ForeColor = System.Drawing.Color.White
        Me.lblSegApellido.Location = New System.Drawing.Point(20, 249)
        Me.lblSegApellido.Name = "lblSegApellido"
        Me.lblSegApellido.Size = New System.Drawing.Size(155, 20)
        Me.lblSegApellido.TabIndex = 6
        Me.lblSegApellido.Text = "Segundo Apellido:"
        '
        'txtSegApellido
        '
        Me.txtSegApellido.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtSegApellido.Location = New System.Drawing.Point(20, 272)
        Me.txtSegApellido.Name = "txtSegApellido"
        Me.txtSegApellido.Size = New System.Drawing.Size(200, 26)
        Me.txtSegApellido.TabIndex = 7
        '
        'txtPriApellido
        '
        Me.txtPriApellido.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtPriApellido.Location = New System.Drawing.Point(20, 202)
        Me.txtPriApellido.Name = "txtPriApellido"
        Me.txtPriApellido.Size = New System.Drawing.Size(200, 26)
        Me.txtPriApellido.TabIndex = 5
        '
        'lblPriApellido
        '
        Me.lblPriApellido.AutoSize = true
        Me.lblPriApellido.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblPriApellido.ForeColor = System.Drawing.Color.White
        Me.lblPriApellido.Location = New System.Drawing.Point(20, 179)
        Me.lblPriApellido.Name = "lblPriApellido"
        Me.lblPriApellido.Size = New System.Drawing.Size(134, 20)
        Me.lblPriApellido.TabIndex = 4
        Me.lblPriApellido.Text = "Primer Apellido:"
        '
        'txtNomCliente
        '
        Me.txtNomCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtNomCliente.Location = New System.Drawing.Point(20, 132)
        Me.txtNomCliente.Name = "txtNomCliente"
        Me.txtNomCliente.Size = New System.Drawing.Size(200, 26)
        Me.txtNomCliente.TabIndex = 3
        '
        'mktCedula
        '
        Me.mktCedula.Location = New System.Drawing.Point(20, 62)
        Me.mktCedula.Name = "mktCedula"
        Me.mktCedula.Size = New System.Drawing.Size(200, 26)
        Me.mktCedula.TabIndex = 1
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = true
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblNombre.ForeColor = System.Drawing.Color.White
        Me.lblNombre.Location = New System.Drawing.Point(20, 109)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(76, 20)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Text = "Nombre:"
        '
        'LblCedula
        '
        Me.LblCedula.AutoSize = true
        Me.LblCedula.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.LblCedula.ForeColor = System.Drawing.Color.White
        Me.LblCedula.Location = New System.Drawing.Point(20, 39)
        Me.LblCedula.Name = "LblCedula"
        Me.LblCedula.Size = New System.Drawing.Size(65, 20)
        Me.LblCedula.TabIndex = 0
        Me.LblCedula.Text = "Cédula"
        '
        'lblNomEmpresa
        '
        Me.lblNomEmpresa.AutoSize = true
        Me.lblNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblNomEmpresa.ForeColor = System.Drawing.Color.White
        Me.lblNomEmpresa.Location = New System.Drawing.Point(20, 389)
        Me.lblNomEmpresa.Name = "lblNomEmpresa"
        Me.lblNomEmpresa.Size = New System.Drawing.Size(152, 20)
        Me.lblNomEmpresa.TabIndex = 9
        Me.lblNomEmpresa.Text = "Nombre Empresa:"
        '
        'txtNomEmpresa
        '
        Me.txtNomEmpresa.Enabled = false
        Me.txtNomEmpresa.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.txtNomEmpresa.Location = New System.Drawing.Point(20, 412)
        Me.txtNomEmpresa.Name = "txtNomEmpresa"
        Me.txtNomEmpresa.Size = New System.Drawing.Size(200, 26)
        Me.txtNomEmpresa.TabIndex = 10
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(154,Byte),Integer), CType(CType(164,Byte),Integer), CType(CType(175,Byte),Integer))
        Me.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Location = New System.Drawing.Point(35, 31)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(900, 68)
        Me.Panel2.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(154,Byte),Integer), CType(CType(164,Byte),Integer), CType(CType(175,Byte),Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(897, 68)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Administración de Clientes"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = true
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(26,Byte),Integer), CType(CType(42,Byte),Integer), CType(CType(68,Byte),Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1019, 756)
        Me.ControlBox = false
        Me.Controls.Add(Me.Panel1)
        Me.ForeColor = System.Drawing.Color.White
        Me.Name = "frmClientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Panel1.ResumeLayout(false)
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        Me.GroupBox5.ResumeLayout(false)
        Me.GroupBox5.PerformLayout
        Me.GroupBox2.ResumeLayout(false)
        Me.GroupBox2.PerformLayout
        Me.GroupBox4.ResumeLayout(false)
        CType(Me.dvgCorreo,System.ComponentModel.ISupportInitialize).EndInit
        Me.GroupBox3.ResumeLayout(false)
        CType(Me.dvgTelefono,System.ComponentModel.ISupportInitialize).EndInit
        Me.Panel2.ResumeLayout(false)
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Button2 As Button
    Friend WithEvents btnModificar As Button
    Friend WithEvents btnAgregar As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtNomCliente As TextBox
    Friend WithEvents rbnEmpresa As RadioButton
    Friend WithEvents rbnPersona As RadioButton
    Friend WithEvents mktCedula As MaskedTextBox
    Friend WithEvents lblNombre As Label
    Friend WithEvents LblCedula As Label
    Friend WithEvents lblNomEmpresa As Label
    Friend WithEvents txtNomEmpresa As TextBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents lblSegApellido As Label
    Friend WithEvents txtSegApellido As TextBox
    Friend WithEvents txtPriApellido As TextBox
    Friend WithEvents lblPriApellido As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents dvgCorreo As DataGridView
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents dvgTelefono As DataGridView
    Friend WithEvents rbnPasivo As RadioButton
    Friend WithEvents rbnActivo As RadioButton
    Friend WithEvents txtOtrasSenas As TextBox
    Friend WithEvents lblOtrasSenas As Label
    Friend WithEvents lblProvincia As Label
    Friend WithEvents lblCanton As Label
    Friend WithEvents lblDistrito As Label
    Friend WithEvents lblDireccion As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents btnBuscar As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents btnIr As Button
    Friend WithEvents IDEMAIL As DataGridViewTextBoxColumn
    Friend WithEvents EMAIL As DataGridViewTextBoxColumn
    Friend WithEvents idTelefono As DataGridViewTextBoxColumn
    Friend WithEvents NUMERO As DataGridViewTextBoxColumn
    Friend WithEvents cbxProvincia As ComboBox
    Friend WithEvents cbxDistrito As ComboBox
    Friend WithEvents cbxCanton As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
End Class
