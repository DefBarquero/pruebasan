﻿Public Class clsEntidadCompraMateria

#Region "Atributo"

    Private strIdCompra As String
    Private strCedulaPro As String
    Private strCantidadIndividual As String
    Private strCantidadTotal As String
    Private strPrecioIndividual As String
    Private strPrecioTotal As String
    Private strFecha As String
    Private strDescripcion As String


#End Region

#Region "Constructores"

    Public Sub New()

        pIdCompra = ""
        pCedulaPro = ""
        pCantidadIndividual = ""
        pCantidadTotal = ""
        pPrecioIndividual = ""
        pPrecioTotal = ""
        pFecha = "00/00/0000"
        pDescripcion = ""

    End Sub
#End Region

#Region "Propiedades"

    Public Property pIdCompra() As String
        Get
            Return Me.strIdCompra
        End Get
        Set(value As String)
            Me.strIdCompra = value.Trim
        End Set
    End Property



    Public Property pCedulaPro() As String
        Get
            Return Me.strCedulaPro
        End Get
        Set(value As String)
            Me.strCedulaPro = value.Trim
        End Set
    End Property

    Public Property pCantidadIndividual() As String
        Get
            Return Me.strCantidadIndividual
        End Get
        Set(value As String)
            Me.strCantidadIndividual = value.Trim
        End Set
    End Property




    Public Property pCantidadTotal() As String
        Get
            Return Me.strCantidadTotal
        End Get
        Set(value As String)
            Me.strCantidadTotal = value.Trim
        End Set
    End Property







    Public Property pPrecioIndividual() As String
        Get
            Return Me.strPrecioIndividual
        End Get
        Set(value As String)
            Me.strPrecioIndividual = value.Trim
        End Set
    End Property




    Public Property pPrecioTotal() As String
        Get
            Return Me.strPrecioTotal
        End Get
        Set(value As String)
            Me.strPrecioTotal = value.Trim
        End Set
    End Property



    Public Property pFecha() As String
        Get
            Return Me.strFecha
        End Get
        Set(value As String)
            Me.strFecha = value.Trim
        End Set
    End Property

    Public Property pDescripcion() As String
        Get
            Return Me.strDescripcion
        End Get
        Set(value As String)
            Me.strDescripcion = value.Trim
        End Set
    End Property


#End Region



End Class
