﻿Public Class clsEntidadFacturaEnc

#Region "Atributo"

    Private strIdFactura As String
    Private strCedulaCli As String
    Private strLogin As String
    Private strTotal As String
    Private strFecha As String
    Private strTalonario As String
    Private strTipoPago As String
    Private douIva As Double




#End Region

#Region "Constructores"

    Public Sub New()

        pIdFactura = ""
        pCedulaCli = ""
        pLogin = ""
        pTotal = ""
        pFecha = ""
        pTalonario = ""
        pTipoPago = ""
        douIva=0.0
    End Sub
#End Region

#Region "Propiedades"

    Public Property pIdFactura() As String
        Get
            Return Me.strIdFactura
        End Get
        Set(value As String)
            Me.strIdFactura = value.Trim
        End Set
    End Property

    Public Property pCedulaCli() As String
        Get
            Return Me.strCedulaCli
        End Get
        Set(value As String)
            Me.strCedulaCli = value.Trim
        End Set
    End Property

    Public Property pTalonario() As String
        Get
            Return Me.strTalonario
        End Get
        Set(value As String)
            Me.strTalonario = value.Trim
        End Set
    End Property

    Public Property pLogin() As String
        Get
            Return Me.strLogin
        End Get
        Set(value As String)
            Me.strLogin = value.Trim
        End Set
    End Property

    Public Property pTotal() As String
        Get
            Return Me.strTotal
        End Get
        Set(value As String)
            Me.strTotal = value.Trim
        End Set
    End Property

    Public Property pFecha() As String
        Get
            Return Me.strFecha
        End Get
        Set(value As String)
            Me.strFecha = value.Trim
        End Set
    End Property

    Public Property pTipoPago() As String
        Get
            Return Me.strTipoPago
        End Get
        Set(value As String)
            Me.strTipoPago = value.Trim
        End Set
    End Property

    Public Property pIva() As Double
        Get
            Return Me.douIva
        End Get
        Set(value As Double)
            Me.douIva = value
        End Set
    End Property

#End Region



End Class
