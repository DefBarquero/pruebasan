﻿
Public Class clsEntidadPermisosUsersModulo

#Region "ATRIBUTOS"

    Private strIdVentana As String
    Private strLoginUsuario As String
    Private strPermisoAgregar As String
    Private strPermisoModificar As String


#End Region

#Region "CONSTRUCTOR"

    Public Sub New()

        pIdVentana = ""
        pLoginUsuario = ""
        pPermisoAgregar = ""
        pPermisoModificar = ""
    End Sub
#End Region

#Region "PROPIEDADES"

    Public Property pIdVentana() As String
        Get
            Return Me.strIdVentana.Trim
        End Get
        Set(value As String)
            Me.strIdVentana = value.Trim
        End Set
    End Property

    Public Property pLoginUsuario() As String
        Get
            Return Me.strLoginUsuario.Trim
        End Get
        Set(value As String)
            Me.strLoginUsuario = value.Trim
        End Set
    End Property

    Public Property pPermisoAgregar() As String
        Get
            Return Me.strPermisoAgregar
        End Get
        Set(value As String)
            Me.strPermisoAgregar = value.Trim
        End Set
    End Property

    Public Property pPermisoModificar() As String
        Get
            Return Me.strPermisoModificar.Trim
        End Get
        Set(value As String)
            Me.strPermisoModificar = value.Trim
        End Set
    End Property

#End Region

End Class
