﻿Public Class clsEntidadCorreoCliente

#Region "Atributo"

    Private strIDCorreoCli As String
    Private strCedulaCli As String
    Private strCorreoCli As String


#End Region

#Region "Constructores"

    Public Sub New()

        pIDCorreoCli = ""
        pCedulaCli = ""
        pCorreoCli = ""


    End Sub
#End Region

#Region "Propiedades"

    Public Property pIDCorreoCli() As String
        Get
            Return Me.strIDCorreoCli
        End Get
        Set(value As String)
            Me.strIDCorreoCli = value.Trim
        End Set
    End Property


    Public Property pCedulaCli() As String
        Get
            Return Me.strCedulaCli
        End Get
        Set(value As String)
            Me.strCedulaCli = value.Trim
        End Set
    End Property



    Public Property pCorreoCli() As String
        Get
            Return Me.strCorreoCli
        End Get
        Set(value As String)
            Me.strCorreoCli = value.Trim
        End Set
    End Property


#End Region




End Class
