﻿Public Class clsEntidadFacturaCostoDet


#Region "Atributo"

    Private strIdFactura As String
    Private strIdProducto As String
    Private strCantidad As String
    Private strSubTotal As String
    Private douIva As Double



#End Region

#Region "Constructores"

    Public Sub New()

        pIdFactura = ""
        pIdProducto = ""
        pCantidad = ""
        pSubTotal = ""
        douIva=0.0

    End Sub
#End Region

#Region "Propiedades"

    Public Property pIdFactura() As String
        Get
            Return Me.strIdFactura
        End Get
        Set(value As String)
            Me.strIdFactura = value.Trim
        End Set
    End Property

    Public Property pIdProducto() As String
        Get
            Return Me.strIdProducto
        End Get
        Set(value As String)
            Me.strIdProducto = value.Trim
        End Set
    End Property

    Public Property pCantidad() As String
        Get
            Return Me.strCantidad
        End Get
        Set(value As String)
            Me.strCantidad = value.Trim
        End Set
    End Property
    
    Public Property pSubTotal() As String
        Get
            Return Me.strSubTotal
        End Get
        Set(value As String)
            Me.strSubTotal = value.Trim
        End Set
    End Property

    Public Property pIva() As Double
        Get
            Return Me.douIva
        End Get
        Set(value As Double)
            Me.douIva = value
        End Set
    End Property

#End Region

End Class
