﻿Public Class clsEntidadProvincia

#Region "ATRIBUTOS"

    Private strIdProvincia As String
    Private strNombreProvincia As String

#End Region

#Region "CONSTRUCTOR"

    Public Sub New()

        pIdProvincia = ""
        pNombreProvincia = ""

    End Sub
#End Region

#Region "PROPIEDADES"

    Public Property pIdProvincia() As String
        Get
            Return Me.strIdProvincia.Trim
        End Get
        Set(value As String)
            Me.strIdProvincia = value.Trim
        End Set
    End Property

    Public Property pNombreProvincia() As String
        Get
            Return Me.strNombreProvincia.Trim
        End Get
        Set(value As String)
            Me.strNombreProvincia = value.Trim
        End Set
    End Property
#End Region

End Class
