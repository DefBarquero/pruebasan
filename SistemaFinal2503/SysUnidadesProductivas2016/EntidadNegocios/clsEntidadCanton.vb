﻿Public Class clsEntidadCanton

#Region "ATRIBUTOS"

    Private strIdCanton As String
    Private strIdProvincia As String
    Private strNombreCanton As String

#End Region

#Region "CONSTRUCTOR"
    Public Sub New()

        pIdCanton = ""
        pIdProvincia = ""
        pNombreCanton = ""

    End Sub
#End Region

#Region "PROPIEDADES"

    Public Property pIdCanton() As String
        Get
            Return Me.strIdCanton.Trim
        End Get
        Set(value As String)
            Me.strIdCanton = value.Trim

        End Set
    End Property

    Public Property pIdProvincia() As String
        Get
            Return Me.strIdProvincia.Trim
        End Get
        Set(value As String)
            Me.strIdProvincia = value.Trim
        End Set
    End Property

    Public Property pNombreCanton() As String
        Get
            Return Me.strNombreCanton.Trim
        End Get
        Set(value As String)
            Me.strNombreCanton = value.Trim
        End Set
    End Property

#End Region

End Class
