﻿

Public Class clsEntidadVentana

#Region "ATRIBUTOS"

    Private strIdVentana As String
    Private strNombreVentana As String

#End Region

#Region "Constructor"

    Public Sub New()
        pIDVentana = ""
        pNombreVentana = ""
    End Sub
#End Region

#Region "PROPIEDADES"
    Public Property pIDVentana() As String
        Get
            Return Me.strIdVentana.Trim
        End Get
        Set(value As String)
            Me.strIdVentana = value.Trim
        End Set
    End Property

    Public Property pNombreVentana() As String
        Get
            Return Me.strNombreVentana.Trim
        End Get
        Set(value As String)
            Me.strNombreVentana = value.Trim

        End Set
    End Property
#End Region

End Class
