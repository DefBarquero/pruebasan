﻿Public Class clsEntidadDistrito

#Region "ATRIBUTOS"

    Private strIdDistrito As String
    Private strIdCanton As String
    Private strNombreDistrito As String

#End Region

#Region "CONSTRUCTOR"
    Public Sub New()

        pIdDistrito = ""
        pIdCanton = ""
        pNombreDistrito = ""

    End Sub
#End Region

#Region "PROPIEDADES"

    Public Property pIdDistrito() As String
        Get
            Return Me.strIdDistrito.Trim
        End Get
        Set(value As String)
            Me.strIdDistrito = value.Trim

        End Set
    End Property

    Public Property pIdCanton() As String
        Get
            Return Me.strIdCanton.Trim
        End Get
        Set(value As String)
            Me.strIdCanton = value.Trim
        End Set
    End Property

    Public Property pNombreDistrito() As String
        Get
            Return Me.strNombreDistrito.Trim
        End Get
        Set(value As String)
            Me.strNombreDistrito = value.Trim
        End Set
    End Property

#End Region

End Class
