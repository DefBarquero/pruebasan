﻿Public Class clsEntidadCliente

#Region "Atributos"
    Private strCedulaCli As String
    Private strNombreCli As String
    Private strApellido1Cli As String
    Private strApellido2Cli As String
    Private strProvinciaCli As String
    Private strCantionCli As String
    Private strDistritoCli As String
    Private strOtraSenasCli As String
    Private strEstadoCli As String
    Private strTipoCli As String
    Private strNombreEmpresa As String



#End Region

#Region "Constructores"

    Public Sub New()
        pCedulaCli = ""
        pNombreCli = ""
        pApellido1Cli = ""
        pApellido2Cli = ""
        pProvinciaCli = ""
        pCantonCli = ""
        pDistritoCli = ""
        pOtraSenasCli = ""
        pEstadoCli = ""
        pTipoCli = ""
        pNombreEmpresa = ""
    End Sub

#End Region

#Region "Propiedad"
    Public Property pCedulaCli() As String
        Get
            Return Me.strCedulaCli.Trim
        End Get
        Set(value As String)
            Me.strCedulaCli = value.Trim
        End Set
    End Property

    Public Property pNombreCli() As String
        Get
            Return Me.strNombreCli.Trim
        End Get
        Set(value As String)
            Me.strNombreCli = value.Trim
        End Set
    End Property

    Public Property pApellido1Cli() As String
        Get
            Return Me.strApellido1Cli.Trim
        End Get
        Set(value As String)
            Me.strApellido1Cli = value.Trim
        End Set
    End Property

    Public Property pApellido2Cli() As String
        Get
            Return Me.strApellido2Cli.Trim
        End Get
        Set(value As String)
            Me.strApellido2Cli = value.Trim
        End Set
    End Property

    Public Property pProvinciaCli() As String
        Get
            Return Me.strProvinciaCli.Trim
        End Get
        Set(value As String)
            Me.strProvinciaCli = value.Trim
        End Set
    End Property

    Public Property pCantonCli() As String
        Get
            Return Me.strCantionCli.Trim
        End Get
        Set(value As String)
            Me.strCantionCli = value.Trim
        End Set
    End Property

    Public Property pDistritoCli() As String
        Get
            Return Me.strDistritoCli.Trim
        End Get
        Set(value As String)
            Me.strDistritoCli = value.Trim
        End Set
    End Property

    Public Property pOtraSenasCli() As String
        Get
            Return Me.strOtraSenasCli.Trim
        End Get
        Set(value As String)
            Me.strOtraSenasCli = value.Trim
        End Set
    End Property

    Public Property pEstadoCli() As String
        Get
            Return Me.strEstadoCli.Trim
        End Get
        Set(value As String)
            Me.strEstadoCli = value.Trim
        End Set
    End Property

    Public Property pTipoCli() As String
        Get
            Return Me.strTipoCli.Trim
        End Get
        Set(value As String)
            Me.strTipoCli = value.Trim
        End Set
    End Property

    Public Property pNombreEmpresa() As String
        Get
            Return Me.strNombreEmpresa.Trim
        End Get
        Set(value As String)
            Me.strNombreEmpresa = value.Trim
        End Set
    End Property

#End Region

End Class
