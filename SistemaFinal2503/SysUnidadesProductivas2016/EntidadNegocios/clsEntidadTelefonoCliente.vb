﻿
Public Class clsEntidadTelefonoCliente

#Region "Atributos"
    Private strIDTelefonoCli As String
    Private strCedulaCli As String
    Private strTelefonoCli As String
#End Region

#Region "Constructores"
    Public Sub New()
        pIDTelefonoCli = ""
        pCedulaCli = ""
        pTelefonoCli = ""
    End Sub
#End Region

#Region "Propiedades"

    Public Property pIDTelefonoCli() As String
        Get
            Return Me.strIDTelefonoCli.Trim
        End Get
        Set(value As String)
            Me.strIDTelefonoCli = value.Trim
        End Set
    End Property

    Public Property pCedulaCli() As String
        Get
            Return Me.strCedulaCli.Trim
        End Get
        Set(value As String)
            Me.strCedulaCli = value.Trim
        End Set
    End Property

    Public Property pTelefonoCli() As String
        Get
            Return Me.strTelefonoCli.Trim
        End Get
        Set(value As String)
            Me.strTelefonoCli = value.Trim
        End Set
    End Property
#End Region
End Class
