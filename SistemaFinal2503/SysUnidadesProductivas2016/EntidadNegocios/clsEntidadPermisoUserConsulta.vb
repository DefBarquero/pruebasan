﻿Public Class clsEntidadPermisoUserConsulta

#Region "ATRIBUTOS"

    Private strIdVentana As String
    Private strLoginUsuario As String
    Private strPermisoConsultar As String



#End Region

#Region "CONSTRUCTOR"

    Public Sub New()

        pIdVentana = ""
        pLoginUsuario = ""
        pPermisoConsultar = ""

    End Sub
#End Region

#Region "PROPIEDADES"

    Public Property pIdVentana() As String
        Get
            Return Me.strIdVentana.Trim
        End Get
        Set(value As String)
            Me.strIdVentana = value.Trim
        End Set
    End Property

    Public Property pLoginUsuario() As String
        Get
            Return Me.strLoginUsuario.Trim
        End Get
        Set(value As String)
            Me.strLoginUsuario = value.Trim
        End Set
    End Property

    Public Property pPermisoConsultar() As String
        Get
            Return Me.strPermisoConsultar
        End Get
        Set(value As String)
            Me.strPermisoConsultar = value.Trim
        End Set
    End Property



#End Region

End Class
