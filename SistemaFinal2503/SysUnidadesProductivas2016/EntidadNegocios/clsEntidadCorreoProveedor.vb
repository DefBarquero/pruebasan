﻿Public Class clsEntidadCorreoProveedor

#Region "Atributo"

    Private strCedulaPro As String
    Private strCorreoPro As String
    Private stridCorreoPro As String


#End Region

#Region "Constructores"

    Public Sub New()

        pCedulaPro = ""
        pCorreoPro = ""
        pIdCorreoPro = ""

    End Sub


#End Region

#Region "Propiedades"

    Public Property pCedulaPro() As String
        Get
            Return Me.strCedulaPro
        End Get
        Set(value As String)
            Me.strCedulaPro = value.Trim
        End Set
    End Property



    Public Property pCorreoPro() As String
        Get
            Return Me.strCorreoPro
        End Get
        Set(value As String)
            Me.strCorreoPro = value.Trim
        End Set
    End Property

    Public Property pIdCorreoPro() As String
        Get
            Return Me.stridCorreoPro
        End Get
        Set(value As String)
            Me.stridCorreoPro = value.Trim
        End Set
    End Property

#End Region





End Class
