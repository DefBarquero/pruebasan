﻿Public Class clsEntidadProductoServicio

#Region "Atributos"
    Private strIdProductoServicio As String
    Private strNombreProSer As String
    Private strDescripcionProSer As String
    Private strCostoProSer As String
    Private strPrecioProSer As String
    Private strImagenProSer As String
    Private strTipoProSer As String
#End Region


#Region "Constructor"
    Public Sub New()
        pIdProductoSer = ""
        pNombreProSer = ""
        pDescripcionProSer = ""
        pCostoProSer = ""
        pPrecioProSer = ""
        pImagenProSer = ""
        pTipoProSer = ""
    End Sub

#End Region

#Region "Propiedades"

    Public Property pIdProductoSer() As String
        Get
            Return Me.strIdProductoServicio.Trim
        End Get
        Set(value As String)
            Me.strIdProductoServicio = value.Trim
        End Set
    End Property

    Public Property pNombreProSer() As String
        Get
            Return Me.strNombreProSer.Trim
        End Get
        Set(value As String)
            Me.strNombreProSer = value.Trim
        End Set
    End Property

    Public Property pDescripcionProSer() As String
        Get
            Return Me.strDescripcionProSer.Trim
        End Get
        Set(value As String)
            Me.strDescripcionProSer = value.Trim
        End Set
    End Property

    Public Property pCostoProSer() As String
        Get
            Return Me.strCostoProSer.Trim
        End Get
        Set(value As String)
            Me.strCostoProSer = value.Trim
        End Set
    End Property

    Public Property pPrecioProSer() As String
        Get
            Return Me.strPrecioProSer.Trim
        End Get
        Set(value As String)
            Me.strPrecioProSer = value.Trim
        End Set
    End Property


    Public Property pImagenProSer() As String
        Get
            Return Me.strImagenProSer.Trim
        End Get
        Set(value As String)
            Me.strImagenProSer = value.Trim
        End Set
    End Property

    Public Property pTipoProSer() As String
        Get
            Return Me.strTipoProSer.Trim
        End Get
        Set(value As String)
            Me.strTipoProSer = value.Trim
        End Set
    End Property

#End Region

End Class
