﻿Public Class clsEntidadUsuario

#Region "Atributos"
    Private strLogin As String
    Private strContrasena As String
    Private strConfirmarCont As String
    Private strNombre As String
    Private strApellido1Usu As String
    Private strApellido2Usu As String
    Private strImagenUsu As String
#End Region


#Region "Constructor"

    Public Sub New()
        pLogin = ""
        pContrasena = ""
        pConfirmarCont = ""
        pNombre = ""
        pApellido1Usu = ""
        pApellido2Usu = ""
        pImagenUsus = ""
    End Sub

#End Region


#Region "Propiedades"

    Public Property pLogin() As String
        Get
            Return Me.strLogin.Trim
        End Get
        Set(value As String)
            Me.strLogin = value.Trim
        End Set
    End Property

    Public Property pContrasena() As String
        Get
            Return Me.strContrasena.Trim
        End Get
        Set(value As String)
            Me.strContrasena = value.Trim
        End Set
    End Property

    Public Property pConfirmarCont() As String
        Get
            Return Me.strConfirmarCont.Trim
        End Get
        Set(value As String)
            Me.strConfirmarCont = value.Trim
        End Set
    End Property

    Public Property pNombre() As String
        Get
            Return Me.strNombre.Trim
        End Get
        Set(value As String)
            Me.strNombre = value.Trim
        End Set
    End Property

    Public Property pApellido1Usu() As String
        Get
            Return Me.strApellido1Usu.Trim
        End Get
        Set(value As String)
            Me.strApellido1Usu = value.Trim
        End Set
    End Property

    Public Property pApellido2Usu() As String
        Get
            Return Me.strApellido2Usu.Trim
        End Get
        Set(value As String)
            Me.strApellido2Usu = value.Trim
        End Set
    End Property

    Public Property pImagenUsus() As String
        Get
            Return Me.strImagenUsu.Trim
        End Get
        Set(value As String)
            Me.strImagenUsu = value
        End Set
    End Property


#End Region



End Class
