﻿Public Class clsEntidadTelefonoProveedor

#Region "Atributos"
    Private strCedulaPro As String
    Private strTelefonoPro As String
    Private strIdTelefonoPro As String

#End Region

#Region "Constructores"
    Public Sub New()
        pCedulaPro = ""
        pTelefonoPro = ""
        pIdTelefonoPro = ""
    End Sub
#End Region

#Region "Propiedades"
    Public Property pCedulaPro() As String
        Get
            Return Me.strCedulaPro.Trim
        End Get
        Set(value As String)
            Me.strCedulaPro = value.Trim
        End Set
    End Property

    Public Property pTelefonoPro() As String
        Get
            Return Me.strTelefonoPro.Trim
        End Get
        Set(value As String)
            Me.strTelefonoPro = value.Trim
        End Set
    End Property

    Public Property pIdTelefonoPro As String
        Get
            Return Me.strIdTelefonoPro.Trim
        End Get
        Set(value As String)
            Me.strIdTelefonoPro = value.Trim
        End Set
    End Property
#End Region


End Class
