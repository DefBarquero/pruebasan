﻿Public Class clsEntidadProveedor

#Region "Atributos"
    Private strCedulaPro As String
    Private strTipoPro As String
    Private strNombrePro As String
    Private strApellido1Pro As String
    Private strApellido2Pro As String
    Private strProvinciaPro As String
    Private strCantonPro As String
    Private strDistritoPro As String
    Private strOtrasSenasPro As String
    Private strDescripcionPro As String
    Private strNomEmpresaPro As String
#End Region

#Region "Constructor"
    Public Sub New()
        pCedulaPro = ""
        pTipoPro = ""
        pNombrePro = ""
        pApellido1Pro = ""
        pApellido2Pro = ""
        pProvinciaPro = ""
        pCantonPro = ""
        pDistritoPro = ""
        pOtrasSenasPro = ""
        pDescripcionPro = ""
        pNomEmpresaPro = ""
    End Sub
#End Region

#Region "Propiedades"

    Public Property pCedulaPro() As String
        Get
            Return Me.strCedulaPro.Trim
        End Get
        Set(value As String)
            Me.strCedulaPro = value.Trim
        End Set
    End Property

    Public Property pTipoPro() As String
        Get
            Return Me.strTipoPro.Trim
        End Get
        Set(value As String)
            Me.strTipoPro = value.Trim
        End Set
    End Property

    Public Property pNombrePro() As String
        Get
            Return Me.strNombrePro.Trim
        End Get
        Set(value As String)
            Me.strNombrePro = value.Trim
        End Set
    End Property

    Public Property pApellido1Pro() As String
        Get
            Return Me.strApellido1Pro.Trim
        End Get
        Set(value As String)
            Me.strApellido1Pro = value.Trim
        End Set
    End Property


    Public Property pApellido2Pro() As String
        Get
            Return Me.strApellido2Pro.Trim
        End Get
        Set(value As String)
            Me.strApellido2Pro = value.Trim
        End Set
    End Property


    Public Property pProvinciaPro() As String
        Get
            Return Me.strProvinciaPro.Trim
        End Get
        Set(value As String)
            Me.strProvinciaPro = value.Trim
        End Set
    End Property

    Public Property pCantonPro() As String
        Get
            Return Me.strCantonPro.Trim
        End Get
        Set(value As String)
            Me.strCantonPro = value.Trim
        End Set
    End Property

    Public Property pDistritoPro() As String
        Get
            Return Me.strDistritoPro.Trim
        End Get
        Set(value As String)
            Me.strDistritoPro = value.Trim
        End Set
    End Property

    Public Property pOtrasSenasPro() As String
        Get
            Return Me.strOtrasSenasPro.Trim
        End Get
        Set(value As String)
            Me.strOtrasSenasPro = value.Trim
        End Set
    End Property

    Public Property pDescripcionPro() As String
        Get
            Return Me.strDescripcionPro.Trim
        End Get
        Set(value As String)
            Me.strDescripcionPro = value.Trim
        End Set
    End Property

    Public Property pNomEmpresaPro() As String
        Get
            Return Me.strNomEmpresaPro.Trim
        End Get
        Set(value As String)
            Me.strNomEmpresaPro = value.Trim
        End Set
    End Property

#End Region
End Class
