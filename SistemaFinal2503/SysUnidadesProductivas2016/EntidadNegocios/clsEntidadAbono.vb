﻿Public Class clsEntidadAbono
#Region "Atributos"
    Private strIdAbono As String
    Private strCedulaCli As String
    Private strMontoCompra As String
    Private strFechaAbono As String
    Private strSaldoActual As String
    Private strIdFactura As String
    Private strSaldoAbonado As String

#End Region

#Region "Constructores"
    Public Sub New()
        pIdAbono = ""
        pCedulaCli = ""
        pMontoCompra = ""
        pFechaAbono = ""
        pSaldoActual = ""
        pIdFactura = ""
        pSaldoAbonado = ""

    End Sub

#End Region

#Region "Propiedades"
    Public Property pIdAbono As String
        Get
            Return Me.strIdAbono
        End Get
        Set(value As String)
            Me.strIdAbono = value.Trim

        End Set
    End Property

    Public Property pCedulaCli As String
        Get
            Return Me.strCedulaCli
        End Get
        Set(value As String)
            Me.strCedulaCli = value.Trim
        End Set
    End Property

    Public Property pMontoCompra As String
        Get
            Return Me.strMontoCompra
        End Get
        Set(value As String)
            Me.strMontoCompra = value.Trim
        End Set
    End Property

    Public Property pFechaAbono As String
        Get
            Return Me.strFechaAbono
        End Get
        Set(value As String)
            Me.strFechaAbono = value.Trim
        End Set

    End Property

    Public Property pSaldoActual As String
        Get
            Return Me.strSaldoActual
        End Get
        Set(value As String)
            Me.strSaldoActual = value.Trim
        End Set
    End Property

    Public Property pIdFactura As String
        Get
            Return Me.strIdFactura
        End Get
        Set(value As String)
            Me.strIdFactura = value.Trim
        End Set
    End Property

    Public Property pSaldoAbonado As String
        Get
            Return Me.strSaldoAbonado
        End Get
        Set(value As String)
            Me.strSaldoAbonado = value.Trim
        End Set
    End Property

#End Region


End Class
