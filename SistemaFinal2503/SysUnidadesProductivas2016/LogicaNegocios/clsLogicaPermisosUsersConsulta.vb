﻿Imports MySql.Data.MySqlClient
Imports EntidadNegocios.clsEntidadPermisoUserConsulta

Public Class clsLogicaPermisosUsersConsulta
#Region "ATRIBUTOS"
    Dim strSentencia As String

#End Region

#Region "METODOS"
    Public Function mAgregarPermisoConsulta(clsEnPerUserConsulta As EntidadNegocios.clsEntidadPermisoUserConsulta, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Insert into tbPermisoUsuarioConsulta(idVentana, login, permisoConsultar) values ('" & clsEnPerUserConsulta.pIdVentana & "', '" & clsEnPerUserConsulta.pLoginUsuario & "', '" & clsEnPerUserConsulta.pPermisoConsultar & "')"
            mAgregarPermisoConsulta = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            mAgregarPermisoConsulta = False

        End Try
    End Function

    Public Function mCosultarPermisoConsulta(clsEnPerUserConsulta As EntidadNegocios.clsEntidadPermisoUserConsulta, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "select * from tbPermisoUsuarioConsulta where login= '" & clsEnPerUserConsulta.pLoginUsuario & "' "
        mCosultarPermisoConsulta = clsConexion.mConsultar(strSentencia)

    End Function

    Public Function mModificarPerimsos(clsEntidadPerUserConsulta As EntidadNegocios.clsEntidadPermisoUserConsulta, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "update tbPermisoUsuarioConsulta set permisoConsultar='" & clsEntidadPerUserConsulta.pPermisoConsultar & "' where idVentana='" & clsEntidadPerUserConsulta.pIdVentana & "' and login= '" & clsEntidadPerUserConsulta.pLoginUsuario & "' "
            mModificarPerimsos = clsConexion.mEjecutar(strSentencia)
        Catch ex As Exception
            mModificarPerimsos = False
        End Try
    End Function
#End Region
End Class
