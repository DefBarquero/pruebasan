﻿Imports MySql.Data.MySqlClient
Imports EntidadNegocios.clsEntidadCorreoCliente
Imports AccesoDatos.clsConexion

Public Class clsLogicaClienteCorreo

#Region "Atributos"
    Private strSentencia As String
#End Region

#Region "Metodos"

    'por medio de este metodo se puede agregar los correos a los clientes
    Public Function mAgregarCorreoCli(clsEntidadCorreoCli As EntidadNegocios.clsEntidadCorreoCliente, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Insert into tbcorreocliente(idCorreoCli,cedulaCli,correoCli) values ('" & clsEntidadCorreoCli.pIDCorreoCli & "', '" & clsEntidadCorreoCli.pCedulaCli & "', '" & clsEntidadCorreoCli.pCorreoCli & "') "
            mAgregarCorreoCli = clsConexion.mEjecutar(strSentencia)
        Catch ex As Exception
            Return False
        End Try
    End Function

    'modifica el correo de un usuario, por medio de idCorreoCli
    Public Function mModificarCorreoCli(clsEntidadCorreoCli As EntidadNegocios.clsEntidadCorreoCliente, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Update tbcorreocliente set correoCli = '" & clsEntidadCorreoCli.pCorreoCli & "' where idCorreoCli= '" & clsEntidadCorreoCli.pIDCorreoCli & "' "
            mModificarCorreoCli = clsConexion.mEjecutar(strSentencia)
        Catch ex As Exception
            Return False
        End Try
    End Function

    'este metodo sirve para consultar los correos especificos de un determinado usuario.
    Public Function mConsultarCorreoCliCedula(clsEntidadCorreoCli As EntidadNegocios.clsEntidadCorreoCliente, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbcorreocliente where cedulaCli= '" & clsEntidadCorreoCli.pCedulaCli & "' "
        mConsultarCorreoCliCedula = clsConexion.mConsultar(strSentencia)
    End Function

    'este metodo sirve para consultar los correos en forma general
    Public Function mConsultarGeneralCorreoCli(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbcorreocliente"
        mConsultarGeneralCorreoCli = clsConexion.mConsultar(strSentencia)
    End Function



    'por medio de este metodo se generan los identificadores de los codigos de los correos clientes
    Public Function mGenerarIDCorreoCli(clsConexion As AccesoDatos.clsConexion) As MySqlDataReader
        strSentencia = "select uuid() as uid"
        mGenerarIDCorreoCli = clsConexion.mConsultar(strSentencia)

    End Function

    Public Function mEliminarCorreoCli(clsEntiCorreoCli As EntidadNegocios.clsEntidadCorreoCliente, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Delete from tbcorreocliente where idCorreoCli = '" & clsEntiCorreoCli.pIDCorreoCli & "'"
            mEliminarCorreoCli = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function mConsultaCorreoCliID(clsEntidadCorreoCli As EntidadNegocios.clsEntidadCorreoCliente, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbcorreocliente where idCorreoCli = '" & clsEntidadCorreoCli.pIDCorreoCli & "'"
        mConsultaCorreoCliID = clsConexion.mConsultar(strSentencia)
    End Function

#End Region

End Class
