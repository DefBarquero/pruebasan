﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadCorreoProveedor


Public Class clsLogicaProveedorCorreo
#Region "Atributos"
    Private strSentencia As String

#End Region

#Region "Metodos"

    ''Metodo que permite realizar una consulta especifica de los correos de proveedores por cedula
    Public Function mConsultaCorreoProCedula(clsEntidadProveedor As EntidadNegocios.clsEntidadCorreoProveedor, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbcorreoproveedor Where cedulaPro='" & clsEntidadProveedor.pCedulaPro & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaCorreoProCedula = clsConexion.mConsultar(strSentencia)
    End Function

    'Este metodo permite hacer una consulta general de los correos proveedores
    Public Function mConsultaGenProveedor(clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbCorreoProveedor   "
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaGenProveedor = clConexion.mConsultar(strSentencia)
    End Function

    'Este metodo permite agregar correos de proveedores
    Public Function mAgregarCorreoPro(clEntidadCorreoPro As EntidadNegocios.clsEntidadCorreoProveedor, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "INSERT INTO tbcorreoproveedor (idCorreoPro, cedulaPro, correoPro) VALUES ('" & clEntidadCorreoPro.pIdCorreoPro & "','" & clEntidadCorreoPro.pCedulaPro & "' , '" & clEntidadCorreoPro.pCorreoPro & "')"
            mAgregarCorreoPro = clConexion.mEjecutar(strSentencia)
            Exit Function
        Catch
            Return False
        End Try
    End Function

    'Este metodo permite modificar los proveedores
    Public Function mModificarCorreoPro(clEntidadProveedor As EntidadNegocios.clsEntidadCorreoProveedor, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Update tbcorreoproveedor set correoPro= '" & clEntidadProveedor.pCorreoPro & "' Where idCorreoPro='" & clEntidadProveedor.pIdCorreoPro & "' "
            mModificarCorreoPro = clConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            Return False
        End Try
    End Function

    'por medio de este metodo se generan los identificadores de los codigos de los correos proveedores
    Public Function mGenerarIDCorreoPro(clsConexion As AccesoDatos.clsConexion) As MySqlDataReader
        strSentencia = "select uuid() as uid"
        mGenerarIDCorreoPro = clsConexion.mConsultar(strSentencia)

    End Function

    Public Function mEliminarCorreoPro(clsEntidadCorreoPro As EntidadNegocios.clsEntidadCorreoProveedor, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Delete from tbcorreoproveedor where idCorreoPro= '" & clsEntidadCorreoPro.pIdCorreoPro & "' "
            mEliminarCorreoPro = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            Return False
        End Try
    End Function

#End Region

End Class
