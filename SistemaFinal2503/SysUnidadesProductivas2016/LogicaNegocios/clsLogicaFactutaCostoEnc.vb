﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadFacturaCostoEnc

Public Class clsLogicaFactutaCostoEnc

#Region "ATRIBUTOS"
    Private strSentencia As String

#End Region

#Region "METODOS"
    'METODO AGREGA LA FACTURACOSTOENC
    Public Function mAgregarFacturaCostoEnc(clEntidadFacturaCostoEnc As EntidadNegocios.clsEntidadFacturaCostoEnc, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "INSERT INTO tbfacturacostoenc (idFactura, cedulaCli, login, total,fechaFactura, talonario) VALUES ('" & clEntidadFacturaCostoEnc.pIdFactura & "' , '" & clEntidadFacturaCostoEnc.pCedulaCli & "' , '" & clEntidadFacturaCostoEnc.pLogin & "' , '" & clEntidadFacturaCostoEnc.pTotal & "','" & clEntidadFacturaCostoEnc.pFecha & "' , '" & clEntidadFacturaCostoEnc.pTotal & "' )"
            mAgregarFacturaCostoEnc = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            mAgregarFacturaCostoEnc = False
        End Try
    End Function

    'METODO PARA CONSULTAR TODA LA INFORMACION DE LA TABLA 
    Public Function mConsultarFacturaCostoEnc(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader

        strSentencia = "Select * from tbfacturacostoenc"
        mConsultarFacturaCostoEnc = clsConexion.mConsultar(strSentencia)

    End Function

    Public Function mConsultarFacturaCostoEncIDFactura(clEntidadFacturaCostoEnc As EntidadNegocios.clsEntidadFacturaCostoEnc, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader

        strSentencia = "Select * from tbfacturacostoenc where idFactura ='" & clEntidadFacturaCostoEnc.pIdFactura & "'"
        mConsultarFacturaCostoEncIDFactura = clsConexion.mConsultar(strSentencia)

    End Function

    Public Function mConsultarFacturaCostoEncCedulaCli(clEntidadFacturaCostoEnc As EntidadNegocios.clsEntidadFacturaCostoEnc, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader

        strSentencia = "Select * from tbfacturacostoenc where cedulaCli ='" & clEntidadFacturaCostoEnc.pCedulaCli & "'"
        mConsultarFacturaCostoEncCedulaCli = clsConexion.mConsultar(strSentencia)

    End Function

    ''Metodo que permite consultar facturas por fehchas
    Public Function mConsultaFacturaCostoEncPorFechas(dateFecha1 As String, datefecha2 As String, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "SELECT * FROM tbfacturacostoenc WHERE (fechaFactura BETWEEN   '" & dateFecha1 & "' AND '" & datefecha2 & "')"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaFacturaCostoEncPorFechas = clsConexion.mConsultar(strSentencia)
    End Function

#End Region

End Class
