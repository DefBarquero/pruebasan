﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadTelefonoCliente
Public Class clsLogicaClienteTelefono

#Region "Atributos"
    Private strSentencia As String
#End Region

#Region "Metodos"
    'Por medio de este metodo se agrega la información  a la base de datos con respecto a los telefono de los clientes
    Public Function mAgregarTelefonoCli(clsEntidadTelefonoCli As EntidadNegocios.clsEntidadTelefonoCliente, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Insert into tbtelefonocliente (idTelefonoCli, cedulaCli, telefonoCli) values ('" & clsEntidadTelefonoCli.pIDTelefonoCli & "','" & clsEntidadTelefonoCli.pCedulaCli & "', '" & clsEntidadTelefonoCli.pTelefonoCli & "')"
            'viaja al Acceso de datos
            mAgregarTelefonoCli = clsConexion.mEjecutar(strSentencia)

        Catch ex As Exception
            Return False
        End Try
    End Function

    'Por medio de este metodo se modifica los telefonos de los clientes

    Public Function mModificarTelefonoCli(clsEntidadTelefonCli As EntidadNegocios.clsEntidadTelefonoCliente, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Update tbtelefonocliente set telefonoCli='" & clsEntidadTelefonCli.pTelefonoCli & "' where idTelefonoCli= '" & clsEntidadTelefonCli.pIDTelefonoCli & "' "
            'viaja a al Acceso de datos
            mModificarTelefonoCli = clsConexion.mEjecutar(strSentencia)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function mEliminarTelefonoCli(clsEntidadTelefonoCli As EntidadNegocios.clsEntidadTelefonoCliente, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Delete from tbtelefonocliente where idTelefonoCli = '" & clsEntidadTelefonoCli.pIDTelefonoCli & "' "
            mEliminarTelefonoCli = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            Return False
        End Try
    End Function

    'por es metodo se consultan los telefonos de los clientes, por medio de la cedula del cliente

    Public Function mBuscarTelefonoCliCedula(clsEntidadTelefonoCli As EntidadNegocios.clsEntidadTelefonoCliente, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbtelefonocliente where cedulaCli = '" & clsEntidadTelefonoCli.pCedulaCli & "' "
        'viaja a al Acceso Datos
        mBuscarTelefonoCliCedula = clsConexion.mConsultar(strSentencia)
    End Function

    'por medio de este metodo se genera el identificador para los telefonos de los clientes
    Public Function mGenerarIDTelefonoCli(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "select uuid() as uid"
        mGenerarIDTelefonoCli = clsConexion.mConsultar(strSentencia)

    End Function

#End Region
End Class
