﻿Imports MySql.Data.MySqlClient
Imports EntidadNegocios.clsEntidadCliente
Imports AccesoDatos.clsConexion
Public Class clsLogicaCliente

#Region "Atributos"
    Private strSentencia As String

#End Region

#Region "Metodos"
    'Este metodo sirve para agregar los datos del cliente a la base de datos
    Public Function mAgregarCliente(clsEntidadCliente As EntidadNegocios.clsEntidadCliente, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Insert into tbcliente (cedulaCli, nombreCli, apellido1Cli, apellido2Cli, provinciaCli, cantonCli, distritoCli, otrasSenasCli, estadoCli, tipoCli, nomEmpresaCli) values ('" & clsEntidadCliente.pCedulaCli & "', '" & clsEntidadCliente.pNombreCli & "', '" & clsEntidadCliente.pApellido1Cli & "', '" & clsEntidadCliente.pApellido2Cli & "', '" & clsEntidadCliente.pProvinciaCli & "','" & clsEntidadCliente.pCantonCli & "', '" & clsEntidadCliente.pDistritoCli & "','" & clsEntidadCliente.pOtraSenasCli & "', '" & clsEntidadCliente.pEstadoCli & "', '" & clsEntidadCliente.pTipoCli & "' , '" & clsEntidadCliente.pNombreEmpresa & "')"
            mAgregarCliente = clsConexion.mEjecutar(strSentencia)

        Catch ex As Exception
            Return False

        End Try
    End Function

    'Por medio de este metodo se realiza la modificación de los datos del cliente
    Public Function mModificarCliente(clsEntidadCliente As EntidadNegocios.clsEntidadCliente, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Update tbcliente set provinciaCli = '" & clsEntidadCliente.pProvinciaCli & "', cantonCli = '" & clsEntidadCliente.pCantonCli & "', distritoCli= '" & clsEntidadCliente.pDistritoCli & "', otrasSenasCli= '" & clsEntidadCliente.pOtraSenasCli & "', estadoCli= '" & clsEntidadCliente.pEstadoCli & "', nomEmpresaCli='" & clsEntidadCliente.pNombreEmpresa & "' where cedulaCli = '" & clsEntidadCliente.pCedulaCli & "' "
            mModificarCliente = clsConexion.mEjecutar(strSentencia)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function mBuscarCliente(clsEntidadCliente As EntidadNegocios.clsEntidadCliente, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbcliente where cedulaCli = '" & clsEntidadCliente.pCedulaCli & "' "
        mBuscarCliente = clsConexion.mConsultar(strSentencia)
    End Function

    'Public Function mID(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
    '    strSentencia = "select uuid() as uid"
    '    mID = clsConexion.mConsultar(strSentencia)

    'End Function

    Public Function mConsultaGeneralCli(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbcliente"
        mConsultaGeneralCli = clsConexion.mConsultar(strSentencia)
    End Function

    'Metodo permite consultar cliente por medio del cedula'
    Public Function mConsultarCedulaCliente(ByVal clCliente As String, clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbCliente where cedulaCli like'" & clCliente + "%" & "' "
        mConsultarCedulaCliente = clConexion.mConsultar(strSentencia)
    End Function

    'Metodo permite consultar al usuario por medio del nombre'
    Public Function mConsultarNombreCliente(ByVal clCliente As String, clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbCliente where nombrecli like'" & clCliente + "%" & "' "
        mConsultarNombreCliente = clConexion.mConsultar(strSentencia)
    End Function



#End Region

End Class
