﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadFacturaDet
Imports System.Security.Cryptography
Imports System.Text


Public Class clsLogicaFacturaDet

#Region "Atributos"

    Private strSentencia As String

#End Region

#Region "Metodos"

    ''Metodo que permite consultar los productos especificamente
    Public Function mConsultaFacturaDet(clsEntidadFacturaDet As EntidadNegocios.clsEntidadFacturaDet, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbfacturadet Where idFactura='" & clsEntidadFacturaDet.pIdFactura & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaFacturaDet = clsConexion.mConsultar(strSentencia)
    End Function

    'chino----------------
    Public Function mConsultaIdMayor(clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader

        'strSentencia = "Select DMax(idFactura,tbfacturadet) + 1 "
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaIdMayor = clConexion.mConsultar(strSentencia)
    End Function






    'Este metodo permite hacer una consulta general de los producto
    Public Function mConsultaGenFacturaDet(clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbfacturadet"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaGenFacturaDet = clConexion.mConsultar(strSentencia)
    End Function

    'Este metodo permite agregar productos en la base de datos
    Public Function mAgregarFacturaDet(clEntidadFactDet As EntidadNegocios.clsEntidadFacturaDet, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "insert into tbfacturadet (idFactura, idProducto, cantidad, subtotal) values ('" & clEntidadFactDet.pIdFactura & "', '" & clEntidadFactDet.pIdProducto & "', '" & clEntidadFactDet.pCantidad & "', '" & clEntidadFactDet.pSubTotal & "') "
            mAgregarFacturaDet = clConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            mAgregarFacturaDet = False
        End Try
    End Function

    Public Function mEliminarFacturaDet(clsEntidadFactDet As EntidadNegocios.clsEntidadFacturaDet, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Delete from tbfacturadet where idFactura = '" & clsEntidadFactDet.pIdFactura & "' "
            mEliminarFacturaDet = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            mEliminarFacturaDet = False


        End Try
    End Function


    ''Metodo que permite consultar las facturas que son de credito
    Public Function mConsultaIDFacturaDet(idFactura As String, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbfacturadet Where idFactura='" & idFactura & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaIDFacturaDet = clsConexion.mConsultar(strSentencia)
    End Function


#End Region






End Class
