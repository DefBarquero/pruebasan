﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadProductoServicio
Imports System.Security.Cryptography
Imports System.Text

Public Class clsLogicaProductoServicio
#Region "Atributos"
    Private strSentencia As String

#End Region

#Region "Metodos"

    ''Metodo que permite consultar los productos especificamente
    Public Function mConsultaProducto(clsEntidadProducto As EntidadNegocios.clsEntidadProductoServicio, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbproductoservicio Where idProducto_Servicio = '" & clsEntidadProducto.pIdProductoSer & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaProducto = clsConexion.mConsultar(strSentencia)
    End Function



    'Este metodo permite hacer una consulta general de los producto
    Public Function mConsultaGenProducto(clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbproductoservicio"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaGenProducto = clConexion.mConsultar(strSentencia)
    End Function

    'Este metodo permite agregar productos en la base de datos
    Public Function mAgregarProducto(clEntidadProducto As EntidadNegocios.clsEntidadProductoServicio, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "INSERT INTO tbproductoservicio (idProducto_Servicio, nombrePro_Ser, descripcionPro_Ser, costoPro_Ser, precioPro_Ser, imagenPro_Ser, tipoPro_Ser) VALUES ('" & clEntidadProducto.pIdProductoSer & "' , '" & clEntidadProducto.pNombreProSer & "' , '" & clEntidadProducto.pDescripcionProSer & "' , '" & clEntidadProducto.pCostoProSer & "' , '" & clEntidadProducto.pPrecioProSer & "' ,'" & clEntidadProducto.pImagenProSer & "' , '" & clEntidadProducto.pTipoProSer & "')"
            mAgregarProducto = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            mAgregarProducto = False
        End Try
    End Function

    'Este metodo permite modificar la informacion de los productos en la base de datos
    Public Function mModificarProducto(clEntidadProducto As EntidadNegocios.clsEntidadProductoServicio, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Update tbproductoservicio set costoPro_Ser= '" & clEntidadProducto.pCostoProSer & "', precioPro_Ser ='" & clEntidadProducto.pPrecioProSer & "', imagenPro_Ser ='" & clEntidadProducto.pImagenProSer & "' Where idProducto_Servicio='" & clEntidadProducto.pIdProductoSer & "' "
            mModificarProducto = clConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            Return False
        End Try
    End Function

    'Este metodo permite elim productos en la base de datos
    Public Function mEliminarProducto(clEntidadProducto As EntidadNegocios.clsEntidadProductoServicio, clConexion As AccesoDatos.clsConexion) As Boolean

        Try
            strSentencia = "Delete from tbproductoservicio where idProducto_Servicio= '" & clEntidadProducto.pIdProductoSer & "'"

            mEliminarProducto = clConexion.mEjecutar(strSentencia)

        Catch ex As Exception

            mEliminarProducto = False

        End Try

    End Function


    'Metodo desencripta
    Public Function mDesencriptar(ByVal Clave As String) As String
        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("qualityi") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("rpaSPvIvVLlrcmtzPU9/c67Gkj7yL1S5") 'No se puede alterar la cantidad de caracteres pero si la clave
        Dim buffer() As Byte = Convert.FromBase64String(Clave)
        Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        des.Key = EncryptionKey
        des.IV = IV
        Return Encoding.UTF8.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))
    End Function

    'Metodo permite consultar producto por medio del idProducto'
    Public Function mConsultarCodigoProducto(ByVal clProducto As String, clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbproductoservicio where idProducto_Servicio like'" & clProducto + "%" & "' "
        mConsultarCodigoProducto = clConexion.mConsultar(strSentencia)
    End Function

    'Metodo permite consultar usuario por medio del nombre'
    Public Function mConsultarProductoNombre(ByVal clProducto As String, clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbproductoservicio where nombrePro_Ser like'" & clProducto + "%" & "' "
        mConsultarProductoNombre = clConexion.mConsultar(strSentencia)
    End Function


    ''Metodo que permite consultar los productos especificamente, será utulizado en la parte de facturas
    'ya que en ocaciones al ingresar por ENTIDAD, se cae el sistema
    Public Function mConsultaProductoString(ByVal strCodProducto As String, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbproductoservicio Where idProducto_Servicio= '" & strCodProducto & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaProductoString = clsConexion.mConsultar(strSentencia)
    End Function

    ''Metodo que permite consultar los productos especificamente
    Public Function mConsultaCostoProducto(ByVal codex As String, clsConexion As AccesoDatos.clsConexion) As Decimal
        strSentencia = "Select costoPro_Ser From tbproductoservicio Where idProducto_Servicio= '" & codex & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaCostoProducto = Convert.ToDecimal(clsConexion.mConsultar(strSentencia))
    End Function

#End Region


End Class
