﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadDistrito

Public Class clsLogicaDistrito

#Region "ATRIBUTOS"
    Private strSentencia As String
#End Region

#Region "METODOS"

    Public Function mConsultarDistritoIdCanton(clsEntidadDistrito As EntidadNegocios.clsEntidadDistrito, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbdistrito where idCanton= '" & clsEntidadDistrito.pIdCanton & "' "
        mConsultarDistritoIdCanton = clsConexion.mConsultar(strSentencia)

    End Function

    Public Function mConsultaDistritoNombre(clsEntidadDistrito As EntidadNegocios.clsEntidadDistrito, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbdistrito where nombreDistrito = '" & clsEntidadDistrito.pNombreDistrito & "' "
        mConsultaDistritoNombre = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultaDistritoNombreIdCanton(clsEntidadDistrito As EntidadNegocios.clsEntidadDistrito, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbdistrito where nombreDistrito = '" & clsEntidadDistrito.pNombreDistrito & "' and idCanton= '" & clsEntidadDistrito.pIdCanton & "' "
        mConsultaDistritoNombreIdCanton = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultarDistritoID(clsEntidadDistrito As EntidadNegocios.clsEntidadDistrito, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbdistrito where idDistrito= '" & clsEntidadDistrito.pIdDistrito & "' "
        mConsultarDistritoID = clsConexion.mConsultar(strSentencia)

    End Function


#End Region



End Class
