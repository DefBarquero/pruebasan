﻿Imports MySql.Data.MySqlClient
Imports EntidadNegocios.clsEntidadPermisosUsersModulo

Public Class clsLogicaPermisosUsersModulo

#Region "ATRIBUTOS"
    Dim strSentencia As String

#End Region

#Region "METODOS"
    Public Function mAgregarPermisoModulo(clsEnPerUserModulo As EntidadNegocios.clsEntidadPermisosUsersModulo, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Insert into tbPermisoUsuarioModulo(idVentana, login, permisoAgregar, permisoModificar) values ('" & clsEnPerUserModulo.pIdVentana & "', '" & clsEnPerUserModulo.pLoginUsuario & "', '" & clsEnPerUserModulo.pPermisoAgregar & "', '" & clsEnPerUserModulo.pPermisoModificar & "')"
            mAgregarPermisoModulo = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            mAgregarPermisoModulo = False

        End Try
    End Function

    Public Function mCosultarPermisoModulo(clsEnPerUserModulo As EntidadNegocios.clsEntidadPermisosUsersModulo, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "select * from tbpermisousuariomodulo where login= '" & clsEnPerUserModulo.pLoginUsuario & "' "
        mCosultarPermisoModulo = clsConexion.mConsultar(strSentencia)

    End Function

    Public Function mCosultarPermisoModulo2(ByVal strLogin As String, ByVal strIdVentana As String, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "select * from tbpermisousuariomodulo where login= '" & strLogin & "' and idVentana = '" & strIdVentana & "' "
        mCosultarPermisoModulo2 = clsConexion.mConsultar(strSentencia)

    End Function

    Public Function mModificarPermisos(clsEnPerUserModulo As EntidadNegocios.clsEntidadPermisosUsersModulo, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Update tbPermisoUsuarioModulo set permisoAgregar='" & clsEnPerUserModulo.pPermisoAgregar & "', permisoModificar='" & clsEnPerUserModulo.pPermisoModificar & "' where idVentana='" & clsEnPerUserModulo.pIdVentana & "' and login='" & clsEnPerUserModulo.pLoginUsuario & "' "
            mModificarPermisos = clsConexion.mEjecutar(strSentencia)
        Catch ex As Exception
            Return False
        End Try
    End Function
#End Region

End Class
