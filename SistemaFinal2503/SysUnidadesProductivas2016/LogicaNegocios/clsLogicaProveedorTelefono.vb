﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadTelefonoProveedor


Public Class clsLogicaProveedorTelefono
#Region "Atributos"
    Private strSentencia As String

#End Region

#Region "Metodos"

    ''Metodo que permite realizar una consulta especifica de los telefonos de proveedores por cedula
    Public Function mConsultaTelefonoProCedula(clsEntidadProveedor As EntidadNegocios.clsEntidadTelefonoProveedor, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbtelefonoproveedor Where cedulaPro='" & clsEntidadProveedor.pCedulaPro & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaTelefonoProCedula = clsConexion.mConsultar(strSentencia)
    End Function

    'Este metodo permite hacer una consulta general de los telefonos proveedores
    Public Function mConsultaGenTelProveedor(clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbTelefonoProveedor   "
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaGenTelProveedor = clConexion.mConsultar(strSentencia)
    End Function

    'Este metodo permite agregar telefonos de proveedores
    Public Function mAgregarTelefonoPro(clEntidadTelefonoPro As EntidadNegocios.clsEntidadTelefonoProveedor, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "INSERT INTO tbtelefonoproveedor (idTelefonoPro, cedulaPro, telefonoPro) VALUES ('" & clEntidadTelefonoPro.pIdTelefonoPro & "','" & clEntidadTelefonoPro.pCedulaPro & "' , '" & clEntidadTelefonoPro.pTelefonoPro & "')"
            mAgregarTelefonoPro = clConexion.mEjecutar(strSentencia)
            Exit Function
        Catch
            Return False
        End Try
    End Function

    'Este metodo permite modificar los telefonos de proveedores
    Public Function mModificarTelefonoPro(clEntidadProveedor As EntidadNegocios.clsEntidadTelefonoProveedor, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Update tbtelefonoproveedor set telefonoPro= '" & clEntidadProveedor.pTelefonoPro & "' Where idTelefonoPro='" & clEntidadProveedor.pIdTelefonoPro & "' "
            mModificarTelefonoPro = clConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function mGenerarIDTelefonoPro(clsConexion As AccesoDatos.clsConexion) As MySqlDataReader
        strSentencia = "select uuid() as uid"
        mGenerarIDTelefonoPro = clsConexion.mConsultar(strSentencia)

    End Function

    Public Function mEliminarTelPro(clsEntiTelPro As EntidadNegocios.clsEntidadTelefonoProveedor, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Delete from tbtelefonoproveedor where idTelefonoPro = '" & clsEntiTelPro.pIdTelefonoPro & "' "
            mEliminarTelPro = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            Return False
        End Try

    End Function

#End Region
End Class
