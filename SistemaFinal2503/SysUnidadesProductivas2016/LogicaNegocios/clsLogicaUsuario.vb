﻿Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadUsuario
Imports System.Security.Cryptography
Imports System.Text
Imports MySql.Data.MySqlClient

Public Class clsLogicaUsuario

#Region "Atributos"
    Private strSentencia As String
    Private contrasena As String

#End Region

#Region "Metodos"

    'Metodo permite consultar usuario que estan en la base de datos, por medio del login y la contraseña'
    Public Function mConsultarUsuarioLoginCont(clUsuario As EntidadNegocios.clsEntidadUsuario, clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbUsuario where login='" & clUsuario.pLogin & "' and contrasena='" & clUsuario.pContrasena & "'    "
        mConsultarUsuarioLoginCont = clConexion.mConsultar(strSentencia)
    End Function

    'Metodo permite consultar usuario por medio del login'
    Public Function mConsultarUsuarioLogin(clUsuario As EntidadNegocios.clsEntidadUsuario, clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbusuario where login='" & clUsuario.pLogin & "' "
        mConsultarUsuarioLogin = clConexion.mConsultar(strSentencia)
    End Function





    'Metodo permite consultar usuario por medio del login'
    Public Function mConsultarUsuarioLogin2(ByVal clUsuario As String, clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbUsuario where login like'" & clUsuario + "%" & "' "
        mConsultarUsuarioLogin2 = clConexion.mConsultar(strSentencia)
    End Function

    'Metodo permite consultar usuario por medio del nombre'
    Public Function mConsultarUsuarioNombre(ByVal clUsuario As String, clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbUsuario where nombre like'" & clUsuario + "%" & "' "
        mConsultarUsuarioNombre = clConexion.mConsultar(strSentencia)
    End Function


    'Metodo consulta general los usuarios de la tabla
    Public Function mConsultarGenUsuario(clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbUsuario  where login <> 'aadmin' "
        mConsultarGenUsuario = clConexion.mConsultar(strSentencia)
    End Function



    'Metodo permite agregar usuario a la base de datos'
    Public Function mAgregarUsuario(clUsuario As EntidadNegocios.clsEntidadUsuario, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Insert into tbUsuario(login, contrasena, nombre, apellido1Usu, apellido2Usu, imagenUsu) values('" & clUsuario.pLogin & "', '" & clUsuario.pContrasena & "', '" & clUsuario.pNombre & "', '" & clUsuario.pApellido1Usu & "', '" & clUsuario.pApellido2Usu & "', '" & clUsuario.pImagenUsus & "')"
            mAgregarUsuario = clConexion.mEjecutar(strSentencia)
        Catch ex As Exception
            mAgregarUsuario = False
        End Try
    End Function

    'Metodo permite modificar la contraseña del usuario'
    Public Function mModificarUsuario(clUsuario As EntidadNegocios.clsEntidadUsuario, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Update tbUsuario set contrasena='" & clUsuario.pContrasena & "' where login='" & clUsuario.pLogin & "'"
            mModificarUsuario = clConexion.mEjecutar(strSentencia)
        Catch ex As Exception
            Return False
        End Try
    End Function


    'Metodo desencripta
    Public Function mDesencriptar(ByVal Clave As String) As String
        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("qualityi") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("rpaSPvIvVLlrcmtzPU9/c67Gkj7yL1S5") 'No se puede alterar la cantidad de caracteres pero si la clave
        Dim buffer() As Byte = Convert.FromBase64String(Clave)
        Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        des.Key = EncryptionKey
        des.IV = IV
        Return Encoding.UTF8.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

        Return contrasena

    End Function



#End Region
End Class
