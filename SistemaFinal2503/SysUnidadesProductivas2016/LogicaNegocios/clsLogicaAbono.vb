﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadAbono
Imports System.Security.Cryptography
Imports System.Text
Public Class clsLogicaAbono
#Region "Atributos"
    Private strSentencia As String
    Private abono, fecha As String

#End Region

#Region "Metodos"
    ''Metodo para agregar los abonos que se realicen
    Public Function mIngresarAbono(clsEntidadAbono As EntidadNegocios.clsEntidadAbono, clsConexion As AccesoDatos.clsConexion) As Boolean
        strSentencia = "Insert into tbAbonos (idAbono, cedulaCli, montoCompra, fechaAbono, saldoActual, idFactura, montoAbonado) values ('" & clsEntidadAbono.pIdAbono & "', '" & clsEntidadAbono.pCedulaCli & "', '" & clsEntidadAbono.pMontoCompra & "' , '" & clsEntidadAbono.pFechaAbono & "' , '" & clsEntidadAbono.pSaldoActual & "', '" & clsEntidadAbono.pIdFactura & "', '" & clsEntidadAbono.pSaldoAbonado & "') "
        mIngresarAbono = clsConexion.mEjecutar(strSentencia)
    End Function

    Public Function mUltimoAbono(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "SELECT MAX(idAbono) AS idAbono FROM tbAbonos"
        mUltimoAbono = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mUltimoAbono(clsEntidadAbono As EntidadNegocios.clsEntidadAbono, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select sum(saldoActual) from tbAbonos where idFactura = '" & clsEntidadAbono.pIdFactura & "'"
        mUltimoAbono = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mUltimoMonto(clsEntidadAbono As EntidadNegocios.clsEntidadAbono, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select saldoActual from tbAbonos where idFactura = '" & clsEntidadAbono.pIdFactura & "' and idAbono = (SELECT MAX(idAbono) AS idAbono FROM tbAbonos where idFactura = '" & clsEntidadAbono.pIdFactura & "')"
        mUltimoMonto = clsConexion.mConsultar(strSentencia)
    End Function


    Public Function mContarId(clsEntidadAbono As EntidadNegocios.clsEntidadAbono, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "select count(idAbono)  from tbAbonos where cedulaCli = '" & clsEntidadAbono.pCedulaCli & "'"
        mContarId = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mBuscarFactura(clsEntidadAbono As EntidadNegocios.clsEntidadAbono, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia ="Select saldoActual from tbAbonos where idFactura = '"& clsEntidadAbono.pIdFactura  &"' ORDER BY idAbono DESC LIMIT 1"
        mBuscarFactura = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultaGeneralAbono(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbAbonos"
        mConsultaGeneralAbono = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultaEspecificaAbono(clsEntidadCliente As EntidadNegocios.clsEntidadCliente, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbAbonos where cedulaCli = '" & clsEntidadCliente.pCedulaCli & "' "
        mConsultaEspecificaAbono = clsConexion.mConsultar(strSentencia)
    End Function

#End Region
End Class
