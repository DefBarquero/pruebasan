﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadProveedor

Public Class clsLogicaProveedor

#Region "Atributos"
    Private strSentencia As String

#End Region

#Region "Metodos"

    ''Metodo que permite consultar la cedula  de los proveedores
    Public Function mConsultaCedulaProveedor(clsEntidadProveedor As EntidadNegocios.clsEntidadProveedor, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbProveedor Where cedulaPro='" & clsEntidadProveedor.pCedulaPro & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaCedulaProveedor = clsConexion.mConsultar(strSentencia)
    End Function

    'Este metodo permite hacer una consulta general de los proveedore
    Public Function mConsultaGenProveedor(clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbproveedor   "
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaGenProveedor = clConexion.mConsultar(strSentencia)
    End Function

    'Este metodo permite Incluir en la base de datos
    Public Function mAgregarProveedor(clEntidadProveedor As EntidadNegocios.clsEntidadProveedor, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "INSERT INTO tbproveedor (cedulaPro, tipoPro, nombrePro, apellido1Pro, apellido2Pro, provinciaPro, cantonPro, distritoPro, otrasSenasPro, descripcionPro, nomEmpresaPro) VALUES ('" & clEntidadProveedor.pCedulaPro & "','" & clEntidadProveedor.pTipoPro & "' , '" & clEntidadProveedor.pNombrePro & "','" & clEntidadProveedor.pApellido1Pro & "', '" & clEntidadProveedor.pApellido2Pro & "','" & clEntidadProveedor.pProvinciaPro & "', '" & clEntidadProveedor.pCantonPro & "', '" & clEntidadProveedor.pDistritoPro & "', '" & clEntidadProveedor.pOtrasSenasPro & "','" & clEntidadProveedor.pDescripcionPro & "' ,'" & clEntidadProveedor.pNomEmpresaPro & "')"
            mAgregarProveedor = clConexion.mEjecutar(strSentencia)
            Exit Function
        Catch
            Return False
        End Try
    End Function

    Public Function mModificarProveedor(clEntidadProveedor As EntidadNegocios.clsEntidadProveedor, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Update tbproveedor set tipoPro= '" & clEntidadProveedor.pTipoPro & "', provinciaPro ='" & clEntidadProveedor.pProvinciaPro & "', cantonPro ='" & clEntidadProveedor.pCantonPro & "', distritoPro ='" & clEntidadProveedor.pDistritoPro & "', otrasSenasPro ='" & clEntidadProveedor.pOtrasSenasPro & "', nomEmpresaPro ='" & clEntidadProveedor.pNomEmpresaPro & "', descripcionPro = '" & clEntidadProveedor.pDescripcionPro & "' Where cedulaPro='" & clEntidadProveedor.pCedulaPro & "' "
            mModificarProveedor = clConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            Return False
        End Try
    End Function

    'Metodo permite consultar cliente por medio del cedula'
    Public Function mConsultarCedulaProveedor(ByVal clProveedor As String, clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbProveedor where cedulaPro like'" & clProveedor + "%" & "' "
        mConsultarCedulaProveedor = clConexion.mConsultar(strSentencia)
    End Function

    'Metodo permite consultar al usuario por medio del nombre'
    Public Function mConsultarNombreProveedor(ByVal clProveedor As String, clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbProveedor where nombrePro like'" & clProveedor + "%" & "' "
        mConsultarNombreProveedor = clConexion.mConsultar(strSentencia)
    End Function

#End Region

End Class
