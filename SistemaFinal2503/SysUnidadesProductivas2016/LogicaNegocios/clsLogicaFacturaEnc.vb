﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadFacturaDet
Imports System.Security.Cryptography
Imports System.Text


Public Class clsLogicaFacturaEnc

#Region "Atributos"

    Private strSentencia As String

#End Region

#Region "Metodos"

    ''Metodo que permite consultar los productos especificamente
    Public Function mConsultaFacturaEnc(clsEntidadFacturaEnc As EntidadNegocios.clsEntidadFacturaEnc, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbfacturaenc Where cedulaCli='" & clsEntidadFacturaEnc.pCedulaCli & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaFacturaEnc = clsConexion.mConsultar(strSentencia)
    End Function

    'Este metodo permite hacer una consulta general de los producto
    Public Function mConsultaGenFacturaEnc(clConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbfacturaEnc"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaGenFacturaEnc = clConexion.mConsultar(strSentencia)
    End Function

    'Este metodo permite agregar facturas en la base de datos
    Public Function mAgregarFacturaEnc(clEntidadFacturaEnc As EntidadNegocios.clsEntidadFacturaEnc, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "INSERT INTO tbFacturaEnc (idFactura, cedulaCli, login, total,fechaFactura, talonario, tipoPago, iva) VALUES ('" & clEntidadFacturaEnc.pIdFactura & "' , '" & clEntidadFacturaEnc.pCedulaCli & "' , '" & clEntidadFacturaEnc.pLogin & "' , '" & clEntidadFacturaEnc.pTotal & "' , '" & clEntidadFacturaEnc.pFecha & "' , '" & clEntidadFacturaEnc.pTalonario & "' , '" & clEntidadFacturaEnc.pTipoPago & "', '" & clEntidadFacturaEnc.pIva & "' )"
            mAgregarFacturaEnc = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            mAgregarFacturaEnc = False
        End Try
    End Function

    ''Metodo que permite consultar facturas por fehchas
    Public Function mConsultaFacturaEncPorFechas(dateFecha1 As String, datefecha2 As String, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "SELECT * FROM tbfacturaenc WHERE (fechaFactura BETWEEN   '" & dateFecha1 & "' AND '" & datefecha2 & "')"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaFacturaEncPorFechas = clsConexion.mConsultar(strSentencia)
    End Function

    ''Metodo que permite consultar los productos especificamente
    Public Function mConsultaFacturaEncCed(cedulaCli As String, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbfacturaenc Where cedulaCli = '" & cedulaCli & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaFacturaEncCed = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultaFacturaCredito(cedulaCli As String, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbfacturaenc where cedulaCli = '" & cedulaCli & "' and tipoPago = 'Credito'"
        mConsultaFacturaCredito = clsConexion.mConsultar(strSentencia)
    End Function


    ''Metodo que permite consultar los productos especificamente
    Public Function mConsultaNumFacturaEnc(clsEntidadFacturaEnc As EntidadNegocios.clsEntidadFacturaEnc, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbfacturaenc Where idFactura = '" & clsEntidadFacturaEnc.pIdFactura & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaNumFacturaEnc = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mActualizarEstado(clsEntidadAbono As EntidadNegocios.clsEntidadAbono, clsConexion As AccesoDatos.clsConexion) As Boolean
        strSentencia = "Update tbFacturaEnc set tbFacturaEnc.tipoPago = 'PAGADA' where tbFacturaEnc.idFactura = '" & clsEntidadAbono.pIdFactura & "' "
        mActualizarEstado = clsConexion.mEjecutar(strSentencia)
    End Function

#End Region






End Class

