﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadFacturaCostoDet

Public Class clsLogicaFacturaCostoDet

#Region "ATRIBUTOS"
    Private strSentencia As String
#End Region

#Region "METODOS"
    ''Metodo que permite consultar los productos especificamente
    Public Function mConsultaFacturaCostoDet(clsEntidadFacturaCostoDet As EntidadNegocios.clsEntidadFacturaCostoDet, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * From tbfacturacostodet Where idFactura='" & clsEntidadFacturaCostoDet.pIdFactura & "'"
        ''Este metodo permite realizar las extraccion de datos por medio de los select a la BD
        mConsultaFacturaCostoDet = clsConexion.mConsultar(strSentencia)
    End Function

    'METODO AGREGA LA FACTURACOSTODET
    Public Function mAgregarFacturaCostoDet(clEntidadFactCostoDet As EntidadNegocios.clsEntidadFacturaCostoDet, clConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "insert into tbfacturacostodet (idFactura, idProducto, cantidad, subtotal) values ('" & clEntidadFactCostoDet.pIdFactura & "', '" & clEntidadFactCostoDet.pIdProducto & "', '" & clEntidadFactCostoDet.pCantidad & "', '" & clEntidadFactCostoDet.pSubTotal & "') "
            mAgregarFacturaCostoDet = clConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            mAgregarFacturaCostoDet = False
        End Try
    End Function

    'METODO PARA ELIMINAR LA FACTURA COSTO DET
    Public Function mEliminarFacturaCostoDet(clsEntidadFactCostoDet As EntidadNegocios.clsEntidadFacturaCostoDet, clsConexion As AccesoDatos.clsConexion) As Boolean
        Try
            strSentencia = "Delete from tbfacturacostodet where idFactura = '" & clsEntidadFactCostoDet.pIdFactura & "' "
            mEliminarFacturaCostoDet = clsConexion.mEjecutar(strSentencia)
            Exit Function
        Catch ex As Exception
            mEliminarFacturaCostoDet = False


        End Try
    End Function

#End Region

End Class
