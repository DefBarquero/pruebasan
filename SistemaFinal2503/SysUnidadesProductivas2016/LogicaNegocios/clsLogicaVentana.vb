﻿Imports EntidadNegocios.clsEntidadVentana
Imports MySql.Data.MySqlClient

Public Class clsLogicaVentana

#Region "ATRIBUTOS"
    Dim strSentencia As String
#End Region

#Region "METODOS"
    Public Function mConsultarVentanas000_099(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "SELECT * FROM tbventas WHERE (idVentana BETWEEN '003' AND '099')"
        mConsultarVentanas000_099 = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultarVentanas000_099_SinPermiso(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "SELECT * FROM tbventas WHERE (idVentana BETWEEN '003' AND '099') and idVentana <> '008' "
        mConsultarVentanas000_099_SinPermiso = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultarVentanas100_199(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "SELECT * FROM tbventas WHERE (idVentana BETWEEN '100' AND '199')"
        mConsultarVentanas100_199 = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultarVentanas(clsEntidadVentana As EntidadNegocios.clsEntidadVentana, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "SELECT * FROM tbventas WHERE (idVentana = '" & clsEntidadVentana.pIDVentana & "' )"
        mConsultarVentanas = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultarVentanasID(ByVal strIdVentana As String, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "SELECT * FROM tbventas WHERE (idVentana = '" & strIdVentana & "' )"
        mConsultarVentanasID = clsConexion.mConsultar(strSentencia)
    End Function

#End Region

End Class
