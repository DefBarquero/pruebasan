﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadProvincia

Public Class clsLogicaProvincia

#Region "ATRIBUTOS"
    Private strSentencia As String
#End Region

#Region "METODOS"
    'por medio de este metodo se consultan las provincias
    Public Function mConsultaProvincia(clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbprovincia"
        mConsultaProvincia = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultaProvinciaNombre(clsEntiProvincia As EntidadNegocios.clsEntidadProvincia, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbprovincia where nombreProvincia = '" & clsEntiProvincia.pNombreProvincia & "' "
        mConsultaProvinciaNombre = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultaProvinciaID(clsEntiProvincia As EntidadNegocios.clsEntidadProvincia, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbprovincia where idProvincia = '" & clsEntiProvincia.pIdProvincia & "' "
        mConsultaProvinciaID = clsConexion.mConsultar(strSentencia)
    End Function

#End Region

End Class
