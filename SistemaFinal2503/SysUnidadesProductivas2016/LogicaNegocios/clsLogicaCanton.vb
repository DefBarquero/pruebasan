﻿Imports MySql.Data.MySqlClient
Imports AccesoDatos.clsConexion
Imports EntidadNegocios.clsEntidadCanton

Public Class clsLogicaCanton

#Region "ATRIBUTOS"

    Private strSentencia As String
#End Region

#Region "METODOS"

    'por medio de este metodo se consultan los cantones correspondientes a cada provincia 
    Public Function mConsultarCantonIdProvincia(clsEntidadCanton As EntidadNegocios.clsEntidadCanton, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbcanton where idProvincia = '" & clsEntidadCanton.pIdProvincia & "' "
        mConsultarCantonIdProvincia = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultarCantonNombre(clsEntidadCanton As EntidadNegocios.clsEntidadCanton, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbcanton where nombreCanton = '" & clsEntidadCanton.pNombreCanton & "' "
        mConsultarCantonNombre = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultarCantonNombreIdProvincia(clsEntidadCanton As EntidadNegocios.clsEntidadCanton, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbcanton where nombreCanton = '" & clsEntidadCanton.pNombreCanton & "' and idProvincia = '" & clsEntidadCanton.pIdProvincia & "' "
        mConsultarCantonNombreIdProvincia = clsConexion.mConsultar(strSentencia)
    End Function

    Public Function mConsultarCantonID(clsEntidadCanton As EntidadNegocios.clsEntidadCanton, clsConexion As AccesoDatos.clsConexion) As MySql.Data.MySqlClient.MySqlDataReader
        strSentencia = "Select * from tbcanton where idCanton = '" & clsEntidadCanton.pIdCanton & "' "
        mConsultarCantonID = clsConexion.mConsultar(strSentencia)
    End Function

#End Region

End Class
