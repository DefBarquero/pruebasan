use dbproyectosan;

/*--VENTANAS--*/


/*VENTANAS DE MODULOS*/
insert into tbventas (idVentana,nombreVentana) value ('003','Usuario');
insert into tbventas (idVentana,nombreVentana) value ('004','Cliente');
insert into tbventas (idVentana,nombreVentana) value ('005','Producto');
insert into tbventas (idVentana,nombreVentana) value ('006','Proveedor');
insert into tbventas (idVentana,nombreVentana) value ('007','Factura');
insert into tbventas (idVentana,nombreVentana) value ('008','Permiso');

/*VENTANAS DE CONSULTAS*/
insert into tbventas (idVentana,nombreVentana) value ('100','Usuario Cosulta-Especifica');
insert into tbventas (idVentana,nombreVentana) value ('101','Usuario Cosulta-General');
insert into tbventas (idVentana,nombreVentana) value ('102','Cliente Cosulta-Especifica');
insert into tbventas (idVentana,nombreVentana) value ('103','Cliente Cosulta-General');
insert into tbventas (idVentana,nombreVentana) value ('104','Producto Cosulta-Especifica');
insert into tbventas (idVentana,nombreVentana) value ('105','Producto Cosulta-General');
insert into tbventas (idVentana,nombreVentana) value ('106','Proveedor Cosulta-Especifica');
insert into tbventas (idVentana,nombreVentana) value ('107','Proveedor Cosulta-General');
insert into tbventas (idVentana,nombreVentana) value ('108','Factura Cosulta-Especifica');
insert into tbventas (idVentana,nombreVentana) value ('109','Factura Cosulta-General');

/*TABLAS DE PROVINCIAS-CANTONES-DISTRITOS*/

/*PROVINCIA SAN JOSÉ*/
insert into tbprovincia(idProvincia,nombreProvincia) value ('CR-SJ','SAN JOSÉ');

/*CANTONES DE LA PROVINCIA DE SAN JOSE*/

insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-001','CR-SJ', 'SAN JOSÉ');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-002','CR-SJ', 'ESCAZÚ');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-003','CR-SJ', 'DESAMPARADOS');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-004','CR-SJ', 'PURISCAL');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-005','CR-SJ', 'TARRAZÚ');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-006','CR-SJ', 'ASERRÍ');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-007','CR-SJ', 'MORA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-008','CR-SJ', 'GOICHOECHEA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-009','CR-SJ', 'SANTA ANA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-010','CR-SJ', 'ALAJUELITA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-011','CR-SJ', 'ACOSTA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-012','CR-SJ', 'TIBÁS');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-013','CR-SJ', 'MORAVIA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-014','CR-SJ', 'MONTES DE OCA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-015','CR-SJ', 'TURRUBARES');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-016','CR-SJ', 'DOTA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-017','CR-SJ', 'CURRIDABAT');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-018','CR-SJ', 'PERÉZ ZELEDÓN');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-SJ-019','CR-SJ', 'LEÓN CORTÉS');

/*DISTRITOS DE LA PROVINCIA DE SAN JOSE*/

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-001','CR-SJ-001','CARMEN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-002','CR-SJ-001','MERCED' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-003','CR-SJ-001','HOSPITAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-004','CR-SJ-001','CATETRAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-005','CR-SJ-001','ZAPOTE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-006','CR-SJ-001','SAN FRANCISCO DE DOS RÍOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-007','CR-SJ-001','LA URUCA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-008','CR-SJ-001','MATA REDONDA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-009','CR-SJ-001','PAVAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-010','CR-SJ-001','HATILLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-001-011','CR-SJ-001','SAN SEBASTIÁN' );


insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-002-001','CR-SJ-002','ESCAZÚ CENTRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-002-002','CR-SJ-002','SAN RAFAEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-002-003','CR-SJ-002','SAN ANTONIO' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-001','CR-SJ-003','DESAMPARADOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-002','CR-SJ-003','SAN MIGUEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-003','CR-SJ-003','SAN JUAN DE DIOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-004','CR-SJ-003','SAN RAFAEL ARRIBA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-005','CR-SJ-003','SAN ANTONIO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-006','CR-SJ-003','FRAILES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-007','CR-SJ-003','PATARRÁ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-008','CR-SJ-003','SAN CRISTÓBAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-009','CR-SJ-003','ROSARIO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-010','CR-SJ-003','DAMAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-011','CR-SJ-003','SAN RAFAEL ABAJO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-012','CR-SJ-003','GRAVILIAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-003-013','CR-SJ-003','LOS GUIDOS' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-004-001','CR-SJ-004','SANTIAGO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-004-002','CR-SJ-004','MERCEDES SUR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-004-003','CR-SJ-004','BARBACOAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-004-004','CR-SJ-004','GRIFO ALTO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-004-005','CR-SJ-004','SAN RAFAEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-004-006','CR-SJ-004','CANDELARITA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-004-007','CR-SJ-004','DESAMPARADITOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-004-008','CR-SJ-004','SAN ANTONIO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-004-009','CR-SJ-004','CHIRES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-004-010','CR-SJ-004','LA CANGREJA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-005-001','CR-SJ-005','SAN MARCOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-005-002','CR-SJ-005','SAN LORENZO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-005-003','CR-SJ-005','SAN CARLOS' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-006-001','CR-SJ-006','ASERRÍ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-006-002','CR-SJ-006','TARBACA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-006-003','CR-SJ-006','VUELTA DE JORCO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-006-004','CR-SJ-006','SAN GABRIEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-006-005','CR-SJ-006','LEGUA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-006-006','CR-SJ-006','MONSTERREY' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-006-007','CR-SJ-006','SALITRILLOS' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-007-001','CR-SJ-007','COLÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-007-002','CR-SJ-007','GUAYABO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-007-003','CR-SJ-007','TABARCIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-007-004','CR-SJ-007','PIEDRAS NEGRAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-007-005','CR-SJ-007','PICAGRES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-007-006','CR-SJ-007','JARIS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-007-007','CR-SJ-007','QUITIRRISÍ' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-008-001','CR-SJ-008','GUADALUPE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-008-002','CR-SJ-008','SAN FRANCISCO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-008-003','CR-SJ-008','CALLE BLANCOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-008-004','CR-SJ-008','MATA DE PLATANO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-008-005','CR-SJ-008','IPÍS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-008-006','CR-SJ-008','RANCHO REDONDO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-008-007','CR-SJ-008','PURRAL' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-009-001','CR-SJ-009','SANTA ANA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-009-002','CR-SJ-009','SALITRAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-009-003','CR-SJ-009','POZOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-009-004','CR-SJ-009','URUCA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-009-005','CR-SJ-009','PIEDADES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-009-006','CR-SJ-009','BRASIL' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-010-001','CR-SJ-010','ALAJUELITA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-010-002','CR-SJ-010','SAN JOSECITO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-010-003','CR-SJ-010','SAN ANTONIO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-010-004','CR-SJ-010','CONCEPCIÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-010-005','CR-SJ-010','SAN FELIPE' );


insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-011-001','CR-SJ-011','SAN IGNACIO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-011-002','CR-SJ-011','GUAITIL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-011-003','CR-SJ-011','PALMICHAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-011-004','CR-SJ-011','CANGREJAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-011-005','CR-SJ-011','SABANILLAS' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-012-001','CR-SJ-012','SAN JUAN DE TIBÁS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-012-002','CR-SJ-012','CINCO ESQUINAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-012-003','CR-SJ-012','ANSELMO LLORENTE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-012-004','CR-SJ-012','LEÓN XIII' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-012-005','CR-SJ-012','COLIMA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-013-001','CR-SJ-013','SAN VICENTE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-013-002','CR-SJ-013','SAN JERÓNIMO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-013-003','CR-SJ-013','LA TRINIDAD' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-014-001','CR-SJ-014','SAN PEDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-014-002','CR-SJ-014','SABANILLA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-014-003','CR-SJ-014','MERCEDES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-014-004','CR-SJ-014','SAN RAFAEL' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-015-001','CR-SJ-015','SAN PABLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-015-002','CR-SJ-015','SAN PEDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-015-003','CR-SJ-015','SAN JUAN DE MATA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-015-004','CR-SJ-015','SAN LUIS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-015-005','CR-SJ-015','CARARA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-016-001','CR-SJ-016','SANTA MARÍA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-016-002','CR-SJ-016','JARDÍN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-016-003','CR-SJ-016','COPEY' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-017-001','CR-SJ-017','CURRIDABAT' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-017-002','CR-SJ-017','GRANADILLA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-017-003','CR-SJ-017','SÁNCHEZ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-017-004','CR-SJ-017','TIRRASES' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-001','CR-SJ-018','SAN ISIDRO DE EL GENERAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-002','CR-SJ-018','EL GENERAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-003','CR-SJ-018','DANIEL FLORES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-004','CR-SJ-018','RIVAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-005','CR-SJ-018','SAN PEDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-006','CR-SJ-018','PLATANARES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-007','CR-SJ-018','PEJIBAYE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-008','CR-SJ-018','CAJÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-009','CR-SJ-018','BARÚ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-010','CR-SJ-018','RÍO NUEVO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-011','CR-SJ-018','PÁRAMO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-018-012','CR-SJ-018','LA AMISTDA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-019-001','CR-SJ-019','SAN PABLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-019-002','CR-SJ-019','SAN ANDRÉS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-019-003','CR-SJ-019','LLANO BONITO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-019-004','CR-SJ-019','SAN ISIDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-019-005','CR-SJ-019','SANTA CRUZ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-SJ-019-006','CR-SJ-019','SAN ANTONIO' );

/*PROVINCIA DE ALAJUELA */

insert into tbprovincia(idProvincia,nombreProvincia) value ('CR-A','ALAJUELA');

/*CANTONES PROVINCIA DE ALAJUELA*/
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-001','CR-A', 'ALAJUELA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-002','CR-A', 'SAN RAMÓN');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-003','CR-A', 'GRECIA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-004','CR-A', 'SAN MATEO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-005','CR-A', 'ATENAS');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-006','CR-A', 'NARANJO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-007','CR-A', 'PALMARES');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-008','CR-A', 'POÁS');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-009','CR-A', 'OROTINA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-010','CR-A', 'SAN CARLOS');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-011','CR-A', 'ZARCERO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-012','CR-A', 'VALVERDE VEGA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-013','CR-A', 'UPALA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-014','CR-A', 'LOS CHILES');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-A-015','CR-A', 'GUATUSO');

/*DISTRITO PROVINCIA DE ALAJUELA*/

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-001','CR-A-001','ALAJUELA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-002','CR-A-001','SAN JOSÉ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-003','CR-A-001','CARRIZAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-004','CR-A-001','SAN ANTONIO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-005','CR-A-001','GUÁCIMA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-006','CR-A-001','SAN ISIDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-007','CR-A-001','SABANILLA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-008','CR-A-001','SAN RAFAEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-009','CR-A-001','RÍO SEGUNDO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-010','CR-A-001','DESAMPARADOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-011','CR-A-001','TURRÚCARES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-012','CR-A-001','TAMBOR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-013','CR-A-001','GARITA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-001-014','CR-A-001','SAN MIGUEL' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-001','CR-A-002','SAN RAMÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-002','CR-A-002','SANTIAGO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-003','CR-A-002','SAN JUAN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-004','CR-A-002','PIEDADES NORTE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-005','CR-A-002','PIEDADES SUR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-006','CR-A-002','SAN RAFAEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-007','CR-A-002','SAN ISIDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-008','CR-A-002','ÁNGELES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-009','CR-A-002','ALFARO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-010','CR-A-002','VOLIO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-011','CR-A-002','CONCEPCIÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-012','CR-A-002','ZAPOTAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-013','CR-A-002','PEÑAS BLANCAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-014','CR-A-002','LA PAZ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-015','CR-A-002','LA ESPERANZA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-002-016','CR-A-002','BAJO ZUÑOGA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-003-001','CR-A-003','GRECIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-003-002','CR-A-003','SAN ISIDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-003-003','CR-A-003','SAN JOSÉ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-003-004','CR-A-003','SAN ROQUE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-003-005','CR-A-003','TACARES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-003-006','CR-A-003','RÍO CUARTO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-003-007','CR-A-003','PUENTE DE PIEDRA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-003-008','CR-A-003','BOLÍVAR' );


insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-004-001','CR-A-004','SAN MATEO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-004-002','CR-A-004','DESMONTE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-004-003','CR-A-004','JESÚS MARÍA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-004-004','CR-A-004','LABRADOR' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-005-001','CR-A-005','ATENAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-005-002','CR-A-005','JESÚS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-005-003','CR-A-005','MERCEDES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-005-004','CR-A-005','SAN ISIDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-005-005','CR-A-005','CONCEPCIÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-005-006','CR-A-005','SAN JOSÉ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-005-007','CR-A-005','SANTA EULALIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-005-008','CR-A-005','ESCOBAL' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-006-001','CR-A-006','NARANJO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-006-002','CR-A-006','SAN MIGUEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-006-003','CR-A-006','SAN JOSÉ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-006-004','CR-A-006','CIRRÍ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-006-005','CR-A-006','SAN JERÓNIMO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-006-006','CR-A-006','SAN JUAN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-006-007','CR-A-006','ROSARIO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-006-008','CR-A-006','PALMITOS' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-007-001','CR-A-007','PALMARES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-007-002','CR-A-007','ZARAGOZA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-007-003','CR-A-007','BUENOS AIRES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-007-004','CR-A-007','SANTIAGO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-007-005','CR-A-007','CANDELARIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-007-006','CR-A-007','ESQUIPULAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-007-007','CR-A-007','LA GRANJA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-008-001','CR-A-008','SAN PEDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-008-002','CR-A-008','SAN JUAN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-008-003','CR-A-008','SAN RAFAEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-008-004','CR-A-008','CARRILLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-008-005','CR-A-008','SABANA REDONDA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-009-001','CR-A-009','OROTINA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-009-002','CR-A-009','EL MASTATE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-009-003','CR-A-009','HACIENDA VIEJA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-009-004','CR-A-009','EL COYOLAR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-009-005','CR-A-009','LA CEIBA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-001','CR-A-010','QUESADA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-002','CR-A-010','FLORENCIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-003','CR-A-010','BUENAVISTA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-004','CR-A-010','AGUAS ZARCAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-005','CR-A-010','VENECIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-006','CR-A-010','PITAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-007','CR-A-010','LA FORTUNA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-008','CR-A-010','LA TIGRA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-009','CR-A-010','PALMERA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-010','CR-A-010','VENADO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-011','CR-A-010','CUTRIS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-012','CR-A-010','MONTERREY' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-010-013','CR-A-010','POCOSOL' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-011-001','CR-A-011','ZARCERO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-011-002','CR-A-011','LAGUNA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-011-003','CR-A-011','TAPEZCO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-011-004','CR-A-011','GUADALUPE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-011-005','CR-A-011','PALMIRA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-011-006','CR-A-011','ZAPOTE');
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-011-007','CR-A-011','BRISAS' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-012-001','CR-A-012','SARCHÍ NORTE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-012-002','CR-A-012','SARCHÍ SUR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-012-003','CR-A-012','TORO AMARILLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-012-004','CR-A-012','SAN PEDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-012-005','CR-A-012','RODRÍGUEZ' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-013-001','CR-A-013','UPALA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-013-002','CR-A-013','AGUAS CLARAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-013-003','CR-A-013','SAN JOSÉ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-013-004','CR-A-013','BIJAGUA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-013-005','CR-A-013','DELICIAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-013-006','CR-A-013','DOS RÍOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-013-007','CR-A-013','YOLILLAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-013-008','CR-A-013','CANALETE' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-014-001','CR-A-014','LOS CHILES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-014-002','CR-A-014','CAÑO NEGRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-014-003','CR-A-014','EL AMPARO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-014-004','CR-A-014','SAN JORGE' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-015-001','CR-A-015','SAN RAFAEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-015-002','CR-A-015','BUENAVISTA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-015-003','CR-A-015','COTE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-A-015-004','CR-A-015','KATIRA' );


/**PROVINCIAA DE CARTAGO */
insert into tbprovincia(idProvincia,nombreProvincia) value ('CR-C','CARTAGO');


/*CANTONES PROVINCIA DE CARTAGO*/
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-C-001','CR-C', 'CARTAGO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-C-002','CR-C', 'PARAÍSO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-C-003','CR-C', 'LA UNIÓN');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-C-004','CR-C', 'JIMÉNEZ');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-C-005','CR-C', 'TURRIALBA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-C-006','CR-C', 'ALVARADO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-C-007','CR-C', 'OREAMUNO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-C-008','CR-C', 'EL GUARCO');

/*DISTRITOS PROVINCIA DE CARTAGO*/
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-001','CR-C-001','ORIENTAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-002','CR-C-001','OCCIDENTAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-003','CR-C-001','CARMEN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-004','CR-C-001','SAN NICOLÁS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-005','CR-C-001','AGUA CALIENTE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-006','CR-C-001','GUADALUPE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-007','CR-C-001','CORRALILLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-008','CR-C-001','TIERRAS BLANCAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-009','CR-C-001','DULCE NOMBRE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-010','CR-C-001','LLANO GRANDE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-001-011','CR-C-001','QUEBRADILLA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-002-001','CR-C-002','PARAÍSO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-002-002','CR-C-002','OROSI' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-002-003','CR-C-002','CACHÍ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-002-004','CR-C-002','SANTIAGO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-002-005','CR-C-002','LLANOS DE SANTA LUCÍA');

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-003-001','CR-C-003','TRES RÍOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-003-002','CR-C-003','SAN DIEGO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-003-003','CR-C-003','SAN JUAN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-003-004','CR-C-003','SAN RAFAEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-003-005','CR-C-003','CONCEPCIÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-003-006','CR-C-003','DULCE NOMBRE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-003-007','CR-C-003','SAN RAMÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-003-008','CR-C-003','RÍO AZUL' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-004-001','CR-C-004','JUAN VIÑA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-004-002','CR-C-004','TUCURRIQUE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-004-003','CR-C-004','PEJIBAYE' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-001','CR-C-005','TURRIALBA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-002','CR-C-005','LA SUIZA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-003','CR-C-005','PERALTA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-004','CR-C-005','SANTA CRUZ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-005','CR-C-005','SANTA TERESITA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-006','CR-C-005','PAVONES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-007','CR-C-005','TUIS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-008','CR-C-005','TAYUTIC' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-009','CR-C-005','SANTA ROSA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-010','CR-C-005','TRES EQUIS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-011','CR-C-005','LA ISABEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-005-012','CR-C-005','CHIRRIPÓ' );



insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-006-001','CR-C-006','PACAYAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-006-002','CR-C-006','CERVANTES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-006-003','CR-C-006','CAPELLADES' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-007-001','CR-C-007','SAN RAFAEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-007-002','CR-C-007','COT' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-007-003','CR-C-007','POTRERO CERRADO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-007-004','CR-C-007','CIPRESES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-007-005','CR-C-007','SANTA ROSA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-008-001','CR-C-008','TEJAR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-008-002','CR-C-008','SAN ISIDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-008-003','CR-C-008','TOBOSI' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-C-008-004','CR-C-008','PATIO DE AGUA' );



/*PROVIANCIA DE  HEREDIA*/
insert into tbprovincia(idProvincia,nombreProvincia) value ('CR-H','HEREDIA');

/*CANTONES PROVINCIA DE HEREDIA*/
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-H-001','CR-H', 'HEREDIA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-H-002','CR-H', 'BARVA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-H-003','CR-H', 'SANTO DOMINGO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-H-004','CR-H', 'SANTA BÁRBARA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-H-005','CR-H', 'SAN RAFAEL');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-H-006','CR-H', 'SAN ISIDRO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-H-007','CR-H', 'BELÉN');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-H-008','CR-H', 'FLORES');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-H-009','CR-H', 'SAN PABLO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-H-010','CR-H', 'SARAPIQUÍ');

/*DISTRITOS PROVINCIA DE HEREDIA*/
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-001-001','CR-H-001','HEREDIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-001-002','CR-H-001','MERCEDES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-001-003','CR-H-001','SAN FRANCISCO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-001-004','CR-H-001','ULLOA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-001-005','CR-H-001','VARABLANCA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-002-001','CR-H-002','BARVA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-002-002','CR-H-002','SAN PEDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-002-003','CR-H-002','SAN PABLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-002-004','CR-H-002','SAN ROQUE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-002-005','CR-H-002','SANTA LUCÍA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-002-006','CR-H-002','SAN JOSÉ DE LA MONTAÑA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-003-001','CR-H-003','SANTO DOMINDO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-003-002','CR-H-003','SAN VICENTE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-003-003','CR-H-003','SAN MIGUEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-003-004','CR-H-003','PARACITO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-003-005','CR-H-003','SAN TOMÁS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-003-006','CR-H-003','SANTA ROSA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-003-007','CR-H-003','TURES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-003-008','CR-H-003','PARÁ' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-004-001','CR-H-004','SANTA BÁRBARA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-004-002','CR-H-004','SAN PEDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-004-003','CR-H-004','SAN JUAN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-004-004','CR-H-004','JESÚS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-004-005','CR-H-004','SANTO DOMINGO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-004-006','CR-H-004','PURABA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-005-001','CR-H-005','SAN RAFAEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-005-002','CR-H-005','SAN JOSECITO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-005-003','CR-H-005','SANTIAGO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-005-004','CR-H-005','LOS ÁNGELES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-005-005','CR-H-005','CONCEPCIÓN' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-006-001','CR-H-006','SAN ISIDRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-006-002','CR-H-006','SAN JOSÉ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-006-003','CR-H-006','CONCEPCIÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-006-004','CR-H-006','SAN FRANCISCO' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-007-001','CR-H-007','SAN ANTONIO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-007-002','CR-H-007','LA RIBERA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-007-003','CR-H-007','LA ASUSNCIÓN' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-008-001','CR-H-008','SAN JOAQUÍN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-008-002','CR-H-008','BARRANTES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-008-003','CR-H-008','LLORENTE' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-009-001','CR-H-009','SAN PABLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-009-002','CR-H-009','RINCÓN DE SABANILLA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-010-001','CR-H-010','PUERTO VIEJO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-010-002','CR-H-010','LA VIRGEN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-010-003','CR-H-010','LAS HORQUESTAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-010-004','CR-H-010','LLANURAS DEL GASPAR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-H-010-005','CR-H-010','CUREÑA' );




/*PROVIANCIA DE  GUANACASTE*/
insert into tbprovincia(idProvincia,nombreProvincia) value ('CR-G','GUANACASTE');
/*CANTONES PROVINCIA DE GUANACASTE */
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-001','CR-G', 'LIBERIA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-002','CR-G', 'NICOYA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-003','CR-G', 'SANTA CRUZ');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-004','CR-G', 'BAGACES');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-005','CR-G', 'CARRILLO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-006','CR-G', 'CAÑAS');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-007','CR-G', 'ABANGARES');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-008','CR-G', 'TILARÁN');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-009','CR-G', 'NANDAYURE');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-010','CR-G', 'LA CRUZ');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-G-011','CR-G', 'HOJANCHA');

/*DISTRITOS PROVINCIA DE GUANACASTE*/
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-001-001','CR-G-001','LIBERIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-001-002','CR-G-001','CAÑAS DULCES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-001-003','CR-G-001','MAYORGA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-001-004','CR-G-001','NACASCOLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-001-005','CR-G-001','CURUBANDÉ' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-002-001','CR-G-002','NICOYA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-002-002','CR-G-002','MANSIÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-002-003','CR-G-002','SAN ANTONIO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-002-004','CR-G-002','QUEBRADA HONDA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-002-005','CR-G-002','SÁMARA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-002-006','CR-G-002','NOSARA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-002-007','CR-G-002','BELÉN DE NOSARITA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-003-001','CR-G-003','SANTA CRUZ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-003-002','CR-G-003','BOLSÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-003-003','CR-G-003','VEINTISIETE DE ABRIL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-003-004','CR-G-003','TEMPATE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-003-005','CR-G-003','CARTEGENA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-003-006','CR-G-003','CUAJINIQUIL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-003-007','CR-G-003','DIRIÁ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-003-008','CR-G-003','CABO VELAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-003-009','CR-G-003','TAMARINDO' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-004-001','CR-G-004','BAGACES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-004-002','CR-G-004','LA FORTUNA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-004-003','CR-G-004','MOGOTE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-004-004','CR-G-004','RÍO NARANJO' );


insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-005-001','CR-G-005','FILADELFIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-005-002','CR-G-005','PALMIRA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-005-003','CR-G-005','SARDINAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-005-004','CR-G-005','BELÉN' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-006-001','CR-G-006','CAÑAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-006-002','CR-G-006','PALMIRA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-006-003','CR-G-006','SAN MIGUEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-006-004','CR-G-006','BEBEDERO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-006-005','CR-G-006','POROZAL' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-007-001','CR-G-007','LAS JUNTAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-007-002','CR-G-007','SIERRA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-007-003','CR-G-007','SAN JUAN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-007-004','CR-G-007','COLORADO' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-008-001','CR-G-008','TILARÁN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-008-002','CR-G-008','QUEBRADA GRANDE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-008-003','CR-G-008','TRONADORA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-008-004','CR-G-008','SANTA ROSA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-008-005','CR-G-008','LÍBANO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-008-006','CR-G-008','TIERRAS MORENAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-008-007','CR-G-008','ARENAL' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-009-001','CR-G-009','CARMONA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-009-002','CR-G-009','SANTA RITA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-009-003','CR-G-009','ZAPOTAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-009-004','CR-G-009','SAN PABLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-009-005','CR-G-009','PORVENIR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-009-006','CR-G-009','BEJUCO' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-010-001','CR-G-010','LA CRUZ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-010-002','CR-G-010','SANTA CECILIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-010-003','CR-G-010','LA GARITA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-010-004','CR-G-010','SANTA ELENA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-011-001','CR-G-011','HOJANCHA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-011-002','CR-G-011','MONTE ROMO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-011-003','CR-G-011','PUERTO CARRILLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-011-004','CR-G-011','HUACAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-G-011-005','CR-G-011','MATAMBÚ' );



/*PROVIANCIA DE  PUNTARENAS*/
insert into tbprovincia(idProvincia,nombreProvincia) value ('CR-P','PUNTARENAS');
/*CANTONES PROVINCIA DE PUNTARENAS*/
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-001','CR-P', 'PUNTARENAS');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-002','CR-P', 'ESPARZA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-003','CR-P', 'BUENOS AIRES');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-004','CR-P', 'MONTES DE ORO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-005','CR-P', 'OSA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-006','CR-P', 'QUEPOS');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-007','CR-P', 'GOLFITO');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-008','CR-P', 'COTO BRUS');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-009','CR-P', 'PARRITA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-010','CR-P', 'CORREDORES');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-P-011','CR-P', 'GARABITO');


/*DISTRITOS PROVINCIA DE PUNTARENAS*/
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-001','CR-P-001','PUNTARENAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-002','CR-P-001','PITAHAYA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-003','CR-P-001','CHOMES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-004','CR-P-001','LEPANTO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-005','CR-P-001','PAQUERA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-006','CR-P-001','MANZANILLO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-007','CR-P-001','GUACIMAL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-008','CR-P-001','BARRANCA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-009','CR-P-001','MONTEVERDE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-010','CR-P-001','ISLA DEL COCO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-011','CR-P-001','CÓBANO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-012','CR-P-001','CHACARITA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-013','CR-P-001','CHIRA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-014','CR-P-001','ACAPULCO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-015','CR-P-001','EL ROBLE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-001-016','CR-P-001','ARANCIBIA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-002-001','CR-P-002','ESPÍRITU SANTO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-002-002','CR-P-002','SAN JUAN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-002-003','CR-P-002','MACACONA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-002-004','CR-P-002','SAN RAFAEL' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-002-005','CR-P-002','SAN JERÓNIMO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-002-006','CR-P-002','CALDERA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-003-001','CR-P-003','BUENOS ARIES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-003-002','CR-P-003','VOLCÁN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-003-003','CR-P-003','POTRERO GRANDE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-003-004','CR-P-003','BORUCA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-003-005','CR-P-003','PILAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-003-006','CR-P-003','COLINAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-003-007','CR-P-003','CHÁNGUENA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-003-008','CR-P-003','BIOLLEY' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-003-009','CR-P-003','BRUNKA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-004-001','CR-P-004','MIRAMAR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-004-002','CR-P-004','LA UNIÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-004-003','CR-P-004','SAN ISIDRO' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-005-001','CR-P-005','PUERTO CORTÉS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-005-002','CR-P-005','PALMAR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-005-003','CR-P-005','SIERPE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-005-004','CR-P-005','PIEDRAS BLANCAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-005-005','CR-P-005','BAHÍA BALLENA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-005-006','CR-P-005','BAHÍA DRAKE' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-006-001','CR-P-006','QUEPOS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-006-002','CR-P-006','SAVEGRE' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-006-003','CR-P-006','NARANJITO' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-007-001','CR-P-007','GOLFITO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-007-002','CR-P-007','PUERTO JIMÉNEZ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-007-003','CR-P-007','GUAYCARÁ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-007-004','CR-P-007','PAVÓN' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-008-001','CR-P-008','SAN VITO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-008-002','CR-P-008','SABILITO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-008-003','CR-P-008','AGUABUENA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-008-004','CR-P-008','LIMONCITO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-008-005','CR-P-008','PITIER' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-008-006','CR-P-008','GUTIÉRREZ BROWN' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-009-001','CR-P-009','PARRITA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-010-001','CR-P-010','CORRERDOR' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-010-002','CR-P-010','LA CUESTA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-010-003','CR-P-010','PASO CANOAS' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-010-004','CR-P-010','LAUREL' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-011-001','CR-P-011','JACÓ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-P-011-002','CR-P-011','TÁRCOLES' );


/*PROVIANCIA DE  LIMÓN*/

insert into tbprovincia(idProvincia,nombreProvincia) value ('CR-L','LIMÓN');

/*CANTONES PROVINCIA DE LIMÓN*/
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-L-001','CR-L', 'LIMÓN');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-L-002','CR-L', 'POCOCÍ');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-L-003','CR-L', 'SIQUIRRES');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-L-004','CR-L', 'TALAMANCA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-L-005','CR-L', 'MATINA');
insert into tbcanton (idCanton, idProvincia, nombreCanton) value ('CR-L-006','CR-L', 'GUÁCIMO');


/*DISTRITOS PROVINCIA DE LIMÓN*/

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-001-001','CR-L-001','LIMÓN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-001-002','CR-L-001','VALLE DE LA ESTRELLA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-001-003','CR-L-001','RÍO BLANCO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-001-004','CR-L-001','MATAMA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-002-001','CR-L-002','GUÁPILES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-002-002','CR-L-002','JIMÉNEZ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-002-003','CR-L-002','LA RITA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-002-004','CR-L-002','ROXANA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-002-005','CR-L-002','CARIARI' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-002-006','CR-L-002','COLORADO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-002-007','CR-L-002','LA COLINA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-003-001','CR-L-003','SIQUIRRES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-003-002','CR-L-003','PACUARITO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-003-003','CR-L-003','FLORIDA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-003-004','CR-L-003','GRMANIA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-003-005','CR-L-003','CAIRO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-003-006','CR-L-003','ALEGRÍA' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-004-001','CR-L-004','BRATSI' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-004-002','CR-L-004','SIXAOLA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-004-003','CR-L-004','CAHUITA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-004-004','CR-L-004','TELIRE' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-005-001','CR-L-005','MATINA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-005-002','CR-L-005','BATAÁN' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-005-003','CR-L-005','CARRANDÍ' );

insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-006-001','CR-L-006','GUÁCIMO' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-006-002','CR-L-006','MERCEDES' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-006-003','CR-L-006','POCORA' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-006-004','CR-L-006','RÍO JIMÉNEZ' );
insert into tbdistrito(idDistrito, idCanton, nombreDistrito) value('CR-L-006-005','CR-L-006','DUACARÍ' );

/*USUARIO ADMINISTRADOR PARA INFORMATICA*/

insert into tbusuario(login,contrasena,nombre,apellido1Usu,apellido2Usu,imagenUsu) value('aadmin','lbTwoyTtAYKZ/XV7NVhiXQ==','admin','admin','admin','LXhI5PrBYJDnqdVpOLXVjxaT0kkQsKf+tlS0QB3jDfFNBmqrUgFqVQ==' );

insert into tbpermisousuarioconsulta(idVentana,login,permisoConsultar) values('100','aadmin','SI');
insert into tbpermisousuarioconsulta(idVentana,login,permisoConsultar) values('101','aadmin','SI');
insert into tbpermisousuarioconsulta(idVentana,login,permisoConsultar) values('102','aadmin','SI');
insert into tbpermisousuarioconsulta(idVentana,login,permisoConsultar) values('103','aadmin','SI');
insert into tbpermisousuarioconsulta(idVentana,login,permisoConsultar) values('104','aadmin','SI');
insert into tbpermisousuarioconsulta(idVentana,login,permisoConsultar) values('105','aadmin','SI');
insert into tbpermisousuarioconsulta(idVentana,login,permisoConsultar) values('106','aadmin','SI');
insert into tbpermisousuarioconsulta(idVentana,login,permisoConsultar) values('107','aadmin','SI');
insert into tbpermisousuarioconsulta(idVentana,login,permisoConsultar) values('108','aadmin','SI');
insert into tbpermisousuarioconsulta(idVentana,login,permisoConsultar) values('109','aadmin','SI');

insert into tbpermisousuariomodulo(idVentana,login,permisoAgregar,permisoModificar) values('003','aadmin','SI','SI');
insert into tbpermisousuariomodulo(idVentana,login,permisoAgregar,permisoModificar) values('004','aadmin','SI','SI');
insert into tbpermisousuariomodulo(idVentana,login,permisoAgregar,permisoModificar) values('005','aadmin','SI','SI');
insert into tbpermisousuariomodulo(idVentana,login,permisoAgregar,permisoModificar) values('006','aadmin','SI','SI');
insert into tbpermisousuariomodulo(idVentana,login,permisoAgregar,permisoModificar) values('007','aadmin','SI','SI');
insert into tbpermisousuariomodulo(idVentana,login,permisoAgregar,permisoModificar) values('008','aadmin','SI','SI');



