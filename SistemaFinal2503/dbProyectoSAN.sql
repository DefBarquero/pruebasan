-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 25, 2017 at 01:32 PM
-- Server version: 5.1.33
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `dbproyectosan`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbabonos`
--

CREATE TABLE IF NOT EXISTS `tbabonos` (
  `idAbono` varchar(100) NOT NULL,
  `cedulaCli` varchar(20) NOT NULL,
  `montoCompra` varchar(100) NOT NULL,
  `fechaAbono` varchar(100) NOT NULL,
  `saldoActual` varchar(100) NOT NULL,
  `idFactura` varchar(100) NOT NULL,
  `montoAbonado` varchar(100) NOT NULL,
  PRIMARY KEY (`idAbono`,`cedulaCli`),
  KEY `cedulaCli` (`cedulaCli`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbabonos`
--


-- --------------------------------------------------------

--
-- Table structure for table `tbcanton`
--

CREATE TABLE IF NOT EXISTS `tbcanton` (
  `idCanton` varchar(100) NOT NULL,
  `idProvincia` varchar(100) NOT NULL,
  `nombreCanton` varchar(100) NOT NULL,
  PRIMARY KEY (`idCanton`),
  KEY `idProvincia` (`idProvincia`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbcanton`
--

INSERT INTO `tbcanton` (`idCanton`, `idProvincia`, `nombreCanton`) VALUES
('CR-SJ-001', 'CR-SJ', 'SAN JOSÉ'),
('CR-SJ-002', 'CR-SJ', 'ESCAZÚ'),
('CR-SJ-003', 'CR-SJ', 'DESAMPARADOS'),
('CR-SJ-004', 'CR-SJ', 'PURISCAL'),
('CR-SJ-005', 'CR-SJ', 'TARRAZÚ'),
('CR-SJ-006', 'CR-SJ', 'ASERRÍ'),
('CR-SJ-007', 'CR-SJ', 'MORA'),
('CR-SJ-008', 'CR-SJ', 'GOICHOECHEA'),
('CR-SJ-009', 'CR-SJ', 'SANTA ANA'),
('CR-SJ-010', 'CR-SJ', 'ALAJUELITA'),
('CR-SJ-011', 'CR-SJ', 'ACOSTA'),
('CR-SJ-012', 'CR-SJ', 'TIBÁS'),
('CR-SJ-013', 'CR-SJ', 'MORAVIA'),
('CR-SJ-014', 'CR-SJ', 'MONTES DE OCA'),
('CR-SJ-015', 'CR-SJ', 'TURRUBARES'),
('CR-SJ-016', 'CR-SJ', 'DOTA'),
('CR-SJ-017', 'CR-SJ', 'CURRIDABAT'),
('CR-SJ-018', 'CR-SJ', 'PERÉZ ZELEDÓN'),
('CR-SJ-019', 'CR-SJ', 'LEÓN CORTÉS'),
('CR-A-001', 'CR-A', 'ALAJUELA'),
('CR-A-002', 'CR-A', 'SAN RAMÓN'),
('CR-A-003', 'CR-A', 'GRECIA'),
('CR-A-004', 'CR-A', 'SAN MATEO'),
('CR-A-005', 'CR-A', 'ATENAS'),
('CR-A-006', 'CR-A', 'NARANJO'),
('CR-A-007', 'CR-A', 'PALMARES'),
('CR-A-008', 'CR-A', 'POÁS'),
('CR-A-009', 'CR-A', 'OROTINA'),
('CR-A-010', 'CR-A', 'SAN CARLOS'),
('CR-A-011', 'CR-A', 'ZARCERO'),
('CR-A-012', 'CR-A', 'VALVERDE VEGA'),
('CR-A-013', 'CR-A', 'UPALA'),
('CR-A-014', 'CR-A', 'LOS CHILES'),
('CR-A-015', 'CR-A', 'GUATUSO'),
('CR-C-001', 'CR-C', 'CARTAGO'),
('CR-C-002', 'CR-C', 'PARAÍSO'),
('CR-C-003', 'CR-C', 'LA UNIÓN'),
('CR-C-004', 'CR-C', 'JIMÉNEZ'),
('CR-C-005', 'CR-C', 'TURRIALBA'),
('CR-C-006', 'CR-C', 'ALVARADO'),
('CR-C-007', 'CR-C', 'OREAMUNO'),
('CR-C-008', 'CR-C', 'EL GUARCO'),
('CR-H-001', 'CR-H', 'HEREDIA'),
('CR-H-002', 'CR-H', 'BARVA'),
('CR-H-003', 'CR-H', 'SANTO DOMINGO'),
('CR-H-004', 'CR-H', 'SANTA BÁRBARA'),
('CR-H-005', 'CR-H', 'SAN RAFAEL'),
('CR-H-006', 'CR-H', 'SAN ISIDRO'),
('CR-H-007', 'CR-H', 'BELÉN'),
('CR-H-008', 'CR-H', 'FLORES'),
('CR-H-009', 'CR-H', 'SAN PABLO'),
('CR-H-010', 'CR-H', 'SARAPIQUÍ'),
('CR-G-001', 'CR-G', 'LIBERIA'),
('CR-G-002', 'CR-G', 'NICOYA'),
('CR-G-003', 'CR-G', 'SANTA CRUZ'),
('CR-G-004', 'CR-G', 'BAGACES'),
('CR-G-005', 'CR-G', 'CARRILLO'),
('CR-G-006', 'CR-G', 'CAÑAS'),
('CR-G-007', 'CR-G', 'ABANGARES'),
('CR-G-008', 'CR-G', 'TILARÁN'),
('CR-G-009', 'CR-G', 'NANDAYURE'),
('CR-G-010', 'CR-G', 'LA CRUZ'),
('CR-G-011', 'CR-G', 'HOJANCHA'),
('CR-P-001', 'CR-P', 'PUNTARENAS'),
('CR-P-002', 'CR-P', 'ESPARZA'),
('CR-P-003', 'CR-P', 'BUENOS AIRES'),
('CR-P-004', 'CR-P', 'MONTES DE ORO'),
('CR-P-005', 'CR-P', 'OSA'),
('CR-P-006', 'CR-P', 'QUEPOS'),
('CR-P-007', 'CR-P', 'GOLFITO'),
('CR-P-008', 'CR-P', 'COTO BRUS'),
('CR-P-009', 'CR-P', 'PARRITA'),
('CR-P-010', 'CR-P', 'CORREDORES'),
('CR-P-011', 'CR-P', 'GARABITO'),
('CR-L-001', 'CR-L', 'LIMÓN'),
('CR-L-002', 'CR-L', 'POCOCÍ'),
('CR-L-003', 'CR-L', 'SIQUIRRES'),
('CR-L-004', 'CR-L', 'TALAMANCA'),
('CR-L-005', 'CR-L', 'MATINA'),
('CR-L-006', 'CR-L', 'GUÁCIMO');

-- --------------------------------------------------------

--
-- Table structure for table `tbcliente`
--

CREATE TABLE IF NOT EXISTS `tbcliente` (
  `cedulaCli` varchar(20) NOT NULL,
  `nombrecli` varchar(20) NOT NULL,
  `apellido1Cli` varchar(20) NOT NULL,
  `apellido2Cli` varchar(20) NOT NULL,
  `provinciaCli` varchar(20) DEFAULT NULL,
  `cantonCli` varchar(20) DEFAULT NULL,
  `distritoCli` varchar(20) DEFAULT NULL,
  `otrasSenasCli` varchar(100) NOT NULL,
  `estadoCli` varchar(20) NOT NULL,
  `tipoCli` varchar(20) NOT NULL,
  `nomEmpresaCli` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cedulaCli`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbcliente`
--

INSERT INTO `tbcliente` (`cedulaCli`, `nombrecli`, `apellido1Cli`, `apellido2Cli`, `provinciaCli`, `cantonCli`, `distritoCli`, `otrasSenasCli`, `estadoCli`, `tipoCli`, `nomEmpresaCli`) VALUES
('0207280909', 'BRYAN', 'CASTILLO', 'CASTRO', 'CR-A', 'CR-A-007', 'CR-A-007-007', '350 METROS OESTE DE PASTAS VIENA', 'ACTIVO', 'PERSONA', ''),
('12345', 'BRY', 'CAS', 'CAS', 'CR-A', 'CR-A-001', 'CR-A-001-003', 'JNGJEJBGOJN', 'ACTIVO', 'PERSONA', ''),
('1985', 'JDGJ', 'GJBJELB', 'MVBJBE', 'CR-A', 'CR-A-007', 'CR-A-007-007', 'N,DBKJEV', 'ACTIVO', 'PERSONA', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbcorreocliente`
--

CREATE TABLE IF NOT EXISTS `tbcorreocliente` (
  `idCorreoCli` varchar(100) NOT NULL,
  `cedulaCli` varchar(20) NOT NULL,
  `correoCli` varchar(100) NOT NULL,
  PRIMARY KEY (`idCorreoCli`),
  KEY `cedulaCli` (`cedulaCli`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbcorreocliente`
--

INSERT INTO `tbcorreocliente` (`idCorreoCli`, `cedulaCli`, `correoCli`) VALUES
('5dd4c5e7-8ca5-11e6-8edc-46e092396860', '0207280909', 'brycascas@gmail.com'),
('aaffd6dc-e31b-11e6-9e5d-9abff1798b4e', '12345', 'jvjbwejbvo'),
('b3854030-e320-11e6-9e5d-9abff1798b4e', '1985', 'sdfghjk');

-- --------------------------------------------------------

--
-- Table structure for table `tbcorreoproveedor`
--

CREATE TABLE IF NOT EXISTS `tbcorreoproveedor` (
  `idCorreoPro` varchar(100) NOT NULL,
  `cedulaPro` varchar(20) NOT NULL,
  `correoPro` varchar(100) NOT NULL,
  PRIMARY KEY (`idCorreoPro`),
  KEY `cedulaPro` (`cedulaPro`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbcorreoproveedor`
--

INSERT INTO `tbcorreoproveedor` (`idCorreoPro`, `cedulaPro`, `correoPro`) VALUES
('fb57025e-8ca7-11e6-8edc-46e092396860', '0207140153', 'luis.quiros@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbdistrito`
--

CREATE TABLE IF NOT EXISTS `tbdistrito` (
  `idDistrito` varchar(100) NOT NULL,
  `idCanton` varchar(100) NOT NULL,
  `nombreDistrito` varchar(100) NOT NULL,
  PRIMARY KEY (`idDistrito`),
  KEY `idCanton` (`idCanton`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbdistrito`
--

INSERT INTO `tbdistrito` (`idDistrito`, `idCanton`, `nombreDistrito`) VALUES
('CR-SJ-001-001', 'CR-SJ-001', 'CARMEN'),
('CR-SJ-001-002', 'CR-SJ-001', 'MERCED'),
('CR-SJ-001-003', 'CR-SJ-001', 'HOSPITAL'),
('CR-SJ-001-004', 'CR-SJ-001', 'CATETRAL'),
('CR-SJ-001-005', 'CR-SJ-001', 'ZAPOTE'),
('CR-SJ-001-006', 'CR-SJ-001', 'SAN FRANCISCO DE DOS RÍOS'),
('CR-SJ-001-007', 'CR-SJ-001', 'LA URUCA'),
('CR-SJ-001-008', 'CR-SJ-001', 'MATA REDONDA'),
('CR-SJ-001-009', 'CR-SJ-001', 'PAVAS'),
('CR-SJ-001-010', 'CR-SJ-001', 'HATILLO'),
('CR-SJ-001-011', 'CR-SJ-001', 'SAN SEBASTIÁN'),
('CR-SJ-002-001', 'CR-SJ-002', 'ESCAZÚ CENTRO'),
('CR-SJ-002-002', 'CR-SJ-002', 'SAN RAFAEL'),
('CR-SJ-002-003', 'CR-SJ-002', 'SAN ANTONIO'),
('CR-SJ-003-001', 'CR-SJ-003', 'DESAMPARADOS'),
('CR-SJ-003-002', 'CR-SJ-003', 'SAN MIGUEL'),
('CR-SJ-003-003', 'CR-SJ-003', 'SAN JUAN DE DIOS'),
('CR-SJ-003-004', 'CR-SJ-003', 'SAN RAFAEL ARRIBA'),
('CR-SJ-003-005', 'CR-SJ-003', 'SAN ANTONIO'),
('CR-SJ-003-006', 'CR-SJ-003', 'FRAILES'),
('CR-SJ-003-007', 'CR-SJ-003', 'PATARRÁ'),
('CR-SJ-003-008', 'CR-SJ-003', 'SAN CRISTÓBAL'),
('CR-SJ-003-009', 'CR-SJ-003', 'ROSARIO'),
('CR-SJ-003-010', 'CR-SJ-003', 'DAMAS'),
('CR-SJ-003-011', 'CR-SJ-003', 'SAN RAFAEL ABAJO'),
('CR-SJ-003-012', 'CR-SJ-003', 'GRAVILIAS'),
('CR-SJ-003-013', 'CR-SJ-003', 'LOS GUIDOS'),
('CR-SJ-004-001', 'CR-SJ-004', 'SANTIAGO'),
('CR-SJ-004-002', 'CR-SJ-004', 'MERCEDES SUR'),
('CR-SJ-004-003', 'CR-SJ-004', 'BARBACOAS'),
('CR-SJ-004-004', 'CR-SJ-004', 'GRIFO ALTO'),
('CR-SJ-004-005', 'CR-SJ-004', 'SAN RAFAEL'),
('CR-SJ-004-006', 'CR-SJ-004', 'CANDELARITA'),
('CR-SJ-004-007', 'CR-SJ-004', 'DESAMPARADITOS'),
('CR-SJ-004-008', 'CR-SJ-004', 'SAN ANTONIO'),
('CR-SJ-004-009', 'CR-SJ-004', 'CHIRES'),
('CR-SJ-004-010', 'CR-SJ-004', 'LA CANGREJA'),
('CR-SJ-005-001', 'CR-SJ-005', 'SAN MARCOS'),
('CR-SJ-005-002', 'CR-SJ-005', 'SAN LORENZO'),
('CR-SJ-005-003', 'CR-SJ-005', 'SAN CARLOS'),
('CR-SJ-006-001', 'CR-SJ-006', 'ASERRÍ'),
('CR-SJ-006-002', 'CR-SJ-006', 'TARBACA'),
('CR-SJ-006-003', 'CR-SJ-006', 'VUELTA DE JORCO'),
('CR-SJ-006-004', 'CR-SJ-006', 'SAN GABRIEL'),
('CR-SJ-006-005', 'CR-SJ-006', 'LEGUA'),
('CR-SJ-006-006', 'CR-SJ-006', 'MONSTERREY'),
('CR-SJ-006-007', 'CR-SJ-006', 'SALITRILLOS'),
('CR-SJ-007-001', 'CR-SJ-007', 'COLÓN'),
('CR-SJ-007-002', 'CR-SJ-007', 'GUAYABO'),
('CR-SJ-007-003', 'CR-SJ-007', 'TABARCIA'),
('CR-SJ-007-004', 'CR-SJ-007', 'PIEDRAS NEGRAS'),
('CR-SJ-007-005', 'CR-SJ-007', 'PICAGRES'),
('CR-SJ-007-006', 'CR-SJ-007', 'JARIS'),
('CR-SJ-007-007', 'CR-SJ-007', 'QUITIRRISÍ'),
('CR-SJ-008-001', 'CR-SJ-008', 'GUADALUPE'),
('CR-SJ-008-002', 'CR-SJ-008', 'SAN FRANCISCO'),
('CR-SJ-008-003', 'CR-SJ-008', 'CALLE BLANCOS'),
('CR-SJ-008-004', 'CR-SJ-008', 'MATA DE PLATANO'),
('CR-SJ-008-005', 'CR-SJ-008', 'IPÍS'),
('CR-SJ-008-006', 'CR-SJ-008', 'RANCHO REDONDO'),
('CR-SJ-008-007', 'CR-SJ-008', 'PURRAL'),
('CR-SJ-009-001', 'CR-SJ-009', 'SANTA ANA'),
('CR-SJ-009-002', 'CR-SJ-009', 'SALITRAL'),
('CR-SJ-009-003', 'CR-SJ-009', 'POZOS'),
('CR-SJ-009-004', 'CR-SJ-009', 'URUCA'),
('CR-SJ-009-005', 'CR-SJ-009', 'PIEDADES'),
('CR-SJ-009-006', 'CR-SJ-009', 'BRASIL'),
('CR-SJ-010-001', 'CR-SJ-010', 'ALAJUELITA'),
('CR-SJ-010-002', 'CR-SJ-010', 'SAN JOSECITO'),
('CR-SJ-010-003', 'CR-SJ-010', 'SAN ANTONIO'),
('CR-SJ-010-004', 'CR-SJ-010', 'CONCEPCIÓN'),
('CR-SJ-010-005', 'CR-SJ-010', 'SAN FELIPE'),
('CR-SJ-011-001', 'CR-SJ-011', 'SAN IGNACIO'),
('CR-SJ-011-002', 'CR-SJ-011', 'GUAITIL'),
('CR-SJ-011-003', 'CR-SJ-011', 'PALMICHAL'),
('CR-SJ-011-004', 'CR-SJ-011', 'CANGREJAL'),
('CR-SJ-011-005', 'CR-SJ-011', 'SABANILLAS'),
('CR-SJ-012-001', 'CR-SJ-012', 'SAN JUAN DE TIBÁS'),
('CR-SJ-012-002', 'CR-SJ-012', 'CINCO ESQUINAS'),
('CR-SJ-012-003', 'CR-SJ-012', 'ANSELMO LLORENTE'),
('CR-SJ-012-004', 'CR-SJ-012', 'LEÓN XIII'),
('CR-SJ-012-005', 'CR-SJ-012', 'COLIMA'),
('CR-SJ-013-001', 'CR-SJ-013', 'SAN VICENTE'),
('CR-SJ-013-002', 'CR-SJ-013', 'SAN JERÓNIMO'),
('CR-SJ-013-003', 'CR-SJ-013', 'LA TRINIDAD'),
('CR-SJ-014-001', 'CR-SJ-014', 'SAN PEDRO'),
('CR-SJ-014-002', 'CR-SJ-014', 'SABANILLA'),
('CR-SJ-014-003', 'CR-SJ-014', 'MERCEDES'),
('CR-SJ-014-004', 'CR-SJ-014', 'SAN RAFAEL'),
('CR-SJ-015-001', 'CR-SJ-015', 'SAN PABLO'),
('CR-SJ-015-002', 'CR-SJ-015', 'SAN PEDRO'),
('CR-SJ-015-003', 'CR-SJ-015', 'SAN JUAN DE MATA'),
('CR-SJ-015-004', 'CR-SJ-015', 'SAN LUIS'),
('CR-SJ-015-005', 'CR-SJ-015', 'CARARA'),
('CR-SJ-016-001', 'CR-SJ-016', 'SANTA MARÍA'),
('CR-SJ-016-002', 'CR-SJ-016', 'JARDÍN'),
('CR-SJ-016-003', 'CR-SJ-016', 'COPEY'),
('CR-SJ-017-001', 'CR-SJ-017', 'CURRIDABAT'),
('CR-SJ-017-002', 'CR-SJ-017', 'GRANADILLA'),
('CR-SJ-017-003', 'CR-SJ-017', 'SÁNCHEZ'),
('CR-SJ-017-004', 'CR-SJ-017', 'TIRRASES'),
('CR-SJ-018-001', 'CR-SJ-018', 'SAN ISIDRO DE EL GENERAL'),
('CR-SJ-018-002', 'CR-SJ-018', 'EL GENERAL'),
('CR-SJ-018-003', 'CR-SJ-018', 'DANIEL FLORES'),
('CR-SJ-018-004', 'CR-SJ-018', 'RIVAS'),
('CR-SJ-018-005', 'CR-SJ-018', 'SAN PEDRO'),
('CR-SJ-018-006', 'CR-SJ-018', 'PLATANARES'),
('CR-SJ-018-007', 'CR-SJ-018', 'PEJIBAYE'),
('CR-SJ-018-008', 'CR-SJ-018', 'CAJÓN'),
('CR-SJ-018-009', 'CR-SJ-018', 'BARÚ'),
('CR-SJ-018-010', 'CR-SJ-018', 'RÍO NUEVO'),
('CR-SJ-018-011', 'CR-SJ-018', 'PÁRAMO'),
('CR-SJ-018-012', 'CR-SJ-018', 'LA AMISTDA'),
('CR-SJ-019-001', 'CR-SJ-019', 'SAN PABLO'),
('CR-SJ-019-002', 'CR-SJ-019', 'SAN ANDRÉS'),
('CR-SJ-019-003', 'CR-SJ-019', 'LLANO BONITO'),
('CR-SJ-019-004', 'CR-SJ-019', 'SAN ISIDRO'),
('CR-SJ-019-005', 'CR-SJ-019', 'SANTA CRUZ'),
('CR-SJ-019-006', 'CR-SJ-019', 'SAN ANTONIO'),
('CR-A-001-001', 'CR-A-001', 'ALAJUELA'),
('CR-A-001-002', 'CR-A-001', 'SAN JOSÉ'),
('CR-A-001-003', 'CR-A-001', 'CARRIZAL'),
('CR-A-001-004', 'CR-A-001', 'SAN ANTONIO'),
('CR-A-001-005', 'CR-A-001', 'GUÁCIMA'),
('CR-A-001-006', 'CR-A-001', 'SAN ISIDRO'),
('CR-A-001-007', 'CR-A-001', 'SABANILLA'),
('CR-A-001-008', 'CR-A-001', 'SAN RAFAEL'),
('CR-A-001-009', 'CR-A-001', 'RÍO SEGUNDO'),
('CR-A-001-010', 'CR-A-001', 'DESAMPARADOS'),
('CR-A-001-011', 'CR-A-001', 'TURRÚCARES'),
('CR-A-001-012', 'CR-A-001', 'TAMBOR'),
('CR-A-001-013', 'CR-A-001', 'GARITA'),
('CR-A-001-014', 'CR-A-001', 'SAN MIGUEL'),
('CR-A-002-001', 'CR-A-002', 'SAN RAMÓN'),
('CR-A-002-002', 'CR-A-002', 'SANTIAGO'),
('CR-A-002-003', 'CR-A-002', 'SAN JUAN'),
('CR-A-002-004', 'CR-A-002', 'PIEDADES NORTE'),
('CR-A-002-005', 'CR-A-002', 'PIEDADES SUR'),
('CR-A-002-006', 'CR-A-002', 'SAN RAFAEL'),
('CR-A-002-007', 'CR-A-002', 'SAN ISIDRO'),
('CR-A-002-008', 'CR-A-002', 'ÁNGELES'),
('CR-A-002-009', 'CR-A-002', 'ALFARO'),
('CR-A-002-010', 'CR-A-002', 'VOLIO'),
('CR-A-002-011', 'CR-A-002', 'CONCEPCIÓN'),
('CR-A-002-012', 'CR-A-002', 'ZAPOTAL'),
('CR-A-002-013', 'CR-A-002', 'PEÑAS BLANCAS'),
('CR-A-002-014', 'CR-A-002', 'LA PAZ'),
('CR-A-002-015', 'CR-A-002', 'LA ESPERANZA'),
('CR-A-002-016', 'CR-A-002', 'BAJO ZUÑOGA'),
('CR-A-003-001', 'CR-A-003', 'GRECIA'),
('CR-A-003-002', 'CR-A-003', 'SAN ISIDRO'),
('CR-A-003-003', 'CR-A-003', 'SAN JOSÉ'),
('CR-A-003-004', 'CR-A-003', 'SAN ROQUE'),
('CR-A-003-005', 'CR-A-003', 'TACARES'),
('CR-A-003-006', 'CR-A-003', 'RÍO CUARTO'),
('CR-A-003-007', 'CR-A-003', 'PUENTE DE PIEDRA'),
('CR-A-003-008', 'CR-A-003', 'BOLÍVAR'),
('CR-A-004-001', 'CR-A-004', 'SAN MATEO'),
('CR-A-004-002', 'CR-A-004', 'DESMONTE'),
('CR-A-004-003', 'CR-A-004', 'JESÚS MARÍA'),
('CR-A-004-004', 'CR-A-004', 'LABRADOR'),
('CR-A-005-001', 'CR-A-005', 'ATENAS'),
('CR-A-005-002', 'CR-A-005', 'JESÚS'),
('CR-A-005-003', 'CR-A-005', 'MERCEDES'),
('CR-A-005-004', 'CR-A-005', 'SAN ISIDRO'),
('CR-A-005-005', 'CR-A-005', 'CONCEPCIÓN'),
('CR-A-005-006', 'CR-A-005', 'SAN JOSÉ'),
('CR-A-005-007', 'CR-A-005', 'SANTA EULALIA'),
('CR-A-005-008', 'CR-A-005', 'ESCOBAL'),
('CR-A-006-001', 'CR-A-006', 'NARANJO'),
('CR-A-006-002', 'CR-A-006', 'SAN MIGUEL'),
('CR-A-006-003', 'CR-A-006', 'SAN JOSÉ'),
('CR-A-006-004', 'CR-A-006', 'CIRRÍ'),
('CR-A-006-005', 'CR-A-006', 'SAN JERÓNIMO'),
('CR-A-006-006', 'CR-A-006', 'SAN JUAN'),
('CR-A-006-007', 'CR-A-006', 'ROSARIO'),
('CR-A-006-008', 'CR-A-006', 'PALMITOS'),
('CR-A-007-001', 'CR-A-007', 'PALMARES'),
('CR-A-007-002', 'CR-A-007', 'ZARAGOZA'),
('CR-A-007-003', 'CR-A-007', 'BUENOS AIRES'),
('CR-A-007-004', 'CR-A-007', 'SANTIAGO'),
('CR-A-007-005', 'CR-A-007', 'CANDELARIA'),
('CR-A-007-006', 'CR-A-007', 'ESQUIPULAS'),
('CR-A-007-007', 'CR-A-007', 'LA GRANJA'),
('CR-A-008-001', 'CR-A-008', 'SAN PEDRO'),
('CR-A-008-002', 'CR-A-008', 'SAN JUAN'),
('CR-A-008-003', 'CR-A-008', 'SAN RAFAEL'),
('CR-A-008-004', 'CR-A-008', 'CARRILLO'),
('CR-A-008-005', 'CR-A-008', 'SABANA REDONDA'),
('CR-A-009-001', 'CR-A-009', 'OROTINA'),
('CR-A-009-002', 'CR-A-009', 'EL MASTATE'),
('CR-A-009-003', 'CR-A-009', 'HACIENDA VIEJA'),
('CR-A-009-004', 'CR-A-009', 'EL COYOLAR'),
('CR-A-009-005', 'CR-A-009', 'LA CEIBA'),
('CR-A-010-001', 'CR-A-010', 'QUESADA'),
('CR-A-010-002', 'CR-A-010', 'FLORENCIA'),
('CR-A-010-003', 'CR-A-010', 'BUENAVISTA'),
('CR-A-010-004', 'CR-A-010', 'AGUAS ZARCAS'),
('CR-A-010-005', 'CR-A-010', 'VENECIA'),
('CR-A-010-006', 'CR-A-010', 'PITAL'),
('CR-A-010-007', 'CR-A-010', 'LA FORTUNA'),
('CR-A-010-008', 'CR-A-010', 'LA TIGRA'),
('CR-A-010-009', 'CR-A-010', 'PALMERA'),
('CR-A-010-010', 'CR-A-010', 'VENADO'),
('CR-A-010-011', 'CR-A-010', 'CUTRIS'),
('CR-A-010-012', 'CR-A-010', 'MONTERREY'),
('CR-A-010-013', 'CR-A-010', 'POCOSOL'),
('CR-A-011-001', 'CR-A-011', 'ZARCERO'),
('CR-A-011-002', 'CR-A-011', 'LAGUNA'),
('CR-A-011-003', 'CR-A-011', 'TAPEZCO'),
('CR-A-011-004', 'CR-A-011', 'GUADALUPE'),
('CR-A-011-005', 'CR-A-011', 'PALMIRA'),
('CR-A-011-006', 'CR-A-011', 'ZAPOTE'),
('CR-A-011-007', 'CR-A-011', 'BRISAS'),
('CR-A-012-001', 'CR-A-012', 'SARCHÍ NORTE'),
('CR-A-012-002', 'CR-A-012', 'SARCHÍ SUR'),
('CR-A-012-003', 'CR-A-012', 'TORO AMARILLO'),
('CR-A-012-004', 'CR-A-012', 'SAN PEDRO'),
('CR-A-012-005', 'CR-A-012', 'RODRÍGUEZ'),
('CR-A-013-001', 'CR-A-013', 'UPALA'),
('CR-A-013-002', 'CR-A-013', 'AGUAS CLARAS'),
('CR-A-013-003', 'CR-A-013', 'SAN JOSÉ'),
('CR-A-013-004', 'CR-A-013', 'BIJAGUA'),
('CR-A-013-005', 'CR-A-013', 'DELICIAS'),
('CR-A-013-006', 'CR-A-013', 'DOS RÍOS'),
('CR-A-013-007', 'CR-A-013', 'YOLILLAL'),
('CR-A-013-008', 'CR-A-013', 'CANALETE'),
('CR-A-014-001', 'CR-A-014', 'LOS CHILES'),
('CR-A-014-002', 'CR-A-014', 'CAÑO NEGRO'),
('CR-A-014-003', 'CR-A-014', 'EL AMPARO'),
('CR-A-014-004', 'CR-A-014', 'SAN JORGE'),
('CR-A-015-001', 'CR-A-015', 'SAN RAFAEL'),
('CR-A-015-002', 'CR-A-015', 'BUENAVISTA'),
('CR-A-015-003', 'CR-A-015', 'COTE'),
('CR-A-015-004', 'CR-A-015', 'KATIRA'),
('CR-C-001-001', 'CR-C-001', 'ORIENTAL'),
('CR-C-001-002', 'CR-C-001', 'OCCIDENTAL'),
('CR-C-001-003', 'CR-C-001', 'CARMEN'),
('CR-C-001-004', 'CR-C-001', 'SAN NICOLÁS'),
('CR-C-001-005', 'CR-C-001', 'AGUA CALIENTE'),
('CR-C-001-006', 'CR-C-001', 'GUADALUPE'),
('CR-C-001-007', 'CR-C-001', 'CORRALILLO'),
('CR-C-001-008', 'CR-C-001', 'TIERRAS BLANCAS'),
('CR-C-001-009', 'CR-C-001', 'DULCE NOMBRE'),
('CR-C-001-010', 'CR-C-001', 'LLANO GRANDE'),
('CR-C-001-011', 'CR-C-001', 'QUEBRADILLA'),
('CR-C-002-001', 'CR-C-002', 'PARAÍSO'),
('CR-C-002-002', 'CR-C-002', 'OROSI'),
('CR-C-002-003', 'CR-C-002', 'CACHÍ'),
('CR-C-002-004', 'CR-C-002', 'SANTIAGO'),
('CR-C-002-005', 'CR-C-002', 'LLANOS DE SANTA LUCÍA'),
('CR-C-003-001', 'CR-C-003', 'TRES RÍOS'),
('CR-C-003-002', 'CR-C-003', 'SAN DIEGO'),
('CR-C-003-003', 'CR-C-003', 'SAN JUAN'),
('CR-C-003-004', 'CR-C-003', 'SAN RAFAEL'),
('CR-C-003-005', 'CR-C-003', 'CONCEPCIÓN'),
('CR-C-003-006', 'CR-C-003', 'DULCE NOMBRE'),
('CR-C-003-007', 'CR-C-003', 'SAN RAMÓN'),
('CR-C-003-008', 'CR-C-003', 'RÍO AZUL'),
('CR-C-004-001', 'CR-C-004', 'JUAN VIÑA'),
('CR-C-004-002', 'CR-C-004', 'TUCURRIQUE'),
('CR-C-004-003', 'CR-C-004', 'PEJIBAYE'),
('CR-C-005-001', 'CR-C-005', 'TURRIALBA'),
('CR-C-005-002', 'CR-C-005', 'LA SUIZA'),
('CR-C-005-003', 'CR-C-005', 'PERALTA'),
('CR-C-005-004', 'CR-C-005', 'SANTA CRUZ'),
('CR-C-005-005', 'CR-C-005', 'SANTA TERESITA'),
('CR-C-005-006', 'CR-C-005', 'PAVONES'),
('CR-C-005-007', 'CR-C-005', 'TUIS'),
('CR-C-005-008', 'CR-C-005', 'TAYUTIC'),
('CR-C-005-009', 'CR-C-005', 'SANTA ROSA'),
('CR-C-005-010', 'CR-C-005', 'TRES EQUIS'),
('CR-C-005-011', 'CR-C-005', 'LA ISABEL'),
('CR-C-005-012', 'CR-C-005', 'CHIRRIPÓ'),
('CR-C-006-001', 'CR-C-006', 'PACAYAS'),
('CR-C-006-002', 'CR-C-006', 'CERVANTES'),
('CR-C-006-003', 'CR-C-006', 'CAPELLADES'),
('CR-C-007-001', 'CR-C-007', 'SAN RAFAEL'),
('CR-C-007-002', 'CR-C-007', 'COT'),
('CR-C-007-003', 'CR-C-007', 'POTRERO CERRADO'),
('CR-C-007-004', 'CR-C-007', 'CIPRESES'),
('CR-C-007-005', 'CR-C-007', 'SANTA ROSA'),
('CR-C-008-001', 'CR-C-008', 'TEJAR'),
('CR-C-008-002', 'CR-C-008', 'SAN ISIDRO'),
('CR-C-008-003', 'CR-C-008', 'TOBOSI'),
('CR-C-008-004', 'CR-C-008', 'PATIO DE AGUA'),
('CR-H-001-001', 'CR-H-001', 'HEREDIA'),
('CR-H-001-002', 'CR-H-001', 'MERCEDES'),
('CR-H-001-003', 'CR-H-001', 'SAN FRANCISCO'),
('CR-H-001-004', 'CR-H-001', 'ULLOA'),
('CR-H-001-005', 'CR-H-001', 'VARABLANCA'),
('CR-H-002-001', 'CR-H-002', 'BARVA'),
('CR-H-002-002', 'CR-H-002', 'SAN PEDRO'),
('CR-H-002-003', 'CR-H-002', 'SAN PABLO'),
('CR-H-002-004', 'CR-H-002', 'SAN ROQUE'),
('CR-H-002-005', 'CR-H-002', 'SANTA LUCÍA'),
('CR-H-002-006', 'CR-H-002', 'SAN JOSÉ DE LA MONTAÑA'),
('CR-H-003-001', 'CR-H-003', 'SANTO DOMINDO'),
('CR-H-003-002', 'CR-H-003', 'SAN VICENTE'),
('CR-H-003-003', 'CR-H-003', 'SAN MIGUEL'),
('CR-H-003-004', 'CR-H-003', 'PARACITO'),
('CR-H-003-005', 'CR-H-003', 'SAN TOMÁS'),
('CR-H-003-006', 'CR-H-003', 'SANTA ROSA'),
('CR-H-003-007', 'CR-H-003', 'TURES'),
('CR-H-003-008', 'CR-H-003', 'PARÁ'),
('CR-H-004-001', 'CR-H-004', 'SANTA BÁRBARA'),
('CR-H-004-002', 'CR-H-004', 'SAN PEDRO'),
('CR-H-004-003', 'CR-H-004', 'SAN JUAN'),
('CR-H-004-004', 'CR-H-004', 'JESÚS'),
('CR-H-004-005', 'CR-H-004', 'SANTO DOMINGO'),
('CR-H-004-006', 'CR-H-004', 'PURABA'),
('CR-H-005-001', 'CR-H-005', 'SAN RAFAEL'),
('CR-H-005-002', 'CR-H-005', 'SAN JOSECITO'),
('CR-H-005-003', 'CR-H-005', 'SANTIAGO'),
('CR-H-005-004', 'CR-H-005', 'LOS ÁNGELES'),
('CR-H-005-005', 'CR-H-005', 'CONCEPCIÓN'),
('CR-H-006-001', 'CR-H-006', 'SAN ISIDRO'),
('CR-H-006-002', 'CR-H-006', 'SAN JOSÉ'),
('CR-H-006-003', 'CR-H-006', 'CONCEPCIÓN'),
('CR-H-006-004', 'CR-H-006', 'SAN FRANCISCO'),
('CR-H-007-001', 'CR-H-007', 'SAN ANTONIO'),
('CR-H-007-002', 'CR-H-007', 'LA RIBERA'),
('CR-H-007-003', 'CR-H-007', 'LA ASUSNCIÓN'),
('CR-H-008-001', 'CR-H-008', 'SAN JOAQUÍN'),
('CR-H-008-002', 'CR-H-008', 'BARRANTES'),
('CR-H-008-003', 'CR-H-008', 'LLORENTE'),
('CR-H-009-001', 'CR-H-009', 'SAN PABLO'),
('CR-H-009-002', 'CR-H-009', 'RINCÓN DE SABANILLA'),
('CR-H-010-001', 'CR-H-010', 'PUERTO VIEJO'),
('CR-H-010-002', 'CR-H-010', 'LA VIRGEN'),
('CR-H-010-003', 'CR-H-010', 'LAS HORQUESTAS'),
('CR-H-010-004', 'CR-H-010', 'LLANURAS DEL GASPAR'),
('CR-H-010-005', 'CR-H-010', 'CUREÑA'),
('CR-G-001-001', 'CR-G-001', 'LIBERIA'),
('CR-G-001-002', 'CR-G-001', 'CAÑAS DULCES'),
('CR-G-001-003', 'CR-G-001', 'MAYORGA'),
('CR-G-001-004', 'CR-G-001', 'NACASCOLO'),
('CR-G-001-005', 'CR-G-001', 'CURUBANDÉ'),
('CR-G-002-001', 'CR-G-002', 'NICOYA'),
('CR-G-002-002', 'CR-G-002', 'MANSIÓN'),
('CR-G-002-003', 'CR-G-002', 'SAN ANTONIO'),
('CR-G-002-004', 'CR-G-002', 'QUEBRADA HONDA'),
('CR-G-002-005', 'CR-G-002', 'SÁMARA'),
('CR-G-002-006', 'CR-G-002', 'NOSARA'),
('CR-G-002-007', 'CR-G-002', 'BELÉN DE NOSARITA'),
('CR-G-003-001', 'CR-G-003', 'SANTA CRUZ'),
('CR-G-003-002', 'CR-G-003', 'BOLSÓN'),
('CR-G-003-003', 'CR-G-003', 'VEINTISIETE DE ABRIL'),
('CR-G-003-004', 'CR-G-003', 'TEMPATE'),
('CR-G-003-005', 'CR-G-003', 'CARTEGENA'),
('CR-G-003-006', 'CR-G-003', 'CUAJINIQUIL'),
('CR-G-003-007', 'CR-G-003', 'DIRIÁ'),
('CR-G-003-008', 'CR-G-003', 'CABO VELAS'),
('CR-G-003-009', 'CR-G-003', 'TAMARINDO'),
('CR-G-004-001', 'CR-G-004', 'BAGACES'),
('CR-G-004-002', 'CR-G-004', 'LA FORTUNA'),
('CR-G-004-003', 'CR-G-004', 'MOGOTE'),
('CR-G-004-004', 'CR-G-004', 'RÍO NARANJO'),
('CR-G-005-001', 'CR-G-005', 'FILADELFIA'),
('CR-G-005-002', 'CR-G-005', 'PALMIRA'),
('CR-G-005-003', 'CR-G-005', 'SARDINAL'),
('CR-G-005-004', 'CR-G-005', 'BELÉN'),
('CR-G-006-001', 'CR-G-006', 'CAÑAS'),
('CR-G-006-002', 'CR-G-006', 'PALMIRA'),
('CR-G-006-003', 'CR-G-006', 'SAN MIGUEL'),
('CR-G-006-004', 'CR-G-006', 'BEBEDERO'),
('CR-G-006-005', 'CR-G-006', 'POROZAL'),
('CR-G-007-001', 'CR-G-007', 'LAS JUNTAS'),
('CR-G-007-002', 'CR-G-007', 'SIERRA'),
('CR-G-007-003', 'CR-G-007', 'SAN JUAN'),
('CR-G-007-004', 'CR-G-007', 'COLORADO'),
('CR-G-008-001', 'CR-G-008', 'TILARÁN'),
('CR-G-008-002', 'CR-G-008', 'QUEBRADA GRANDE'),
('CR-G-008-003', 'CR-G-008', 'TRONADORA'),
('CR-G-008-004', 'CR-G-008', 'SANTA ROSA'),
('CR-G-008-005', 'CR-G-008', 'LÍBANO'),
('CR-G-008-006', 'CR-G-008', 'TIERRAS MORENAS'),
('CR-G-008-007', 'CR-G-008', 'ARENAL'),
('CR-G-009-001', 'CR-G-009', 'CARMONA'),
('CR-G-009-002', 'CR-G-009', 'SANTA RITA'),
('CR-G-009-003', 'CR-G-009', 'ZAPOTAL'),
('CR-G-009-004', 'CR-G-009', 'SAN PABLO'),
('CR-G-009-005', 'CR-G-009', 'PORVENIR'),
('CR-G-009-006', 'CR-G-009', 'BEJUCO'),
('CR-G-010-001', 'CR-G-010', 'LA CRUZ'),
('CR-G-010-002', 'CR-G-010', 'SANTA CECILIA'),
('CR-G-010-003', 'CR-G-010', 'LA GARITA'),
('CR-G-010-004', 'CR-G-010', 'SANTA ELENA'),
('CR-G-011-001', 'CR-G-011', 'HOJANCHA'),
('CR-G-011-002', 'CR-G-011', 'MONTE ROMO'),
('CR-G-011-003', 'CR-G-011', 'PUERTO CARRILLO'),
('CR-G-011-004', 'CR-G-011', 'HUACAS'),
('CR-G-011-005', 'CR-G-011', 'MATAMBÚ'),
('CR-P-001-001', 'CR-P-001', 'PUNTARENAS'),
('CR-P-001-002', 'CR-P-001', 'PITAHAYA'),
('CR-P-001-003', 'CR-P-001', 'CHOMES'),
('CR-P-001-004', 'CR-P-001', 'LEPANTO'),
('CR-P-001-005', 'CR-P-001', 'PAQUERA'),
('CR-P-001-006', 'CR-P-001', 'MANZANILLO'),
('CR-P-001-007', 'CR-P-001', 'GUACIMAL'),
('CR-P-001-008', 'CR-P-001', 'BARRANCA'),
('CR-P-001-009', 'CR-P-001', 'MONTEVERDE'),
('CR-P-001-010', 'CR-P-001', 'ISLA DEL COCO'),
('CR-P-001-011', 'CR-P-001', 'CÓBANO'),
('CR-P-001-012', 'CR-P-001', 'CHACARITA'),
('CR-P-001-013', 'CR-P-001', 'CHIRA'),
('CR-P-001-014', 'CR-P-001', 'ACAPULCO'),
('CR-P-001-015', 'CR-P-001', 'EL ROBLE'),
('CR-P-001-016', 'CR-P-001', 'ARANCIBIA'),
('CR-P-002-001', 'CR-P-002', 'ESPÍRITU SANTO'),
('CR-P-002-002', 'CR-P-002', 'SAN JUAN'),
('CR-P-002-003', 'CR-P-002', 'MACACONA'),
('CR-P-002-004', 'CR-P-002', 'SAN RAFAEL'),
('CR-P-002-005', 'CR-P-002', 'SAN JERÓNIMO'),
('CR-P-002-006', 'CR-P-002', 'CALDERA'),
('CR-P-003-001', 'CR-P-003', 'BUENOS ARIES'),
('CR-P-003-002', 'CR-P-003', 'VOLCÁN'),
('CR-P-003-003', 'CR-P-003', 'POTRERO GRANDE'),
('CR-P-003-004', 'CR-P-003', 'BORUCA'),
('CR-P-003-005', 'CR-P-003', 'PILAS'),
('CR-P-003-006', 'CR-P-003', 'COLINAS'),
('CR-P-003-007', 'CR-P-003', 'CHÁNGUENA'),
('CR-P-003-008', 'CR-P-003', 'BIOLLEY'),
('CR-P-003-009', 'CR-P-003', 'BRUNKA'),
('CR-P-004-001', 'CR-P-004', 'MIRAMAR'),
('CR-P-004-002', 'CR-P-004', 'LA UNIÓN'),
('CR-P-004-003', 'CR-P-004', 'SAN ISIDRO'),
('CR-P-005-001', 'CR-P-005', 'PUERTO CORTÉS'),
('CR-P-005-002', 'CR-P-005', 'PALMAR'),
('CR-P-005-003', 'CR-P-005', 'SIERPE'),
('CR-P-005-004', 'CR-P-005', 'PIEDRAS BLANCAS'),
('CR-P-005-005', 'CR-P-005', 'BAHÍA BALLENA'),
('CR-P-005-006', 'CR-P-005', 'BAHÍA DRAKE'),
('CR-P-006-001', 'CR-P-006', 'QUEPOS'),
('CR-P-006-002', 'CR-P-006', 'SAVEGRE'),
('CR-P-006-003', 'CR-P-006', 'NARANJITO'),
('CR-P-007-001', 'CR-P-007', 'GOLFITO'),
('CR-P-007-002', 'CR-P-007', 'PUERTO JIMÉNEZ'),
('CR-P-007-003', 'CR-P-007', 'GUAYCARÁ'),
('CR-P-007-004', 'CR-P-007', 'PAVÓN'),
('CR-P-008-001', 'CR-P-008', 'SAN VITO'),
('CR-P-008-002', 'CR-P-008', 'SABILITO'),
('CR-P-008-003', 'CR-P-008', 'AGUABUENA'),
('CR-P-008-004', 'CR-P-008', 'LIMONCITO'),
('CR-P-008-005', 'CR-P-008', 'PITIER'),
('CR-P-008-006', 'CR-P-008', 'GUTIÉRREZ BROWN'),
('CR-P-009-001', 'CR-P-009', 'PARRITA'),
('CR-P-010-001', 'CR-P-010', 'CORRERDOR'),
('CR-P-010-002', 'CR-P-010', 'LA CUESTA'),
('CR-P-010-003', 'CR-P-010', 'PASO CANOAS'),
('CR-P-010-004', 'CR-P-010', 'LAUREL'),
('CR-P-011-001', 'CR-P-011', 'JACÓ'),
('CR-P-011-002', 'CR-P-011', 'TÁRCOLES'),
('CR-L-001-001', 'CR-L-001', 'LIMÓN'),
('CR-L-001-002', 'CR-L-001', 'VALLE DE LA ESTRELLA'),
('CR-L-001-003', 'CR-L-001', 'RÍO BLANCO'),
('CR-L-001-004', 'CR-L-001', 'MATAMA'),
('CR-L-002-001', 'CR-L-002', 'GUÁPILES'),
('CR-L-002-002', 'CR-L-002', 'JIMÉNEZ'),
('CR-L-002-003', 'CR-L-002', 'LA RITA'),
('CR-L-002-004', 'CR-L-002', 'ROXANA'),
('CR-L-002-005', 'CR-L-002', 'CARIARI'),
('CR-L-002-006', 'CR-L-002', 'COLORADO'),
('CR-L-002-007', 'CR-L-002', 'LA COLINA'),
('CR-L-003-001', 'CR-L-003', 'SIQUIRRES'),
('CR-L-003-002', 'CR-L-003', 'PACUARITO'),
('CR-L-003-003', 'CR-L-003', 'FLORIDA'),
('CR-L-003-004', 'CR-L-003', 'GRMANIA'),
('CR-L-003-005', 'CR-L-003', 'CAIRO'),
('CR-L-003-006', 'CR-L-003', 'ALEGRÍA'),
('CR-L-004-001', 'CR-L-004', 'BRATSI'),
('CR-L-004-002', 'CR-L-004', 'SIXAOLA'),
('CR-L-004-003', 'CR-L-004', 'CAHUITA'),
('CR-L-004-004', 'CR-L-004', 'TELIRE'),
('CR-L-005-001', 'CR-L-005', 'MATINA'),
('CR-L-005-002', 'CR-L-005', 'BATAÁN'),
('CR-L-005-003', 'CR-L-005', 'CARRANDÍ'),
('CR-L-006-001', 'CR-L-006', 'GUÁCIMO'),
('CR-L-006-002', 'CR-L-006', 'MERCEDES'),
('CR-L-006-003', 'CR-L-006', 'POCORA'),
('CR-L-006-004', 'CR-L-006', 'RÍO JIMÉNEZ'),
('CR-L-006-005', 'CR-L-006', 'DUACARÍ');

-- --------------------------------------------------------

--
-- Table structure for table `tbfacturacostodet`
--

CREATE TABLE IF NOT EXISTS `tbfacturacostodet` (
  `idFactura` varchar(20) NOT NULL,
  `idProducto` varchar(20) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  PRIMARY KEY (`idFactura`,`idProducto`),
  KEY `idProducto` (`idProducto`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbfacturacostodet`
--

INSERT INTO `tbfacturacostodet` (`idFactura`, `idProducto`, `cantidad`, `subtotal`) VALUES
('20170125114010', '1224', 2, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `tbfacturacostoenc`
--

CREATE TABLE IF NOT EXISTS `tbfacturacostoenc` (
  `idFactura` varchar(20) NOT NULL,
  `cedulaCli` varchar(20) NOT NULL,
  `login` varchar(100) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `fechaFactura` varchar(100) NOT NULL,
  `talonario` int(11) DEFAULT NULL,
  PRIMARY KEY (`idFactura`),
  KEY `cedulaCli` (`cedulaCli`),
  KEY `login` (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbfacturacostoenc`
--

INSERT INTO `tbfacturacostoenc` (`idFactura`, `cedulaCli`, `login`, `total`, `fechaFactura`, `talonario`) VALUES
('20170125114010', '0207280909', 'aadmin', 2400.00, '25/01/2017', 2345);

-- --------------------------------------------------------

--
-- Table structure for table `tbfacturadet`
--

CREATE TABLE IF NOT EXISTS `tbfacturadet` (
  `idFactura` varchar(20) NOT NULL,
  `idProducto` varchar(20) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  PRIMARY KEY (`idFactura`,`idProducto`),
  KEY `idProducto` (`idProducto`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbfacturadet`
--

INSERT INTO `tbfacturadet` (`idFactura`, `idProducto`, `cantidad`, `subtotal`) VALUES
('20170125114010', '1224', 2, 2938.00);

-- --------------------------------------------------------

--
-- Table structure for table `tbfacturaenc`
--

CREATE TABLE IF NOT EXISTS `tbfacturaenc` (
  `idFactura` varchar(20) NOT NULL,
  `cedulaCli` varchar(20) NOT NULL,
  `login` varchar(100) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `fechaFactura` varchar(100) NOT NULL,
  `talonario` int(11) DEFAULT NULL,
  `tipoPago` varchar(100) NOT NULL,
  `iva` int(10) NOT NULL,
  PRIMARY KEY (`idFactura`),
  KEY `cedulaCli` (`cedulaCli`),
  KEY `login` (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbfacturaenc`
--

INSERT INTO `tbfacturaenc` (`idFactura`, `cedulaCli`, `login`, `total`, `fechaFactura`, `talonario`, `tipoPago`, `iva`) VALUES
('20170125114010', '0207280909', 'aadmin', 2938.00, '25/01/2017', 2345, 'Credito', 338);

-- --------------------------------------------------------

--
-- Table structure for table `tbpermisousuarioconsulta`
--

CREATE TABLE IF NOT EXISTS `tbpermisousuarioconsulta` (
  `idVentana` varchar(20) NOT NULL,
  `login` varchar(100) NOT NULL,
  `permisoConsultar` varchar(20) NOT NULL,
  PRIMARY KEY (`idVentana`,`login`),
  KEY `login` (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbpermisousuarioconsulta`
--

INSERT INTO `tbpermisousuarioconsulta` (`idVentana`, `login`, `permisoConsultar`) VALUES
('100', 'aadmin', 'SI'),
('101', 'aadmin', 'SI'),
('102', 'aadmin', 'SI'),
('103', 'aadmin', 'SI'),
('104', 'aadmin', 'SI'),
('105', 'aadmin', 'SI'),
('106', 'aadmin', 'SI'),
('107', 'aadmin', 'SI'),
('108', 'aadmin', 'SI'),
('109', 'aadmin', 'SI');

-- --------------------------------------------------------

--
-- Table structure for table `tbpermisousuariomodulo`
--

CREATE TABLE IF NOT EXISTS `tbpermisousuariomodulo` (
  `idVentana` varchar(20) NOT NULL,
  `login` varchar(100) NOT NULL,
  `permisoAgregar` varchar(20) NOT NULL,
  `permisoModificar` varchar(20) NOT NULL,
  PRIMARY KEY (`idVentana`,`login`),
  KEY `login` (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbpermisousuariomodulo`
--

INSERT INTO `tbpermisousuariomodulo` (`idVentana`, `login`, `permisoAgregar`, `permisoModificar`) VALUES
('003', 'aadmin', 'SI', 'SI'),
('004', 'aadmin', 'SI', 'SI'),
('005', 'aadmin', 'SI', 'SI'),
('006', 'aadmin', 'SI', 'SI'),
('007', 'aadmin', 'SI', 'SI'),
('008', 'aadmin', 'SI', 'SI');

-- --------------------------------------------------------

--
-- Table structure for table `tbproductoservicio`
--

CREATE TABLE IF NOT EXISTS `tbproductoservicio` (
  `idProducto_Servicio` varchar(20) NOT NULL,
  `nombrePro_Ser` varchar(100) NOT NULL,
  `descripcionPro_Ser` varchar(100) NOT NULL,
  `costoPro_Ser` int(11) DEFAULT NULL,
  `precioPro_Ser` int(11) NOT NULL,
  `imagenPro_Ser` varchar(1000) DEFAULT NULL,
  `tipoPro_Ser` varchar(100) NOT NULL,
  PRIMARY KEY (`idProducto_Servicio`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbproductoservicio`
--

INSERT INTO `tbproductoservicio` (`idProducto_Servicio`, `nombrePro_Ser`, `descripcionPro_Ser`, `costoPro_Ser`, `precioPro_Ser`, `imagenPro_Ser`, `tipoPro_Ser`) VALUES
('001', 'Papa', 'Es una papa', 100, 120, '7qtB4N+aV6M=', 'PRODUCTO'),
('1224', 'Arroz', 'ksngdkbng', 1200, 1300, '7qtB4N+aV6M=', 'PRODUCTO'),
('6789', 'lokl', 'jdhgjldvn', 300, 400, '7qtB4N+aV6M=', 'PRODUCTO');

-- --------------------------------------------------------

--
-- Table structure for table `tbproveedor`
--

CREATE TABLE IF NOT EXISTS `tbproveedor` (
  `cedulaPro` varchar(20) NOT NULL,
  `tipoPro` varchar(100) NOT NULL,
  `nombrePro` varchar(100) NOT NULL,
  `apellido1Pro` varchar(100) NOT NULL,
  `apellido2Pro` varchar(100) NOT NULL,
  `provinciaPro` varchar(100) DEFAULT NULL,
  `cantonPro` varchar(100) DEFAULT NULL,
  `distritoPro` varchar(100) DEFAULT NULL,
  `otrasSenasPro` varchar(100) NOT NULL,
  `descripcionPro` varchar(255) NOT NULL,
  `nomEmpresaPro` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`cedulaPro`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbproveedor`
--

INSERT INTO `tbproveedor` (`cedulaPro`, `tipoPro`, `nombrePro`, `apellido1Pro`, `apellido2Pro`, `provinciaPro`, `cantonPro`, `distritoPro`, `otrasSenasPro`, `descripcionPro`, `nomEmpresaPro`) VALUES
('0207140153', 'PERSONA', 'LUIS EFREN', 'QUIROS', 'ELIZONDO', 'CR-A', 'CR-A-002', 'CR-A-002-002', 'TU CULILLO', '�KSHGFKEKFHAISHSKIKSHDLGNS', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbprovincia`
--

CREATE TABLE IF NOT EXISTS `tbprovincia` (
  `idProvincia` varchar(20) NOT NULL,
  `nombreProvincia` varchar(100) NOT NULL,
  PRIMARY KEY (`idProvincia`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbprovincia`
--

INSERT INTO `tbprovincia` (`idProvincia`, `nombreProvincia`) VALUES
('CR-SJ', 'SAN JOSÉ'),
('CR-A', 'ALAJUELA'),
('CR-C', 'CARTAGO'),
('CR-H', 'HEREDIA'),
('CR-G', 'GUANACASTE'),
('CR-P', 'PUNTARENAS'),
('CR-L', 'LIMÓN');

-- --------------------------------------------------------

--
-- Table structure for table `tbtelefonocliente`
--

CREATE TABLE IF NOT EXISTS `tbtelefonocliente` (
  `idTelefonoCli` varchar(100) NOT NULL,
  `cedulaCli` varchar(20) NOT NULL,
  `telefonoCli` varchar(20) NOT NULL,
  PRIMARY KEY (`idTelefonoCli`),
  KEY `cedulaCli` (`cedulaCli`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbtelefonocliente`
--

INSERT INTO `tbtelefonocliente` (`idTelefonoCli`, `cedulaCli`, `telefonoCli`) VALUES
('5f0bdb87-8ca5-11e6-8edc-46e092396860', '0207280909', '87899330'),
('6044e5ed-8ca5-11e6-8edc-46e092396860', '0207280909', '24530818'),
('ac3e11b6-e31b-11e6-9e5d-9abff1798b4e', '12345', '47893983'),
('b4bd897a-e320-11e6-9e5d-9abff1798b4e', '1985', '345678');

-- --------------------------------------------------------

--
-- Table structure for table `tbtelefonoproveedor`
--

CREATE TABLE IF NOT EXISTS `tbtelefonoproveedor` (
  `idTelefonoPro` varchar(100) NOT NULL,
  `cedulaPro` varchar(20) NOT NULL,
  `telefonoPro` varchar(20) NOT NULL,
  PRIMARY KEY (`idTelefonoPro`),
  KEY `cedulaPro` (`cedulaPro`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbtelefonoproveedor`
--

INSERT INTO `tbtelefonoproveedor` (`idTelefonoPro`, `cedulaPro`, `telefonoPro`) VALUES
('fc8f181d-8ca7-11e6-8edc-46e092396860', '0207140153', '83627395');

-- --------------------------------------------------------

--
-- Table structure for table `tbusuario`
--

CREATE TABLE IF NOT EXISTS `tbusuario` (
  `login` varchar(100) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido1Usu` varchar(100) NOT NULL,
  `apellido2Usu` varchar(100) NOT NULL,
  `imagenUsu` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`login`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbusuario`
--

INSERT INTO `tbusuario` (`login`, `contrasena`, `nombre`, `apellido1Usu`, `apellido2Usu`, `imagenUsu`) VALUES
('aadmin', 'lbTwoyTtAYKZ/XV7NVhiXQ==', 'admin', 'admin', 'admin', 'LXhI5PrBYJDnqdVpOLXVjxaT0kkQsKf+tlS0QB3jDfFNBmqrUgFqVQ==');

-- --------------------------------------------------------

--
-- Table structure for table `tbventas`
--

CREATE TABLE IF NOT EXISTS `tbventas` (
  `idVentana` varchar(20) NOT NULL,
  `nombreVentana` varchar(30) NOT NULL,
  PRIMARY KEY (`idVentana`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbventas`
--

INSERT INTO `tbventas` (`idVentana`, `nombreVentana`) VALUES
('999', 'SistemaAcceso'),
('003', 'Usuario'),
('004', 'Cliente'),
('005', 'Producto'),
('006', 'Proveedor'),
('007', 'Factura'),
('008', 'Permiso'),
('100', 'Usuario Cosulta-Especifica'),
('101', 'Usuario Cosulta-General'),
('102', 'Cliente Cosulta-Especifica'),
('103', 'Cliente Cosulta-General'),
('104', 'Producto Cosulta-Especifica'),
('105', 'Producto Cosulta-General'),
('106', 'Proveedor Cosulta-Especifica'),
('107', 'Proveedor Cosulta-General'),
('108', 'Factura Cosulta-Especifica'),
('109', 'Factura Cosulta-General');